# hdcm_mud

#### 介绍

hdcm_mud

#### 软件架构

软件架构说明

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

```git
# 回退GIT仓库到指定版本
git reset --hard 55e6e648d307b8c282ca366e9a90e7c15bde71c4

# 强制提交到master分支
git push -f -u origin master
```



#### 更换目录的方法

1. 运行 PHPStudy 程序
2. 修改 PHPStudy 配置

`D:\Servers\phpStudy\MySQL\my.ini`

```ini
 [mysql]
-default-character-set=latin1
+default-character-set=utf8

 [mysqld]
-character-set-server=latin1
+character-set-server=utf8
```

 `D:\Servers\phpStudy\nginx\conf\nginx.conf`

```ini
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   "D:/Servers/phpStudy/WWW";
        }
```



#### 服务端端口分析

**M2Server.exe**

```ini
D:\Servers\mud2.0\Mir200\Gs1\M2Server.exe

操作资源：File Modify(4)
modified truncate : \Mir200\Gs1\!setup.txt
modified truncate : \Mir200\Gs1\ItemNumber.Dat
touch modified truncate : \Mir200\Gs1\log\2022-3-10.14-22.log
modified truncate : \Mir200\Share\NewYearPresent.ini

TCP/IP:
TCP	0.0.0.0:5000		0.0.0.0
TCP 127.0.0.1:6366		127.0.0.1:3306
TCP 127.0.0.1:6367		127.0.0.1:3306
TCP 127.0.0.1:6370		127.0.0.1:6000
TCP 192.168.1.104:5000	192.168.1.104:6379
UDP	0.0.0.0:63644		0.0.0.0
UDP	0.0.0.0:63645		0.0.0.0
```



**GGService.exe**

```ini
D:\Servers\mud2.0\GateServer\GameGate\ggservice.exe

操作资源：File Modify(4)
truncate : C:Windows\z1
modified truncate : \GameGate\MirGate.ini
truncate : \GameGate\NameList.txt
truncate : \GameGate\网关公告.txt

TCP/IP:
TCP	0.0.0.0:7100		0.0.0.0
TCP 192.168.1.104:6358	192.168.1.104:5100
TCP 192.168.1.104:6379	192.168.1.104:5000
```



**LoginGate**

```ini
D:\Servers\mud2.0\GateServer\logingate\LoginGate.exe

TCP/IP:
TCP	0.0.0.0:3201			0.0.0.0
TCP	0.0.0.0:5600			0.0.0.0
TCP	0.0.0.0:7000			0.0.0.0
TCP	0.0.0.0:7766			0.0.0.0
TCP 192.168.1.104:5600		192.168.1.104:6344
```



**Nginx**

```ini
D:\Servers\mud2.0\logincenter\Openresty_For_Windows_1.13.5.1001_64Bit\x64\nginx\nginx.exe

TCP/IP:
TCP	0.0.0.0:8088			0.0.0.0
```



**ItemLogServer**

```ini
D:\Servers\mud2.0\ItemLogServer\ItemLogServer.exe

TCP/IP:
TCP	127.0.0.1:6339			127.0.0.1:3306
TCP	127.0.0.1:6340			127.0.0.1:3306
TCP	0.0.0.0:10000			0.0.0.0
TCP	0.0.0.0:62470			0.0.0.0
TCP	0.0.0.0:62471			0.0.0.0
```



**DBServer**

```ini
D:\Servers\mud2.0\DBServer\DBServer.exe

操作资源：File Modify(1)
modified truncate : \DBServer\Logs\202203\10.txt

TCP/IP:0
TCP	0.0.0.0:5100			0.0.0.0
TCP	0.0.0.0:6000			0.0.0.0
TCP	127.0.0.1:6000			127.0.0.1:6370
TCP	127.0.0.1:6324			127.0.0.1:3306
TCP	127.0.0.1:6325			127.0.0.1:3306
TCP	127.0.0.1:6326			127.0.0.1:3306
TCP	127.0.0.1:6327			127.0.0.1:3306
TCP	127.0.0.1:6328			127.0.0.1:3306
TCP	127.0.0.1:6329			127.0.0.1:3306
TCP	127.0.0.1:6330			127.0.0.1:3306
TCP	127.0.0.1:6331			127.0.0.1:3306
TCP	127.0.0.1:6332			127.0.0.1:3306
TCP	127.0.0.1:6333			127.0.0.1:3306
TCP	127.0.0.1:6336			127.0.0.1:3306
TCP	192.168.1.104:5100		192.168.1.104:6358
TCP	192.168.1.104:6344		192.168.1.104:5600
UDP	0.0.0.0:7800			0.0.0.0
UDP	0.0.0.0:62467			0.0.0.0
UDP	0.0.0.0:62469			0.0.0.0
```



**phpStudy**

```ini
D:\Servers\phpStudy\phpStudy.exe
D:\Servers\phpStudy\Apache\bin\httpd.exe
2.4.23

TCP/IP:
TCP	0.0.0.0:99				0.0.0.0

D:\Servers\phpStudy\MySQL\bin\mysqld.exe
5.5.47.0

TCP/IP:
TCP	0.0.0.0:3306				0.0.0.0
TCP	127.0.0.1:3306				127.0.0.1:6324~6333
TCP	127.0.0.1:3306				127.0.0.1:6336
TCP	127.0.0.1:3306				127.0.0.1:6339
TCP	127.0.0.1:3306				127.0.0.1:6340
TCP	127.0.0.1:3306				127.0.0.1:6366
TCP	127.0.0.1:3306				127.0.0.1:6367
```



#### M2Server 启动流程分析

```
1. 读取文件 D:\Servers\mud2.0\Mir200\Gs1\FormSet.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Gs1\!Setup.txt
3. 读取文件 D:\Servers\mud2.0\Mir200\Share\Mir2Actor.ini
4. 读取文件 D:\Servers\mud2.0\Mir200\Envir\传送石禁用地图.txt
5. 读写文件 D:\Servers\mud2.0\Mir200\Gs1\log\2022-3-10.15-19.log
6. 读取文件 D:\Servers\mud2.0\Mir200\Gs1\FormGMSet.ini
7. 读取文件 D:\Servers\mud2.0\Mir200\Gs1\SetConfig\hero_lv_exp.txt
8. 读取文件 D:\Servers\mud2.0\Mir200\Gs1\hero_lv_nq.txt
9. 读取文件 D:\Servers\mud2.0\Mir200\Gs1\!Setup.txt
10. 读取文件 D:\Servers\mud2.0\Mir200\Gs1\log\2022-3-10.15-19.log
2. 读写文件 D:\Servers\mud2.0\Mir200\Share\NewYearPresent.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\config\SimpleActTemplate.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\异步存储管理\main.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\行会管理[NEW]\main.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\系统信息管理\main.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\卧龙英雄管理\main.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\gamelog管理\main.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\跨服管理\main.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\DBEngn管理\main.ini
....
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\warr.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\fashi.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\taos.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\bigitem.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\self100.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\config\tianmon.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\config\NewSelfPrize.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\config\NewServPrize.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Envir\FamousAppr.txt
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\NPC管理\main.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\中央活动服务器客户端\main.ini
2. 读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\gate管理\main.ini
....
读取文件 D:\Servers\mud2.0\Mir200\Envir\AdminList.txt
读取文件 D:\Servers\mud2.0\Mir200\Share\FeastDays.ini
读取文件 D:\Servers\mud2.0\Mir200\Envir\SmsUserList.txt
读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\AppEngn管理\main.ini
读取文件 D:\Servers\mud2.0\Mir200\Gs1\ItemNumber.Dat
读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\道具管理\main.ini
读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\世界暴率管理\main.ini
读取文件 D:\Servers\mud2.0\Mir200\Share\EngineConfig\新元宝变动管理\main.ini
读取文件 D:\Servers\mud2.0\Mir200\Envir\freeRetrieve.txt
读取文件 D:\Servers\mud2.0\Mir200\Share\config\ServerSwitch.Bin
读取文件 D:\Servers\mud2.0\Mir200\Share\config\LogSwitch.Bin
读取文件 D:\Servers\mud2.0\Mir200\Share\NewYearPresent.ini
读取文件 D:\Servers\mud2.0\Mir200\Envir\CommonScripts\compiler.inc
读取文件 D:\Servers\mud2.0\Mir200\Envir\PsItemScript\1元宝.pas
读取文件 D:\Servers\mud2.0\Mir200\Envir\PsItemScript
读取文件D:\Servers\mud2.0\Mir200\Envir\PsItemScript\游戏指南.pas
读取文件 D:\Servers\mud2.0\Mir200\Envir\MonItems\...
读取文件 D:\Servers\mud2.0\Mir200\Notice\Notice.txt
读取文件 D:\Servers\mud2.0\Mir200\Envir\MiniMap.txt
读取文件 D:\Servers\mud2.0\Mir200\Envir\StartPoint.txt
读取文件 D:\Servers\mud2.0\Mir200\Envir\SafeZone.txt
读取文件 D:\Servers\mud2.0\Mir200\Envir\mapInfoEx.txt
读取文件 D:\Servers\mud2.0\Mir200\Envir\mapinfo.txt
读取 *.map 地图文件 
读取文件 D:\Servers\mud2.0\Mir200\Share\config\maparea.txt
读取文件 D:\Servers\mud2.0\Mir200\Share\config\MapDesc.dat
读取文件 D:\Servers\mud2.0\Mir200\Envir\Castle\TakeCastleInfo.txt
读取文件 D:\Servers\mud2.0\Mir200\Share\config\MapDesc.dat
读取 D:\Servers\mud2.0\Mir200\Envir\MonItems 怪物信息


```

