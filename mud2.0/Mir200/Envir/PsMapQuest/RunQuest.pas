program Mir2;
{$I ActiveValidateCom.pas}
{$I gaojishuxing.pas}
{$I TaoZhuang.pas}
{$I huiyuan.pas}
{$I kuangbao.pas}

// @AddDrug~~1
//
//add(x)
procedure givefenghao;
var fh0,fh1, fh2, fh3, fh4, fh5, exfh1,exfh2,exfh3,exfh4 ,exfh5,exfh6: integer;
begin
	fh0	 := This_Player.GetV(69,1);		//首位封号 狂暴
	if  This_Player.GetS(34,1) = 1 then  fh1  := This_Player.GetV(69,2) else fh1  := 0; //一体时装 
	fh2  := This_Player.GetV(69,3);		//衣服
	fh3  := This_Player.GetV(69,4);		//武器
	if  This_Player.GetS(34,2) = 1 then  fh4  := This_Player.GetV(69,5) else fh4  := 0; //翅膀
	fh5  := This_Player.GetV(69,6);		//斗笠
	exfh1  := This_Player.GetV(69,7);	//扩展封号1  转生
	exfh2  := This_Player.GetV(69,8);	//扩展封号2   爵位
	exfh3  := This_Player.GetV(69,9);	//扩展封号3  龙魂绕体
	exfh4  := This_Player.GetV(69,10);	//扩展封号4  vip等级
	exfh5  := This_Player.GetV(69,11);	//扩展封号5   蓝buff
	exfh6  := This_Player.GetV(69,12);	//扩展封号6	   红buff
	if fh0 < 0 then fh0 := 0;
	if fh1 < 0 then fh1 := 0;
	if fh2 < 0 then fh2 := 0;
	if fh3 < 0 then fh3 := 0;
	if fh4 < 0 then fh4 := 0;
	if fh5 < 0 then fh5 := 0;
	if exfh1 < 0 then exfh1 := 0;
	if exfh2 < 0 then exfh2 := 0;
	if exfh3 < 0 then exfh3 := 0;
	if exfh4 < 0 then exfh4 := 0;
	if exfh5 < 0 then exfh5 := 0;
	if exfh6 < 0 then exfh6 := 0;
	// 封号给予编号 = 素材ID号/2+1
	This_Player.QuestInfo(inttostr(fh0)+':'+inttostr(fh1)+':'+inttostr(fh2)+':'+inttostr(fh3)+':'+inttostr(fh4)+':'+inttostr(fh5)+':'+inttostr(exfh1)+':'+inttostr(exfh2)+':'+inttostr(exfh3)+':'+inttostr(exfh4)+':'+inttostr(exfh5)+':'+inttostr(exfh6));
end;

{ procedure SetAbil(AbilType, value:Integer);
//SetAbil函数内代码尽量不要修改,直接调用即可;
var H8, L8 : Integer;
begin
	if value < 65536 then
	begin
		H8 := value / 256;
		L8 := value mod 256;
		//下面这行是调试输出语句,正式使用时可屏蔽;
	//	This_Player.PlayerNotice('AbilType='+inttostr(AbilType)+';value='+inttostr(value)+';H8='+inttostr(H8)+';L8='+inttostr(L8),0);
		case AbilType of
			1://倍攻
			begin
			This_Player.AuthByHelped(6212 + 0,L8);
			end;
			2://备用1
			begin
			This_Player.AuthByHelped(6212 + 1,L8);	
			end;
			3://备用2
			begin
			This_Player.AuthByHelped(6212 + 2,L8);
			end;
			4://备用3
			begin
			This_Player.AuthByHelped(6212 + 3,L8);
			end;
			5://MAXHP
			begin
			This_Player.AuthByHelped(6212 + 4,L8);
			This_Player.AuthByHelped(6212 + 5,H8);
			end;
			6://MAXMP
			begin
			This_Player.AuthByHelped(6212 + 6,L8);
			This_Player.AuthByHelped(6212 + 7,H8);
			end;
			7://MAX攻击
			begin
			This_Player.AuthByHelped(6212 + 8,L8);
			This_Player.AuthByHelped(6212 + 20,H8);
			end;
			8://MAX魔法
			begin
			This_Player.AuthByHelped(6212 + 9,L8);
			This_Player.AuthByHelped(6212 + 21,H8);
			end;
			9://MAX道术
			begin
			This_Player.AuthByHelped(6212 + 10,L8);
			This_Player.AuthByHelped(6212 + 22,H8);
			end;
			10://MAX防御
			begin
			This_Player.AuthByHelped(6212 + 11,L8);
			This_Player.AuthByHelped(6212 + 23,H8);
			end;
			11://MAX魔御
			begin
			This_Player.AuthByHelped(6212 + 12,L8);
			This_Player.AuthByHelped(6212 + 13,H8);
			end;
			12://备用4
			begin
			This_Player.AuthByHelped(6212 + 14,L8);
			end;
			13://备用5
			begin
			This_Player.AuthByHelped(6212 + 15,L8);
			end;
			14://刺术上限
			begin
			This_Player.AuthByHelped(6212 + 16,L8);
			This_Player.AuthByHelped(6212 + 17,H8);
			end;
			15://刺术下限
			begin
			This_Player.AuthByHelped(6212 + 18,L8);
			This_Player.AuthByHelped(6212 + 19,H8);
			end;
			16://百分比免伤
			begin
			This_Player.AuthByHelped(1400 + 0,L8);
			end;			
			17://死亡防爆(小退属性消失,需要每次重新设置)
			begin
			This_Player.AuthByHelped(396 + 0,L8);
			This_Player.AuthByHelped(396 + 1,H8);
			end;
			18://杀人爆率(小退属性消失,需要每次重新设置)
			begin
			This_Player.AuthByHelped(1401 + 0,L8);
			end;
		end;	
    end; 
end; }

function UseYB(nType, nNum: Integer): Boolean;
begin
  Result := True;
end;

function UseLF(nType, nNum: Integer): Boolean;
begin
  Result := True;
end;  

procedure SetAwardCodeCDTime();
var
d1 , d2 , d3 : integer;
s1 , td : double;
temp_2 : integer;
begin
      s1 := GetNow;
      d2 := This_Player.GetS(23,1);
      //将getnow的浮点数转换为整型进行保存； 

      td := ConvertDBToDateTime(d2);
      
      d3 := minusDataTime(s1,td);
      
      temp_2 := This_Player.GetS(23,2);
      
      
      if temp_2 <0 then temp_2 := 0;
      
      if (d3 <= 300)  then
      begin
         This_Player.SetS(23,2,temp_2 + 1);
         This_Player.PlayerNotice('已连续输入错误卡密' + inttostr(temp_2 + 1) + '/3次!',0);
      end 
      else  
      begin                     
          This_Player.SetS(23,1, ConvertDateTimeToDB(s1));
          This_Player.SetS(23,2,1);
      end;
end; 
//卡密

function getActiveItem(atvId : integer) : string;  //激活码AwardCodeType字段必须与该方法中的id匹配 
begin
    case atvId of
        1 : result:= '炼狱';
        2 : result:= '魔杖';
        3 : result:= '银蛇';
        else
        result := ''; 
    end;
end;

procedure AwardCodeExecCallBack(ExecRes: Integer; const CodeStr: string; AwardCodeType, ActiveParam: Integer);
var 
czs : integer;
begin
  //AWARDCODE_EXEC_ERROR        = 0;
  //AWARDCODE_EXEC_QUERY        = 1;
  //AWARDCODE_EXEC_UPDATE       = 2;
  //AWARDCODE_EXEC_ADD          = 3;
  //AWARDCODE_EXEC_DEL          = 4;  
  //
  //This_Player.PlayerNotice('ExecRes：' + inttostr(ExecRes) + '！',0);  
  //This_Player.PlayerNotice('AwardCodeType：' + inttostr(AwardCodeType) + '！',0);  
  //This_Player.PlayerNotice('ActiveParam：' + inttostr(ActiveParam) + '！',0);
   begin 
   if  This_Player.GetV(88,88)< 0
    then  
   begin 
   This_Player.setV(88,88,0)
   czs := This_Player.getV(88,88);
   end else
   czs := This_Player.getV(88,88);
   end; 
  
  case ExecRes of
       0 : 
       begin
           SetAwardCodeCDTime();
           This_Player.PlayerNotice('卡密无效!',0);
           
       end;
       1 : 
       begin 
           if ActiveParam = 1 then
           This_Player.SetAwardCodeActiveParam(CodeStr, -1)
           else if ActiveParam = 2 then 
           begin
              if getActiveItem(AwardCodeType) = '' then 
              This_Player.PlayerNotice('该激活码未配置物品！',0)
              else if This_Player.FreeBagNum >= 1 then
              This_Player.SetAwardCodeActiveParam(CodeStr, -2)
              else
              This_Player.PlayerNotice('包裹空间不足！',0);
           
           end else
           This_Player.PlayerNotice('该卡密已使用！',0);
       end;
       2 : 
       begin
          This_Player.SetS(23,1,0);
          This_Player.SetS(23,2,0); 
           if ActiveParam = -1 then 
           begin      
              This_Player.ScriptRequestAddYBNum(AwardCodeType);
			  This_Player.AddLF(0,AwardCodeType/300);
              This_Player.setv(88,88,AwardCodeType/300+czs);
              This_Player.PlayerNotice('你获得元宝'+inttostr(AwardCodeType)+'，赞助值'+inttostr(AwardCodeType/300)+'!',2); 
                         end else if ActiveParam = -2 then  
          // This_Player.Give(getActiveItem(AwardCodeType),1);
     
       end;
       5 : This_Player.PlayerNotice('卡密操作失败!',0);
   end;   
end;






//穿戴触发
procedure ChangeEquip();
begin	
	TZdemo();
end;






procedure OnBackButton();
begin
if This_Player.GetV(55,1) > 0 then
begin
  This_Player.PlayerDialog(
  '<尊贵的会员。欢迎使用会员功能！/c=red>\ \'+  
  '<服务有：/c=red>\ \'+  
   '|{cmd}<【一键回收】/@HS> ^<【远程买药】/@my>^<【远程仓库】/@cangku>\'
  +'|{cmd}<【每日福利】/@HYFL>  ^<【清洗红名】/@hongming>^<【一键回城】/@huichenga>\'
    );
end else
	   This_Player.PlayerDialog(
		 '你还不是会员，可以到土城购买！ \ \'
		 +'你可以在我这里购买回城卷！ \ \'
		 +'|{cmd}<购买回城/@huicjz> '
		 );
end;











function baobao(Zlv : integer) : string;
begin
    case Zlv of
        1 : result := '神兽';
        2 : result := '变异骷髅';
		3 : result := '狂暴麒麟';
        4 : result := '暴雷虎王';
        5 : result := '玄の暴雷虎王';
        6 : result := '地狱狼王';
		7 : result := '狂の地狱狼王';
        8 : result := '灭世剑神';
		9 : result := '神の灭世剑神';
		10 : result := '夺命妖姬';
		11 : result := '霸王の妖姬';
		12 : result := '炎帝剑尊';
		13 : result := '终极の炎帝';
		14 : result := '灭世狂战[十转]';
    end;
end;


//召唤神兽
procedure SummonShinsu();
begin
 if This_Player.GetSlaveCount('') < 1 then   
begin
if This_Player.GetS(12,12) >= 1 then
begin
This_Player.MakeSlaveEx(baobao(This_Player.GetS(12, 12)) , 1 ,3);
end else
begin
This_Player.MakeSlaveEx('神兽' , 1 ,0);
end;
end else
begin
This_Player.PlayerNotice('你已经召唤了一个宝宝了！！',2);
end;
end;

















//主号升级
procedure PlayerExpLevelUp();
var
  iLev : integer;
begin
   iLev := This_Player.Level;
   
   case iLev of
       7:
       begin
           case This_Player.Job of
               0 : 
               begin
			       This_Player.SetV(68,2,0);
                   This_Player.Give('基本剑术',1);
				   This_Player.Give('金币',10000);
                   This_Player.GiveBindItem('魔血石(小)',1);
                   
               end;
               1 : 
               begin
			       This_Player.SetV(68,2,0);
                   This_Player.Give('雷电术',1);
				   This_Player.Give('金币',10000);
                   This_Player.GiveBindItem('魔血石(小)',1);                 
               end;
               2 : 
               begin
                   This_Player.SetV(68,2,0);
                   This_Player.Give('治愈术',1);
				   This_Player.Give('金币',10000);
                   This_Player.Give('精神力战法',1);
                   This_Player.GiveBindItem('魔血石(小)',1);
               end;
           end;
       end;
       15:
       begin
           case This_Player.Job of
               0 : 
               begin
                   This_Player.Give('攻杀剑术',1);
                   if This_Player.Gender = 0 then
                   This_Player.Give('中型盔甲(男)',1)
                   else
                   This_Player.Give('中型盔甲(女)',1);
               end;
               1 : 
               begin
                   if This_Player.Gender = 0 then
                   This_Player.Give('中型盔甲(男)',1)
                   else
                   This_Player.Give('中型盔甲(女)',1);
               end;
               2 : 
               begin
                   This_Player.Give('灵魂火符',1);
                   This_Player.GiveBindItem('灰色药粉(少量)',1);
				   This_Player.GiveBindItem('黄色药粉(少量)',1);
				   This_Player.GiveBindItem('护身符',1);
                   if This_Player.Gender = 0 then
                   This_Player.Give('中型盔甲(男)',1)
                   else
                   This_Player.Give('中型盔甲(女)',1);
               end;
           end;
       end;
       25:
       begin
            case This_Player.Job of
               0 : 
               begin
                   This_Player.Give('炼狱',1);
				   This_Player.FlyTo('3', 333, 333);
                   if This_Player.Gender = 0 then
                   This_Player.Give('重盔甲(男)',1)
                   else
                   This_Player.Give('重盔甲(女)',1);
               end;
               1 : 
               begin
                   This_Player.Give('魔杖',1);
				   This_Player.FlyTo('3', 333, 333);
                   if This_Player.Gender = 0 then
                   This_Player.Give('魔法长袍(男)',1)
                   else
                   This_Player.Give('魔法长袍(女)',1);
               end;
               2 : 
               begin
                   This_Player.Give('银蛇',1);
				   This_Player.FlyTo('3', 333, 333);
                   if This_Player.Gender = 0 then
                   This_Player.Give('灵魂战衣(男)',1)
                   else
                   This_Player.Give('灵魂战衣(女)',1);
               end;
           end;
       end;
       30:
       begin
           case This_Player.Job of
               0 : 
               begin
                   This_Player.Give('道士头盔',1);
				   This_Player.Give('金币',150000);
                   This_Player.Give('蓝翡翠项链',1);
                   This_Player.Give('死神手套',2);
                   This_Player.Give('珊瑚戒指',2);
                   This_Player.Give('刺杀剑术',1);
				This_Player.Give('半月弯刀' , 1);
				This_Player.Give('野蛮冲撞' , 1);
				This_Player.Give('烈火剑法' , 1);
               end;
               1 : 
               begin
                   This_Player.Give('道士头盔',1);
				   This_Player.Give('金币',150000);
                   This_Player.Give('放大镜',1);
                   This_Player.Give('黑檀手镯',2);
                   This_Player.Give('降妖除魔戒指',2);
                This_Player.Give('雷电术', 1);
				This_Player.Give('火墙', 1);
				This_Player.Give('疾光电影', 1);
				This_Player.Give('魔法盾', 1);
				This_Player.Give('冰咆哮', 1);
				This_Player.Give('抗拒火环', 1);
				This_Player.Give('诱惑之光', 1);
				This_Player.Give('地狱雷光', 1);					
				This_Player.Give('火球术', 1);
				This_Player.Give('大火球', 1);
				This_Player.Give('地狱火', 1);
				This_Player.Give('瞬息移动', 1);
				This_Player.Give('爆裂火焰', 1);
				This_Player.Give('圣言术', 1);					   
               end;
               2 : 
               begin
                   This_Player.Give('道士头盔',1);
				   This_Player.Give('金币',150000);
                   This_Player.Give('竹笛',1);
                   This_Player.Give('道士手镯',2);
                   This_Player.Give('降妖除魔戒指',2);
                This_Player.Give('治愈术', 1);
				This_Player.Give('精神力战法', 1);
				This_Player.Give('灵魂火符', 1);
				This_Player.Give('施毒术', 1);
				This_Player.Give('困魔咒', 1);
				This_Player.Give('幽灵盾', 1);
				This_Player.Give('神圣战甲术', 1);
				This_Player.Give('召唤神兽', 1);
				This_Player.Give('隐身术', 1);
				This_Player.Give('集体隐身术', 1);
				This_Player.Give('召唤骷髅', 1);
				This_Player.Give('召唤神兽', 1);
               end;
           end;
       end;
	   56:
       begin
           case This_Player.Job of
               0 : 
               begin
			   This_Player.MultiTempExpRate := 3;  
			   end;
               1 : 
			   begin
			   This_Player.MultiTempExpRate := 3;  
			   end;
               2 : 
               begin
			   This_Player.MultiTempExpRate := 3;  
               end;
           end;
       end;		


   end;
   
   
   
   
   
   
   
   
   
   
begin
if (This_Player.Level > 125) and (This_Player.GetV(25,5) < 1) then
begin
This_Player.SetPlayerLevel(125); 
This_Player.PlayerNotice('你没有突破125级极限，等级归于125级。',0);	
end else
if (This_Player.Level > 130) and (This_Player.GetV(25,5) < 2) then
begin
This_Player.SetPlayerLevel(130); 
This_Player.PlayerNotice('你没有突破130级极限，等级归于130级。',0);	
end else
if (This_Player.Level > 135) and (This_Player.GetV(25,5) < 3) then
begin
This_Player.SetPlayerLevel(135); 
This_Player.PlayerNotice('你没有突破135级极限，等级归于135级。',0);	
end else

end;     
end;                          






//主号内功升级
procedure PlayerForceLevelUp();
begin

end;

//击杀触发  
procedure OnKill(KillerName ,MapDesc:string);
//默认传递 被击杀玩家对象(This_Player),参数一传递杀人玩家名字(KillerName),参数二传递当前地图名称(MapDesc)
//通过This_Player.FindPlayerByName('KillerName');获取杀人玩家对象.
var Killer:TPlayer;
	KillNumber , KilledNumber : Integer;
begin
	//获取杀人玩家对象
	Killer := This_Player.FindPlayerByName(KillerName);
	
	//击杀提示
	ServerSay('击杀快报: 玩家 [' + KillerName + '] 挥刀在 '+MapDesc+'('+inttostr(This_Player.My_X)+','+inttostr(This_Player.My_Y)+') 将 ['+This_Player.Name
+'] 斩于刀下!!!',3);

	//玩家变量设置,例如:斩杀牺牲数量,狂暴状态等!
	//设置被击杀玩家牺牲次数 任务号5 字段号2  不可改变
	KilledNumber := This_Player.GetV(5,2);	
	if KilledNumber < 1 then 
	begin
	KilledNumber := 1;
	end else
	begin
	KilledNumber := KilledNumber + 1;
	end;
	This_Player.SetV(5,2,KilledNumber);
	//设置杀人玩家斩杀次数 任务号5 字段号1  不可改变
	KillNumber := Killer.GetV(5,1);
	if KillNumber < 1 then 
	begin
	KillNumber := 1;
	end else
	begin
	KillNumber := KillNumber + 1;
	end;
	Killer.SetV(5,1,KillNumber);
	
	//狂暴玩法请参照例子自行添加
		if This_Player.GetV(111,10) > 0 then
	begin
	This_Player.SetV(69,1,0);
			   givefenghao;
	This_Player.Setv(111,10,0);
			if this_player.GetV(111,13) = 1  then
			begin
			This_Player.SetV(69,1,0);
			end else
			if this_player.GetV(111,13) = 2  then
			begin
			This_Player.SetV(69,1,0);
			end else
			if this_player.GetV(111,13) = 3  then
			begin
			This_Player.SetV(69,1,0);			
			end else
			begin
			This_Player.SetV(69,1,0);	
			end;
			 This_Player.SetV(170,9,This_Player.GetV(170,9)-2000);//血
			This_Player.SetV(170,11,This_Player.GetV(170,11)-50);//攻
			This_Player.SetV(170,12,This_Player.GetV(170,12)-50);//魔
			This_Player.SetV(170,13,This_Player.GetV(170,13)-50);//道
    This_Player.findPlayerByName(KillerName).AddLF(0, 10);  //杀人获得元宝
	ServerSay('击杀快报: 玩家 [' + KillerName + '] 挥刀在 '+MapDesc+'('+inttostr(This_Player.My_X)+','+inttostr(This_Player.My_Y)+') 将狂暴玩家['+This_Player.Name
+'] 斩于刀下,并获得该玩家10灵符!',5); 
	end;
	TZJH_NPC1;
end;
















//英雄升级
procedure HeroExpLevelUp();
var
  iLev : integer;
begin
   iLev := This_Player.HeroLevel;
   
   case iLev of
       7:
       begin
           case This_Player.HeroJob of
               0 : 
               begin
                   This_Player.Give('白日门剑术',1);
				   This_Player.Give('白日门攻杀',1);
                   This_Player.Give('魔血石(小)',1);
                   
               end;
               1 : 
               begin
                   This_Player.Give('白日门雷电术',1);
				   This_Player.Give('白日门爆裂',1);
				   This_Player.Give('白日门火球术',1);
                   This_Player.Give('魔血石(小)',1);                 
               end;
               2 : 
               begin
                   
                   This_Player.Give('白日门治愈术',1);
                   This_Player.Give('白日门战法',1);
				   This_Player.Give('白日门火符',1);
				   This_Player.Give('白日门施毒术',1);
				   This_Player.Give('超级护身符',1);
				   This_Player.Give('超级灰色药粉',1);
				   This_Player.Give('超级黄色药粉',1);
                   This_Player.Give('魔血石(小)',1);
               end;
           end;
       end;
	     
       
       30:
       begin
            case This_Player.HeroJob of
               0 : 
               begin
                   This_Player.Give('道士头盔',1);
				   This_Player.Give('蓝翡翠项链',1);
				   This_Player.Give('死神手套',2);
				   This_Player.Give('珊瑚戒指',2);
				   This_Player.Give('炼狱',1);
                   if This_Player.HeroGender = 0 then
                   This_Player.Give('重盔甲(男)',1)
                   else
                   This_Player.Give('重盔甲(女)',1);
               end;
               1 : 
               begin
                   This_Player.Give('道士头盔',1);
				   This_Player.Give('魔杖',1);
				   This_Player.Give('放大镜',1);
				   This_Player.Give('黑檀手镯',2);
				   This_Player.Give('降妖除魔戒指',2);
                   if This_Player.HeroGender = 0 then
                   This_Player.Give('魔法长袍(男)',1)
                   else
                   This_Player.Give('魔法长袍(女)',1);
               end;
               2 : 
               begin
				   This_Player.Give('道士头盔',1);
				   This_Player.Give('银蛇',1);
				   This_Player.Give('竹笛',1);
				   This_Player.Give('道士手镯',2);
				   This_Player.Give('降妖除魔戒指',2);
                   if This_Player.HeroGender = 0 then
                   This_Player.Give('灵魂战衣(男)',1)
                   else
                   This_Player.Give('灵魂战衣(女)',1);
               end;
           end;
       end;
      
           end;
		   end;
procedure SetNoKillMapLv(ID:Integer);
begin
	case ID of
		1:
		begin
			This_Player.PlayerNotice('autorelive',5);
			This_Player.PlayerNotice('你已原地复活',5);
			if This_Player.MyLFnum > 10 then
			begin
				This_Player.DecLF(10001, 10, false);
			end;
			This_Player.AutoReLive(This_Player.MapName,This_Player.My_X+random(3)-1,This_Player.My_Y+random(3)-1,true,true); //原地复活
		end;
		2:
		begin
			This_Player.PlayerNotice('autorelive',5);
			This_Player.PlayerNotice('你已盟重复活',5);
			if This_Player.MyLFnum > 10 then
			begin
				This_Player.DecLF(10001, 10, false);
			end;
			This_Player.AutoReLive('3',330+random(3)-1,330+random(3)-1,true,true);
		end;
		3:
		begin
			This_Player.NewBieGiftConsume;//一行代码实现随身仓库
		end;
		4:
		begin
		_kuangbao;
		end;
	end;
end;   
	

//英雄内功升级
procedure HeroForceLevelUp();
begin

end;
//活跃度系统 
procedure PlayerActivePoint(paytape,payNO,PayNUM:integer;PayName:string);
var
addPoint:integer;
begin

    
end;

procedure PlayerActiveWithMap(MapName:string);
begin

end;

procedure PlayerActiveValidate;
begin
 
end;

procedure OpenTimeWithMap(MapName:string);
begin

end;

begin

end.