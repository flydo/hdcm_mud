program Mir2;

{$I ActiveValidateCom.pas}

{$I huiyuan.pas}
{$I gaojishuxing.pas}
procedure givefenghao;
var fh0,fh1, fh2, fh3, fh4, fh5, exfh1 : integer;
begin
	fh0	 := This_Player.GetV(69,1);		//首位封号
	fh1  := This_Player.GetV(69,2);		//一体时装
	fh2  := This_Player.GetV(69,3);		//衣服
	fh3  := This_Player.GetV(69,4);		//武器
	fh4  := This_Player.GetV(69,5);		//翅膀
	fh5  := This_Player.GetV(69,6);		//斗笠
	exfh1  := This_Player.GetV(69,7);	//扩展封号1
	
	if fh0 < 0 then fh0 := 0;
	if fh1 < 0 then fh1 := 0;
	if fh2 < 0 then fh2 := 0;
	if fh3 < 0 then fh3 := 0;
	if fh4 < 0 then fh4 := 0;
	if fh5 < 0 then fh5 := 0;
	if exfh1 < 0 then exfh1 := 0;
	// 封号给予编号 = 素材ID号/2+1
	This_Player.QuestInfo(inttostr(fh0)+':'+inttostr(fh1)+':'+inttostr(fh2)+':'+inttostr(fh3)+':'+inttostr(fh4)+':'+inttostr(fh5)+':'+inttostr(exfh1));
end;
procedure _AboutActive;
begin
   This_Player.PlayerDialog(
   '增强传奇信用分的方式有以下:\' 
    +'|1.使用浪漫星雨或者地下组队卷轴进入地图挑战怪物会增加你的\传奇信用分。\' 
    +'|2.击杀BOSS与活动怪物也会增加传奇信用分。\' 
    +'|3.参加地下夺宝活动,并点击到活动NPC,会增加传奇信用分。\' 
    +'|4.在商城中购买绑定道具会增加传奇信用分。\' 
    +'|5.鉴定精力值,金刚石抽取武器等会增加传奇信用分。\' 
    +'|6.学习高级技能和佩戴高属性装备可以增加临时信用分。\'
    +'|7.还有许多会增加传奇信用分的方式,等待你去发现。\' 
    ); 
end; 
procedure _ValidateActive;
var StorageNum_b , StorageNum_N : integer;
begin
   if This_Player.CheckAuthen(1,100) = false then
   begin
     if This_Player.GetActivePoint + This_Player.GetTmpActivePoint >= 0 then
     begin
      StorageNum_b := This_Player.GetStorageSpaceCount;
      if This_Player.ActiveAuthen(1,100) = 1 then
      begin
      StorageNum_N := This_Player.GetStorageSpaceCount;
      
      This_Player.ExpandStorageSpace(StorageNum_b + 24 - StorageNum_N);

      This_Player.IncActivePoint(50);
      This_Player.PlayerDialog(
      '验证成功！\'+
      '您已成为验证用户，祝您游戏愉快！'  
      ); 
      end else
      This_Player.PlayerDialog(
      '验证失败，请稍后再试。\ \'  
      ); 
     end else 
     This_Player.PlayerDialog(
     '验证失败，您的传奇信用分不足30分。\ \'+ 
     '|{cmd}<了解增强传奇信用分的方式/@AboutActive>\'  
     ); 
   end else
   This_Player.PlayerDialog(
   '您是验证用户，无需再次验证，祝您游戏愉快！\'  
   ); 
end; 

procedure EventNotify(EventNO: Integer; EventParam: Integer);   //此接口由程序调用 
begin

end;



  //远程召唤：················································································ 

















procedure _zhshenshou;
 begin
  
         if  This_Player.GetBagItemCount('道士召唤') < 2 then
            begin  
            if This_Player.Job = 2 then           //判断职业
			begin 
			if This_Player.Level >= 35 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('神兽' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			   end else
			  This_Player.PlayerDialog(
              '你等级不够，需要35级召唤！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你没有：兽皇远程召唤!\');       
 end;   



procedure _zhqiling;
 begin
  
         if  This_Player.GetBagItemCount('道士召唤') < 2 then
            begin  
            if This_Player.Job = 2 then           //判断职业
			begin 
			if This_Player.Level >= 44 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('变异骷髅' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			   end else
			  This_Player.PlayerDialog(
              '你等级不够，需要44级召唤！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你没有：兽皇远程召唤!\');       
 end;   

procedure _OnKillBuff;
var AbilNum:integer;
begin
	if This_Player.GetV(111,10) > 0 then
	begin
	This_Player.PlayerNotice('你已经开启狂暴之力，不需要再开启！',0);

	end else
	begin
		if This_Player.MyLFnum >= 20 then
		begin
			This_Player.DecLF(0, 20, false);  							
			   This_Player.Setv(111,10,10);
            This_Player.SetV(170,9,This_Player.GetV(170,9)+2000);//血
			This_Player.SetV(170,11,This_Player.GetV(170,11)+50);//攻
			This_Player.SetV(170,12,This_Player.GetV(170,12)+50);//魔
			This_Player.SetV(170,13,This_Player.GetV(170,13)+50);//道
			if this_player.GetV(111,13) = 1  then
			begin
			This_Player.SetV(69,1,1);
			end else
			if this_player.GetV(111,13) = 2  then
			begin
			This_Player.SetV(69,1,1);
			end else
			if this_player.GetV(111,13) = 3  then
			begin
			This_Player.SetV(69,1,1);			
			end else
			begin
			This_Player.SetV(69,1,1);	
			end;
			
			//嗜血杀戮
			ServerSay('狂暴系统：玩家【' + This_Player.Name + '】开启了狂暴之力.眼中只有杀戮.生人勿近！！',3); 	
		end else
       	This_Player.PlayerNotice('你没有20灵符,无法开启狂暴之力！',0);
	end;
	TZJH_NPC1;
	givefenghao;
end;




procedure _zhqiling2;
 begin
  
         if  This_Player.MaxSC >= 50 then
            begin  
            if This_Player.Job = 2 then           //判断职业
			begin 
			if This_Player.Level >= 52 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('狂暴麒麟' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			   end else
			  This_Player.PlayerDialog(
              '你等级不够，需要52级召唤！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');       
 end;   


procedure _zhhuwang;
 begin
  
         if  This_Player.MaxSC >= 70 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.Level >= 60 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('暴雷虎王' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你等级不够，需要65级召唤！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');      
 end;   
 
 
 procedure _zhhuwang2;
 begin
  
         if  This_Player.MaxSC >= 100 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 1 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('玄の暴雷虎王' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');       
 end;   


procedure _zhlangwang;
 begin
  
         if  This_Player.MaxSC >= 120 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 2 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('地狱狼王' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');      
 end;   
 
 
 procedure _zhlangwang2;
 begin
  
          if  This_Player.MaxSC >= 130 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 3 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('狂の地狱狼王' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');        
 end;   


 procedure _zhjianshen;
 begin
  
         if  This_Player.MaxSC >= 140 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 4 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('灭世剑神' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
               This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');        
 end;   



 procedure _zhjianshen2;
 begin
  
         if  This_Player.MaxSC >= 150 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 5 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('神の灭世剑神' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');       
 end;  
 
 
 
 procedure _duoming;
 begin
  
         if  This_Player.MaxSC >= 200 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 6 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('夺命妖姬' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');       
 end;   


procedure _duoming2;
 begin
  
         if  This_Player.MaxSC >= 250 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 7 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('霸王の妖姬' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');       
 end;  
 
 
 procedure _yandi;
 begin
  
         if  This_Player.MaxSC >= 300 then
            begin  
            if This_Player.Job = 2 then  			//判断职业亲测 源 码网www.qc ymw.com
			begin 
			if This_Player.GetV(42,1) >= 8 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('炎帝剑尊' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');       
 end;  
 
 procedure _yandi2;
 begin
  
         if  This_Player.MaxSC >= 400 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 9 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('终极の炎帝' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');       
 end; 
 
 
 
  procedure _shizhuan;
 begin
  
         if  This_Player.MaxSC >= 600 then
            begin  
            if This_Player.Job = 2 then  			//判断职业
			begin 
			if This_Player.GetV(42,1) >= 10 then
            begin 
              if This_Player.GetSlaveCount('') < 1 then   
              begin 
               This_Player.MakeSlaveEx('灭世狂战[十转]' , 1 ,0);
               This_Player.PlayerDialog(
              '召唤成功！!\'); 
              end else
              This_Player.PlayerDialog(
              '你已经召唤了1个BB了，请收回现在的BB！!\'); 
			  end else
			  This_Player.PlayerDialog(
              '你的转生等级不够！！\'); 
              end else
              This_Player.PlayerDialog(
              '你不是道士!\');
            end else
              This_Player.PlayerDialog(
              '你的道术不足以召唤出该兽皇！!\');       
 end; 
 
 
 
 
 
 
 
//远程召唤：················································································ 









procedure TempExpBylv();
var fushou : string;
var tempLv,dji : integer;
begin
    tempLv := This_Player.Level;
    if (tempLv >= 1) and (tempLv <= 70) then 
    begin
    This_Player.MultiTempExpRate := 10;  
    This_Player.PlayerNotice('当前经验倍数：10倍',2);
	This_Player.PlayerNotice('开区期间，70级以内所有玩家经验翻倍！！',2);
    //This_Player.PlayerNotice('55级之前你都可以获得系统赠送的10倍经验',2);
    end; 
	
	
	
	begin
    if This_Player.Job = 0 then 
    begin
	if This_Player.Level <= 65 then
	begin
	This_Player.AddPlayerAbil(8, 10, 3599);
	This_Player.AddPlayerAbil(9, 10, 3599);
    This_Player.PlayerNotice('你是战士，65级前获得属性补偿奖励！',2);
    end; 
	end; 
		
end; 
	
end;
 




begin
   
   TempExpBylv();
  
end.