{
*******************************************************************}

program Mir2;

{$I common.pas}


procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;


procedure _newYB(); 
begin
    if This_Player.GetV(11,10) <> 888 then
    begin
        This_Player.SetV(11,10,888);
        This_NPC.YBDealDialogShowMode(This_Player,true);
    end
    else
    begin
        This_NPC.YBDealDialogShowMode(This_Player,false);
    end;
end;

Procedure _DoInputDialog1;
var wrongStr : string; 
d1 , d2 , d3 : integer;
s1 , td : double;
temp_2 : integer;
begin
      s1 := GetNow;
      d2 := This_Player.GetS(23,1);
      //将getnow的浮点数转换为整型进行保存； 

      td := ConvertDBToDateTime(d2);
      
      d3 := minusDataTime(s1,td);
      
      temp_2 := This_Player.GetS(23,2);
      

   if (d3 >= 300) then 
   wrongStr := ''
   else
   wrongStr := '错误次数' + inttostr(This_Player.GetS(23,2)) + '/3';
   
   This_Npc.InputDialog(This_Player,'请输入卡密：' + wrongStr , 0 , 100);
end;


Procedure P100;
var
d1 , d2 , d3 : integer;
s1 , td : double;
temp_2 : integer;
begin
      s1 := GetNow;
      d2 := This_Player.GetS(23,1);
      //将getnow的浮点数转换为整型进行保存； 

      td := ConvertDBToDateTime(d2);
      
      d3 := minusDataTime(s1,td);
      
      temp_2 := This_Player.GetS(23,2);
      

   if (d3 >= 300) or (This_Player.GetS(23,2) < 3) then 
   begin
       If This_Npc.InputOK then
       begin
          //This_Player.PlayerNotice(This_Npc.InputStr,0);
          This_Player.QueryAwardCode(This_Npc.InputStr);
       end;
   end else
   begin
      
       
   This_Npc.NpcDialog(This_Player,
   '你已连续输入错误卡密3次，将锁定5分钟！请' + inttostr(300 - d3) + '秒后再次尝试！'
   );
   end;
end;



Begin     
    This_Npc.NpcDialog(This_Player,
    '您好，有什么可以效劳的？我可以为您提供关于元宝的各类服务。\ \'
    +'|{cmd}<开始元宝交易/@NewYB>               ^<使用卡密兑换元宝/@DoInputDialog1>'
    );

end.