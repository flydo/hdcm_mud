
procedure _GetFreeGold;  
var BDYB : Integer;//局部语法变量声明
begin
  BDYB := This_Player.GetV(20,9); //获取元宝数量
  if This_Player.GetV(13,4) <> 1 then
   begin
   if This_Player.Level >= 40 then
   begin
   if This_Player.FreeBagNum >= 3 then
   begin 
     This_Player.Give('10倍秘籍',3); 
     This_Player.SetV(13,4,1);
     This_NPC.NpcDialog(This_Player,
                '恭喜你领取成功');
    end else
            This_NPC.NpcDialog(This_Player,'你的包裹不足3格') 
  end else
  This_NPC.NpcDialog(This_Player,'你的等级不足40级'); 
  end  else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
end;





procedure _GetFreeGold1;  
var BDYB : Integer;//局部语法变量声明
begin
  BDYB := This_Player.GetV(20,9); //获取元宝数量
  if This_Player.GetV(14,4) <> 1 then
   begin
   if This_Player.Level >= 45 then
   begin
   if This_Player.FreeBagNum >= 3 then
   begin 
     This_Player.Give('10倍秘籍',3);
     This_Player.SetV(14,4,1);
	 This_Player.ScriptRequestAddYBNum(300);
     This_NPC.NpcDialog(This_Player,
                '恭喜你领取成功');
    end else
            This_NPC.NpcDialog(This_Player,'你的包裹不足3格') 
  end else
  This_NPC.NpcDialog(This_Player,'你的等级不足45级'); 
  end  else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
end;




procedure _GetFreeGold2;  
var BDYB : Integer;//局部语法变量声明
begin
  BDYB := This_Player.GetV(20,9); //获取元宝数量
  if This_Player.GetV(15,4) <> 1 then
   begin
   if This_Player.Level >= 50 then
   begin
   if This_Player.FreeBagNum >= 3 then
   begin 
     This_Player.Give('10倍秘籍',3);
     This_Player.SetV(15,4,1);
	 This_Player.ScriptRequestAddYBNum(800);
     This_NPC.NpcDialog(This_Player,
                '恭喜你领取成功');
    end else
            This_NPC.NpcDialog(This_Player,'你的包裹不足3格') 
  end else
  This_NPC.NpcDialog(This_Player,'你的等级不足50级'); 
  end  else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
end;



procedure _GetFreeGold3;  
var BDYB : Integer;//局部语法变量声明
begin
  BDYB := This_Player.GetV(20,9); //获取元宝数量
  if This_Player.GetV(16,4) <> 1 then
   begin
   if This_Player.Level >= 55 then
   begin
   if This_Player.FreeBagNum >= 3 then
   begin 
     This_Player.Give('10倍秘籍',3);
     This_Player.SetV(16,4,1);
	 This_Player.AddLF(0,5);
	 This_Player.ScriptRequestAddYBNum(1200);
     This_NPC.NpcDialog(This_Player,
                '恭喜你领取成功');
    end else
            This_NPC.NpcDialog(This_Player,'你的包裹不足3格') 
  end else
  This_NPC.NpcDialog(This_Player,'你的等级不足55级'); 
  end  else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
end;



procedure _GetFreeGold4;  
var BDYB : Integer;//局部语法变量声明
begin
  BDYB := This_Player.GetV(20,9); //获取元宝数量
  if This_Player.GetV(17,4) <> 1 then
   begin
   if This_Player.Level >= 60 then
   begin
   if This_Player.FreeBagNum >= 3 then
   begin 
     This_Player.Give('10倍秘籍',3);
     This_Player.SetV(17,4,1);
	 This_Player.ScriptRequestAddYBNum(1800);
	 This_Player.AddLF(0,10);
     This_NPC.NpcDialog(This_Player,
                '恭喜你领取成功');
    end else
            This_NPC.NpcDialog(This_Player,'你的包裹不足3格') 
  end else
  This_NPC.NpcDialog(This_Player,'你的等级不足60级'); 
  end  else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
end;


procedure _GetFreeGold5;  
var BDYB : Integer;//局部语法变量声明
begin
  BDYB := This_Player.GetV(20,9); //获取元宝数量
  if This_Player.GetV(18,4) <> 1 then
   begin
   if This_Player.Level >= 65 then
   begin
   if This_Player.FreeBagNum >= 5 then
   begin 
     This_Player.SetV(18,4,1);
	 This_Player.ScriptRequestAddYBNum(2500);
	 This_Player.AddLF(0,20);
     This_NPC.NpcDialog(This_Player,
                '恭喜你领取成功');
    end else
            This_NPC.NpcDialog(This_Player,'你的包裹不足5格') 
  end else
  This_NPC.NpcDialog(This_Player,'你的等级不足65级'); 
  end  else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
end;






var BDYB : Integer; //主函数入口
begin
 BDYB := This_Player.GetV(20,9); //获取元宝数量
    This_NPC.NpcDialog( This_Player,
    '40级奖励：10倍秘籍*3 \|' +
    '45级奖励：10倍秘籍*3,元宝+300\|' +
    '50级奖励：10倍秘籍*3,元宝+800。\|' +
    '55级奖励：10倍秘籍*3,赞助值+5。\|' +
    '60级奖励：10倍秘籍*3,元宝+1800,赞助值+10\|' +
    '65级奖励：元宝+2500,赞助值+20\|' +
    '|{cmd}<40级冲级奖励/@GetFreeGold> ^<45级冲级奖励/@GetFreeGold1>\'  +
    '|{cmd}<50级冲级奖励/@GetFreeGold2> ^<55级冲级奖励/@GetFreeGold3>\' + 
    '|{cmd}<60级冲级奖励/@GetFreeGold4> ^<65级冲级奖励/@GetFreeGold5>'
	);
end.