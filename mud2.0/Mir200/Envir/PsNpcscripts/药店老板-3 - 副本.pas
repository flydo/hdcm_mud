{********************************************************************

*******************************************************************}

program mir2;

{$I common.pas}

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure domain;
begin
    This_Npc.NpcDialog(This_Player,
    '欢迎，你需要点什么？\ \'
    +'|{cmd}<买药品/@buy>\'
    +'|{cmd}<卖药品/@sell>\'
    +'|{cmd}<退出/@doexit>'
    );
end;




procedure _buy;
begin
  This_Npc.NpcDialog(This_Player, 
    '你想买点什么药品？\ \ \|{cmd}<返回/@main>'
  );
  This_Npc.Click_Buy(This_Player);
end;

procedure _Sell;
begin
  This_Npc.NpcDialog(This_Player, 
    '给我看看你的东西。\ \ \|{cmd}<返回/@main>'
  );
  This_Npc.Click_Sell(This_Player);
end;


//初始化操作
procedure OnInitialize;
begin
  This_NPC.AddStdMode(0);
  This_NPC.AddStdMode(3); 
  This_NPC.AddStdMode(5);
  This_NPC.AddStdMode(6);
  This_NPC.AddStdMode(10);
  This_NPC.AddStdMode(11);
  This_NPC.AddStdMode(15);
  This_NPC.AddStdMode(16);
  This_NPC.AddStdMode(30);
  This_NPC.AddStdMode(42);
  This_NPC.AddStdMode(43);
  This_Npc.FillGoods('强效金创药',100,1);
  This_Npc.FillGoods('强效魔法药',100,1);
  This_Npc.FillGoods('超级金创药',100,1); 
  This_Npc.FillGoods('超级魔法药',100,1);
  This_Npc.FillGoods('超强金创药',100,1);
  This_Npc.FillGoods('超强魔法药',100,1);
  This_Npc.FillGoods('超强金创药包',100,1); 
  This_Npc.FillGoods('超强魔法药包',100,1);
  This_Npc.FillGoods('万年雪霜包',20,60); 
  This_Npc.FillGoods('疗伤药包',20,60);
  This_Npc.SetRebate(100);
end;

//脚本执行的入口
begin
  domain;
end.