{
/*******************************************************************************

/*******************************************************************************
}

program Mir2;

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _gettitle;
begin
  This_Npc.NpcDialog(This_Player,
  '能够成为沙巴克城主的人，绝对担得起一代天骄这四个字。\'
  +'领取称号需要雕像荣耀值达到1100点以上，并且每次领取称号需要消\'
  +'耗荣耀值100点。\ \'
  +'|{cmd}<确认领取/@gettitle_1>         ^<了解称号/@gettitle_2>\ \'
  +'|{cmd}<返回/@main>' 
  );         
end;
{
procedure _gettitle_1;
var
temp:integer;
begin
  if This_Npc.GetCelebName = This_Player.Name then     //雕像名是本人 
  begin
    temp := This_NPC.GetPlayerHonorValue; 
    if temp >= 1100 then
    begin 
      if not This_Player.HaveTitle(4) then
      begin
      This_NPC.SubPlayerHonorValue(100);
      if This_NPC.GetPlayerHonorValue >= 6000 then This_NPC.ChgCelebColor(1) else This_NPC.ChgCelebColor(0);
      This_Player.AddTitle(4,0,0,true);
      This_Npc.NpcDialog(This_Player,
      '恭喜你获得了称号：“一代天骄”！\'
      );
      end else
      This_Npc.NpcDialog(This_Player,
      '你已经拥有该称号！\'
      ); 
    end else
    This_Npc.NpcDialog(This_Player,
    '你的荣耀还不足以授予你如此尊贵的称号，荣耀值达到1100点之后，\'
    +'再来领取吧。'
    );   
  end else
  This_Npc.NpcDialog(This_Player,
  '最荣耀的称号只能赐予玛法大陆上的最强者，你还达不到要求，继续\'
  +'加油吧。'
  );         
end;
   }
procedure _gettitle_2;
begin
  This_Npc.NpcDialog(This_Player,
  '沙巴克城主会获得独一无二的称号：\'
  +'一代天骄：体力值上限+100 \ \'
  +'|{cmd}<返回/@gettitle>' 
  );         
end;

procedure _worship;
begin
  if This_Npc.GetCelebName <> '' then     //雕像已经申请 
  begin
  This_Npc.NpcDialog(This_Player,
   '当前沙巴克城主为'+This_NPC.GetCastleGuildName+'行会的'+This_Npc.GetCelebName+'\'
  +'王者降临，四海统一。 \'
  +'膜拜与供奉可以增加雕像的荣耀度。\'
  +'|当前荣耀值：<'+inttostr(This_NPC.GetPlayerHonorValue)+'/c=red>\ \'
  +'|{cmd}<普通膜拜/@worshipNormal>                       ^<虔诚膜拜/@worshipDevout>\'
  +'|{cmd}<供奉精力水晶/@worshipEnergy>                   ^<返回/@main>\'
  );  
  end else
  This_Npc.NpcDialog(This_Player,
  '对不起，该雕像尚未被申请，不能膜拜！\'
  );         
end;

procedure _worshipNormal;
begin
  if This_Player.GetS(80,41) < 0 then This_Player.SetS(80,41,0);
  if This_Player.GetS(80,41) < 53 then
  begin
  This_Npc.NpcDialog(This_Player,
  '向沙巴克城主'+This_Npc.GetCelebName+'的雕像进行普通膜拜。\'
  +'普通膜拜每天只能进行1次，需要供奉50000金币，可以获得一定\'
  +'的绑定书页，只是我这里的书页数量有限，要想获得更多的书页\'
  +'可以尝试前往藏经峡谷，那里汇集了许多由书卷幻化而成的怪物。\ \'
  +'|{cmd}<进行普通膜拜/@worshipNormal_1>          ^<返回/@worship>'
  );   
  end else
  This_Npc.NpcDialog(This_Player,
  '向沙巴克城主'+This_Npc.GetCelebName+'的雕像进行普通膜拜。\'
  +'普通膜拜每天只能进行1次，需要供奉50000金币，可以获得丰厚的\'
  +'经验值奖励，不过已经没有更多的书页可以送你了。\ \'
  +'|{cmd}<进行普通膜拜/@worshipNormal_1>          ^<返回/@worship>'
  );           
end;

procedure _worshipNormal_1;
var
temp,temp1,ranexp:integer;
str:string;
begin
  if This_Npc.GetCelebName <> '' then     //雕像已经申请
  begin
    if This_Player.GetS(80,38) <> GetDateNum(GetNow) then
    begin 
      if This_Player.Level >= 30 then
      begin 
        if This_Player.GoldNum >= 50000 then
        begin 
          if This_Player.FreeBagNum > 0 then
          begin 
            temp := This_NPC.GetPlayerHonorValue; 
            This_NPC.AddPlayerHonorValue(1);
            if temp >= 5999 then This_NPC.ChgCelebColor(1) else This_NPC.ChgCelebColor(0);
            This_Player.SetS(80,38,GetDateNum(GetNow));
            This_Player.DecGold(50000);
            if This_Player.GetS(80,41) < 0 then This_Player.SetS(80,41,0);
            temp1 := This_Player.GetS(80,41);
            if temp1 < 53 then
            begin
              This_Player.SetS(80,41,temp1 + 1);
              if temp1 = 52 then
              begin
              This_Player.Give('绑定书页',20);
              str:='你膜拜了沙巴克城主并获得了20张绑定书页。\我这里只剩最后20张绑定书页了，今后可能没有书页可以赠送了。';
              end else
              begin
              This_Player.Give('绑定书页',15);
              str:='你膜拜了沙巴克城主并获得了15张绑定书页。';
              end;
            end else
            begin
             ranexp := random(250000) + 100001; 
             This_Player.Give('经验',ranexp);
             str:='你膜拜了沙巴克城主并获得了'+inttostr(ranexp)+'经验。';
            end; 
            This_Npc.NpcDialog(This_Player,
            str 
            ); 
          end else
          This_Npc.NpcDialog(This_Player,
          '你的包裹太满了。\'
          ); 
        end else
        This_Npc.NpcDialog(This_Player,
        '你身上并没有足够的香火钱啊。\'
        ); 
      end else
      This_Npc.NpcDialog(This_Player,
      '你的等级太低了，需要达到30级。\'
      +'在膜拜高手之前还是先增强一下自己的实力吧。\'
      ); 
    end else
    This_Npc.NpcDialog(This_Player,
    '对不起，你今天已经进行过膜拜，明天再来吧！\'
    ); 
  end else
  This_Npc.NpcDialog(This_Player,
  '对不起，该雕像尚未被申请，不能膜拜！\'
  );         
end;

procedure _worshipDevout;
begin 
This_Npc.NpcDialog(This_Player,
'向沙巴克城主'+This_Npc.GetCelebName+'的雕像进行虔诚膜拜。\'
+'虔诚膜拜每天可以进行8次，每次需要消耗1串虔诚挂坠，获得1点\'
+'精力值。\ \'
+'|{cmd}<进行虔诚膜拜/@worshipDevout_1>          ^<返回/@worship>'
);           
end;

procedure _worshipDevout_1;
var                                         
temp,temp1:integer;
begin 
  if This_Npc.GetCelebName <> '' then     //雕像已经申请
  begin
    if This_Player.Level >= 30 then
    begin
      if This_Player.GetS(80,40) <> GetDateNum(GetNow) then
      begin
      This_Player.SetS(80,40,GetDateNum(GetNow));
      This_Player.SetS(80,39,0);
      end; 
      temp1:= This_Player.GetS(80,39);
      if temp1 < 8 then
      begin 
        if This_Player.GetBagItemCount('虔诚挂坠') > 0 then
        begin 
        temp := This_NPC.GetPlayerHonorValue; 
        This_NPC.AddPlayerHonorValue(1);
        if temp >= 5999 then This_NPC.ChgCelebColor(1) else This_NPC.ChgCelebColor(0);
        This_Player.Take('虔诚挂坠',1); 
        This_Player.SetS(80,39,temp1 + 1);
        This_NPC.GivePositiveVValue(1,1,This_Player);
        This_Player.PlayerNotice('精力增加1点，当前共有'+inttostr(This_Player.GetVitalityValue(1))+'点。',2);
        if temp1 = 7 then
        begin
        This_Npc.NpcDialog(This_Player,
        '通过一番虔诚的膜拜，你获得了1点精力值。\'
        +'今日已经虔诚膜拜'+inttostr(temp1 + 1)+'次。\ \'
        );  
        end else                                                                 
        This_Npc.NpcDialog(This_Player,
        '通过一番虔诚的膜拜，你获得了1点精力值。\'
        +'今日已经虔诚膜拜'+inttostr(temp1 + 1)+'次。\ \'
        +'|{cmd}<继续进行虔诚膜拜/@worshipDevout_1>          ^<返回/@worship>'
        );  
        end else
        This_Npc.NpcDialog(This_Player,
        '你身上并没有虔诚挂坠，看来你并不是一个虔诚的信徒啊。\'
         ); 
      end else
      This_Npc.NpcDialog(This_Player,
      '对不起，你今天已经虔诚膜拜过8次，明天再来吧！\'
      ); 
    end else
    This_Npc.NpcDialog(This_Player,
    '你的等级太低了，需要达到30级。\'
    +'在膜拜高手之前还是先增强一下自己的实力吧。\'
    );   
  end else
  This_Npc.NpcDialog(This_Player,
  '对不起，该雕像尚未被申请，不能膜拜！\'
  );         
end;

procedure _worshipEnergy;
begin
This_Npc.NpcDialog(This_Player,
'向沙巴克城主'+This_Npc.GetCelebName+'的雕像供奉1个精力水晶。\'
+'每天可以无限次向雕像供奉精力水晶，每供奉1个精力水晶雕像的荣\'
+'耀值会增加1点。\ \'
+'|{cmd}<确认供奉精力水晶/@worshipEnergy_1>          ^<返回/@worship>'
);           
end;

procedure _worshipEnergy_1;
var
temp:integer;
begin 
  if This_Npc.GetCelebName <> '' then     //雕像已经申请
  begin
    if This_Player.GetBagItemCount('精力水晶') > 0 then
    begin 
    temp := This_NPC.GetPlayerHonorValue; 
    This_NPC.AddPlayerHonorValue(1);
    if temp >= 5999 then This_NPC.ChgCelebColor(1) else This_NPC.ChgCelebColor(0);
    This_Player.Take('精力水晶',1); 
    This_Player.PlayerNotice('成功供奉1个精力水晶，雕像的荣耀值增加1点。',2);                                                                
    This_Npc.NpcDialog(This_Player,
    '成功供奉1个精力水晶，雕像的荣耀值增加1点。\'
    +'当前荣耀值：<'+inttostr(temp+1)+'/c=red>\ \'
    +'|{cmd}<继续供奉精力水晶/@worshipEnergy_1>          ^<返回/@worship>'
    );  
    end else
    This_Npc.NpcDialog(This_Player,
    '你没有带来精力水晶，无法供奉。\'
     ); 
  end else
  This_Npc.NpcDialog(This_Player,
  '对不起，该雕像尚未被申请，不能膜拜！\'
  );         
end;

procedure OnInitialize;
begin
  This_Npc.AddNpcProp(3);
end;

begin
  if This_Npc.GetCelebName = This_Player.Name then     //雕像拥有者 
  begin
  This_Npc.NpcDialog(This_Player,
  '尊敬的沙巴克城主'+This_Player.Name+'\'
  +'现在您的雕像荣耀值为'+inttostr(This_NPC.GetPlayerHonorValue)+'点，\'
  +'请接受万千勇士对您的敬仰与膜拜吧。\ \'
  +'|{cmd}<膜拜沙城城主/@worship>               \'  //^<领取一代天骄称号/@gettitle>
  +'|{cmd}<退出/@doexit>'
  );
  end else
  This_Npc.NpcDialog(This_Player,
  '当前沙巴克城主为'+This_NPC.GetCastleGuildName+'行会的'+This_Npc.GetCelebName+'\'
  +'只有玛法大陆最强行会的掌门人才有机会成为沙巴克城主雕像的主\'
  +'人。成功申请到沙城城主雕像的强者不但可以接受来自玛法大陆各\'
  +'界勇士的顶礼膜拜，更能获得象征象征城主独一无二身份的一代天\'
  +'骄称号。\ \' 
  +'|{cmd}<膜拜沙城城主/@worship>               \'//^<领取一代天骄称号/@gettitle>
  +'|{cmd}<退出/@doexit>'
  );
end.