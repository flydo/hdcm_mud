procedure _GetFreeGold;  
var Snum : Integer;//局部语法变量声明
begin
   Snum := GetG(3,2)
     if Snum < 999 then
      begin 
        if This_Player.GetV(13,4) <> 1 then
        begin
            if This_Player.Level >= 40 then
          begin
            if This_Player.FreeBagNum >= 3 then
            begin 
                This_Player.Give('10倍秘籍',3);
                This_Player.SetV(13,4,1);
                SetG(3,2,Snum + 1);
                This_NPC.NpcDialog(This_Player,
                '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹不足3格') 
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足40级'); 
        end  else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
      end  else
      This_NPC.NpcDialog(This_Player,'40级奖励已全部领取！');
end;

procedure _GetFreeGold1;  //方法
var  Snum1 : Integer;
begin 
   Snum1 := GetG(4,2)
    if Snum1 < 999 then
    begin 
         if This_Player.GetV(14,4) <> 1 then
        begin
          if This_Player.Level >= 45 then
           begin
             if This_Player.FreeBagNum >= 5 then
             begin 
                This_Player.AddLF(0,300);
				This_Player.Give('10倍秘籍',3);
                This_Player.SetV(14,4,1);
                SetG(4,2,Snum1 + 1);
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
             end else
             This_NPC.NpcDialog(This_Player,'你的包裹剩余不足5格') 
           end else
            This_NPC.NpcDialog(This_Player,'你的等级不足45级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'45级奖励已全部领取！');
end;









procedure _GetFreeGold2;  //方法
var  Snum2: Integer;//局部语法变量声明
begin 
    Snum2 := GetG(5,2)
    if Snum2 < 999 then
    begin 
        if This_Player.GetV(15,4) <> 1 then
        begin
          if This_Player.Level >= 50 then
          begin
            if This_Player.FreeBagNum >= 5 then
            begin 
                This_Player.Give('10倍秘籍',3);
                This_Player.Give('100元宝',2);
                This_Player.SetV(15,4,1);
                SetG(5,2,Snum2 + 1);
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹剩余不足5格')
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足50级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'50级奖励已全部领取！');
end;



procedure _GetFreeGold3;  //方法
var  Snum3: Integer;//局部语法变量声明
begin
    Snum3 := GetG(6,2)
    if Snum3 < 999 then
    begin 
        if This_Player.GetV(16,4) <> 1 then
        begin
          if This_Player.Level >= 55 then
          begin
            if This_Player.FreeBagNum >= 8 then
            begin 
                This_Player.Give('10倍秘籍',5);
                This_Player.Give('100元宝',3);
                This_Player.SetV(16,4,1);
                SetG(6,2,Snum3 + 1); 
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹剩余不足8格')
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足55级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'55级奖励已全部领取！');
end;

procedure _GetFreeGold4;  //方法
var Snum4: Integer;//局部语法变量声明
begin
    Snum4 := GetG(7,2)
    if Snum4 < 999 then
    begin 
        if This_Player.GetV(17,4) <> 1 then
        begin
            if This_Player.Level >= 60 then
          begin
            if This_Player.FreeBagNum >= 9 then
            begin 
                This_Player.Give('10倍秘籍',5);
                This_Player.Give('100元宝',4);
				This_Player.AddLF(0,300);
                This_Player.SetV(17,4,1);
                SetG(7,2,Snum4 + 1);
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹剩余不足9格')
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足60级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'60级奖励已全部领取！');
end;


procedure _GetFreeGold5;  //方法
var    Snum5: Integer;//局部语法变量声明
begin
    Snum5 := GetG(8,2)
    if Snum5 < 999 then
    begin 
        if This_Player.GetV(18,4) <> 1 then
        begin
            if This_Player.Level >= 65 then
          begin
            if This_Player.FreeBagNum >= 10 then
            begin 
                This_Player.Give('100元宝',8);
				This_Player.AddLF(0,800);
                This_Player.SetV(18,4,1);
                SetG(8,2,Snum5 + 1);
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹剩余不足10格')
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足65级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'65级奖励已全部领取！');
end;






var BDYB : Integer; //主函数入口
begin
 BDYB := This_Player.GetV(20,9); //获取元宝数量
    This_NPC.NpcDialog( This_Player,
    '40级奖励：10倍秘籍*3 \|' +
    '45级奖励：10倍秘籍*3,灵符+300.\|' +
    '50级奖励：10倍秘籍*3，元宝200。\|' +
    '55级奖励：10倍秘籍*5,元宝300。\|' +
    '60级奖励：10倍秘籍*5，元宝400，灵符400,\|' +
    '65级奖励：元宝+800,灵符+800.\|' +
    '|{cmd}<40级冲级奖励/@GetFreeGold> ^<45级冲级奖励/@GetFreeGold1>\'  +
    '|{cmd}<50级冲级奖励/@GetFreeGold2> ^<55级冲级奖励/@GetFreeGold3>\' + 
    '|{cmd}<60级冲级奖励/@GetFreeGold4> ^<65级冲级奖励/@GetFreeGold5>'
	);
end.