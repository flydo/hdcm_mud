procedure _GetFreeGold;  
var Snum : Integer;//局部语法变量声明
begin
   Snum := GetG(3,2)
     if Snum < 50 then
      begin 
        if This_Player.GetV(13,4) <> 1 then
        begin
            if This_Player.Level >= 40 then
          begin
            if This_Player.FreeBagNum >= 1 then
            begin 
                This_Player.Give('金条',1);
                
                This_Player.SetV(13,4,1);
                SetG(3,2,Snum + 1);
                This_NPC.NpcDialog(This_Player,
                '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹不足1格') 
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足40级'); 
        end  else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
      end  else
      This_NPC.NpcDialog(This_Player,'40级奖励已全部领取！');
end;

procedure _GetFreeGold1;  //方法
var  Snum1 : Integer;
begin 
   Snum1 := GetG(4,2)
    if Snum1 < 30 then
    begin 
         if This_Player.GetV(14,4) <> 1 then
        begin
          if This_Player.Level >= 45 then
           begin
             if This_Player.FreeBagNum >= 5 then
             begin 
                This_Player.Give('100元宝',5);

                This_Player.SetV(14,4,1);
                SetG(4,2,Snum1 + 1);
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
             end else
             This_NPC.NpcDialog(This_Player,'你的包裹剩余不足5格') 
           end else
            This_NPC.NpcDialog(This_Player,'你的等级不足45级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'45级奖励已全部领取！');
end;









procedure _GetFreeGold2;  //方法
var  Snum2: Integer;//局部语法变量声明
begin 
    Snum2 := GetG(5,2)
    if Snum2 < 20 then
    begin 
        if This_Player.GetV(15,4) <> 1 then
        begin
          if This_Player.Level >= 50 then
          begin
            if This_Player.FreeBagNum >= 3 then
            begin 
                This_Player.Give('绑定书页',200);

                This_Player.SetV(15,4,1);
                SetG(5,2,Snum2 + 1);
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹剩余不足3格')
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足50级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'50级奖励已全部领取！');
end;



procedure _GetFreeGold3;  //方法
var  Snum3: Integer;//局部语法变量声明
begin
    Snum3 := GetG(6,2)
    if Snum3 < 10 then
    begin 
        if This_Player.GetV(16,4) <> 1 then
        begin
          if This_Player.Level >= 55 then
          begin
            if This_Player.FreeBagNum >= 3 then
            begin 
                This_Player.Give('绑定书页',200);

                This_Player.SetV(16,4,1);
                SetG(6,2,Snum3 + 1); 
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹剩余不足3格')
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足55级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'55级奖励已全部领取！');
end;

procedure _GetFreeGold4;  //方法
var Snum4: Integer;//局部语法变量声明
begin
    Snum4 := GetG(7,2)
    if Snum4 < 5 then
    begin 
        if This_Player.GetV(17,4) <> 1 then
        begin
            if This_Player.Level >= 60 then
          begin
            if This_Player.FreeBagNum >= 3 then
            begin 
                This_Player.Give('绑定书页',600);

                This_Player.SetV(17,4,1);
                SetG(7,2,Snum4 + 1);
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹剩余不足3格')
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足60级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'60级奖励已全部领取！');
end;


procedure _GetFreeGold5;  //方法
var    Snum5: Integer;//局部语法变量声明
begin
    Snum5 := GetG(8,2)
    if Snum5 < 2 then
    begin 
        if This_Player.GetV(18,4) <> 1 then
        begin
            if This_Player.Level >= 65 then
          begin
            if This_Player.FreeBagNum >= 3 then
            begin 
                This_Player.Give('200灵符' , 1);

                This_Player.SetV(18,4,1);
                SetG(8,2,Snum5 + 1);
                This_NPC.NpcDialog(This_Player,
               '恭喜你领取成功');
            end else
            This_NPC.NpcDialog(This_Player,'你的包裹剩余不足3格')
          end else
          This_NPC.NpcDialog(This_Player,'你的等级不足65级'); 
        end else
        This_NPC.NpcDialog(This_Player,'你已领取了奖励');
    end else
    This_NPC.NpcDialog(This_Player,'65级奖励已全部领取！');
end;


procedure _qingkong;  //方法
var Snum , Snum1 , Snum2 , Snum3 , Snum4 , Snum5 : Integer;//局部语法变量声明
begin
   SetG(3,2,Snum); 
   SetG(4,2,Snum1); 
   SetG(5,2,Snum2); 
   SetG(6,2,Snum3); 
   SetG(7,2,Snum4); 
   SetG(8,2,Snum5);   
end;



var Snum , Snum1 , Snum2 , Snum3 , Snum4 , Snum5 : Integer; //主函数入口
begin
    begin
     Snum := GetG(3,2)
     Snum1 := GetG(4,2)
     Snum2 := GetG(5,2)
     Snum3 := GetG(6,2)
     Snum4 := GetG(7,2)
     Snum5 := GetG(8,2)
    end;
    This_NPC.NpcDialog( This_Player,
    '40级奖励：金条*1,当前剩余数量:  ' + inttostr(50 - Snum) + '\|' +
    '45级奖励：500元宝,当前剩余数量:  ' + inttostr(30 - Snum1) + '\|' +
    '50级奖励：200书页,当前剩余数量:  ' + inttostr(20 - Snum2) + '\|' +
    '55级奖励：200书页,当前剩余数量:  ' + inttostr(10 - Snum3) + '\|' +
    '60级奖励：600书页,当前剩余数量:  ' + inttostr(5 - Snum4) + '\|' +
    '65级奖励：200灵符,当前剩余数量:  ' + inttostr(2 - Snum5) + '\|' +
    '|{cmd}<清空奖励/@qingkong> \'  
	);
end.