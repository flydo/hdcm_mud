{******************************************************************
版本脚本制作：张益达 QQ：1626293287  |制作不易，手下留情，请勿删除|
*******************************************************************}




program mir2;
var
bxsl, zzsl : integer;
procedure _exit;	// 调用此方法对话框会被关闭
begin
	This_Npc.CloseDialog(This_Player); // 执行此代码对话框会被关闭
end; 

procedure _doexit;	// 调用此方法对话框会被关闭
begin
	This_Npc.CloseDialog(This_Player); // 执行此代码对话框会被关闭
end; 



procedure domain;
begin
if This_Player.GMLevel > 4  then
begin
This_NPC.NpcDialog(This_Player,
        +'欢迎来到【<寒刀沉默/fcolor=243>】 <GM后台调试/@GM>\'	
        +'|<─────────────────────>\' 
	   +'|<主号变性元宝数量/fcolor=250>     <'+inttostr(GetG(108,4))+'/fcolor=251>\'	     
	   +'|<主号转职元宝数量/fcolor=250>     <'+inttostr(GetG(108,5))+'/fcolor=251>\'  
       +'|<─────────────────────>\'		
		+'|<注意:转职后请小退.给予基础技能石！>\'			
		+'|{cmd}<主号转职/@1zhzz> ^<主号变性/@1zhbx>^<领取技能包/@jnb>\'

		); 
   end else
   begin
 This_NPC.NpcDialog(This_Player,
        +'欢迎来到【<寒刀沉默/fcolor=243>】 <转职变性功能/fcolor=250>\'	
        +'|<─────────────────────>\' 
	   +'|<主号变性元宝数量/fcolor=250>     <'+inttostr(GetG(108,4))+'/fcolor=251>\'	     
	   +'|<主号转职元宝数量/fcolor=250>     <'+inttostr(GetG(108,5))+'/fcolor=251>\'  
       +'|<─────────────────────>\'
			
		+'|<注意:转职后请小退.给予基础技能石！>\'			
		+'|{cmd}<主号转职/@zhzz> ^<主号变性/@zhbx>^<领取技能包/@jnb>\'		

		);   
end;
end;
















//==========================================================后台调整元宝


 procedure _GM;
begin 
if This_Player.GMLevel > 4  then
begin
  This_Npc.NpcDialog(This_Player,	   
	   +'|尊敬的管理大人，我这里可以控制元宝数量!\'   
      +'|<─────────────────────>\'	 	   
	   +'|<主号变性元宝数量/fcolor=250>     <'+inttostr(GetG(108,4))+'/@bxyb>\'	     
	   +'|<主号转职元宝数量/fcolor=250>     <'+inttostr(GetG(108,5))+'/@zyyb>\'  
       +'|<─────────────────────>\'   
       +'|默认：变性 <20000/fcolor=250> 元宝  转职 <50000/fcolor=250> 元宝\ \' 	   
       +'|<您可以直接点击数量进行调整>  \ \' 
       +'|{cmd}<关闭此页/@exit>^<返回首页/@main>\ \'	 
   );		 
   end else
   begin
 This_NPC.NpcDialog(This_Player,
'警告：你无权此操作，请速速联系管理员！！|\'+
 '|                                                  <返回首页/@main>');
     
  end;
  end;


procedure _bxyb; 
begin    
        This_NPC.InputDialog(This_Player,'请输入变性元宝数量',0,155) ;
end;

procedure p155;
begin
   bxsl :=  StrToIntDef(This_NPC.InputStr,-1);//只能输入数字，否则都返回-1
   if This_NPC.InputOK  then             
    begin
    if (bxsl >= 100) and (bxsl < 10000000) then
         begin 
  SetG(108,4,bxsl);  
  This_Npc.NpcDialog(This_Player,	   
	   +'|尊敬的管理大人，我这里可以控制元宝数量!\'   
      +'|<─────────────────────>\'	 	   
	   +'|<主号变性元宝数量/fcolor=250>     <'+inttostr(GetG(108,4))+'/@bxyb>\'	     
	   +'|<主号转职元宝数量/fcolor=250>     <'+inttostr(GetG(108,5))+'/@zyyb>\'  
       +'|<─────────────────────>\'  
       +'|默认：变性 <20000/fcolor=250> 元宝  转职 <50000/fcolor=250> 元宝\ \' 	   
       +'|<您可以直接点击数量进行调整>  \ \' 
       +'|{cmd}<关闭此页/@exit>^<返回首页/@main>\ \'	 
   );		
  end else
This_NPC.NpcDialog(This_Player,  
'变性元宝数量输入错误：  100-10000000 之内|\'+
 '|{cmd}<返回上页/@GM>     ^<返回首页/@main>       ^<关闭此页/@DoExit>');
  end else
This_NPC.NpcDialog(This_Player,  
'变性元宝数量输入错误：非法字符，只能输入数字！|\'+
 '|{cmd}<返回上页/@GM>     ^<返回首页/@main>       ^<关闭此页/@DoExit>');

end;     


procedure _zyyb; 
begin    
        This_NPC.InputDialog(This_Player,'请输入变性元宝数量',0,156) ;
end;

procedure p156;
begin
   zzsl :=  StrToIntDef(This_NPC.InputStr,-1);//只能输入数字，否则都返回-1
   if This_NPC.InputOK  then             
    begin
    if (zzsl >= 100) and (zzsl < 10000000) then
         begin 
  SetG(108,5,zzsl);  
  This_Npc.NpcDialog(This_Player,	   
	   +'|尊敬的管理大人，我这里可以控制元宝数量!\'   
      +'|<─────────────────────>\'	 	   
	   +'|<主号变性元宝数量/fcolor=250>     <'+inttostr(GetG(108,4))+'/@bxyb>\'	     
	   +'|<主号转职元宝数量/fcolor=250>     <'+inttostr(GetG(108,5))+'/@zyyb>\'  
       +'|<─────────────────────>\'  
       +'|默认：变性 <20000/fcolor=250> 元宝  转职 <50000/fcolor=250> 元宝\ \' 	   
       +'|<您可以直接点击数量进行调整>  \ \' 
       +'|{cmd}<关闭此页/@exit>^<返回首页/@main>\ \'	 
   );	
  end else
This_NPC.NpcDialog(This_Player,  
'转职元宝数量输入错误：  100-10000000 之内|\'+
 '|{cmd}<返回上页/@GM>     ^<返回首页/@main>       ^<关闭此页/@DoExit>');
  end else
This_NPC.NpcDialog(This_Player,  
'转职元宝数量输入错误：非法字符，只能输入数字！|\'+
 '|{cmd}<返回上页/@GM>     ^<返回首页/@main>       ^<关闭此页/@DoExit>');

end;     





//==========================================================后台调整元宝

















procedure _jnb;
begin
This_NPC.NpcDialog(This_Player,
        +'欢迎来到【<寒刀沉默/fcolor=243>】 <点击下方按钮领取技能包/fcolor=250>\'	
        +'|<─────────────────────>\' 	
		+'|<领取要求：/fcolor=242><10格包裹，基础技能石/fcolor=254>\'	
        +'|<─────────────────────>\' 		
		+'|<注意:转职后才会给予基础技能石！>\'	
        +'|{cmd}<领取战士技能/@zhanshijn> ^<领取法师技能/@fashijn>\| ^<领取道士技能/@daoshijn>|\ \'	
		
        +'|{cmd}<关闭此页/@exit>^<返回首页/@main>\ \'		
		); 

end;




procedure _zhanshijn;
begin
  if This_Player.GetBagItemCount('基础技能石') >= 0 then
  begin
  if This_Player.FreeBagNum >= 10 then
  begin
                This_Player.Give('基本剑术' , 1);
				This_Player.Give('攻杀剑术' , 1);
				This_Player.Give('刺杀剑术' , 1);
				This_Player.Give('半月弯刀' , 1);
				This_Player.Give('野蛮冲撞' , 1);
				This_Player.Give('烈火剑法' , 1);
  This_Npc.NpcDialog(This_Player,
'兑换成功！  ');
 end else
  This_Npc.NpcDialog(This_Player,
'包裹不足6格！  ');
  end else
   This_Npc.NpcDialog(This_Player,
'你没有基础技能石，转职后获取！  ');
end;


procedure _daoshijn;
begin
  if This_Player.GetBagItemCount('基础技能石') >= 0 then
  begin
  if This_Player.FreeBagNum >= 10 then
  begin
                This_Player.Give('治愈术', 1);
				This_Player.Give('精神力战法', 1);
				This_Player.Give('灵魂火符', 1);
				This_Player.Give('施毒术', 1);
				This_Player.Give('困魔咒', 1);
				This_Player.Give('幽灵盾', 1);
				This_Player.Give('神圣战甲术', 1);
				This_Player.Give('召唤神兽', 1);
				This_Player.Give('隐身术', 1);
				This_Player.Give('集体隐身术', 1);
				This_Player.Give('召唤骷髅', 1);
				This_Player.Give('召唤神兽', 1);
  This_Npc.NpcDialog(This_Player,
'兑换成功！  ');
 end else
  This_Npc.NpcDialog(This_Player,
'包裹不足12格！  ');
  end else
   This_Npc.NpcDialog(This_Player,
'你没有基础技能石，转职后获取！  ');
end;



procedure _fashijn;
begin
  if This_Player.GetBagItemCount('基础技能石') >= 0 then
  begin
  if This_Player.FreeBagNum >= 14 then
  begin
                This_Player.Give('雷电术', 1);
				This_Player.Give('火墙', 1);
				This_Player.Give('疾光电影', 1);
				This_Player.Give('魔法盾', 1);
				This_Player.Give('冰咆哮', 1);
				This_Player.Give('抗拒火环', 1);
				This_Player.Give('诱惑之光', 1);
				This_Player.Give('地狱雷光', 1);					
				This_Player.Give('火球术', 1);
				This_Player.Give('大火球', 1);
				This_Player.Give('地狱火', 1);
				This_Player.Give('瞬息移动', 1);
				This_Player.Give('爆裂火焰', 1);
				This_Player.Give('圣言术', 1);
  This_Npc.NpcDialog(This_Player,
'兑换成功！  ');
 end else
  This_Npc.NpcDialog(This_Player,
'包裹不足14格！  ');
  end else
   This_Npc.NpcDialog(This_Player,
'你没有基础技能石，转职后获取！  ');
end;






















procedure _zhzz;
begin
This_NPC.NpcDialog(This_Player,
        +'欢迎来到【<寒刀沉默/fcolor=243>】 <主号转职功能/fcolor=250>\'	
        +'|<─────────────────────>\' 	
		+'|<转职要求：/fcolor=242><主号 '+inttostr(GetG(108,5))+'  元宝/fcolor=254>\'	
        +'|<─────────────────────>\' 		
		+'|<注意:转职后请小退.给予基础技能包！>\'	
        +'|{cmd}<转职战士/@zhanshi> ^<转职法师/@fashi> ^<转职道士/@daoshi>|\ \'	
		
        +'|{cmd}<关闭此页/@exit>^<返回首页/@main>\ \'		
		); 

end;







//==========================================================变性开始


procedure _zhbx;
begin
This_NPC.NpcDialog(This_Player,
        +'欢迎来到【<寒刀沉默/fcolor=243>】 <主号/fcolor=250>变性功能\'	
        +'|<─────────────────────>\' 	
		+'|变性要求：<主号/fcolor=250> <'+inttostr(GetG(108,4))+'  元宝/fcolor=254>\'	
        +'|<─────────────────────>\' 		
		+'|<注意:变性后请小退.才能正常体验变性的快感！>\'	
		
        +'|{cmd}<变成男人/@nanren> ^<变成女人/@nanren>|\ \'	
		
        +'|{cmd}<关闭此页/@exit>^<返回首页/@main>\ \'		
		); 

end;




procedure _nanren;            //主号变性
begin
	if This_Player.YBNum > GetG(108,4) then
	begin
    This_Player.ScriptRequestSubYBNum(GetG(108,4));
	This_Player.SetS(1,1,3);
    ServerSay('变性系统：恭喜玩家【' + This_Player.Name + '】品尝到了变性的快感！', 3); 
	    This_Npc.NpcDialog(This_Player,'<恭喜你:变性成功.请小退方可正常游戏！/fcolor=250>|'
		+'|{cmd} <返回上页/@zhbx> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	 
    This_Player.SetS(1,1,16);     //踢玩家下线		
	
end
else
	    This_Npc.NpcDialog(This_Player,'<很遗憾:变性失败.元宝不足/fcolor=243> '+inttostr(GetG(108,4))+' <无法手术>！|'
		+'|{cmd} <返回上页/@zhbx> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	  	
end;





//==========================================================变性结束






//==========================================================主号转职开始


procedure _zhanshi;
begin
This_NPC.NpcDialog(This_Player,
        +'欢迎来到【<寒刀沉默/fcolor=243>】 转职<战士/fcolor=250>功能\'	
        +'|<─────────────────────>\' 	
		+'|转职<战士/fcolor=250>要求：<主号 '+inttostr(GetG(108,5))+' 元宝/fcolor=254>\'	
        +'|<─────────────────────>\' 		
		+'|<注意:转职后请小退.给予基础技能包！>|\'	
        +'|{cmd}<确定转职战士/@zhanshi1>^<返回上页/@zhzz>\ \'				
		); 

end;

procedure _zhanshi1;
begin 
  if (This_Player.Level >= 42) and (This_Player.YBnum >= GetG(108,5)) then 
begin
   if( This_Player.Job = 1)or (This_Player.Job = 2 )then 
     
begin
				This_Player.DeleteSkill('基本剑术');
				This_Player.DeleteSkill('攻杀剑术');
				This_Player.DeleteSkill('刺杀剑术');
				This_Player.DeleteSkill('半月弯刀');
				This_Player.DeleteSkill('野蛮冲撞');
				This_Player.DeleteSkill('烈火剑法');
				This_Player.DeleteSkill('逐日剑法');
				This_Player.DeleteSkill('狮子吼');
				This_Player.DeleteSkill('开天斩');
				This_Player.DeleteSkill('治愈术');
				This_Player.DeleteSkill('精神力战法');
				This_Player.DeleteSkill('灵魂火符');
				This_Player.DeleteSkill('施毒术');
				This_Player.DeleteSkill('困魔咒');
				This_Player.DeleteSkill('幽灵盾');
				This_Player.DeleteSkill('神圣战甲术');
				This_Player.DeleteSkill('召唤神兽');
				This_Player.DeleteSkill('隐身术');
				This_Player.DeleteSkill('集体隐身术');
				This_Player.DeleteSkill('召唤骷髅');
				This_Player.DeleteSkill('召唤神兽');
				This_Player.DeleteSkill('心灵启示');
				This_Player.DeleteSkill('群体治愈术');
				This_Player.DeleteSkill('噬血术');
				This_Player.DeleteSkill('气功波');
				This_Player.DeleteSkill('无极真气');
				This_Player.DeleteSkill('召唤月灵');
				This_Player.DeleteSkill('雷电术');
				This_Player.DeleteSkill('火墙');
				This_Player.DeleteSkill('疾光电影');
				This_Player.DeleteSkill('魔法盾');
				This_Player.DeleteSkill('冰咆哮');
				This_Player.DeleteSkill('抗拒火环');
				This_Player.DeleteSkill('诱惑之光');
				This_Player.DeleteSkill('地狱雷光');					
				This_Player.DeleteSkill('火球术');
				This_Player.DeleteSkill('大火球');
				This_Player.DeleteSkill('地狱火');
				This_Player.DeleteSkill('瞬息移动');
				This_Player.DeleteSkill('爆裂火焰');
				This_Player.DeleteSkill('圣言术');
				This_Player.DeleteSkill('灭天火');
				This_Player.DeleteSkill('流星火雨');
				This_Player.DeleteSkill('寒冰掌');
				This_Player.DeleteSkill('灭天火');
				This_Player.DeleteSkill('火龙气焰');
				This_Player.Give('基础技能石',1)
                This_Player.ScriptRequestSubYBNum(GetG(108,5)); 
	            This_Player.SetS(1,1,0);      //转战士
				This_Player.SetS(1,1,14);     //杀死宝宝
	  ServerSay('转职系统：恭喜玩家[' + This_Player.Name + ']成功转职战士!', 3);
	   This_Npc.NpcDialog(This_Player,'<恭喜你：成功转职战士.请小退方可正常游戏！/fcolor=250>|'
		+'|{cmd} <返回上页/@zhanshi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	  
		        This_Player.SetS(1,1,16);     //踢玩家下线
end else 
	    This_Npc.NpcDialog(This_Player,'很遗憾：<你已经是勇猛的/fcolor=243> 战士 <了.无法转职！>|'
		+'|{cmd} <返回上页/@zhanshi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	
end else
	    This_Npc.NpcDialog(This_Player,'很遗憾：<转职战士失败.元宝不足/fcolor=243> '+inttostr(GetG(108,5))+' <无法转职>！|'
		+'|{cmd} <返回上页/@zhanshi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	
end;










procedure _daoshi;
begin
This_NPC.NpcDialog(This_Player,
        +'欢迎来到【<寒刀沉默/fcolor=243>】 转职<道士/fcolor=250>功能\'	
        +'|<─────────────────────>\' 	
		+'|转职<道士/fcolor=250>要求：<主号 '+inttostr(GetG(108,5))+' 元宝/fcolor=254>\'	
        +'|<─────────────────────>\' 		
		+'|<注意:转职后请小退.给予基础技能包！>|\'	
        +'|{cmd}<确定转职道士/@daoshi1>^<返回上页/@zhzz>\ \'				
		); 

end;

procedure _daoshi1;
begin  
  if (This_Player.Level >= 42) and (This_Player.YBnum >= GetG(108,5)) then 
begin
   if( This_Player.Job = 1)or (This_Player.Job = 0 )then 
     
	           begin
				This_Player.DeleteSkill('基本剑术');
				This_Player.DeleteSkill('攻杀剑术');
				This_Player.DeleteSkill('刺杀剑术');
				This_Player.DeleteSkill('半月弯刀');
				This_Player.DeleteSkill('野蛮冲撞');
				This_Player.DeleteSkill('烈火剑法');
				This_Player.DeleteSkill('逐日剑法');
				This_Player.DeleteSkill('狮子吼');
				This_Player.DeleteSkill('开天斩');
				This_Player.DeleteSkill('治愈术');
				This_Player.DeleteSkill('精神力战法');
				This_Player.DeleteSkill('灵魂火符');
				This_Player.DeleteSkill('施毒术');
				This_Player.DeleteSkill('困魔咒');
				This_Player.DeleteSkill('幽灵盾');
				This_Player.DeleteSkill('神圣战甲术');
				This_Player.DeleteSkill('召唤神兽');
				This_Player.DeleteSkill('隐身术');
				This_Player.DeleteSkill('集体隐身术');
				This_Player.DeleteSkill('召唤骷髅');
				This_Player.DeleteSkill('召唤神兽');
				This_Player.DeleteSkill('心灵启示');
				This_Player.DeleteSkill('群体治愈术');
				This_Player.DeleteSkill('噬血术');
				This_Player.DeleteSkill('气功波');
				This_Player.DeleteSkill('无极真气');
				This_Player.DeleteSkill('召唤月灵');
				This_Player.DeleteSkill('雷电术');
				This_Player.DeleteSkill('火墙');
				This_Player.DeleteSkill('疾光电影');
				This_Player.DeleteSkill('魔法盾');
				This_Player.DeleteSkill('冰咆哮');
				This_Player.DeleteSkill('抗拒火环');
				This_Player.DeleteSkill('诱惑之光');
				This_Player.DeleteSkill('地狱雷光');					
				This_Player.DeleteSkill('火球术');
				This_Player.DeleteSkill('大火球');
				This_Player.DeleteSkill('地狱火');
				This_Player.DeleteSkill('瞬息移动');
				This_Player.DeleteSkill('爆裂火焰');
				This_Player.DeleteSkill('圣言术');
				This_Player.DeleteSkill('灭天火');
				This_Player.DeleteSkill('流星火雨');
				This_Player.DeleteSkill('寒冰掌');
				This_Player.DeleteSkill('灭天火');
				This_Player.DeleteSkill('火龙气焰');
                This_Player.Give('基础技能石',1)				
                This_Player.ScriptRequestSubYBNum(GetG(108,5)); 
	            This_Player.SetS(1,1,2);//转道士
				This_Player.SetS(1,1,14);     //杀死宝宝
	  ServerSay('转职系统：恭喜玩家[' + This_Player.Name + ']成功转职道士!', 3);
	   This_Npc.NpcDialog(This_Player,'<恭喜你：成功转职道士.请小退方可正常游戏！/fcolor=250>|'
		+'|{cmd} <返回上页/@daoshi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	 
                This_Player.SetS(1,1,16);     //踢玩家下线		
end else 
	    This_Npc.NpcDialog(This_Player,'很遗憾：<你已经是伟大的/fcolor=243> 道士 <了.无法转职！>|'
		+'|{cmd} <返回上页/@daoshi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	
end else
	    This_Npc.NpcDialog(This_Player,'很遗憾：<转职道士失败.元宝不足/fcolor=243> '+inttostr(GetG(108,5))+' <无法转职>！|'
		+'|{cmd} <返回上页/@daoshi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;
end;


procedure _fashi;
begin
This_NPC.NpcDialog(This_Player,
        +'欢迎来到【<寒刀沉默/fcolor=243>】 转职<法师/fcolor=250>功能\'	
        +'|<─────────────────────>\' 	
		+'|转职<法师/fcolor=250>要求：<主号 '+inttostr(GetG(108,5))+' 元宝/fcolor=254>\'	
        +'|<─────────────────────>\' 		
		+'|<注意:转职后请小退.给予基础技能包！>|\'	
        +'|{cmd}<确定转职法师/@fashi1>^<返回上页/@zhzz>\ \'				
		); 

end;


procedure _fashi1;
begin  
  if (This_Player.Level >= 42) and (This_Player.YBnum >= GetG(108,5)) then 
begin
   if( This_Player.Job = 0)or (This_Player.Job = 2 )then 
     
	           begin
				This_Player.DeleteSkill('基本剑术');
				This_Player.DeleteSkill('攻杀剑术');
				This_Player.DeleteSkill('刺杀剑术');
				This_Player.DeleteSkill('半月弯刀');
				This_Player.DeleteSkill('野蛮冲撞');
				This_Player.DeleteSkill('烈火剑法');
				This_Player.DeleteSkill('逐日剑法');
				This_Player.DeleteSkill('狮子吼');
				This_Player.DeleteSkill('开天斩');
				This_Player.DeleteSkill('治愈术');
				This_Player.DeleteSkill('精神力战法');
				This_Player.DeleteSkill('灵魂火符');
				This_Player.DeleteSkill('施毒术');
				This_Player.DeleteSkill('困魔咒');
				This_Player.DeleteSkill('幽灵盾');
				This_Player.DeleteSkill('神圣战甲术');
				This_Player.DeleteSkill('召唤神兽');
				This_Player.DeleteSkill('隐身术');
				This_Player.DeleteSkill('集体隐身术');
				This_Player.DeleteSkill('召唤骷髅');
				This_Player.DeleteSkill('召唤神兽');
				This_Player.DeleteSkill('心灵启示');
				This_Player.DeleteSkill('群体治愈术');
				This_Player.DeleteSkill('噬血术');
				This_Player.DeleteSkill('气功波');
				This_Player.DeleteSkill('无极真气');
				This_Player.DeleteSkill('召唤月灵');
				This_Player.DeleteSkill('雷电术');
				This_Player.DeleteSkill('火墙');
				This_Player.DeleteSkill('疾光电影');
				This_Player.DeleteSkill('魔法盾');
				This_Player.DeleteSkill('冰咆哮');
				This_Player.DeleteSkill('抗拒火环');
				This_Player.DeleteSkill('诱惑之光');
				This_Player.DeleteSkill('地狱雷光');					
				This_Player.DeleteSkill('火球术');
				This_Player.DeleteSkill('大火球');
				This_Player.DeleteSkill('地狱火');
				This_Player.DeleteSkill('瞬息移动');
				This_Player.DeleteSkill('爆裂火焰');
				This_Player.DeleteSkill('圣言术');
				This_Player.DeleteSkill('灭天火');
				This_Player.DeleteSkill('流星火雨');
				This_Player.DeleteSkill('寒冰掌');
				This_Player.DeleteSkill('灭天火');
				This_Player.DeleteSkill('火龙气焰');
                This_Player.Give('基础技能石',1)				
                This_Player.ScriptRequestSubYBNum(GetG(108,5)); 
	            This_Player.SetS(1,1,1);       //转法师
				This_Player.SetS(1,1,14);     //杀死宝宝
	  ServerSay('转职系统：恭喜玩家[' + This_Player.Name + ']成功转职法师!', 3);
	   This_Npc.NpcDialog(This_Player,'<恭喜你：成功转职法师.请小退方可正常游戏！/fcolor=250>|'
		+'|{cmd} <返回上页/@fashi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	 		
		This_Player.SetS(1,1,16);     //踢玩家下线
end else 
	    This_Npc.NpcDialog(This_Player,'很遗憾：<你已经是伟大的/fcolor=243> 法师 <了.无法转职！>|'
		+'|{cmd} <返回上页/@fashi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	  
end else
	    This_Npc.NpcDialog(This_Player,'很遗憾：<转职法师失败.元宝不足/fcolor=243> '+inttostr(GetG(108,5))+' <无法转职>！|'
		+'|{cmd} <返回上页/@fashi> ^ <返回首页/@main>^ <关闭此页/@exit>') ;	 
end;


//==========================================================主号转职结束










Begin
	//初始化变量 	
	if GetG(108,4) <= 0 then SetG(108,4,20000); 	// 主号变性
	if GetG(108,5) <= 0 then SetG(108,5,50000);  	// 主号转职

	doMain;		
end.




















{******************************************************************
版本脚本制作：张益达 QQ：1626293287  |制作不易，手下留情，请勿删除|
*******************************************************************}

r