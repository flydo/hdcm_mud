PROGRAM Mir2;
Procedure _doexit;
begin
   This_Npc.CloseDialog(This_Player);
end;
procedure domain; 
begin
    This_NPC.NpcDialog(This_Player,
       '☆欢迎光临【寒刀沉默】装备改造☆\|'              
     +'☆在这里你可以把不同职业的装备进行改造。☆\|'
     +'☆☆也可以改变衣服性别。!☆☆\|'	 
    +'不过需要一定的元宝~！！||\'
    +'{cmd}<衣服男女改造/@yifu> \' 
    +'{cmd}<职业改造/@zhiye> \' 
	);
end;



procedure _yifu;
begin
    This_NPC.NpcDialog(This_Player,
       '☆点击下方兑换☆\|'+   
       '☆65级衣服【800 元宝】☆<女换男/@65jiLN>☆<男换女/@NHN>\|'+  
       '☆65级衣服【2000元宝】☆<女换男/@NHN>☆<男换女/@NHN>\|' 	 
	);
end;









procedure _zhiye;
begin
    This_NPC.NpcDialog(This_Player,
       '☆点击下方兑换☆\|'+   
       '☆65级套装【500 元宝】|☆<道士换/@65DH>☆<战士换/@65ZH>☆<法师换/@65FH>☆\|'+  
       '☆65级套装【2000 元宝】|☆<道士换/@65DH>☆<战士换/@65ZH>☆<法师换/@65FH>☆\|'	 
    
	);
end;


procedure _65DH;
begin
This_NPC.NpcDialog(This_Player,
       '☆65级道士装备换战法|价格：2000元宝|提示：衣服武器翻倍☆\|'+   
       '<☆戒指【道换战】☆/@65JDHZ>☆<☆戒指【道换法】☆/@65JDHF>\|'+  
       '<☆头盔【道换战】☆/@65TDHZ>☆<☆头盔【道换法】☆/@65TDHF>\|'+ 	 
       '<☆项链【道换战】☆/@65LDHZ>☆<☆项链【道换法】☆/@65LDHF>\|'+   
       '<☆手镯【道换战】☆/@65SDHZ>☆<☆手镯【道换法】☆/@65SDHF>\|'+ 
       '<☆衣服男【道换战】☆/@65YDHZ>☆<☆衣服男【道换法】☆/@65YDHF>\|'+ 
       '<☆衣服女【道换战】☆/@65YDHZ1>☆<☆衣服女【道换法】☆/@65YDHF1>\|'+
       '<☆武器【道换战】☆/@65WDHZ>☆<☆武器【道换法】☆/@65WDHF>\|' 
	);
end;

procedure _65JDHZ;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神戒(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神戒(道)' , 1);
This_Player.Give('不灭V神戒(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65JDHF;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神戒(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神戒(道)' , 1);
This_Player.Give('不灭V神戒(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;



procedure _65TDHZ;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神盔(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神盔(道)' , 1);
This_Player.Give('不灭V神盔(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65TDHF;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神盔(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神盔(道)' , 1);
This_Player.Give('不灭V神盔(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65LDHZ;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神链(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神链(道)' , 1);
This_Player.Give('不灭V神链(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65LDHF;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神链(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神链(道)' , 1);
This_Player.Give('不灭V神链(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;


procedure _65SDHZ;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神镯(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神镯(道)' , 1);
This_Player.Give('不灭V神镯(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65SDHF;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神镯(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神镯(道)' , 1);
This_Player.Give('不灭V神镯(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;


procedure _65YDHZ;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸龙甲(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸龙甲(道)' , 1);
This_Player.Give('龙皇霸龙甲(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65YDHF;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸龙甲(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸龙甲(道)' , 1);
This_Player.Give('龙皇霸龙甲(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;




procedure _65YDHZ1;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸凤铠(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸凤铠(道)' , 1);
This_Player.Give('龙皇霸凤铠战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65YDHF1;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸凤铠(道)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸凤铠(道)' , 1);
This_Player.Give('龙皇霸凤铠(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65WDHZ;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸气道剑')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸气道剑' , 1);
This_Player.Give('龙皇霸气战剑' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65WDHF;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸气道剑')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸气道剑' , 1);
This_Player.Give('龙皇霸气法剑' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;


//===========================================
procedure _65ZH;
begin
This_NPC.NpcDialog(This_Player,
       '☆65级战士装备换法道|价格：2000元宝|提示：衣服武器翻倍☆\|'+   
       '<☆戒指【战换道】☆/@65JZHD>☆<☆戒指【战换法】☆/@65JZHF>\|'+  
       '<☆头盔【战换道】☆/@65TZHD>☆<☆头盔【战换法】☆/@65TZHF>\|'+ 	 
       '<☆项链【战换道】☆/@65LZHD>☆<☆项链【战换法】☆/@65LZHF>\|'+   
       '<☆手镯【战换道】☆/@65SZHD>☆<☆手镯【战换法】☆/@65SZHZ>\|'+ 
       '<☆衣服男【战换道】☆/@65YZHD>☆<☆衣服男【战换法】☆/@65YZHF>\|'+ 
       '<☆衣服女【战换道】☆/@65YZHD1>☆<☆衣服女【战换法】☆/@65YZHF1>\|'+
       '<☆武器【战换道】☆/@65WZHD>☆<☆武器【战换法】☆/@65WZHF>\|' 
	);
end;

procedure _65JZHD;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神戒(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神戒(战)' , 1);
This_Player.Give('不灭V神戒(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65JZHF;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神戒(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神戒(战)' , 1);
This_Player.Give('不灭V神戒(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;



procedure _65TZHD;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神盔(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神盔(战)' , 1);
This_Player.Give('不灭V神盔(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65TZHF;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神盔(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神盔(战)' , 1);
This_Player.Give('不灭V神盔(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65LZHD;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神链(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神链(战)' , 1);
This_Player.Give('不灭V神链(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65LZHF;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神链(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神链(战)' , 1);
This_Player.Give('不灭V神链(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;


procedure _65SZHD;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神镯(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神镯(战)' , 1);
This_Player.Give('不灭V神镯(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65SZHF;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神镯(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神镯(战)' , 1);
This_Player.Give('不灭V神镯(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;


procedure _65YZHD;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸龙甲(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸龙甲(战)' , 1);
This_Player.Give('龙皇霸龙甲(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65YZHF;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸龙甲(战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸龙甲(战)' , 1);
This_Player.Give('龙皇霸龙甲(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;




procedure _65YZHD1;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸凤铠战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸凤铠战)' , 1);
This_Player.Give('龙皇霸凤铠(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65YZHF1;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸凤铠战)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸凤铠战)' , 1);
This_Player.Give('龙皇霸凤铠(法)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65WZHD;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸气战剑')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸气战剑' , 1);
This_Player.Give('龙皇霸气道剑' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65WZHF;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸气战剑')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸气战剑' , 1);
This_Player.Give('龙皇霸气法剑' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

//===============================
procedure _65FH;
begin
This_NPC.NpcDialog(This_Player,
       '☆65级法师装备换战道|价格：2000元宝|提示：衣服武器翻倍☆\|'+   
       '<☆戒指【法换道】☆/@65JFHD>☆<☆戒指【法换战】☆/@65JFHZ>\|'+  
       '<☆头盔【法换道】☆/@65TFHD>☆<☆头盔【法换战】☆/@65TFHZ>\|'+ 	 
       '<☆项链【法换道】☆/@65LFHD>☆<☆项链【法换战】☆/@65LFHZ>\|'+   
       '<☆手镯【法换道】☆/@65SFHD>☆<☆手镯【法换战】☆/@65SFHZ>\|'+ 
       '<☆衣服男【法换道】☆/@65YFHD>☆<☆衣服男【法换战】☆/@65YFHZ>\|'+ 
       '<☆衣服女【法换道】☆/@65YFHD1>☆<☆衣服女【法换战】☆/@65YFHZ1>\|'+
       '<☆武器【法换道】☆/@65WFHD>☆<☆武器【法换战】☆/@65WFHZ>\|' 
	);
end;

procedure _65JFHD;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神戒(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神戒(法)' , 1);
This_Player.Give('不灭V神戒(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65JFHZ;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神戒(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神戒(法)' , 1);
This_Player.Give('不灭V神戒(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;



procedure _65TFHD;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神盔(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神盔(法)' , 1);
This_Player.Give('不灭V神盔(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65TFHZ;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神盔(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神盔(法)' , 1);
This_Player.Give('不灭V神盔(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65LFHD;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神链(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神链(法)' , 1);
This_Player.Give('不灭V神链(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65LFHZ;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神链(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神链(法)' , 1);
This_Player.Give('不灭V神链(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;


procedure _65SFHD;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神镯(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神镯(法)' , 1);
This_Player.Give('不灭V神镯(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;

procedure _65SFHZ;
begin
if This_Player.YBNum >= 500 then
begin
if This_Player.GetBagItemCount('不灭V神镯(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(500);
This_Player.Take('不灭V神镯(法)' , 1);
This_Player.Give('不灭V神镯(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有2000元宝！！！  ');
end;


procedure _65YFHD;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸龙甲(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸龙甲(法)' , 1);
This_Player.Give('龙皇霸龙甲(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65YFHZ;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸龙甲(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸龙甲(法)' , 1);
This_Player.Give('龙皇霸龙甲(战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;




procedure _65YFHD1;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸凤铠(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸凤铠(法)' , 1);
This_Player.Give('龙皇霸凤铠(道)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65YFHZ1;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸凤铠(法)')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸凤铠(法)' , 1);
This_Player.Give('龙皇霸凤铠战)' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65WFHD;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸气法剑')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸气法剑' , 1);
This_Player.Give('龙皇霸气道剑' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65WFHZ;
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('龙皇霸气法剑')   >= 1 then    //检测物品
begin
This_Player.ScriptRequestSubYBNum(1000);
This_Player.Take('龙皇霸气法剑' , 1);
This_Player.Give('龙皇霸气战剑' , 1);
This_Npc.NpcDialog(This_Player,
'兑换成功！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有兑换的装备？！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'没有1000元宝！！！  ');
end;

procedure _65DH;
begin
This_NPC.NpcDialog(This_Player,
       '☆65级道士装备换战法|价格：2000元宝|提示：衣服武器翻倍☆\|'+   
       '<☆戒指【道换战】☆/@65jiLN>☆<☆戒指【道换法】☆/@65jiLN>\|'+  
       '<☆头盔【道换战】☆/@65jiLN>☆<☆头盔【道换法】☆/@65jiLN>\|'+ 	 
       '<☆项链【道换战】☆/@65jiLN>☆<☆项链【道换法】☆/@65jiLN>\|'+   
       '<☆手镯【道换战】☆/@65jiLN>☆<☆手镯【道换法】☆/@65jiLN>\|'+ 
       '<☆衣服【道换战】☆/@65jiLN>☆<☆衣服【道换法】☆/@65jiLN>\|'+ 
       '<☆武器【道换战】☆/@65jiLN>☆<☆武器【道换法】☆/@65jiLN>\|' 
	);
end;

procedure _70DH;
begin
This_NPC.NpcDialog(This_Player,
       '☆70级道士装备换战法|价格：4000元宝|提示：衣服武器翻倍☆\|'+   
       '<☆戒指【道换战】☆/@65jiLN>☆<☆戒指【道换法】☆/@65jiLN>\|'+  
       '<☆头盔【道换战】☆/@65jiLN>☆<☆头盔【道换法】☆/@65jiLN>\|'+ 	 
       '<☆项链【道换战】☆/@65jiLN>☆<☆项链【道换法】☆/@65jiLN>\|'+   
       '<☆手镯【道换战】☆/@65jiLN>☆<☆手镯【道换法】☆/@65jiLN>\|'+ 
       '<☆衣服【道换战】☆/@65jiLN>☆<☆衣服【道换法】☆/@65jiLN>\|'+ 
       '<☆武器【道换战】☆/@65jiLN>☆<☆武器【道换法】☆/@65jiLN>\|' 
	);
end;



begin
domain;
end.Z