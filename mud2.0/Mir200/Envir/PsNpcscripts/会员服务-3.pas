{********************************************************************

*******************************************************************}
program mir2;

{$I common.pas}

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

Procedure domain;
//var today , var regday,var exday,typ: integer,flg: integer;
begin
    //flg := This_Player.GetV(55,1);
    //GetV(55,1) = 1  是会员   <>1 不是会员
	   This_NPC.NpcDialog(This_Player,
	   '尊敬的传奇玩家你好，在这里我可以加入月卡会员 \ \'
	   +'月卡会员可以免费进入全部地图且不限制等级哦。\ \'
	   +'|{cmd}<购买月卡/@buy1>  \'
	   +'|{cmd}<月卡服务/@member>  '
	   );
	//end;
end;


procedure _buy1;
begin
     This_NPC.NpcDialog(This_Player,
	   '月卡会员拥有以下服务(每日)： \ \'
	   +'|1.永久在线回收.\'
	   +'|2.金条.！ \'
	   +'|3.超级地图免费传送服务 \'
	   +'|------价格：38888元宝/月 \'
	   +'|<我要购买/@buy11>  ^<我要离开/@exit>'
	   );
end;



procedure _buy11;
begin
	if This_Player.GetV(55,1) <> 1 then
	   begin
	   This_NPC.NpcDialog(This_Player,
		 '你是否确定购买月卡会员？这需要38888个元宝 \ \'
		 +' \ \'
		 +'|{cmd}<购买月卡会员/@buy111>     ^<点错/@exit>'
		 );
	   end
	else
	   This_NPC.NpcDialog(This_Player,
		 '你已经是月卡会员了哦 \ \'
		 +' \ \'
		 +'|{cmd} ^<离开/@exit>'
		 );
end;

procedure _buy111;
var
d1 : integer;
begin
	 d1 :=  GetDateNum(GetNow);
     begin
          if This_Player.YBNum >= 38888 then
			begin
             This_Player.ScriptRequestSubYBNum(38888);
			 This_Player.Give('远程服务', 1);
             This_Player.SetV(55,1,1);
			 This_Player.SetV(55,2,d1);
			 This_Player.SetV(55,3,0);
			 This_Player.PlayerDialog('你已经成功成为月卡会员，有效期为30天');
			end else
            This_Player.PlayerDialog('身上没有38888个元宝。');
     end ;
end;


procedure _member;
begin
	 This_NPC.NpcDialog(This_Player,
	   '您需要什么服务？ \ \'
	   +'|说明： \'
	   +'|<进入月卡服务/@member1>  '
	   +'|<离开/@exit>'
	 );
end;

procedure _member1;
begin
	 if This_Player.GetV(55,1) <> 1 then
	    begin
		    This_Player.SetV(55,1,0);
			This_Player.SetV(55,2,0);
			This_Player.SetV(55,3,0);
			This_NPC.NpcDialog(This_Player,
			   '您不是月卡会员！！ \ \'
			 );
		end
	else
		begin
			 if GetDateNum(GetNow) - This_Player.GetV(55,2) < 31 then
					begin
						 This_NPC.NpcDialog(This_Player,
						   '您需要什么服务？ \ \' 
	                       +'|<领取每日礼包/@getyueka>  ^<免费传送服务/@vipGo>'
						   +'|<返回/@member>'
						 );
					end
			else
					begin
					This_Player.SetV(55,1,0);
					This_Player.SetV(55,2,0);
					This_Player.SetV(55,3,0);
					This_Player.PlayerDialog('你的月卡会员时间已经到期');
					end;
		end;

end;

