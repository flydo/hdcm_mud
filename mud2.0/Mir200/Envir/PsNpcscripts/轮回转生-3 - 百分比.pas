{********************************************************************

*******************************************************************}
program mir2;
var
shouci, shuaxin: Boolean;
xiangcha: integer;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _1;
var dengji : Byte; 
begin
if This_Player.Level >= 60 then
begin
if This_Player.GetV(42,1) <= 0 then
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('一转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('一转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(1000); 
This_Player.SetV(42,1,1);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有一转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经一转了！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;

procedure _2;
var dengji : Byte; 
begin
if This_Player.Level >= 65 then
begin
if This_Player.GetV(42,1) = 1 then
begin
if This_Player.YBNum >= 2000 then
begin
if This_Player.GetBagItemCount('二转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('二转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(2000); 
This_Player.SetV(42,1,2);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有二转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经二转了，或者还没一转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;


procedure _3;
var dengji : Byte; 
begin
if This_Player.Level >= 70 then
begin
if This_Player.GetV(42,1) = 2 then
begin
if This_Player.YBNum >= 5000 then
begin
if This_Player.GetBagItemCount('三转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('三转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(5000); 
This_Player.SetV(42,1,3);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有三转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经三转了，或者还没二转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;


procedure _4;
var dengji : Byte; 
begin
if This_Player.Level >= 75 then
begin
if This_Player.GetV(42,1) = 3 then
begin
if This_Player.YBNum >= 8000 then
begin
if This_Player.GetBagItemCount('四转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('四转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(8000); 
This_Player.SetV(42,1,4);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有四转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经四转了，或者还没三转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;

procedure _5;
var dengji : Byte; 
begin
if This_Player.Level >= 80 then
begin
if This_Player.GetV(42,1) = 4 then
begin
if This_Player.YBNum >= 12000 then
begin
if This_Player.GetBagItemCount('五转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('五转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(12000); 
This_Player.SetV(42,1,5);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有五转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经五转了，或者还没四转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;


procedure _6;
var dengji : Byte; 
begin
if This_Player.Level >= 80 then
begin
if This_Player.GetV(42,1) = 5 then
begin
if This_Player.YBNum >= 20000 then
begin
if This_Player.GetBagItemCount('六转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('六转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(20000); 
This_Player.SetV(42,1,6);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有六转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经六转了，或者还没五转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;


procedure _7;
var dengji : Byte; 
begin
if This_Player.Level >= 85 then
begin
if This_Player.GetV(42,1) = 6 then
begin
if This_Player.YBNum >= 30000 then
begin
if This_Player.GetBagItemCount('七转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('七转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(30000); 
This_Player.SetV(42,1,7);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有七转转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经七转了，或者还没六转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;



procedure _8;
var dengji : Byte; 
begin
if This_Player.Level >= 90 then
begin
if This_Player.GetV(42,1) = 7 then
begin
if This_Player.YBNum >= 60000 then
begin
if This_Player.GetBagItemCount('八转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('八转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(60000); 
This_Player.SetV(42,1,8);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有八转转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经八转了，或者还没七转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;

procedure _9;
var dengji : Byte; 
begin
if This_Player.Level >= 100 then
begin
if This_Player.GetV(42,1) = 8 then
begin
if This_Player.YBNum >= 80000 then
begin
if This_Player.GetBagItemCount('九转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('九转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(80000); 
This_Player.SetV(42,1,9);
ServerSay('玩家<' + This_Player.Name + '>，经历千辛万苦完成了九转，大家恭喜他！！ ', 3);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有九转转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经九转了，或者还没八转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;

procedure _10;
var dengji : Byte; 
begin
if This_Player.Level >= 120 then
begin
if This_Player.GetV(42,1) = 9 then
begin
if This_Player.YBNum >= 100000 then
begin
if This_Player.GetBagItemCount('十转至尊令') >= 1 then
begin
dengji := This_Player.Level - 15;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('十转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(100000); 
This_Player.SetV(42,1,10);
ServerSay('玩家<' + This_Player.Name + '>，经历千辛万苦完成了十转大圆满，大家恭喜他！！ ', 3);
ServerSay('玩家<' + This_Player.Name + '>，经历千辛万苦完成了十转大圆满，大家恭喜他！！ ', 2);
ServerSay('玩家<' + This_Player.Name + '>，经历千辛万苦完成了十转大圆满，大家恭喜他！！ ', 1);
ServerSay('玩家<' + This_Player.Name + '>，经历千辛万苦完成了十转大圆满，大家恭喜他！！ ', 4);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有十转转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经十转了，或者还没九转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;

procedure _zhuansheng;
var zsdj : Integer; 
begin
   zsdj := This_Player.GetV(42,1);
     This_Npc.NpcDialog(This_Player,
       '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'+
   '|<一转需求:/c=red>一转至尊令+60级+1000元宝。转生后掉落5级。<我要1转/@1>\'+
   '|<二转需求:/c=red>二转至尊令+65级+2000元宝。转生后掉落5级。<我要2转/@2>\'+
   '|<三转需求:/c=red>三转至尊令+70级+5000元宝。转生后掉落5级。<我要3转/@3>\'+
   '|<四转需求:/c=red>四转至尊令+75级+8000元宝。转生后掉落5级。<我要4转/@4>\'+
   '|<五转需求:/c=red>五转至尊令+80级+12000元宝。转生后掉落5级。<我要5转/@5>\'+
   '|<六转需求:/c=red>六转至尊令+80级+20000元宝。转生后掉落5级。<我要6转/@6>\'+
   '|                                                  <下一页/@zhuansheng2>\'+
   '|<　　　　　　　　　　　　　　　　　　　　　/c=red>\'
    ); 
end;



procedure _zhuansheng2;
var zsdj : Integer; 
begin
   zsdj := This_Player.GetV(42,1);
     This_Npc.NpcDialog(This_Player,
       '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'+
   '|<七转需求:/c=red>七转至尊令+85级+30000元宝。转生后掉落5级。<我要7转/@7>\'+
   '|<八转需求:/c=red>八转至尊令+90级+60000元宝。转生后掉落5级。<我要8转/@8>\'+
   '|<九转需求:/c=red>九转至尊令+100级+80000元宝。转生后掉落5级。<我要9转/@9>\'+
   '|<十转需求:/c=red>十转至尊令+120级+100000元宝。转生后掉落15级。<我要10转/@10>\'+
   '|                                                    <上一页/@zhuansheng>\'+
   '|<　　　　　　　　　　　　　　　　　　　　　/c=red>\'
    ); 
end;






procedure _shenli;
var zsdj : Integer; 
begin
   zsdj := This_Player.GetV(42,1);
   if This_Player.GetV(42,1) >= 1 then
   begin
   xiangcha := minusDataTime(GetNow,
    ConvertDBToDateTime(This_Player.GetS(12, 11)));
  if (This_Player.GetS(12, 11) = -1) or (xiangcha >= 7199) then
  begin
    shouci := False;
    This_Npc.NpcDialog(This_Player,
       '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'+
   '|<在我这里可以获得转生神力加持！/c=125>\'+
   '|<属性加成：15+【转生等级*5】%（战士额外多10%）/c=125>\'+
   '|<战士额外加成：35+【转生等级*8】的物防。/c=125>\'+
   '|<持续时间4小时，每隔2个小时，可在这里再次加持!/c=80>\'+
   '|<开启神力/@shenli2>　　　　　　　　　　　　\'
   ); 
  end
  else
    This_Npc.NpcDialog(This_Player, '必须' + inttostr(7199 - xiangcha) +
      '秒后才可以再次开启神力！');
 
	 end else
              This_Player.PlayerDialog(
              '你还没转生！\');  
end;


procedure _shenli2;
begin
 This_Player.AddPlayerAbil(0, 3000, 1);
   This_Player.AddPlayerAbil(1, 3000, 1);
   This_Player.AddPlayerAbil(2, 3000, 1);
              This_Player.PlayerDialog(
              '八秒后激活神力属性，请稍等！请勿关闭此窗口！\');  
This_Player.CallOut(This_Npc, 8, 'shenli3');
 end;




procedure shenli3;
var shuxin,shuxin2,shuxin3,zsdj,fangyu,fangyu1 : integer;
begin
   This_Player.SetS(12, 11, ConvertDateTimeToDB(GetNow));
   zsdj := This_Player.GetV(42,1);
  if This_Player.Job = 0 then 
   begin
    shuxin2 := zsdj * 5
	fangyu := zsdj * 8	
	fangyu1 := fangyu + 65	
	shuxin3 := shuxin2 + 25
    shuxin := This_Player.MaxDC * shuxin3 / 100;
	This_Player.AddPlayerAbil(This_Player.Job, shuxin, 14400);
	This_Player.AddPlayerAbil(8, fangyu1, 14400);
	 //This_Player.AddPlayerAbil(9, fangyu, 7199);
     ServerSay('玩家<' + This_Player.Name + '>开启了转生神力，攻击增加了' +
      inttostr(shuxin) + '点，顿时杀气冲天而起 ！ ', 5);
This_Player.PlayerDialog(
              '附加成功！！\'); 
	  
			  
   end;
   
   if This_Player.Job = 1 then 
   begin
    shuxin2 := zsdj * 5
	shuxin3 := shuxin2 + 15
    shuxin := This_Player.MaxMC * shuxin3 / 100;
	This_Player.AddPlayerAbil(This_Player.Job, shuxin, 14400);
	ServerSay('玩家<' + This_Player.Name + '>开启了转生神力，法力增加了' +
      inttostr(shuxin) + '点，顿时杀气冲天而起 ！ ', 5);
	  This_Player.PlayerDialog(
              '附加成功！！\'); 
   end;
   
   
   if This_Player.Job = 2 then 
   begin
    shuxin2 := zsdj * 5
	shuxin3 := shuxin2 + 15
    shuxin := This_Player.MaxSC * shuxin3 / 100;
	This_Player.AddPlayerAbil(This_Player.Job, shuxin, 14400);
	ServerSay('玩家<' + This_Player.Name + '>开启了转生神力，道术增加了' +
      inttostr(shuxin) + '点，顿时杀气冲天而起 ！ ', 5);
	  This_Player.PlayerDialog(
              '附加成功！！\'); 
   end;
   
            
end;


function shuxingzs(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 15;
        2 : result := 30;
        3 : result := 45; 
        4 : result := 70;
        5 : result := 90; 
        6 : result := 110;
		7 : result := 140;
        8 : result := 170; 
        9 : result := 200;
		10 : result := 240;

    end;
end;

function shuxingfs(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 15;
        2 : result := 25;
        3 : result := 35; 
        4 : result := 50;
        5 : result := 65; 
        6 : result := 80;
		7 : result := 100;
        8 : result := 120; 
        9 : result := 140;
		10 : result := 170;

    end;
end;

function shuxingds(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 15;
        2 : result := 30;
        3 : result := 45; 
        4 : result := 70;
        5 : result := 90; 
        6 : result := 110;
		7 : result := 140;
        8 : result := 170; 
        9 : result := 200;
		10 : result := 240;

    end;
end;





procedure _cessss1; 
var zsdj : integer;
begin
zsdj := This_Player.GetV(42,1);
 This_Player.AddPlayerAbil(0, shuxingzs(zsdj), 10);
end ;

procedure _hecheng1; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 30 then    //检测物品
begin
if This_Player.GetV(42,1) >= 1 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 30);
This_Player.Give('一转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【一转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*30不足！！！  ');
end ;
end ;



procedure _hecheng2; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 60 then    //检测物品
begin
if This_Player.GetBagItemCount('一转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 2 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 60);
This_Player.Take('一转轮回靴' , 1);
This_Player.Give('二转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【二转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有一转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*60不足！！！  ');
end ;
end ;


procedure _hecheng3; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 120 then    //检测物品
begin
if This_Player.GetBagItemCount('二转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 3 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 120);
This_Player.Take('二转轮回靴' , 1);
This_Player.Give('三转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【三转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有二转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*120不足！！！  ');
end ;
end ;


procedure _hecheng4; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 200 then    //检测物品
begin
if This_Player.GetBagItemCount('三转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 4 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 200);
This_Player.Take('三转轮回靴' , 1);
This_Player.Give('四转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【四转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有三转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*200不足！！！  ');
end ;
end ;


procedure _hecheng5; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 300 then    //检测物品
begin
if This_Player.GetBagItemCount('四转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 5 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 300);
This_Player.Take('四转轮回靴' , 1);
This_Player.Give('五转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【五转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有四转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*300不足！！！  ');
end ;
end ;


procedure _hecheng6; 
begin
if This_Player.YBNum >= 10000 then
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 450 then    //检测物品
begin
if This_Player.GetBagItemCount('五转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 6 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 450);
This_Player.ScriptRequestSubYBNum(10000); 
This_Player.Take('五转轮回靴' , 1);
This_Player.Give('六转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【六转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有五转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*450不足！！！  ');
end ;
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end ;



procedure _hecheng7; 
begin
if This_Player.YBNum >= 20000 then
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 600 then    //检测物品
begin
if This_Player.GetBagItemCount('六转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 7 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 600);
This_Player.ScriptRequestSubYBNum(20000); 
This_Player.Take('六转轮回靴' , 1);
This_Player.Give('七转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【七转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有六转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*600不足！！！  ');
end ;
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end ;

procedure _hecheng8; 
begin
if This_Player.YBNum >= 30000 then
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 900 then    //检测物品
begin
if This_Player.GetBagItemCount('七转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 8 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 900);
This_Player.ScriptRequestSubYBNum(30000); 
This_Player.Take('七转轮回靴' , 1);
This_Player.Give('八转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【八转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有七转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*900不足！！！  ');
end ;
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end ;


procedure _hecheng9; 
begin
if This_Player.YBNum >= 50000 then
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 1300 then    //检测物品
begin
if This_Player.GetBagItemCount('八转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 9 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 1300);
This_Player.ScriptRequestSubYBNum(50000); 
This_Player.Take('八转轮回靴' , 1);
This_Player.Give('九转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【九转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有八转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*1300不足！！！  ');
end ;
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end ;

procedure _hecheng;
var zsdj : Integer; 
begin
   zsdj := This_Player.GetV(42,1);
   if This_Player.GetV(42,1) >= 1 then
   begin
     This_Npc.NpcDialog(This_Player,
       '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'+
   '|<在我这里可以合成轮回靴！不同轮回靴需要不同的转生等级！/c=150>\'+
   '|<合成一转轮回靴/@hecheng1><：30个轮回碎片/c=150>\'+
   '|<合成二转轮回靴/@hecheng2><：60个轮回碎片+一转轮回靴/c=150>\'+
   '|<合成三转轮回靴/@hecheng3><：120个轮回碎片+二转轮回靴/c=150>\'+
   '|<合成四转轮回靴/@hecheng4><：200个轮回碎片+三转轮回靴/c=150>\'+
   '|<合成五转轮回靴/@hecheng5><：300个轮回碎片+四转轮回靴/c=150>\'+
   '|<合成六转轮回靴/@hecheng6><：450个轮回碎片+五转轮回靴+1W元宝/c=150>\'+
   '|<合成七转轮回靴/@hecheng7><：600个轮回碎片+六转轮回靴+2W元宝/c=150>\'+
   '|<合成八转轮回靴/@hecheng8><：900个轮回碎片+七转轮回靴+3W元宝/c=150>\'+
   '|<合成九转轮回靴/@hecheng9><：1300个轮回碎片+八转轮回靴+5W元宝/c=150>\'
    ); 
	 end else
              This_Player.PlayerDialog(
              '你还没转生！\');  
end;


procedure _jiuzhuanlp; 
begin
if This_Player.GetBagItemCount('九转碎片之一')   >= 1 then    //检测物品
begin
if This_Player.GetBagItemCount('九转碎片之二')   >= 1 then    //检测物品
begin
if This_Player.GetBagItemCount('九转碎片之三')   >= 1 then    //检测物品
begin
This_Player.Take('九转碎片之一' , 1);
This_Player.Take('九转碎片之二' , 1);
This_Player.Take('九转碎片之三' , 1);
This_Player.Give('九转至尊令' , 1);  //给与物品
This_Npc.NpcDialog(This_Player,
'合成成功！  ');
end else
This_Npc.NpcDialog(This_Player,
'碎片不足！  ');
end else
This_Npc.NpcDialog(This_Player,
'碎片不足！  ');
end else
This_Npc.NpcDialog(This_Player,
'碎片不足！  ');
end;






var zsdj : Integer; 
begin 
zsdj := This_Player.GetV(42,1);
   This_NPC.NpcDialog(This_Player,
   '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'
	   +'|<【转生介绍】:/c=red>转生后，可以领取强大的属性附加.\'
	   +'|<【转生介绍】:/c=red>可以进入专属的转生地图，转生地图暴率更高！\'
	   +'|<【轮回靴介绍】:/c=red>转生后，可以获得专属轮回靴，获得强大的属性！！\'
	   +'|<【九转令牌】:/c=red>需要九转碎片之一，之二，之三合成！\'
	   +'|<我要转生/@zhuansheng>  ^<转生神力/@shenli> ^<轮回靴合成/@hecheng>\'
	   +'|<合成九转令牌/@jiuzhuanlp>   <ceshi/@cessss1> \'
   );
end.    
