{
*******************************************************************}

program Mir2;

 

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

function getMonNameByid(Mid : integer) : string;
begin
    case Mid of
        1 : result := '狂暴麒麟';
        2 : result := '暴雷虎王';
		

        else  result := '';
    end;
end;

function getMonTrueNameByid(Mid : integer) : string;
begin
    case Mid of
        1 : result := '狂暴麒麟';
        2 : result := '暴雷虎王';
       

        else  result := '';
    end;
end;

function getMonLvbyId(Mid : integer) : integer;
begin
    case Mid of
        1 : result := 55;
        2 : result := 65;
       

        else  result := 0;

    end;
end; 

function getMonGoldbyId(Mid : integer) : integer;
begin
    case Mid of
        1 : result := 1;
        2 : result := 1;
      

        else  result := 0;

    end;
end; 

function getMonMaxNumbyId(Mid : integer) : integer;
begin
    case Mid of
        1 : result := 0;
        2 : result := 0;
      

        else  result := -1;
    end;
end; 

function getMonNextTime(Mid : integer) : integer;
begin
    case Mid of 
        1 : result := 0;
        2 : result := 0;
       

        else  result := -1;
    end;

end;
procedure setgTask(Ntime : integer);
var intG , i:integer; 
begin
    
    if GetG(15,1) <> Ntime then
    begin
        SetG(15,1,Ntime);
        for i := 2 to 5 do
        begin
            if GetG(15,i) < 0 then
            SetG(15,i,0);
            intG := GetG(15,i);
            SetG(15,i, intG + 1);    
        end; 
    end;
end;
//GetG(15,1...20 使用  
function getMonNum(Mid , decNum : integer) : integer;
var needG , intG , Mnum : integer; 
begin
    
    if getMonNextTime(Mid) > 0 then
    begin
        case Mid of
            7 : needG := 2;
            8 : needG := 3;
            9 : needG := 4;
            12 : needG := 5;
        end;
        intG := GetG(15,needG);  //每分钟+1  除以时间间隔为可刷新数量 
        
        Mnum := intG div getMonNextTime(Mid);
        if Mnum > getMonMaxNumbyId(Mid) then
        begin
         
           SetG(15,needG, getMonMaxNumbyId(Mid) * getMonNextTime(Mid));   
           result := getMonMaxNumbyId(Mid);
        end else
        result := Mnum;
        
        if decNum > 0 then
        begin
           intG := GetG(15,needG); 
           SetG(15,needG, intG - getMonNextTime(Mid));
        end;
    end else
    result := 0;

end;

procedure _GoldCallmob();
var Monstr : string; 
i : integer;
begin
    Monstr := '';
    for i:=1 to 2 do
    begin
        Monstr := Monstr + '<' + getMonNameByid(i) + '/@MonDialog~' + inttostr(i) + '>        ';
        case i of
            3,6,9,12  : Monstr := Monstr + '|\'; 
        end;
        
         
    end;
    This_Npc.NpcDialog(This_Player,          
    '请选择你要召唤的兽王！|\{cmd}'
    + Monstr 
    +'<返回/@main>');

end;

function getMonNumStr(Mid : integer) : string;
begin
    if getMonMaxNumbyId(Mid) = 0 then
    result := '还有很多'
    else
    result := inttostr(getMonNum(Mid , 0)); 
end;


procedure _MonDialog(MidStr : string);
var Mid : integer;
begin
    Mid := StrToIntDef(MidStr,0);
     
    This_Npc.NpcDialog(This_Player, 
   // '你已召唤怪物：' + inttostr(This_Player.RegisterMethod('')) + '/5:\|'     
    +'召唤怪物：' + getMonNameByid(Mid) + '\|'
    +'召唤等级：' + inttostr(getMonLvbyId(Mid)) + '级\|' 
    +'剩余数量：' + getMonNumStr(Mid) + '\|'  
    +'{cmd}<我要召唤/@CallmobNow~' + MidStr + '>');
end;

procedure CallMobTrue(Mid : integer);
var needGold : integer;
MName : string;
begin
    needGold := getMonGoldbyId(Mid);
    MName := getMonNameByid(Mid);
    if This_Player.GoldNum >= needGold then
    begin
        This_Player.DecGold(needGold);
        This_Player.MakeSlaveEx(getMonTrueNameByid(Mid) , 1 ,0);
        getMonNum(Mid , 1)
         This_Npc.NpcDialog(This_Player,          
        MName + '我已帮你召唤了，还有什么事吗？|\'
        +'{cmd}<继续召唤' + MName + '/@CallmobNow~' + inttostr(Mid) + '>      ^<返回/@GoldCallmob>');  
    end else
    This_Npc.NpcDialog(This_Player, 
    '没钱我也没办法！');
end; 

procedure _CallmobNow(MidStr : string);
var Mid : integer;
begin
    Mid := StrToIntDef(MidStr,0);
    
    if This_Player.Job = 2 then
    begin
        if This_Player.Level >= getMonLvbyId(Mid) then
        begin 
            if This_Player.GetSlaveCount('') < 1 then
            begin
                if getMonMaxNumbyId(Mid) < 0 then
                exit
                else if getMonMaxNumbyId(Mid) = 0 then
                CallMobTrue(Mid)
                else if getMonNum(Mid , 0) > 0 then
                CallMobTrue(Mid) else
                This_Npc.NpcDialog(This_Player,          
                '我已体力不支，无法召唤' + getMonNameByid(Mid) + '了！');
            end else 
            This_Npc.NpcDialog(This_Player,          
            '每人最多只能召唤1个怪物！');
        end else
        This_Npc.NpcDialog(This_Player,          
        '请修炼到' + inttostr(getMonLvbyId(Mid)) + '级再尝试召唤' + getMonNameByid(Mid)  + '吧！');  
    end else
    This_Npc.NpcDialog(This_Player,          
    '道士才可以在我这里召唤怪物！');
end;

procedure Execute;
var nowTime : integer;
begin
    nowTime := (GetHour * 100) + GetMin;
    setgTask(nowTime);
end;

procedure _suishen;
begin
     This_NPC.NpcDialog(This_Player,
	   '<兽皇远程召唤：/c=red> \ \'
	   +'|可以随身随地召唤兽王。\'
	   +'|------价格：<1500元宝/c=blue> \'
   	   +'|限制：道士才能使用哟，别的职业请勿购买！ \'
	   +'|<我要购买/@goumai>  ^<我要离开/@exit>'
	   );
end;

procedure _goumai;
begin
   if This_Player.YBNum >= 1500 then
   begin
   This_Player.ScriptRequestSubYBNum(1500);
    This_Player.Give('兽皇远程召唤', 1);
	This_Player.PlayerDialog('购买成功！！！');
   end else
            This_Player.PlayerDialog('身上没有1500个元宝。');
end;


begin

    This_Npc.NpcDialog(This_Player,          
    '我是兽尊，如果你是道士，等级到达一定要求，可以在我这里召唤特殊BB！\'
    +'|{cmd}<召唤兽王/@GoldCallmob>  <购买随身召唤/@suishen>');
  //  _MonDialog('2');
end.