{元宝抽奖
作者：开心就好
内容：全服每天限制为300次，个人一天只能抽20次，每抽奖一次需要1元宝
}



PROGRAM Mir2;

{$I common.pas}
var
today:integer; 
num:integer;
Snum :integer; 
Snum0 :string ;
num0 :string ;
Procedure _doexit;
begin
   This_Npc.CloseDialog(This_Player);
end;

procedure domain;
var BDYB : Integer;
begin
    BDYB := This_Player.GetV(20,9); //获取元宝数量
    Snum0 :=inttostr(5000-Snum);//全服剩余抽奖次数
    num0 :=inttostr(200-Snum);//个人剩余抽奖次数
    This_NPC.NpcDialog(This_Player,
       '☆☆☆欢迎光临【寒刀沉默】抽奖中心☆☆☆\|'
      +'                   '+'每抽奖一次需要<50元宝/c=red>！|\'
	+'            '+'全服每天限制为5000次'+' '+'<还剩/C=red>'+snum0+'<个/C=red>\|'
    +'            '+'个人一天只能抽200次'+' '+'<还剩/C=red>'+num0+'<个/C=red>\|'
     +'当前绑定元宝为：' + inttostr(BDYB) + '\|'
     +' ☆☆奖品绝对物有所值 心动不如行动哦☆☆\|'
     +'               '+'{cmd}<我要抽奖/@cj>\'); //^<我要抽奖【绑定元宝】/@cjb>\');
end;

procedure _cj;
var 
Rdm_int : integer; 
WpName : string;

begin
    today := GetDateNum(GetNow);
    if This_Player.GetV(63,3) <> today then
    begin
       This_Player.SetV(63,3,today);
       This_Player.SetV(63,4,0);
    end;

   if GetG(65,1) <> today then
   begin
       SetG(65,1,today);
       SetG(65,2,0);
   end;
   Snum := GetG(65,2)
   num := This_Player.GetV(63,4);
   
    if Snum < 5000 then//可以更改全服抽奖次数上限，上面的显示项目最好同时修改
    begin 
        if num < 100 then//更改个人抽奖次数上限
begin
     Rdm_int := random(100);
     if This_Player.YBNum >= 50 then   
     begin 
       if This_Player.FreeBagNum >= 2 then  
       begin
	   if Rdm_int < 1 then  
                begin
                case random(250) of

					1 :WpName := '会员兑换证';
					2: WpName := '辉煌炽火法剑';
                    3 :WpName := '辉煌炽火道剑';
					4: WpName := '辉煌炽火战剑';
					5: WpName := '灭世天神掌';
					6: WpName := '1000元宝';
					7: WpName := '1000元宝';
					
                end;
            end else if Rdm_int < 10 then  
                begin
                case random(35) of
                    0 : WpName := '强化麻痹戒指';
                    1 : WpName := '强化复活戒指';
                    2 : WpName := '强化护身戒指';
					4 : WpName := '噬血术';
                    5 : WpName := '逐日剑法';
                    6 : WpName := '流星火雨';
					7 : WpName := '1000W脑白金';
					8 : WpName := '1000W脑白金';
					9 : WpName := '1000W脑白金';
					10 : WpName := '100元宝';
					
                end;
				end else if Rdm_int < 20 then  
                begin
                case random(30) of
                    0 : WpName := '皮爪';
                    1 : WpName := '钢爪';
                    2 : WpName := '战神之刃';
					4 : WpName := '摄魂之剑';
                    5 : WpName := '仙人蒲扇';
                    6 : WpName := '铁骑战甲(男)';
					7 : WpName := '铁骑战甲(女)';
                    8 : WpName := '猩猩魔袍(男)';
                    9 : WpName := '猩猩魔袍(女)';
					10: WpName := '麒麟道衣(男)';
					11 :WpName := '麒麟道衣(女)';
					12 :WpName := '麻痹戒指';
					13 :WpName := '麻痹戒指';
					14 :WpName := '500W脑白金';
					15 :WpName := '千年暖玉';
					16 :WpName := '万年玄冰';
					17 : WpName := '护身戒指';
					18 : WpName := '护身戒指';
					19 : WpName := '护身戒指';
					20 : WpName := '复活戒指';
					21 : WpName := '复活戒指';
					22 : WpName := '复活戒指';
					23 : WpName := '麻痹戒指';
					24 : WpName := '麻痹戒指';
					25 : WpName := '复活戒指';
					26 : WpName := '复活戒指';
					27 : WpName := '50W脑白金';
					28 : WpName := '50W脑白金';
					29 : WpName := '50W脑白金';
					30 : WpName := '50W脑白金';
					
                end;
            end else if Rdm_int < 40 then 
                begin
                case random(20) of
                    0 : WpName := '麻痹戒指';
                    1 : WpName := '护身戒指';
                    2 : WpName := '麻痹戒指';
					3 : WpName := '麻痹戒指';
                    4 : WpName := '复活戒指';
                    5 : WpName := '复活戒指';
                    6 : WpName := '复活戒指';
					7 : WpName := '100W脑白金';
					8 : WpName := '100W脑白金';
					9 : WpName := '100W脑白金';
					10 : WpName := '真龙天骨';
					11 : WpName := '白虎巨牙';
					12 : WpName := '复活戒指';
					13 : WpName := '复活戒指';
					14 : WpName := '复活戒指';
					15 : WpName := '护身戒指';
					16 : WpName := '护身戒指';
					17 : WpName := '麻痹戒指';
					18 : WpName := '麻痹戒指';
					19 : WpName := '麻痹戒指';
					20 : WpName := '麻痹戒指';
                end;
            end else if Rdm_int < 100 then 
                begin
                case random(6) of

					0 : WpName := '10W脑白金';
                    1 : WpName := '50W脑白金';
                    2 : WpName := '50W脑白金';
					3 : WpName := '10W脑白金';
					4 : WpName := '修炼石';
					5 : WpName := '天外飞石';
					6 : WpName := '死亡坟章';
                end;
       end;
            
            This_Player.PsYBConsum(This_NPC,'xin',20001,50,1);//1为扣除元宝个数
            This_Player.Give(WpName , 1);
			 setG(65,2,Snum + 1);
           This_Player.setV(63,4, num + 1);
            This_NPC.NpcDialog(This_Player,
            WpName + '已放入您的包裹!\|'
            +'{cmd}<继续使用50元宝抽取/@cj>');
        end else
        This_NPC.NpcDialog(This_Player,
        '没有足够的包裹空间!\|'
        +'{cmd}<返回/@main>');
         end else
         This_NPC.NpcDialog(This_Player,
        '没有足够的元宝，不可抽取\|'
        +'{cmd}<返回/@main>');
		end else
        This_NPC.NpcDialog(This_Player,'你今天已抽奖30次！');
    end else
    This_NPC.NpcDialog(This_Player,'今日服务器800次限额抽奖已经抽完！');
end;



procedure _cjb;
var 
Rdm_int : integer; 
WpName : string;
BDYB : Integer; 
begin
    today := GetDateNum(GetNow);
    if This_Player.GetV(63,3) <> today then
    begin
       This_Player.SetV(63,3,today);
       This_Player.SetV(63,4,0);
    end;

   if GetG(65,1) <> today then
   begin
       SetG(65,1,today);
       SetG(65,2,0);
   end;
   Snum := GetG(65,2)
   num := This_Player.GetV(63,4);
   
    if Snum < 800 then//可以更改全服抽奖次数上限，上面的显示项目最好同时修改
    begin 
        if num < 30 then//更改个人抽奖次数上限
begin
     Rdm_int := random(100);
     if This_Player.GetV(20,9) >= 50 then   //判断绑定元宝
     begin 
       if This_Player.FreeBagNum >= 2 then  
       begin
	   if Rdm_int < 1 then  
                begin
                case random(250) of

					1 :WpName := '会员兑换证';
					2: WpName := '龙皇霸气甲(男)';
                    3 :WpName := '龙皇霸气甲(女)';
					4: WpName := '不灭V紫神甲(男)';
					5: WpName := '不灭V紫神甲(女)';
					6: WpName := '1000元宝';
					7: WpName := '1000元宝';
					
                end;
            end else if Rdm_int < 10 then  
                begin
                case random(35) of
                    0 : WpName := '强化麻痹戒指';
                    1 : WpName := '强化复活戒指';
                    2 : WpName := '强化护身戒指';
					4 : WpName := '噬血术';
                    5 : WpName := '逐日剑法';
                    6 : WpName := '流星火雨';
					7 : WpName := '1000W脑白金';
					8 : WpName := '1000W脑白金';
					9 : WpName := '1000W脑白金';
					10 : WpName := '100元宝';
					
                end;
				end else if Rdm_int < 20 then  
                begin
                case random(30) of
                    0 : WpName := '皮爪';
                    1 : WpName := '钢爪';
                    2 : WpName := '战神之刃';
					4 : WpName := '摄魂之剑';
                    5 : WpName := '仙人蒲扇';
                    6 : WpName := '铁骑战甲(男)';
					7 : WpName := '铁骑战甲(女)';
                    8 : WpName := '猩猩魔袍(男)';
                    9 : WpName := '猩猩魔袍(女)';
					10: WpName := '麒麟道衣(男)';
					11 :WpName := '麒麟道衣(女)';
					12 :WpName := '麻痹戒指';
					13 :WpName := '麻痹戒指';
					14 :WpName := '500W脑白金';
					15 :WpName := '千年暖玉';
					16 :WpName := '万年玄冰';
					17 : WpName := '护身戒指';
					18 : WpName := '护身戒指';
					19 : WpName := '护身戒指';
					20 : WpName := '复活戒指';
					21 : WpName := '复活戒指';
					22 : WpName := '复活戒指';
					23 : WpName := '50W脑白金';
					24 : WpName := '50W脑白金';
					25 : WpName := '50W脑白金';
					26 : WpName := '50W脑白金';
					27 : WpName := '50W脑白金';
					28 : WpName := '50W脑白金';
					29 : WpName := '50W脑白金';
					30 : WpName := '50W脑白金';
					
                end;
            end else if Rdm_int < 40 then 
                begin
                case random(20) of
                    0 : WpName := '麻痹戒指';
                    1 : WpName := '护身戒指';
                    2 : WpName := '青铜腰带';
					3 : WpName := '钢铁腰带';
                    4 : WpName := '布鞋';
                    5 : WpName := '鹿皮靴';
                    6 : WpName := '紫绸靴';
					7 : WpName := '100W脑白金';
					8 : WpName := '100W脑白金';
					9 : WpName := '100W脑白金';
					10 : WpName := '真龙天骨';
					11 : WpName := '白虎巨牙';
					12 : WpName := '复活戒指';
					13 : WpName := '复活戒指';
					14 : WpName := '复活戒指';
					15 : WpName := '护身戒指';
					16 : WpName := '护身戒指';
					17 : WpName := '麻痹戒指';
					18 : WpName := '麻痹戒指';
					19 : WpName := '麻痹戒指';
					20 : WpName := '50W脑白金';
                end;
            end else if Rdm_int < 100 then 
                begin
                case random(6) of

					0 : WpName := '10W脑白金';
                    1 : WpName := '50W脑白金';
                    2 : WpName := '50W脑白金';
					3 : WpName := '10W脑白金';
					4 : WpName := '修炼石';
					5 : WpName := '天外飞石';
					6 : WpName := '死亡坟章';
                end;
       end;
            
            BDYB := This_Player.GetV(20,9); //获取元宝数量
            This_Player.SetV(20,9,BDYB - 50);
            This_Player.Give(WpName , 1);
			 setG(65,2,Snum + 1);
           This_Player.setV(63,4, num + 1);
            This_NPC.NpcDialog(This_Player,
            WpName + '已放入您的包裹! |当前绑定元宝为：' + inttostr(BDYB) + '\'
            +'{cmd}<继续使用50绑定元宝抽取/@cjb>');
        end else
        This_NPC.NpcDialog(This_Player,
        '没有足够的包裹空间!\|'
        +'{cmd}<返回/@main>');
         end else
         This_NPC.NpcDialog(This_Player,
        '没有足够的绑定元宝，不可抽取\|'
        +'{cmd}<返回/@main>');
		end else
        This_NPC.NpcDialog(This_Player,'你今天已抽奖30次！');
    end else
    This_NPC.NpcDialog(This_Player,'今日服务器800次限额抽奖已经抽完！');
end;








function xin(price, num: Integer):boolean; 
begin
   result := true; 
 
end;

Begin
  domain;
end. 