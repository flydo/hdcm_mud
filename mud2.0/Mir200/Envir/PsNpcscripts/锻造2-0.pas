{
*******************************************************************}


program Mir2;

{$I common.pas}

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure domain;
begin
  This_Npc.NpcDialog(This_Player,
  '<通过我，不论你的等级高低，都可以进行元宝锻造，/c=red>\使用元宝锻造可以获得更多经验，同时也会获得附赠品：金刚石，\你可以到我这里或者其他元宝锻造师那里领取金刚石。\你也可以到比奇城、盟重、苍月岛的炼金师那里使用时间进行锻造。\'
  +'|{cmd}<查询元宝锻造规则/@chaxun>               ^<申请元宝锻造/@shenqing>\'
  +'|{cmd}<领取锻造附赠品：金刚石/@askybdiam>'
  );
end;

procedure _chaxun;
begin
  This_Npc.NpcDialog(This_Player,
  '1、使用元宝申请锻造后，必须到NPC处领取经验值及附赠品：金刚石\2、用于锻造经验值的元宝数量单次申请范围在1~300之间\3、1个元宝可以锻造6小时，随之附赠3颗金刚石\4、使用元宝进行锻造，可以获得更多的经验\5、一次性锻造元宝数在50到300之间，将有机会得到一份礼品\6、每次领取满12颗金刚石时，将有机会得到一份神秘礼品\7、礼品为系统随机生成，也有不获得的可能性\8、普通锻造不能再申请，已申请的还可继续获得经验值及金刚石\|{cmd}<已了解元宝锻造规则，返回/@main>'
  );
  
  if This_Player.GetV(111,2) = 1  then
  begin
     if This_Player.SetV(111,2,10) then
     begin
        This_Player.Give('经验',10000);
        This_Player.PlayerNotice('你已经完成了成长之路：寻找炼金师任务。', 2);
        //#This_Player.DeleteTaskFromUIList(1002);
     end;
  end;
end;

procedure _shenqing;
begin
  This_Npc.NpcDialog(This_Player,
  '锻造需要用强大的意志力，一旦开始锻造不管是否在线都将持续进行\1个元宝可以锻造6小时，随之附赠3颗金刚石\在我这里申请元宝锻造，没有等级限制\一旦申请元宝锻造成功后，此次锻造将不能被终止\锻造所获得的经验值，到我这里或者庄园炼金师那里来领取\如果要申请元宝锻造，就必须先终止当前的普通锻造\'
  +'|{cmd}<同意元宝锻造的规则并申请元宝锻造/@tongyi>\'
  +'|{cmd}<返回/@main>'
  );
end;

procedure _tongyi;
begin
  This_Npc.NpcDialog(This_Player,
  '请输入想要用于锻造经验值的元宝数量\一次申请范围在1~300个元宝之间\ \特别提示：一次性申请满50个元宝，将有机会得到一份礼品；\          一次性申请满300个元宝，将有机会得到一份超级大奖！\领取礼品前请注意留有足够的包裹空间，否则会导致不能正常领取\'
  +'|{cmd}<请输入用于锻造的元宝数量/@reqybdz>\'
  +'|{cmd}<取消/@doexit>'
  );
end;

procedure _reqybdz;
begin
  This_Npc.InputDialog(This_Player, '请输入：', 0, 101);
end;

procedure P101;
//  temp记录输入数值，timeneed\diamondget分别记录所需时间和获得金刚石数量 
var 
  yb_num,timeneed,diamondget : integer;
begin
  if This_Npc.InputOk then
  begin
    yb_num := strtointdef(Trim(This_Npc.InputStr), 0);
    if (This_Player.YBNum >= yb_num) then
    begin
      if (yb_num > 0) and (yb_num <= 300) then
      begin
        timeneed := yb_num * 6;
        diamondget := yb_num * 3;
        This_Npc.NpcDialog(This_Player,
          This_Player.Name + ',您打算花费' + inttostr(timeneed) + '小时的时间来锻造经验值，\'
          +'并且，整个锻造过程你将获得附赠：' +inttostr(diamondget) + '颗金刚石，\'
          +'请注意，锻造一但开始，就无法终止；\锻造成功后，您必须到我这里来领取经验值，\领取经验值的同时，您也将获得锻造附赠品：金刚石。\您确认开始锻造吗？\ \'
          +'|{cmd}<确认开始锻造/@agreetimedz>'+ addSpace('', 20) + '|{cmd}<放弃/@doexit>'
        );       
      end
      else
      begin
        This_Npc.NpcDialog(This_Player,
          '输入的用于锻造经验值的元宝数量\一次申请范围在1~300个元宝之间'
        );       
      end;   
    end
    else
    begin
      This_Npc.NpcDialog(This_Player,
        '对不起，您没有那么多元宝'
      );     
    end;
  end;
end;

procedure _agreetimedz;
//记录输入锻造的数量 
var mount : integer;
begin
  mount := strtointdef(This_Npc.InputStr,0); 
  This_Player.MakeDiamondWithYB(mount);
end;

procedure _askybdiam;
begin
  This_Npc.ClientAskYBDuanZao(This_Player);   //询问元宝锻造情况,并可以领取 
end;

procedure _ybdzlq;
begin
  //这是由GS生成的对话框中,领取所有金刚石
  This_Npc.ClientQuestGetDiam(This_Player, 0);  //0表示领取所有 
end;

procedure _ybdzlq_12;
begin
  //这是由GS生成的对话框中,领取12颗金刚石
  This_Npc.ClientQuestGetDiam(This_Player, 12);   
end;

begin
  domain;
end.  