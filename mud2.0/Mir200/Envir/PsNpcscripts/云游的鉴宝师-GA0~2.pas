{********************************************************************

*******************************************************************}

program mir2;

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

var
ck_name : array[1..2] of string;
ck_value : array[1..2] of integer; 

procedure OnInitialize;
begin
ck_name[1] :='白色虎齿项链';
ck_name[2] :='灯笼项链';

ck_value[1] :=50000;
ck_value[2] :=40000;
end; 

procedure _Checkup;
begin
  This_Npc.NpcDialog(This_Player,
  '可以鉴定的装备：\'
  +'<白色虎齿项链、灯笼项链/c=red>\'
  +'鉴定成功有几率增加以下属性：\'
  +'<攻击、魔法、道术、魔法躲避、幸运/c=red>\'
  +'每次鉴定消耗一定数量的金币：\'
  +'<普通白色虎齿项链：50000金币/c=red>\'
  +'<普通灯笼项链：40000金币/c=red>\'
  +'<待鉴定装备属性越高，鉴定消耗的金币也越多。/c=red>\'
  +'|{cmd}<开始鉴定/@Checkup_1>        ^<关闭/@doexit>' 
  );
  
end; 

procedure _Checkup_1;
begin
   This_NPC.Click_CommitItem(This_Player,1,'待鉴定装备：'); 
end; 

procedure CommitItem(AType:word);
var
i,ck_num,ck_kind,ck_gold,ck_rand:integer;
ck_str,ck_red:string; 
begin
     ck_gold := 0;  //初始化 
     ck_str := '';  //初始化
     ck_red := '';  //初始化
     
     for ck_kind := 1 to 2 do
     begin
       if ck_name[ck_kind] = This_Item.ItemName then 
       begin                             
         ck_num := This_Item.AddPa1 + This_Item.AddPa2 + This_Item.AddPa3 + This_Item.AddPa4 + This_Item.AddPa5; 
         ck_gold := ck_value[ck_kind];
         if (ck_num > 0) and (ck_num < 7) then
         begin
           for i:= 1 to ck_num do
           begin
           ck_gold := ck_gold*3;
           end;
         end;  
       end;
     end;
     
     if ck_num > 6 then
     begin
     This_Player.NotifyClientCommitItem(0,'无法鉴定：你的'+This_Item.ItemName+'已激发出所有属性！');  
     end else
     if ck_gold > 0 then
     begin                                                                                             
       if This_Player.GoldNum >= ck_gold then
       begin
         ck_rand := random(10000);
         if ck_rand < 7024 then
         begin 
         This_Player.TakeByClientID(This_Item.ClientItemID);
         This_Player.NotifyClientCommitItem(1,'');
         This_Player.PlayerNotice('鉴定失败：你的'+This_Item.ItemName+'已破碎。',2);
         end else
         if ck_rand < 7874 then
         begin 
         This_Item.AddPa3 := This_Item.AddPa3 + 1;   //攻击+1 
         ck_str :='攻击+1'; 
         end else
         if ck_rand < 8724 then
         begin 
         This_Item.AddPa4 := This_Item.AddPa4 + 1;   //魔法+1 
         ck_str :='魔法+1'; 
         end else
         if ck_rand < 9574 then
         begin 
         This_Item.AddPa5 := This_Item.AddPa5 + 1;   //道术+1 
         ck_str :='道术+1'; 
         end else
         if ck_rand < 9594 then
         begin 
         This_Item.AddPa1 := This_Item.AddPa1 + 1;   //躲避+10% 
         ck_str :='魔法躲避+10%'; 
         end else
         if ck_rand < 9614 then
         begin 
         This_Item.AddPa2 := This_Item.AddPa2 + 1;   //幸运+1 
         ck_str :='幸运+1'; 
         end else
         if ck_rand < 9742 then
         begin 
         This_Item.AddPa3 := This_Item.AddPa3 + 2;   //攻击+2 
         ck_str :='攻击+2'; 
         ck_red :='红字公告'; 
         end else
         if ck_rand < 9870 then
         begin 
         This_Item.AddPa4 := This_Item.AddPa4 + 2;   //魔法+2 
         ck_str :='魔法+2';
         ck_red :='红字公告'; 
         end else
         if ck_rand < 9998 then
         begin 
         This_Item.AddPa5 := This_Item.AddPa5 + 2;   //道术+2 
         ck_str :='道术+2'; 
         ck_red :='红字公告';
         end else
         if ck_rand < 9999 then
         begin 
         This_Item.AddPa1 := This_Item.AddPa1 + 2;   //躲避+20% 
         ck_str :='魔法躲避+20%'; 
         ck_red :='红字公告';
         end else
         begin 
         This_Item.AddPa2 := This_Item.AddPa2 + 2;   //幸运+2 
         ck_str :='幸运+2'; 
         ck_red :='红字公告';
         end;  
         This_Player.DecGold(ck_gold);
         if ck_str <> '' then 
         begin
         This_Player.NotifyClientCommitItem(0,'鉴定成功：你的'+This_Item.ItemName+'提升了'+ck_str+'！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         end;   
         if ck_red <> '' then This_NPC.NpcNotice('恭喜：'+This_Player.Name+'在苍月岛首饰店鉴定'+This_Item.ItemName+'时提升了'+ck_str+'！');
         This_Player.AddLogRec(9, This_Item.ItemName, 811152, ck_gold, ck_str);
       end else
       begin
       This_Player.NotifyClientCommitItem(0,'无法鉴定：你的金币不足，需要'+inttostr(ck_gold)+'金币。'); 
       end;
     end else 
     begin
        This_Player.NotifyClientCommitItem(0,'该物品不可鉴定，请投入可鉴定的装备！');   
     end;   
end;
procedure _CheckupRule;
begin
  This_Npc.NpcDialog(This_Player,
  '可以鉴定的装备：\'
  +'<白色虎齿项链、灯笼项链/c=red>\'
  +'鉴定成功有几率增加以下属性：\'
  +'<攻击、魔法、道术、魔法躲避、幸运/c=red>\'
  +'每次鉴定消耗一定数量的金币：\'
  +'<普通白色虎齿项链：50000金币/c=red>\'
  +'<普通灯笼项链：40000金币/c=red>\'
  +'<待鉴定装备属性越高，鉴定消耗的金币也越多。/c=red>\'
  +'|{cmd}<返回/@main>' 
  );
end; 

begin
  This_Npc.NpcDialog(This_Player,
  '我云游一生，见识过玛法大陆上的各种奇珍异宝，但是只对两种宝物\'
  +'感兴趣，一种名为<白色虎齿项链/c=red>，一种名为<灯笼项链/c=red>。\'
  +'我曾习得一种鉴宝秘术，可以激发出宝物隐藏的力量，不过也可能会\'
  +'使其烟消云散。如果你手中有这两种宝物，不妨交给我鉴定一番，当\'
  +'然，我会收取你一定金币的手续费，如果破碎了也千万不要怪我。\ \'
  +'|{cmd}<开始鉴定/@Checkup>          ^<查看鉴定规则/@CheckupRule>\' 
  );
end.
   