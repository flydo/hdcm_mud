{
*******************************************************************}

program Mir2;

{$I common.pas}

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure domain;
begin
  This_Npc.NpcDialog(This_Player,
  '奉国王之命，我在这里负责奖赏最出色的玛法勇士。\金牌帐号、“热血勇士”都可以在我这里领取丰厚的奖赏。\你的奖赏一定会特别丰厚！\' 
  +'|{cmd}<获取“王师教头”资格/@heromission>\'   
  +'|{cmd}<领取热血勇士奖励/@goldrole>              <查询“热血勇士”奖励/@goldjiang>\'
  );
end;



procedure _heromission;
begin
  This_Npc.NpcDialog(This_Player,
  '据我掌握的情报，半兽人暗中集结在比奇边境，密谋发动攻击，\比奇国王再次颁下诏书，号令天下豪杰贡献自己的力量，\只要你等级在35级以上，就可成为“王师教头”，\担负起培养玛法年轻勇士的重任！\“王师教头”所收的徒弟，就将成为“王师弟子”，\“王师弟子”将受到最完善培养和最优厚待遇。\|{cmd}<关闭/@doexit>'
  );
end;



procedure _goldrole;
begin
  This_Npc.NpcDialog(This_Player,
  '如果你有30个热血积分，交给我，我会给你一个热血凭证。\双击热血凭证后，你就可以成为热血勇士。\热血勇士与金牌帐号和白金帐号都没有冲突。\从46级到55级，每个级别均可领取相应的丰厚奖励。\领奖之前，请保留10个以上的包裹空间，否则不能正常领取\'
  +'|{cmd}<领取奖品/@golditemreq>                  <使用热血积分获得热血凭证/@goldjifen> \'
  +'|{cmd}<捆热血积分/@bindgold>\'               
  +'|{cmd}<返回/@main>                      <离开/@doexit>'
  );
end;

procedure _golditemreq;
begin
  This_Npc.ReqItemByGoldAct(This_Player);
end;

procedure _goldjifen;
begin
  if This_Player.GetBagItemCount('热血积分') >= 30 then
  begin
    This_Player.Take('热血积分',30);
    This_Player.Give('热血凭证',1);
    This_Npc.NpcDialog(This_Player,
    '热血凭证已经给你了，双击之后，你就是热血勇士了！');
    
    if This_Player.GetV(111,47) = 1 then
    begin
       if This_Player.SetV(111,47,10) then
       begin
          This_Player.Give('经验',10000);
          This_Player.PlayerNotice('你已经完成了成长之路：热血勇士任务。',2);
          //#This_Player.DeleteTaskFromUIList(1047);
       end;
    end; 
  end
  else if this_Player.GetBagItemCount('热血积分包') >= 5 then
  begin
    This_Player.Take('热血积分包',5);
    This_Player.Give('热血凭证',1);
    This_Npc.NpcDialog(This_Player,
    '热血凭证已经给你了，双击之后，你就是热血勇士了！');
    
    if This_Player.GetV(111,47) = 1 then
    begin
       if This_Player.SetV(111,47,10) then
       begin
          This_Player.Give('经验',10000);
          This_Player.PlayerNotice('你已经完成了成长之路：热血勇士任务。',2);
          //#This_Player.DeleteTaskFromUIList(1047);
       end;
    end; 
  end
  else if This_Player.GetBagItemCount('热血积分包') = 4 then
  begin
    if This_Player.GetBagItemCount('热血积分') >= 6 then
    begin
       This_Player.Take('热血积分包',4);
       This_Player.Take('热血积分',6);
       This_Player.Give('热血凭证',1);
       This_Npc.NpcDialog(This_Player,
       '热血凭证已经给你了，双击之后，你就是热血勇士了！');
    
       if This_Player.GetV(111,47) = 1 then
       begin
          if This_Player.SetV(111,47,10) then
          begin
             This_Player.Give('经验',10000);
             This_Player.PlayerNotice('你已经完成了成长之路：热血勇士任务。',2);
             //#This_Player.DeleteTaskFromUIList(1047);
          end;
       end; 
    end;
  end
  else if this_Player.GetBagItemCount('热血积分包') = 3 then
  begin
    if This_Player.GetBagItemCount('热血积分') >= 12 then
    begin
       This_Player.Take('热血积分包',3);
       This_Player.Take('热血积分',12);
       This_Player.Give('热血凭证',1);
       This_Npc.NpcDialog(This_Player,
       '热血凭证已经给你了，双击之后，你就是热血勇士了！');
    
       if This_Player.GetV(111,47) = 1 then
       begin
          if This_Player.SetV(111,47,10) then
          begin
             This_Player.Give('经验',10000);
             This_Player.PlayerNotice('你已经完成了成长之路：热血勇士任务。',2);
             //#This_Player.DeleteTaskFromUIList(1047);
          end;
       end; 
    end;
  end
  else if this_Player.GetBagItemCount('热血积分包') = 2 then
  begin
    if This_Player.GetBagItemCount('热血积分') >= 18 then
    begin
    This_Player.Take('热血积分包',2);
    This_Player.Take('热血积分',18);
    This_Player.Give('热血凭证',1);
    This_Npc.NpcDialog(This_Player,
    '热血凭证已经给你了，双击之后，你就是热血勇士了！');
    end;
  end
  else if this_Player.GetBagItemCount('热血积分包') = 1 then
  begin
    if This_Player.GetBagItemCount('热血积分') >= 24 then
    begin
       This_Player.Take('热血积分包',1);
       This_Player.Take('热血积分',24);
       This_Player.Give('热血凭证',1);
       This_Npc.NpcDialog(This_Player,
       '热血凭证已经给你了，双击之后，你就是热血勇士了！');
       
       if This_Player.GetV(111,47) = 1 then
       begin
          if This_Player.SetV(111,47,10) then
          begin
             This_Player.Give('经验',10000);
             This_Player.PlayerNotice('你已经完成了成长之路：热血勇士任务。',2);
             //#This_Player.DeleteTaskFromUIList(1047);
          end;
       end;
    end;
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '你好像没有足够的热血积分啊，\这个热血凭证我暂时不能给你。\请你继续努力吧！\'
    );
  end;
end;

procedure _bindgold;
begin
  This_Npc.NpcDialog(This_Player,
  '我可以帮你把6个热血积分捆为一个热血积分包，\不过每捆一次收费5000金币。\你想让我帮你捆热血积分吗？\ \'
  +'|{cmd}<捆热血积分/@bindinggold>\ \'
  +'|{cmd}<返回/@goldrole>'
  );
end;

procedure _bindinggold;
begin
  if This_Player.GoldNum < 5000 then
  begin
    This_Npc.NpcDialog(This_Player,
    '你连5000金币都没有，我怎么帮你捆呢？\');
  end
  else if This_Player.GetBagItemCount('热血积分') >= 6 then
  begin
    This_Player.Take('热血积分',6);
    This_Player.DecGold(5000);
    This_Player.Give('热血积分包',1); 
    This_Npc.NpcDialog(This_Player,
    '已经帮你将6个积分捆成1个热血积分包，\如果你还有积分，我可以继续帮你捆成热血积分包。\ \'
    +'|{cmd}<继续捆/@bindinggold>\ \'
    +'|{cmd}<返回/@goldrole>'
    );
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '你没有足够的热血积分啊，没办法帮你捆成一包，\请继续努力吧。\ \|{cmd}<返回/@goldrole>');
  end;
end;

procedure _goldjiang;
begin
  This_Npc.NpcDialog(This_Player,
  '在46-55级，每个等级你都可以到我这里来领取一次奖励，\对于高等级的热血勇士，也可领取小于当前等级的所有奖品。\46级，可以获得<六件重装/c=red>中的任意一件，\47-49级，可以获得<雷霆、光芒、烈焰系列首饰/c=red>中的任意一件，\50-55级，将会在以下超级装备中抽取一件：\|{cmd}<魔龙、战神、真魂、圣魔系列，狂战、太极、混世系列，/c=red>\|{cmd}<誓言腰带、预言头盔、传说魔靴、银星勋章、寂静之手，/c=red>\|{cmd}<星王靴、星王腰带、星王冠/c=red>\|{cmd}<返回/@goldrole>'
  );
end;

procedure _jinpai;
begin
  This_Npc.NpcDialog(This_Player,
  '只要你是金牌帐号，这里的奖品就有你一份！\在转变为金牌账号之后，才能进行奖品的领取领取奖品之前，请保留足够的包裹空间，否则不能正常领取\'
  +'|{cmd}<领取奖品/@liaojie>                  <关于白金积分换取白金凭证/@huanqu>\ \'
  +'|{cmd}<返回/@main>                      <离开/@doexit>'
  );
end;

procedure _liaojie;
begin
  This_Npc.NpcDialog(This_Player,
  '领取奖品之前，请保留足够的包裹空间，否则不能正常领取\对于高等级玩家，可领取小于当前等级的所有奖品。\每个金牌帐号仅一个角色可领取奖品，且每件奖品只能领取1次\我会根据你当前的等级，给你相应的奖品。\ \'
  +'|{cmd}<领取奖品/@lingqu>                      ^<查询奖品设置/@jiang>\'
  +'|{cmd}<离开/@doexit>'
  );
end;

procedure _lingqu;
begin
  This_Npc.NpcDialog(This_Player,
  '请确认有足够的包裹空间放置奖品，否则不能正常领取，\如因包裹空间不足，导致不能正常领取，责任由你承担。\你将不再可以重新领取因包裹空间不足没有正常领取的奖品。\ \'
  +'我有足够的空间，<确认领取/@goldidreq>\ \'
  +'|{cmd}<查询奖品设置/@jiang>              ^<离开/@doexit>'
  );
end;

procedure _goldidreq;
begin
  This_Npc.ReqItemByGoldID(This_Player);
end;

procedure _huanqu;
begin
  This_Npc.NpcDialog(This_Player,
  '如果你已经拥有了30个白金积分，你可以去旁边的领奖使者那里，\他会给你1个白金凭证。\ \|{cmd}<返回/@jinpai>'
  );
end;  

procedure _jiang;
begin
  This_Npc.NpcDialog(This_Player,
  '金牌帐号的奖品设置：在相应等级可领取相应的奖品：\28级：随机获得炼狱、银蛇、魔杖中的任意1件\35级：随机获得祝福油、矿洞组队卷轴、神殿组队卷轴、\      邪窟组队卷轴、地穴组队卷轴、石墓组队卷轴的任意1件\40级：随机获得裁决之杖、骨玉权杖、龙纹剑中的任意1件\42级：随机获得天魔神甲、圣战宝甲、天尊道袍、天师长袍、\      霓裳羽衣、法神披风，这六件40级重装中的任意1件\ \'
  +'|{cmd}<查看下一页/@next5>                   ^<返回/@main>'
  );
end;

procedure _next5;
begin
  This_Npc.NpcDialog(This_Player,
  '44级:随机获得三职业1.8版新技能：灭天火、寒冰掌、\气功波、无极真气、狮子吼中的任意1本\47、48、49级:均可在以下物品中随机领取一样物品\雷霆战戒、雷霆护腕、雷霆项链、雷霆腰带、雷霆战靴\光芒道戒、光芒护腕、光芒项链、光芒道靴、光芒腰带\烈焰腰带、烈焰护腕、烈焰项链、烈焰魔靴、烈焰魔戒\ \'
  +'|{cmd}<查看下一页/@next6>                  ^<返回/@main>'
  );
end;

procedure _next6;
begin
  This_Npc.NpcDialog(This_Player,
  '50级:可以在以下物品中随机领取一样物品：\战神手镯、战神戒指、战神项链\圣魔手镯、圣魔戒指、圣魔项链\真魂手镯、真魂戒指、真魂项链\ \|{cmd}<返回/@main>'
  );
end;

procedure _firstusedyb;
begin
  This_Npc.ReqGetFirstUsedGift(This_Player);
end;

procedure _fenhongask;
begin
  This_Npc.NpcDialog(This_Player,
  '|{cmd}<推广员的报酬/@fenhongask1>\ \'
  +'|{cmd}<热血传奇分红规则/@fenhongask2>\ \'
  +'|{cmd}<如何成为推广员/@fenhongask3>\ \'
  +'|{cmd}<返回/@main>'
  );
end;

procedure _fenhongask1;
begin
  This_Npc.NpcDialog(This_Player,
  '##推广员每推广一个新玩家，\##公司都将根据新玩家在游戏中的消费记录，\每月给与推广员50%的分红。\每个新玩家的最大分红高达45元！\不限时间，直至分满45元/人为止（活动奖金将额外计算）\ \|{cmd}<返回/@fenhongask>'
  );
end;

procedure  _fenhongask2;
begin
  This_Npc.NpcDialog(This_Player,
  '被推广玩家除元宝交易之外的所有元宝消耗都计入分红。\包括元宝兑换灵符；元宝锻造、元宝购买商铺道具等。\被推广的新玩家可以获得丰厚的新手奖励！\ \|{cmd}<返回/@fenhongask>'
  );
end;



begin
  domain;
end. 