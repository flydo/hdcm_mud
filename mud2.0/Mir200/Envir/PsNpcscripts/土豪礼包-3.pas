PROGRAM Mir2;
var 
num :integer;

Procedure _doexit;
begin
   This_Npc.CloseDialog(This_Player);
end;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _gzh;
begin
	This_Player.PlayerNotice('DYP=Bg:cm:gzh|Exit:cm:2:3:385:20:',5);
end;

procedure domain;
begin
    This_NPC.NpcDialog(This_Player,
    +'|<【50元土豪礼包】/fcolor=249>\'
			+'|<《赞助战石*1》/fcolor=250><《赞助嗜血镯*1》/fcolor=250>\'
			+'|<《土豪急速毒斩*1》/fcolor=250><《赞助幸运链*1/fcolor=250>\'
			+'|<《1-4转至尊令*1》/fcolor=250><《回收信物*1》/fcolor=250>\'
			+'|<《强化麻痹戒指*1》/fcolor=250><《40倍经验卷*1》/fcolor=250>\'
			+'|<《强化复活戒指*1》/fcolor=250><《元宝200W》/fcolor=250>\'
			+'|<《强化护身戒指*1》/fcolor=250><《灵符500》/fcolor=250>\'
			+'|<《神级爆率勋章（爆率增加30%）》/fcolor=249>\'			
			+'|<《天之爪+10*1（刀刀切割300）》/fcolor=249>\'

	+'|------------------------------\'
	+'|【随缘购买，需要找管理】	\'
	+'|{cmd}<领取礼包/@lingqulibao>\'
	//+'|{cmd}<领取礼包/@lingqulibao>^<领取测试奖励/@ceshi>\'
	+'|{cmd}<返回/@main>\'
  );
end;

procedure _lingqulibao;
begin
	if This_Player.GetV(2,99) < 0 then
	begin
		This_NPC.InputDialog(This_Player,'请输入推广码',0,123);
	end else
	begin
		This_NPC.NpcDialog(This_Player,
		'你已经领取过推广奖励!' 
		+'|{cmd}<返回/@main>'
		) ;
	end;
end;

procedure p123;
begin
   num :=  StrToIntDef(This_NPC.InputStr,-1);//只能输入数字，否则都返回-1
   if This_NPC.InputOK then             
    begin
		if This_Npc.ChkStrInFile('\one\推广码.txt', This_NPC.InputStr) = true  then  //检测文档是否有该推广码 
		begin
			This_Player.CallOut(This_NPC,1,'jxyb1'); 
		end 
		else
			This_NPC.NpcDialog(This_Player,
			'推广码错误，或已使用!' 
			+'|{cmd}<返回/@main>'
			) ;
    end; 
end;

procedure _ceshi;
begin
	if This_Player.GetV(2,99) = 2 then
	begin
		This_NPC.NpcDialog(This_Player,
		 '你已经领取过测试奖励！'
		 );
		exit;
	end;
	if This_Player.FreeBagNum >= 9 then
	begin
		This_Npc.DelStrFromFile('\one\推广码.txt', This_NPC.InputStr); //删除推广码后给予奖励 
			This_Npc.AddStrToFile('\one\领取人员.txt', This_Player.Name); //写入该玩家名字后给予奖励 
			This_Player.Give('土豪麻痹', 1);
			This_NPC.NpcDialog(This_Player,
				'恭喜：你获得推广礼包!' 
			); 
	end
	else
		 This_NPC.NpcDialog(This_Player,
		 '包袱空间不足。'
		 );
end;
 
procedure jxyb1;
begin
if This_Player.FreeBagNum >= 15 then
begin
    if This_Npc.ChkStrInFile('\one\推广码.txt', This_NPC.InputStr) = true  then  //防刷，再次检测文档是否有该推广码
	   begin
		This_Npc.DelStrFromFile('\one\推广码.txt', This_NPC.InputStr); //删除推广码后给予奖励
		This_Npc.AddStrToFile('\one\领取人员.txt', This_Player.Name); //写入该玩家名字后给予奖励 
        This_Player.Give('神级爆率勋章' , 1);
This_Player.Give('赞助嗜血镯' , 1);
This_Player.Give('赞助幸运链' , 1);
This_Player.Give('土豪急速毒斩' , 1);
			This_Player.Give('一转至尊令',1);
			This_Player.Give('二转至尊令',1);
			This_Player.Give('三转至尊令',1);
			This_Player.Give('四转至尊令',1);
			This_Player.Give('天之爪+10',1);
			This_Player.Give('回收信物',1);
			This_Player.Give('强化麻痹戒指',1);
			This_Player.Give('强化复活戒指',1);
			This_Player.Give('强化护身戒指',1);
			This_Player.Give('40倍经验卷',1);
			This_Player.Give('赞助战石',1);
			This_Player.AddLF(0,500);
			This_Player.ScriptRequestAddYBNum(2000000); 
        This_NPC.NpcDialog(This_Player,
			'恭喜：你获得土豪礼包!' 
		); 
		end 
       else
        This_NPC.NpcDialog(This_Player,
		'推广码错误，或已使用!'
	);
end
else
	 This_NPC.NpcDialog(This_Player,
	 '包袱空间不足。'
	 );
end;

begin
  domain;
end.