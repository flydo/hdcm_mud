                             {
/************************************************************************}


PROGRAM Mir2;

{$I common.pas}
//{$I TaoZhuang.pas}

Procedure _doexit;
begin
   This_Npc.CloseDialog(This_Player);
end;

procedure domain;
begin
  This_Npc.NpcDialog(This_Player,
   '我这里可以回收祖玛以上的装备。\'+
   '如果你有不要的装备，我这里可以给你兑换成经验，金币，元宝，3选一\ \'+
   '|<一键回收，请把有用的装备放入仓库！/c=red>\ \'+
   '|{cmd}<经验回收/@ZhuangBack>    ^<元宝回收/@ZhuangBackyb>\'+
   '|{cmd}<金币回收/@ZhuangBackjb>\'+
   '|{cmd}<武器类回收/@wuqi>  ^<退出/@doexit>');
end;
procedure _wuqi;
begin
  This_Npc.NpcDialog(This_Player,
   '我这里可以回收祖玛以上的武器。\'+
   '如果你有不要的武器，我这里可以给你兑换成经验，金币，元宝，3选一\ \'+
   '|<一键回收，请把有用的装备放入仓库！/c=red>\ \'+
   '|{cmd}<经验回收/@ZhuangBackwq>    ^<元宝回收/@ZhuangBackybwq>\'+
   '|{cmd}<金币回收/@ZhuangBackjbwq>\'+
   '|{cmd}<退出/@doexit>');
end;

function getZBnameById(ZBid : integer) : string;
var ZBlv , ZBlvId : integer;
begin
    ZBlv := ZBid div 100;
    ZBlvId := ZBid mod 100;
    result := '';
    case ZBlv of
        1: //沃玛
        begin
            case ZBlvId of
                1 : result := '天珠项链';
                2 : result := '铂金戒指';
                3 : result := '心灵手镯';
                4 : result := '生命项链';
                5 : result := '思贝儿手镯';
                6 : result := '红宝石戒指';
                7 : result := '幽灵项链';
                8 : result := '幽灵手套';
                9 : result := '龙之戒指';

            end;
        end;	
	
        2 : //祖玛
        begin
            case ZBlvId of
                1 : result := '绿色项链';
                2 : result := '骑士手镯';
                3 : result := '力量戒指';
                4 : result := '恶魔铃铛';
                5 : result := '龙之手镯';
                6 : result := '紫碧螺';
                7 : result := '灵魂项链';
                8 : result := '三眼手镯';
                9 : result := '泰坦戒指';
                10 : result := '黑铁头盔';
                11 : result := '青铜腰带';
                12 : result := '紫绸靴';

            end;
        end;
        3 : //赤月
        begin
            case ZBlvId of
                1 : result := '圣战头盔';
                2 : result := '圣战项链';
                3 : result := '圣战手镯';
                4 : result := '圣战戒指';
                5 : result := '法神头盔';
                6 : result := '法神项链';
                7 : result := '法神手镯';
                8 : result := '法神戒指';
                9 : result := '天尊头盔';
                10 : result := '天尊项链';
                11 : result := '天尊手镯';
                12 : result := '天尊戒指';
                13 : result := '钢铁腰带';
                14 : result := '避魂靴';

            end;
        end;
        4 : //40级套装
        begin
            case ZBlvId of
                1 : result := '勇士头盔';
                2 : result := '魔幻项链';
                3 : result := '魔幻手镯';
                4 : result := '魔幻戒指';
                5 : result := '骷髅骨';
                6 : result := '道域头盔';
                7 : result := '白银项链';
                8 : result := '白银手镯';
                9 : result := '白银戒指';
                10 : result := '吉娃娃';
                11 : result := '宙神头盔';
                12 : result := '浮游手镯';
                13 : result := '浮游戒指';
                14 : result := '浮游项链';
                15 : result := '情人花';

            end;
        end;

        5 : //45级套装
        begin
            case ZBlvId of
                1 : result := '朱雀头盔';
                2 : result := '摄魂项链';
                3 : result := '摄魂手镯';
                4 : result := '摄魂戒指';
                5 : result := '魔法勋章';
                6 : result := '贝迪头盔';
                7 : result := '道尊项链';
                8 : result := '道尊手镯';
                9 : result := '道尊戒指';
                10 : result := '真爱钻石';
                11 : result := '黑暗头盔';
                12 : result := '铁血项链';
                13 : result := '铁血手镯';
                14 : result := '铁血戒指';
                15 : result := '灵魂钻石';

            end;
        end;
        6 :  //50级套装
        begin
            case ZBlvId of
                1 : result := '风铃火山';
                2 : result := '魔天项链';
                3 : result := '魔天手镯';
                4 : result := '魔天戒指';
                5 : result := '卦';
                6 : result := '蓝灵头盔';
                7 : result := '蓝灵项链';
                8 : result := '蓝灵手环';
                9 : result := '蓝灵戒指';
                10 : result := '凤';
				11 : result := '将军头盔';
				12 : result := '将军项链';
				13 : result := '将军手镯';
				14 : result := '将军戒指';
				15 : result := '龙';
            end;
        end;
        7 :  //55级套装
        begin
            case ZBlvId of
                1 : result := '龙皇霸气戒(战)';
                2 : result := '龙皇霸气戒(法)';
                3 : result := '龙皇霸气戒(道)';
                4 : result := '龙皇霸气镯(战)';
                5 : result := '龙皇霸气镯(法)';
                6 : result := '龙皇霸气镯(道)';
                7 : result := '龙皇霸气链(战)';
                8 : result := '龙皇霸气链(法)';
                9 : result := '龙皇霸气链(道)';
                10 : result := '龙皇霸气盔(战)';
				11 : result := '龙皇霸气盔(法)';
				12 : result := '龙皇霸气盔(道)';
				13 : result := '龙皇霸气心';
            end;
        end;
        8 :  //65级套装
        begin
            case ZBlvId of
                1 : result := '不灭V神戒(战)';
                2 : result := '不灭V神镯(战)';
                3 : result := '不灭V神盔(战)';
                4 : result := '不灭V神链(战)';
                5 : result := '不灭V神灵';
				6 : result := '不灭V神戒(道)';
                7 : result := '不灭V神镯(道)';
                8 : result := '不灭V神链(道)';
                9 : result := '不灭V神盔(道)';
                10 : result := '不灭V神戒(法)';
				11 : result := '不灭V神镯(法)';
				12 : result := '不灭V神链(法)';
				13 : result := '不灭V神盔(法)';
            end;
        end;
        9 :  //70级套装
        begin
            case ZBlvId of
                1 : result := '辉煌V神戒(战)';
                2 : result := '辉煌V神镯(战)';
                3 : result := '辉煌V神链(战)';
                4 : result := '辉煌V神冠(战)';
                5 : result := '辉煌神魄心';
				6 : result := '辉煌V神戒(道)';
                7 : result := '辉煌V神镯(道)';
                8 : result := '辉煌V神链(道)';
                9 : result := '辉煌V神冠(道)';
                10 : result := '辉煌V神戒(法)';
				11 : result := '辉煌V神镯(法)';
				12 : result := '辉煌V神链(法)';
				13 : result := '辉煌V神冠(法)';
            end;
        end;		
    end;
end;




function getZBnameByIdWQ(ZBid : integer) : string;
var ZBlv , ZBlvId : integer;
begin
    ZBlv := ZBid div 100;
    ZBlvId := ZBid mod 100;
    result := '';
    case ZBlv of       
            1 : 
        begin
            case ZBlvId of                      //沃玛武器衣服
                1 : result := '银蛇';
                2 : result := '无极棍';
                3 : result := '魔杖';
                4 : result := '炼狱';

            end;
            end; 
             2 : 
        begin
            case ZBlvId of                       //祖玛武器衣服
                1 : result := '龙纹剑';
                2 : result := '骨玉权杖';
                3 : result := '裁决之杖';

            end;
        end;
            3 : 
        begin
            case ZBlvId of                      //赤月武器衣服
                1 : result := '逍遥扇';
                2 : result := '屠龙';
                3 : result := '嗜魂法杖';
                4 : result := '天魔神甲';
				5 : result := '天尊道袍';
				6 : result := '圣战宝甲';
				7 : result := '霓裳羽衣';
				8 : result := '天师长袍';
				9 : result := '法神披风';

            end;
             end;
            4 : 
          begin
            case ZBlvId of                   //40级武器衣服
                1 : result := '圣龙魔袍(男)';
                2 : result := '圣龙魔袍(女)';
                3 : result := '白玉法杖';
                4 : result := '泰坦道衣(男)';
				5 : result := '泰坦道衣(女)';
				6 : result := '黑铁银蛇';
				7 : result := '钢盔战甲(男)';
				8 : result := '钢盔战甲(女)';
				9 : result := '绿玉裁决';
				
            end;
            end;
            5 : 
        begin
            case ZBlvId of                  //45级武器衣服
                1 : result := '朱雀神袍(男)';
                2 : result := '朱雀神袍(女)';
                3 : result := '蓝灵法杖';
                4 : result := '青龙道衣(男)';
				5 : result := '青龙道衣(女)';
				6 : result := '金域无极';
				7 : result := '白虎战甲(女)';
				8 : result := '白虎战甲(男)';
				9 : result := '绿玉屠龙';
				
            end;
             end;
            6 : 
        begin
            case ZBlvId of               // 50级武器衣服
                1 : result := '猩猩魔袍(男)';
                2 : result := '猩猩魔袍(女)';
                3 : result := '摄魂之剑';
                4 : result := '麒麟道衣(男)';
				5 : result := '麒麟道衣(女)';
				6 : result := '仙人蒲扇';
				7 : result := '铁骑战甲(男)';
				8 : result := '铁骑战甲(女)';
				9 : result := '战神之刃';

            end;
             end; 
		    7 : 
        begin
            case ZBlvId of                  //55级武器衣服
                1 : result := '龙皇霸龙甲(法)';
                2 : result := '龙皇霸凤铠(法)';
                3 : result := '龙皇霸气法剑';
				4 : result := '龙皇霸龙甲(道)';
                5 : result := '龙皇霸凤铠(道)';
                6 : result := '龙皇霸气道剑';
				7 : result := '龙皇霸龙甲(战)';
                8 : result := '龙皇霸凤铠(战)';
                9 : result := '龙皇霸气战剑';
				
            end;
             end;
			 8 : 
        begin
            case ZBlvId of               // 65级武器衣服
                1 : result := '不灭V冰火战刀';
                2 : result := '不灭V紫神甲(战)';
                3 : result := '不灭V紫神铠(战)';
				4 : result := '不灭V冰火道刀';
                5 : result := '不灭V紫神甲(道)';
                6 : result := '不灭V紫神铠(道)';
				7 : result := '不灭V冰火法刀';
                8 : result := '不灭V紫神甲(法)';
                9 : result := '不灭V紫神铠(法)';

            end;
             end; 
			 9 : 
        begin
            case ZBlvId of               // 70级武器衣服
                1 : result := '辉煌炽火战剑';
                2 : result := '辉煌寒冰甲(战)';
                3 : result := '辉煌寒冰铠(战)';
				4 : result := '辉煌炽火法剑';
                5 : result := '辉煌炽火道剑';
                6 : result := '辉煌寒冰甲(道)';
				7 : result := '辉煌寒冰甲(法)';
                8 : result := '辉煌寒冰铠(道)';
                9 : result := '辉煌寒冰铠(法)';

            end;
             end; 			 
    end;
end;

function getZexpNum(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 30000;       
        2 : result := 60000;
        3 : result := 150000; 
        4 : result := 350000;
        5 : result := 800000;
        6 : result := 1500000;
        7 : result := 6000000;
		8 : result := 15000000;
		9 : result := 30000000;
    end;
end;

function getZybNum(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 0;
        2 : result := 0;
        3 : result := 3;
        4 : result := 15; 
        5 : result := 25;
        6 : result := 35;
        7 : result := 75;
		8 : result := 150;
		9 : result := 300;
    end;
end;

function getZjbNum(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 10000;
        2 : result := 20000;
        3 : result := 40000;
        4 : result := 100000;
        5 : result := 200000;
        6 : result := 500000;
        7 : result := 2000000;
		8 : result := 8000000;
		9 : result := 15000000;
    end;
end;


//=======================武器


function getZexpNumWuqi(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 60000;
        2 : result := 120000;
        3 : result := 300000; 
        4 : result := 700000;
        5 : result := 1600000; 
        6 : result := 3000000;
        7 : result := 12000000;
		8 : result := 30000000;
		9 : result := 60000000;
    end;
end;

function getZybNumwuqi(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 0;
        2 : result := 0;
        3 : result := 4;
        4 : result := 24;
        5 : result := 50;
        6 : result := 90;
        7 : result := 180;
		8 : result := 300;
		9 : result := 500;
    end;
end;

function getZjbNumwuqi(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 20000;
        2 : result := 40000;
        3 : result := 80000;
        4 : result := 200000;
        5 : result := 400000;
        6 : result := 1000000;
        7 : result := 4000000;
		8 : result := 16000000;	
		9 : result := 30000000;	
    end;
end;



procedure _ZhuangBack;
begin
    This_NPC.NpcDialog(This_Player,
    '|请把不要回收的装备放在仓库，回收概不负责：\'
   +'|经验回收沃玛系列               <一键回收/@Woma_all>\'
   +'|经验回收祖玛系列               <一键回收/@ZUma_all>\'
   +'|经验回收赤月系列              <一键回收/@ChiYue_all>\'
   +'|经验回收40级系列              <一键回收/@40ji_all>\'
   +'|经验回收45级系列             <一键回收/@45ji_all>\'
   +'|经验回收50级系列             <一键回收/@50ji_all>\'
   +'|经验回收55级系列             <一键回收/@55ji_all>\'
   +'|经验回收65级系列             <一键回收/@65ji_all>\'

   +'|<退出/@doexit>'

    );

end;
//==============================武器回收


procedure _ZhuangBackwq;
begin
    This_NPC.NpcDialog(This_Player,
    '|请把不要回收的衣服武器放在仓库，回收概不负责：\'
//   +'|袄玛武器每个回收2万经验               <一键回收/@Womawq_all>\'
   +'|经验回收回收祖玛级               <一键回收/@ZUmawq_all>\'
   +'|经验回收回收赤月级              <一键回收/@ChiYuewq_all>\'
   +'|经验回收回收40级衣服武器              <一键回收/@40jiwq_all>\'
   +'|经验回收回收45级衣服武器             <一键回收/@45jiwq_all>\'
   +'|经验回收回收50级衣服武器             <一键回收/@50jiwq_all>\'
   +'|经验回收回收55级衣服武器             <一键回收/@55jiwq_all>\'
   +'|经验回收回收65级衣服武器             <一键回收/@65jiwq_all>\'
   +'|<退出/@doexit>'
    );

end;
 ////////////////////////
procedure _Womawq_all;
var WMnum , i : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(100 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                WMnum := WMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(WMnum) + '件沃玛武器，回收可获得:' + inttostr(getZexpNumWuqi(1) div 10000 * WMnum) + '万经验，你确定回收所有沃玛武器吗？\|'
    +'|{cmd}<确认回收所有沃玛武器/@Womawq_True>'
    +'|{cmd}<返回/@ZhuangBackwq>');
end;

procedure  _Womawq_True;
var WMnum , i , j , itemNum : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                WMnum := WMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNumWuqi(1));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNumWuqi(1) div 10000 * WMnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBackwqwq>');

end;

///////////////////////////

procedure _ZUmawq_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(200 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) >= 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(ZMnum) + '件装备！，回收可获得:' + inttostr(getZexpNumWuqi(2) div 10000 * ZMnum) + '万经验，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收所有祖玛武器/@ZUmawq_True>'
    +'|{cmd}<返回/@ZhuangBackwq>');
end;

procedure  _ZUmawq_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNumWuqi(2));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNumWuqi(2) div 10000 * ZMnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBackwq>');

end;


procedure _Chiyuewq_all;
var CYnum , i , ShengW : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(300 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                CYnum := CYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(CYnum) + '件赤月衣服武器，回收可获得:' + inttostr(getZexpNumWuqi(3) div 10000 * CYnum) + '万经验。你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收所有赤月武器/@Chiyuewq_True>'
    +'|{cmd}<返回/@ZhuangBackwq>');
end;

procedure  _Chiyuewq_True;
var CYnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(300 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                CYnum := CYnum + itemNum;
                ShengW :=itemNum * 10;
                This_Player.Take(Iname, itemNum);
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNumWuqi(3));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNumWuqi(3) div 10000 * CYnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBackwq>');

end;


procedure _40jiwq_all;
var MLnum , i : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(400 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MLnum := MLnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MLnum) + '件40级衣服武器，回收可获得:' + inttostr(getZexpNumWuqi(4) div 10000 * mlnum) + '万经验，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@40jiwq_True>'
    +'|{cmd}<返回/@ZhuangBackwq>');
end;

procedure  _40jiwq_True;
var MLnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(400 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MLnum := MLnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNumWuqi(4));               
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNumWuqi(4) div 10000 * MLnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBackwq>');

end;

procedure _45jiwq_all;
var QMnum , i : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(500 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                QMnum := QMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(QMnum) + '件45级衣服武器，回收可获得:' + inttostr(getZexpNumWuqi(5) div 10000 * QMnum) + '万经验，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@45jiwq_True>'
    +'|{cmd}<返回/@ZhuangBackwq>');
end;

procedure  _45jiwq_True;
var QMnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(500 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                QMnum := QMnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNumWuqi(5));          
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNumWuqi(5) div 10000 * QMnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBackwq>');

end;

procedure _50jiwq_all;
var MSnum , i : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(600 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MSnum := MSnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MSnum) + '件50级衣服武器，回收可获得:' + inttostr(getZexpNumWuqi(6) div 10000 * MSnum) + '万经验，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@50jiwq_True>'
    +'|{cmd}<返回/@ZhuangBackwq>');
end;

procedure  _50jiwq_True;
var MSnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(600 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MSnum := MSnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
				//This_Player.Give('声望',1);
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNumWuqi(6));
                This_Player.MyShengwan := This_Player.MyShengwan + 10
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNumWuqi(6) div 10000 * MSnum) + '万经验！\|'
	+'恭喜你获得' + inttostr(ShengW) + '声望\|'
    +'|{cmd}<返回/@ZhuangBackwq>');

end;

procedure _55jiwq_all;
var MYnum , i : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(700 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MYnum := MYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MYnum) + '件55级衣服武器，回收可获得:' + inttostr(getZexpNumWuqi(7) div 10000 * MYnum) + '万经验，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@55jiwq_True>'
    +'|{cmd}<返回/@ZhuangBackwq>');
end;

procedure  _55jiwq_True;
var MYnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(700 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MYnum := MYnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
				//This_Player.Give('声望',1);
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNumWuqi(7));
                This_Player.MyShengwan := This_Player.MyShengwan + 10
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNumWuqi(7) div 10000 * MYnum) + '万经验！\|'
	+'恭喜你获得' + inttostr(ShengW) + '声望\|'
    +'|{cmd}<返回/@ZhuangBackwq>');

end;


procedure _65jiwq_all;
var LWnum , i : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(800 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                LWnum := LWnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(LWnum) + '件65级衣服武器，回收可获得:' + inttostr(getZexpNumWuqi(8) div 10000 * LWnum) + '万经验，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@65jiwq_True>'
    +'|{cmd}<返回/@ZhuangBackwq>');
end;

procedure  _65jiwq_True;
var LWnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(800 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                LWnum := LWnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNumWuqi(8));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNumWuqi(8) div 10000 * LWnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBackwq>');

end;

//===============================元宝 


procedure _ZhuangBackybwq;
begin
    This_NPC.NpcDialog(This_Player,
    '|请把不要回收的武器衣服放在仓库，回收概不负责：\'
// +'|袄玛武器每个回收2元宝              <一键回收/@Womaybwq_all>\'
// +'|元宝回收祖玛武器              <一键回收/@ZUmaybwq_all>\'
   +'|元宝回收赤月武器             <一键回收/@ChiYueybwq_all>\'
   +'|元宝回收40级衣服武器            <一键回收/@40jiybwq_all>\'
   +'|元宝回收45级衣服武器            <一键回收/@45jiybwq_all>\'
   +'|元宝回收50级衣服武器            <一键回收/@50jiybwq_all>\'
   +'|元宝回收55级衣服武器           <一键回收/@55jiybwq_all>\'
   +'|元宝回收65级衣服武器           <一键回收/@65jiybwq_all>\'
   +'|元宝回收70级衣服武器           <一键回收/@70jiybwq_all>\'
   +'|{cmd}<退出/@doexit>'

    );

end;

procedure _Womaybwq_all;
var WMnum , i : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(100 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                WMnum := WMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(WMnum) + '件沃玛备，回收可获得:' + inttostr(getZybNumwuqi(1) * WMnum) + '元宝，你确定回收所有沃玛武器吗？\|'
    +'|{cmd}<确认回收所有沃玛武器/@Womaybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;

procedure  _Womaybwq_True;
var WMnum , i , j , itemNum : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                WMnum := WMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                

                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(1) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(1) * WMnum) + '元宝！\|'
    +'|{cmd}<返回/@ZhuangBackybwq>');

end;

///////////////////////////

procedure _ZUmaybwq_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(200 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) >= 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(ZMnum) + '件祖玛武器，回收可获得:' + inttostr(getZybNumwuqi(2) * ZMnum) + '元宝，你确定回收所有祖玛武器吗？\|'
    +'|{cmd}<确认回收所有祖玛武器/@ZUmaybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;

procedure  _ZUmaybwq_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                

                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(2) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(2) * ZMnum) + '元宝！\|'
    +'|{cmd}<返回/@ZhuangBackybwq>');

end;


procedure _Chiyueybwq_all;
var CYnum , i : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(300 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                CYnum := CYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(CYnum) + '件赤月武器，回收可获得:' + inttostr(getZybNumwuqi(3) * CYnum) + '元宝，你确定回收所有赤月武器吗？\|'
    +'|{cmd}<确认回收所有赤月武器/@Chiyueybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;

procedure  _Chiyueybwq_True;
var CYnum , i , j , itemNum : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(300 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                CYnum := CYnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(3) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(3) * CYnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackybwq>');

end;

procedure _40jiybwq_all;
var MLnum , i : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(400 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MLnum := MLnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MLnum) + '件魔龙武器，回收可获得:' + inttostr(getZybNumwuqi(4) * mlnum) + '元宝，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@40jiybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;

procedure  _40jiybwq_True;
var MLnum , i , j , itemNum : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(400 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MLnum := MLnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(4) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(4) * MLnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackybwq>');

end;

procedure _45jiybwq_all;
var QMnum , i : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(500 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                QMnum := QMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(QMnum) + '件45级衣服武器，回收可获得:' + inttostr(getZybNumwuqi(5) * QMnum) + '元宝，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@45jiybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;

procedure  _45jiybwq_True;
var QMnum , i , j , itemNum : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(500 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                QMnum := QMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(5) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(5) * QMnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackybwq>');

end;

procedure _50jiybwq_all;
var MSnum , i : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(600 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MSnum := MSnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MSnum) + '件50级衣服武器，回收可获得:' + inttostr(getZybNumwuqi(6) * MSnum) + '元宝，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@50jiybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;

procedure  _50jiybwq_True;
var MSnum , i , j , itemNum : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(600 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MSnum := MSnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(6) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(6) * MSnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackybwq>');

end;

procedure _55jiybwq_all;
var MYnum , i : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(700 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MYnum := MYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MYnum) + '件55级衣服武器，回收可获得:' + inttostr(getZybNumwuqi(7) * MYnum) + '元宝，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@55jiybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;

procedure  _55jiybwq_True;
var MYnum , i , j , itemNum : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(700 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MYnum := MYnum + itemNum;
                This_Player.Take(Iname, itemNum);

				

                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(7)*itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(7) * MYnum) + '元宝！\|'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;



procedure _65jiybwq_all;
var LWnum , i : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(800 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                LWnum := LWnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(LWnum) + '件65级衣服武器，回收可获得:' + inttostr(getZybNumwuqi(8) * LWnum) + '元宝，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@65jiybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;

procedure  _65jiybwq_True;
var LWnum , i , j , itemNum : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(800 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                LWnum := LWnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(8) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(8) * LWnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackybwq>');

end;

procedure _70jiybwq_all;
var Qsnum , i : integer;
Iname : string;
begin
    Qsnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(900 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                Qsnum := Qsnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(Qsnum) + '件70级衣服武器，回收可获得:' + inttostr(getZybNumwuqi(9) * Qsnum) + '元宝，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@70jiybwq_True>'
    +'|{cmd}<返回/@ZhuangBackybwq>');
end;


procedure  _70jiybwq_True;
var Qsnum , i , j , itemNum : integer;
Iname : string;
begin
    Qsnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(900 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                Qsnum := Qsnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNumwuqi(9) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNumwuqi(9) * Qsnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackybwq>');

end;




//============================金币 

procedure _ZhuangBackjbwq;
begin
    This_NPC.NpcDialog(This_Player,
    '|请把不要回收的武器放在仓库，回收概不负责：\'
  // +'|袄玛武器每个回收50000金币              <一键回收/@Womajbwq_all>\'
   +'|金币回收祖玛武器              <一键回收/@ZUmajbwq_all>\'
   +'|金币回收赤月衣服武器          <一键回收/@ChiYuejbwq_all>\'
   +'|金币回收40级衣服武器          <一键回收/@40jijbwq_all>\'
   +'|金币回收45级衣服武器          <一键回收/@45jijbwq_all>\'
   +'|金币回收50级衣服武器          <一键回收/@50jijbwq_all>\'
   +'|金币回收55级衣服武器          <一键回收/@55jijbwq_all>\'
   +'|金币回收65级衣服武器          <一键回收/@65jijbwq_all>\'
   +'|{cmd}<退出/@doexit>'

    );

end;

procedure _Womajbwq_all;
var WMnum , i : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(100 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                WMnum := WMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(WMnum) + '件沃玛备，回收可获得:' + inttostr(getZjbNumwuqi(1) * WMnum) + '金币，你确定回收所有沃玛武器吗？\|'
    +'|{cmd}<确认回收所有沃玛武器/@Womajbwq_True>'
    +'|{cmd}<返回/@ZhuangBackjbwq>');
end;

procedure  _Womajbwq_True;
var WMnum , i , j , itemNum : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                WMnum := WMnum + itemNum;
                This_Player.Take(Iname, itemNum);
              This_Player.AddGold(getZjbNumwuqi(1) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNumwuqi(1) * WMnum) + '金币！\|'
    +'|{cmd}<返回/@ZhuangBackjbwq>');

end;

///////////////////////////

procedure _ZUmajbwq_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(200 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) >= 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(ZMnum) + '件祖玛武器，回收可获得:' + inttostr(getZjbNumwuqi(2) * ZMnum) + '金币，你确定回收所有祖玛武器吗？\|'
    +'|{cmd}<确认回收所有祖玛武器/@ZUmajbwq_True>'
    +'|{cmd}<返回/@ZhuangBackjbwq>');
end;

procedure  _ZUmajbwq_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.AddGold(1000);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNumwuqi(2) * ZMnum) + '金币！\|'
    +'|{cmd}<返回/@ZhuangBackjbwq>');

end;


procedure _Chiyuejbwq_all;
var CYnum , i : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(300 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                CYnum := CYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(CYnum) + '件赤月衣服武器，回收可获得:' + inttostr(getZjbNumwuqi(3) * CYnum) + '金币，你确定回收所有赤月武器吗？\|'
    +'|{cmd}<确认回收/@Chiyuejbwq_True>'
    +'|{cmd}<返回/@ZhuangBackjbwq>');
end;

procedure  _Chiyuejbwq_True;
var CYnum , i , j , itemNum : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(300 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                CYnum := CYnum + itemNum;
                This_Player.Take(Iname, itemNum);
                 This_Player.AddGold(getZjbNumwuqi(3) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNumwuqi(3) * CYnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjbwq>');

end;


procedure _40jijbwq_all;
var MLnum , i : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(400 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MLnum := MLnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MLnum) + '件40级衣服武器，回收可获得:' + inttostr(getZjbNumwuqi(4) * mlnum) + '金币，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@40jijbwq_True>'
    +'|{cmd}<返回/@ZhuangBackjbwq>');
end;

procedure  _40jijbwq_True;
var MLnum , i , j , itemNum : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(400 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MLnum := MLnum + itemNum;
                This_Player.Take(Iname, itemNum);
                 This_Player.AddGold(getZjbNumwuqi(4) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNumwuqi(4) * MLnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjbwq>');

end;

procedure _45jijbwq_all;
var QMnum , i : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(500 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                QMnum := QMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(QMnum) + '件45级衣服武器，回收可获得:' + inttostr(getZjbNumwuqi(5) * QMnum) + '金币，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@45jijbwq_True>'
    +'|{cmd}<返回/@ZhuangBackjbwq>');
end;

procedure  _45jijbwq_True;
var QMnum , i , j , itemNum : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(500 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                QMnum := QMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                 This_Player.AddGold(getZjbNumwuqi(5) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNumwuqi(5) * QMnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjbwq>');

end;

procedure _50jijbwq_all;
var MSnum , i : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(600 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MSnum := MSnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MSnum) + '件50级衣服武器，回收可获得:' + inttostr(getZjbNumwuqi(6) * MSnum) + '金币，你确定回收吗？\|'
    +'|{cmd}<确认回收/@50jijbwq_True>'
    +'|{cmd}<返回/@ZhuangBackjbwq>');
end;

procedure  _50jijbwq_True;
var MSnum , i , j , itemNum : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(600 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MSnum := MSnum + itemNum;
                This_Player.Take(Iname, itemNum);
				
                 This_Player.AddGold(getZjbNumwuqi(6) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNumwuqi(6) * MSnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjbwq>');

end;

procedure _55jijbwq_all;
var MYnum , i : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(700 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MYnum := MYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MYnum) + '件55级衣服武器，回收可获得:' + inttostr(getZjbNumwuqi(7) * MYnum) + '金币，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@55jijbwq_True>'
    +'|{cmd}<返回/@ZhuangBackjbwq>');
end;

procedure  _55jijbwq_True;
var MYnum , i , j , itemNum : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(700 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MYnum := MYnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.AddGold(getZjbNumwuqi(7)*itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNumwuqi(7) * MYnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjbwq>');

end;

procedure _65jijbwq_all;
var LWnum , i : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(800 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                LWnum := LWnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(LWnum) + '件65级衣服武器，回收可获得:' + inttostr(getZjbNumwuqi(8) * LWnum) + '金币，你确定回收所有吗？\|'
    +'|{cmd}<确认回收/@65jijbwq_True>'
    +'|{cmd}<返回/@ZhuangBackjbwq>');
end;

procedure  _65jijbwq_True;
var LWnum , i , j , itemNum : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameByIdWQ(800 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                LWnum := LWnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.AddGold(getZjbNumwuqi(8)*itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNumwuqi(8) * LWnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjbwq>');

end;


//======================================
 ////////////////////////
procedure _Woma_all;
var WMnum , i : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                WMnum := WMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(WMnum) + '件沃玛备，回收可获得:' + inttostr(getZexpNum(1) div 10000 * WMnum) + '万经验，你确定回收所有沃玛装备吗？\|'
    +'|{cmd}<确认回收所有沃玛装备/@Woma_True>'
    +'|{cmd}<返回/@ZhuangBack>');
end;

procedure  _Woma_True;
var WMnum , i , j , itemNum : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                WMnum := WMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(1));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNum(1) div 10000 * WMnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBack>');

end;



procedure _ZUma_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) >= 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(ZMnum) + '件祖玛装备，回收可获得:' + inttostr(getZexpNum(2) div 10000 * ZMnum) + '万经验，你确定回收所有祖玛装备吗？\|'
    +'|{cmd}<确认回收所有祖玛装备/@ZUma_True>'
    +'|{cmd}<返回/@ZhuangBack>');
end;

procedure  _ZUma_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(2));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNum(2) div 10000 * ZMnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBack>');

end;


procedure _Chiyue_all;
var CYnum , i , ShengW : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                CYnum := CYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(CYnum) + '件赤月装备，回收可获得:' + inttostr(getZexpNum(3) div 10000 * CYnum) + '万经验，获得' + inttostr(CYnum * 10) + '声望。你确定回收所有赤月装备吗？\|'
    +'|{cmd}<确认回收所有赤月装备/@Chiyue_True>'
    +'|{cmd}<返回/@ZhuangBack>');
end;

procedure  _Chiyue_True;
var CYnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                CYnum := CYnum + itemNum;
                ShengW :=itemNum * 10;
                This_Player.Take(Iname, itemNum);
                for j := 1 to itemNum do
                This_Player.MyShengwan := This_Player.MyShengwan + 10
                This_Player.Give('经验', getZexpNum(3));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNum(3) div 10000 * CYnum) + '万经验！\|'
	+'恭喜你获得' + inttostr(ShengW) + '声望\|'
    +'|{cmd}<返回/@ZhuangBack>');

end;


procedure _40ji_all;
var MLnum , i : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MLnum := MLnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MLnum) + '件40级装备，回收可获得:' + inttostr(getZexpNum(4) div 10000 * mlnum) + '万经验，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@40ji_True>'
    +'|{cmd}<返回/@ZhuangBack>');
end;

procedure  _40ji_True;
var MLnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MLnum := MLnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(4));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNum(4) div 10000 * MLnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBack>');

end;

procedure _45ji_all;
var QMnum , i : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                QMnum := QMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(QMnum) + '件45级装备，回收可获得:' + inttostr(getZexpNum(5) div 10000 * QMnum) + '万经验，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@45ji_True>'
    +'|{cmd}<返回/@ZhuangBack>');
end;

procedure  _45ji_True;
var QMnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                QMnum := QMnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(5));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNum(5) div 10000 * QMnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBack>');

end;

procedure _50ji_all;
var MSnum , i : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MSnum := MSnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MSnum) + '件50级装备，回收可获得:' + inttostr(getZexpNum(6) div 10000 * MSnum) + '万经验，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@50ji_True>'
    +'|{cmd}<返回/@ZhuangBack>');
end;

procedure  _50ji_True;
var MSnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MSnum := MSnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
				//This_Player.Give('声望',1);
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(6));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNum(6) div 10000 * MSnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBack>');

end;

procedure _55ji_all;
var MYnum , i : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MYnum := MYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MYnum) + '件55级装备，回收可获得:' + inttostr(getZexpNum(7) div 10000 * MYnum) + '万经验，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@55ji_True>'
    +'|{cmd}<返回/@ZhuangBack>');
end;

procedure  _55ji_True;
var MYnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MYnum := MYnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
				//This_Player.Give('声望',1);
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(7));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNum(7) div 10000 * MYnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBack>');

end;

procedure _65ji_all;
var LWnum , i : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                LWnum := LWnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(LWnum) + '件65级装备，回收可获得:' + inttostr(getZexpNum(8) div 10000 * LWnum) + '万经验，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@65ji_True>'
    +'|{cmd}<返回/@ZhuangBack>');
end;

procedure  _65ji_True;
var LWnum , i , j , itemNum, ShengW : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                LWnum := LWnum + itemNum;
                This_Player.Take(Iname, itemNum);
				ShengW :=itemNum * 10;
				//This_Player.Give('声望',1);
                for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(8));
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZexpNum(8) div 10000 * LWnum) + '万经验！\|'
    +'|{cmd}<返回/@ZhuangBack>');

end;
//===============================元宝 


procedure _ZhuangBackyb;
begin
    This_NPC.NpcDialog(This_Player,
    '|请把不要回收的装备放在仓库，回收概不负责：\'
  // +'|元宝回收袄玛              <一键回收/@Womayb_all>\'
   //+'|祖玛首饰每个回收5元宝              <一键回收/@ZUmayb_all>\'
   +'|元宝回收赤月系列          <一键回收/@ChiYueyb_all>\'
   +'|元宝回收40级系列          <一键回收/@40jiyb_all>\'
   +'|元宝回收45级系列          <一键回收/@45jiyb_all>\'
   +'|元宝回收50级系列          <一键回收/@50jiyb_all>\'
   +'|元宝回收55级系列          <一键回收/@55jiyb_all>\'
   +'|元宝回收65级系列          <一键回收/@65jiyb_all>\'
   +'|元宝回收70级系列          <一键回收/@70jiyb_all>\'
   +'|{cmd}<退出/@doexit>'

    );

end;

procedure _Womayb_all;
var WMnum , i : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                WMnum := WMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(WMnum) + '件沃玛备，回收可获得:' + inttostr(getZybNum(1) * WMnum) + '元宝，你确定回收所有沃玛装备吗？\|'
    +'|{cmd}<确认回收所有沃玛装备/@Womayb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _Womayb_True;
var WMnum , i , j , itemNum : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                WMnum := WMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                

                This_Player.ScriptRequestAddYBNum(getZybNum(1) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(1) * WMnum) + '元宝！\|'
    +'|{cmd}<返回/@ZhuangBackyb>');

end;

///////////////////////////

procedure _ZUmayb_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) >= 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(ZMnum) + '件祖玛装备，回收可获得:' + inttostr(getZybNum(2) * ZMnum) + '元宝，你确定回收所有祖玛装备吗？\|'
    +'|{cmd}<确认回收所有祖玛装备/@ZUmayb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _ZUmayb_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                

                This_Player.ScriptRequestAddYBNum(getZybNum(2) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(2) * ZMnum) + '元宝！\|'
    +'|{cmd}<返回/@ZhuangBackyb>');

end;


procedure _Chiyueyb_all;
var CYnum , i : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                CYnum := CYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(CYnum) + '件赤月装备，回收可获得:' + inttostr(getZybNum(3) * CYnum) + '元宝，你确定回收所有赤月装备吗？\|'
    +'|{cmd}<确认回收所有赤月装备/@Chiyueyb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _Chiyueyb_True;
var CYnum , i , j , itemNum : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                CYnum := CYnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNum(3) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(3) * CYnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackyb>');

end;


procedure _40jiyb_all;
var MLnum , i : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MLnum := MLnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MLnum) + '件40级装备，回收可获得:' + inttostr(getZybNum(4) * mlnum) + '元宝，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@40jiyb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _40jiyb_True;
var MLnum , i , j , itemNum : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MLnum := MLnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNum(4) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(4) * MLnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackyb>');

end;

procedure _45jiyb_all;
var QMnum , i : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                QMnum := QMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(QMnum) + '件45级装备，回收可获得:' + inttostr(getZybNum(5) * QMnum) + '元宝，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@45jiyb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _45jiyb_True;
var QMnum , i , j , itemNum : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                QMnum := QMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNum(5) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(5) * QMnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackyb>');

end;

procedure _50jiyb_all;
var MSnum , i : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MSnum := MSnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MSnum) + '件50级装备，回收可获得:' + inttostr(getZybNum(6) * MSnum) + '元宝，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@50jiyb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _50jiyb_True;
var MSnum , i , j , itemNum : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MSnum := MSnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.ScriptRequestAddYBNum(getZybNum(6) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(6) * MSnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackyb>');

end;

procedure _55jiyb_all;
var MYnum , i : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MYnum := MYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MYnum) + '件55级装备，回收可获得:' + inttostr(getZybNum(7) * MYnum) + '元宝，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@55jiyb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _55jiyb_True;
var MYnum , i , j , itemNum : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MYnum := MYnum + itemNum;
                This_Player.Take(Iname, itemNum);

				

                This_Player.ScriptRequestAddYBNum(getZybNum(7)*itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(7) * MYnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackyb>');

end;

procedure _65jiyb_all;
var LWnum , i : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                LWnum := LWnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(LWnum) + '件65级装备，回收可获得:' + inttostr(getZybNum(8) * LWnum) + '元宝，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@65jiyb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _65jiyb_True;
var LWnum , i , j , itemNum : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                LWnum := LWnum + itemNum;
                This_Player.Take(Iname, itemNum);

				

                This_Player.ScriptRequestAddYBNum(getZybNum(8)*itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(8) * LWnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackyb>');

end;

procedure _70jiyb_all;
var Qsnum , i : integer;
Iname : string;
begin
    Qsnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(900 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                Qsnum := Qsnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(Qsnum) + '件70级装备，回收可获得:' + inttostr(getZybNum(9) * Qsnum) + '元宝，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@70jiyb_True>'
    +'|{cmd}<返回/@ZhuangBackyb>');
end;

procedure  _70jiyb_True;
var Qsnum , i , j , itemNum : integer;
Iname : string;
begin
    Qsnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(900 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                Qsnum := Qsnum + itemNum;
                This_Player.Take(Iname, itemNum);

				

                This_Player.ScriptRequestAddYBNum(getZybNum(9)*itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZybNum(9) * Qsnum) + '元宝！\|'

    +'|{cmd}<返回/@ZhuangBackyb>');

end;
//=============================金刚石 





//=============================金币 

procedure _ZhuangBackjb;
begin
    This_NPC.NpcDialog(This_Player,
    '|请把不要回收的装备放在仓库，回收概不负责：\'
   +'|金币回收沃玛系列              <一键回收/@Womajb_all>\'
   +'|金币回收祖玛系列              <一键回收/@ZUmajb_all>\'
   +'|金币回收赤月系列            <一键回收/@ChiYuejb_all>\'
   +'|金币回收40级系列            <一键回收/@40jijb_all>\'
   +'|金币回收45级系列          <一键回收/@45jijb_all>\'
   +'|金币回收50级系列          <一键回收/@50jijb_all>\'
   +'|金币回收55级系列          <一键回收/@55jijb_all>\'
   +'|金币回收65级系列          <一键回收/@65jijb_all>\'
   +'|{cmd}<退出/@doexit>'

    );

end;

procedure _Womajb_all;
var WMnum , i : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                WMnum := WMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(WMnum) + '件沃玛备，回收可获得:' + inttostr(getZjbNum(1) * WMnum) + '金币，你确定回收所有沃玛装备吗？\|'
    +'|{cmd}<确认回收所有沃玛装备/@Womajb_True>'
    +'|{cmd}<返回/@ZhuangBackjb>');
end;

procedure  _Womajb_True;
var WMnum , i , j , itemNum : integer;
Iname : string;
begin
    WMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                WMnum := WMnum + itemNum;
                This_Player.Take(Iname, itemNum);
              This_Player.AddGold(getZjbNum(1) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNum(1) * WMnum) + '金币！\|'
    +'|{cmd}<返回/@ZhuangBackjb>');

end;

///////////////////////////

procedure _ZUmajb_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) >= 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(ZMnum) + '件祖玛装备，回收可获得:' + inttostr(getZjbNum(2) * ZMnum) + '金币，你确定回收所有祖玛装备吗？\|'
    +'|{cmd}<确认回收所有祖玛装备/@ZUmajb_True>'
    +'|{cmd}<返回/@ZhuangBackjb>');
end;

procedure  _ZUmajb_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.AddGold(getZjbNum(2) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNum(2) * ZMnum) + '金币！\|'
    +'|{cmd}<返回/@ZhuangBackjb>');

end;


procedure _Chiyuejb_all;
var CYnum , i : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                CYnum := CYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(CYnum) + '件赤月装备，回收可获得:' + inttostr(getZjbNum(3) * CYnum) + '金币，你确定回收所有赤月装备吗？\|'
    +'|{cmd}<确认回收所有赤月装备/@Chiyuejb_True>'
    +'|{cmd}<返回/@ZhuangBackjb>');
end;

procedure  _Chiyuejb_True;
var CYnum , i , j , itemNum : integer;
Iname : string;
begin
    CYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                CYnum := CYnum + itemNum;
                This_Player.Take(Iname, itemNum);
                 This_Player.AddGold(getZjbNum(3) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNum(3) * CYnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjb>');

end;


procedure _40jijb_all;
var MLnum , i : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MLnum := MLnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MLnum) + '件40级装备，回收可获得:' + inttostr(getZjbNum(4) * mlnum) + '金币，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收所有魔龙装备/@40jijb_True>'
    +'|{cmd}<返回/@ZhuangBackjb>');
end;

procedure  _40jijb_True;
var MLnum , i , j , itemNum : integer;
Iname : string;
begin
    MLnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MLnum := MLnum + itemNum;
                This_Player.Take(Iname, itemNum);
                 This_Player.AddGold(getZjbNum(4) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNum(4) * MLnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjb>');

end;

procedure _45jijb_all;
var QMnum , i : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                QMnum := QMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(QMnum) + '件45级装备，回收可获得:' + inttostr(getZjbNum(5) * QMnum) + '金币，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@45jijb_True>'
    +'|{cmd}<返回/@ZhuangBackjb>');
end;

procedure  _45jijb_True;
var QMnum , i , j , itemNum : integer;
Iname : string;
begin
    QMnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                QMnum := QMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                 This_Player.AddGold(getZjbNum(5) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNum(5) * QMnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjb>');

end;

procedure _50jijb_all;
var MSnum , i : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MSnum := MSnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MSnum) + '件50级装备，回收可获得:' + inttostr(getZjbNum(6) * MSnum) + '金币，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@50jijb_True>'
    +'|{cmd}<返回/@ZhuangBackjb>');
end;

procedure  _50jijb_True;
var MSnum , i , j , itemNum : integer;
Iname : string;
begin
    MSnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MSnum := MSnum + itemNum;
                This_Player.Take(Iname, itemNum);
				
                 This_Player.AddGold(getZjbNum(6) * itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNum(6) * MSnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjb>');

end;

procedure _55jijb_all;
var MYnum , i : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                MYnum := MYnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(MYnum) + '件55级装备，回收可获得:' + inttostr(getZjbNum(7) * MYnum) + '金币，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@55jijb_True>'
    +'|{cmd}<返回/@ZhuangBackjb>');
end;

procedure  _55jijb_True;
var MYnum , i , j , itemNum : integer;
Iname : string;
begin
    MYnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                MYnum := MYnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.AddGold(getZjbNum(7)*itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNum(7) * MYnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjb>');

end;

procedure _65jijb_all;
var LWnum , i : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                LWnum := LWnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
    This_NPC.NpcDialog(This_Player,
    '你的包裹中有' + inttostr(LWnum) + '件55级装备，回收可获得:' + inttostr(getZjbNum(8) * LWnum) + '金币，你确定回收所有装备吗？\|'
    +'|{cmd}<确认回收/@65jijb_True>'
    +'|{cmd}<返回/@ZhuangBackjb>');
end;

procedure  _65jijb_True;
var LWnum , i , j , itemNum : integer;
Iname : string;
begin
    LWnum := 0;
    for i := 1 to 15 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                LWnum := LWnum + itemNum;
                This_Player.Take(Iname, itemNum);
                This_Player.AddGold(getZjbNum(8)*itemNum);
            end;
        end; 
    end;
    This_NPC.NpcDialog(This_Player,
    '恭喜你获得:' + inttostr(getZjbNum(8) * LWnum) + '金币！\|'

    +'|{cmd}<返回/@ZhuangBackjb>');

end;
   
Begin
//TZJH_NPC;
  domain;
end.