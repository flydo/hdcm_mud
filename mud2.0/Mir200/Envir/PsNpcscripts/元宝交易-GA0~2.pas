{********************************************************************

*******************************************************************}

program Mir2;

{$I common.pas}
{$I ActiveValidateCom.pas} 




procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;


procedure _newYB(); 
begin
    if This_Player.GetV(11,10) <> 888 then
	begin
		This_Player.SetV(11,10,888);
		This_NPC.YBDealDialogShowMode(This_Player,true);
	end else
	begin
		This_NPC.YBDealDialogShowMode(This_Player,false);
	end;
end;

Procedure _GetYB;
var 
d2,d3 : integer;
s1 , td : double;
begin
	s1 := GetNow;
	d2 := This_Player.GetS(23,3);
    td := ConvertDBToDateTime(d2);
	d3 := minusDataTime(s1,td);
	if d3 >= 3 then
	begin
		This_Player.SetS(23,3, ConvertDateTimeToDB(s1));
		This_Player.QueryAwardCode(This_Player.Name);
		This_Npc.CloseDialog(This_Player);
	end else
	   This_Npc.NpcDialog(This_Player,
	   '你操作太快了，请' + inttostr(3 - d3) + '秒后稍后再试！'
	   );
end;


Begin     
    This_Npc.NpcDialog(This_Player,
    '您好，有什么可以效劳的？我可以为您提供关于元宝的各类服务。\ \'
    +'|{cmd}<开始元宝交易/@NewYB>               ^<领取元宝/@GetYB>'
    );

end.