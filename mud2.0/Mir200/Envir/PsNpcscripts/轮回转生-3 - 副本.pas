{********************************************************************

*******************************************************************}
program mir2;

{$I common.pas}

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _1;
var dengji : Byte; 
begin
if This_Player.Level >= 60 then
begin
if This_Player.GetV(42,1) <= 0 then
begin
if This_Player.YBNum >= 1000 then
begin
if This_Player.GetBagItemCount('一转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('一转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(1000); 
This_Player.SetV(42,1,1);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有一转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经一转了！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;

procedure _2;
var dengji : Byte; 
begin
if This_Player.Level >= 65 then
begin
if This_Player.GetV(42,1) = 1 then
begin
if This_Player.YBNum >= 2000 then
begin
if This_Player.GetBagItemCount('二转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('二转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(2000); 
This_Player.SetV(42,1,2);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有二转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经二转了，或者还没一转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;


procedure _3;
var dengji : Byte; 
begin
if This_Player.Level >= 70 then
begin
if This_Player.GetV(42,1) = 2 then
begin
if This_Player.YBNum >= 5000 then
begin
if This_Player.GetBagItemCount('三转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('三转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(5000); 
This_Player.SetV(42,1,3);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有三转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经三转了，或者还没二转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;


procedure _4;
var dengji : Byte; 
begin
if This_Player.Level >= 75 then
begin
if This_Player.GetV(42,1) = 3 then
begin
if This_Player.YBNum >= 8000 then
begin
if This_Player.GetBagItemCount('四转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('四转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(8000); 
This_Player.SetV(42,1,4);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有四转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经四转了，或者还没三转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;

procedure _5;
var dengji : Byte; 
begin
if This_Player.Level >= 80 then
begin
if This_Player.GetV(42,1) = 4 then
begin
if This_Player.YBNum >= 12000 then
begin
if This_Player.GetBagItemCount('五转至尊令') >= 1 then
begin
dengji := This_Player.Level - 5;
This_Player.SetPlayerLevel(dengji); 
This_Player.Take('五转至尊令' , 1);
This_Player.ScriptRequestSubYBNum(12000); 
This_Player.SetV(42,1,5);
This_NPC.NpcDialog(This_Player,
        '转生成功！！！');
end else
This_Npc.NpcDialog(This_Player,
'你没有五转至尊令！。');
end else
This_Npc.NpcDialog(This_Player,
'你的元宝不足！。');
end else
This_Npc.NpcDialog(This_Player,
'你已经五转了，或者还没四转！！');
end else
This_Npc.NpcDialog(This_Player,
'你的等级不足');
end;




procedure _zhuansheng;
var zsdj : Integer; 
begin
   zsdj := This_Player.GetV(42,1);
     This_Npc.NpcDialog(This_Player,
       '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'+
   '|<一转需求:/c=red>一转至尊令+60级+1000元宝。转生后掉落5级。<我要1转/@1>\'+
   '|<二转需求:/c=red>二转至尊令+65级+2000元宝。转生后掉落5级。<我要2转/@2>\'+
   '|<三转需求:/c=red>三转至尊令+70级+5000元宝。转生后掉落5级。<我要3转/@3>\'+
   '|<四转需求:/c=red>四转至尊令+75级+8000元宝。转生后掉落5级。<我要4转/@4>\'+
   '|<五转需求:/c=red>五转至尊令+80级+12000元宝。转生后掉落5级。<我要5转/@5>\'+
   //'|<六转需求:/c=red>六转至尊令+80级+20000元宝。转生后掉落5级。<我要6转/@6>\'+
  // '|<七转需求:/c=red>七转至尊令+90级+30000元宝。转生后掉落5级。<我要7转/@7>\'+
   //'|<八转需求:/c=red>八转至尊令+100级+40000元宝。转生后掉落5级。<我要8转/@8>\'+
   '|<　　　　　　　　　　　　　　　　　　　　　/c=red>\'
    ); 
end;




procedure _shenli;
var zsdj : Integer; 
begin
   zsdj := This_Player.GetV(42,1);
   if This_Player.GetV(42,1) >= 1 then
   begin
     This_Npc.NpcDialog(This_Player,
       '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'+
   '|<在我这里可以获得转生神力加持！/c=125>\'+
   '|<属性加成：15+【转生等级*5】%（战士额外多10%）/c=125>\'+
   '|<持续时间2小时，没了加成，可在这里再次加持!/c=80>\'+
   '|<开启神力/@shenli2>　　　　　　　　　　　　\'
    ); 
	 end else
              This_Player.PlayerDialog(
              '你还没转生！\');  
end;


procedure _shenli2;
var shuxin,shuxin2,shuxin3,zsdj : integer;
begin
   zsdj := This_Player.GetV(42,1);
  if This_Player.Job = 0 then 
   begin
    shuxin2 := zsdj * 5
	shuxin3 := shuxin2 + 25
    shuxin := This_Player.MaxDC * shuxin3 / 100;
	This_Player.AddPlayerAbil(This_Player.Job, shuxin, 7199);
     ServerSay('玩家<' + This_Player.Name + '>开启了转生神力，攻击增加了' +
      inttostr(shuxin) + '点，顿时杀气冲天而起 ！ ', 5);
This_Player.PlayerDialog(
              '附加成功！！\'); 
   end;
   
   if This_Player.Job = 1 then 
   begin
    shuxin2 := zsdj * 5
	shuxin3 := shuxin2 + 15
    shuxin := This_Player.MaxMC * shuxin3 / 100;
	This_Player.AddPlayerAbil(This_Player.Job, shuxin, 7199);
	ServerSay('玩家<' + This_Player.Name + '>开启了转生神力，法力增加了' +
      inttostr(shuxin) + '点，顿时杀气冲天而起 ！ ', 5);
	  This_Player.PlayerDialog(
              '附加成功！！\'); 
   end;
   
   
   if This_Player.Job = 2 then 
   begin
    shuxin2 := zsdj * 5
	shuxin3 := shuxin2 + 15
    shuxin := This_Player.MaxSC * shuxin3 / 100;
	This_Player.AddPlayerAbil(This_Player.Job, shuxin, 7199);
	ServerSay('玩家<' + This_Player.Name + '>开启了转生神力，道术增加了' +
      inttostr(shuxin) + '点，顿时杀气冲天而起 ！ ', 5);
	  This_Player.PlayerDialog(
              '附加成功！！\'); 
   end;
   
            
end;






procedure _hecheng1; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 30 then    //检测物品
begin
if This_Player.GetV(42,1) >= 1 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 30);
This_Player.Give('一转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【一转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*30不足！！！  ');
end ;
end ;



procedure _hecheng2; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 60 then    //检测物品
begin
if This_Player.GetBagItemCount('一转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 2 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 60);
This_Player.Take('一转轮回靴' , 1);
This_Player.Give('二转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【二转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有一转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*60不足！！！  ');
end ;
end ;


procedure _hecheng3; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 120 then    //检测物品
begin
if This_Player.GetBagItemCount('二转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 3 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 120);
This_Player.Take('二转轮回靴' , 1);
This_Player.Give('三转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【三转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有二转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*120不足！！！  ');
end ;
end ;


procedure _hecheng4; 
begin
if This_Player.GetBagItemCount('轮回碎片')   >= 200 then    //检测物品
begin
if This_Player.GetBagItemCount('三转轮回靴')   >= 1 then    //检测物品
begin
if This_Player.GetV(42,1) >= 4 then
begin
if  This_Player.FreeBagNum >= 2 then
begin
This_Player.Take('轮回碎片' , 200);
This_Player.Take('三转轮回靴' , 1);
This_Player.Give('四转轮回靴' , 1);  //给与物品
This_Player.PlayerDialog(
              '合成成功！！\'); 
ServerSay('玩家<' + This_Player.Name + '>合成了【四转轮回靴】，大家恭喜他 ！ ', 5);
end else
begin
This_Npc.NpcDialog(This_Player,
'包裹不足2个空格！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'转生等级不足！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'没有二转轮回靴！！！  ');
end ;
end else
begin
This_Npc.NpcDialog(This_Player,
'轮回碎片*200不足！！！  ');
end ;
end ;


procedure _hecheng;
var zsdj : Integer; 
begin
   zsdj := This_Player.GetV(42,1);
   if This_Player.GetV(42,1) >= 1 then
   begin
     This_Npc.NpcDialog(This_Player,
       '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'+
   '|<在我这里可以合成轮回靴！不同轮回靴需要不同的转生等级！/c=125>\'+
   '|<一转轮回靴：需要30个【轮回碎片】/c=125>\<我要合成/@hecheng1>'+
   '|<二转轮回靴：需要60个【轮回碎片】+【一转轮回靴】/c=100>\             <我要合成/@hecheng2>'+
   '|<三转轮回靴：需要120个【轮回碎片】+【二转轮回靴】/c=150>\            <我要合成/@hecheng3>'+
   '|<四转轮回靴：需要200个【轮回碎片】+【三转轮回靴】/c=150>\            <我要合成/@hecheng4>'+
   '|<五转轮回靴：暂时不开通/c=200>\'+
   '　　　　　　　　　　　\'
    ); 
	 end else
              This_Player.PlayerDialog(
              '你还没转生！\');  
end;








var zsdj : Integer; 
begin 
zsdj := This_Player.GetV(42,1);
   This_NPC.NpcDialog(This_Player,
   '你好，你当前的转生等级为：' + inttostr(zsdj) + ' \ \'
	   +'|<【转生介绍】:/c=red>转生后，可以领取强大的属性附加.\'
	   +'|<【转生介绍】:/c=red>可以进入专属的转生地图，转生地图暴率更高！\'
	   +'|<【轮回靴介绍】:/c=red>转生后，可以获得专属轮回靴，获得强大的属性！！\'
	   +'|<我要转生/@zhuansheng>  ^<转生神力/@shenli> ^<轮回靴合成/@hecheng>\'
   );
end.    
