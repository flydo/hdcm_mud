{项链加幸运
作者：开心就好 and 孩子气
内容：项链升级幸运，每次鉴定请间隔5秒，待鉴定装备属性越高，鉴定消耗的元宝也越多,成功率越低
+1 99%成功 +2 50% +3 15%成功
}

program mir2;

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;
                                                                                                            
var
ck_name : array[1..12 ]of string;
ck_value : array[1..12] of integer; 

procedure OnInitialize; //这下面可以继续添加可以升级幸运的项链 龙皇霸气链(道)
begin
ck_name[1] :='龙皇霸气链(道)';
ck_name[2] :='龙皇霸气链(战)';
ck_name[3] :='龙皇霸气链(法)';
ck_name[4] :='不灭V神链(道)';
ck_name[5] :='不灭V神链(战)';
ck_name[6] :='不灭V神链(法)';
ck_name[7] :='辉煌V神链(战)';
ck_name[8] :='辉煌V神链(道)';
ck_name[9] :='辉煌V神链(法)';
ck_name[9] :='灭世神威链';
ck_name[10] :='凤凰の飞链';
ck_name[11] :='诅咒の链';
ck_name[12] :='浮屠の血链';


ck_value[1] :=500;  //这下面可以继续添加可以升级所需元宝或者修改
ck_value[2] :=500;
ck_value[3] :=500;
ck_value[4] :=600;
ck_value[5] :=600;
ck_value[6] :=600;
ck_value[7] :=800;
ck_value[8] :=800;
ck_value[9] :=800;
ck_value[10] :=900;
ck_value[11] :=1000;
ck_value[12] :=1500;
end; 

procedure _Checkup;
begin
  This_Npc.NpcDialog(This_Player,
  '|可以鉴定的装备：55级以上\'
  +'|升级成功增加：<幸运/c=red>\'
  +'|每次鉴定消耗一定数量的元宝：\'
  +'|<待鉴定装备属性越高，鉴定消耗的元宝也越多,成功率越低。/c=red>\'
  +'|<注意:每次鉴定请间隔5秒，如果多次点击没反应，请关掉NPC重新放入装备升级/c=red>\'
  +'|{cmd}<开始鉴定/@Checkup_1>        ^<关闭/@doexit>' 
  );
  
end; 

procedure _Checkup_1;
begin
   This_NPC.Click_CommitItem(This_Player,1,'待升级装备：'); 
end; 

procedure CommitItem(AType:word);
var
i,ck_num,ck_kind,ck_gold,ck_rand:integer;
ck_str,ck_red:string;
begin
     ck_gold := 0;  //初始化 
     ck_str := '';  //初始化
     ck_red := '';  //初始化
     
     for ck_kind := 1 to 12 do  //如果上面添加到了10个  则这里的3改成10
     begin
       if ck_name[ck_kind] = This_Item.ItemName then 
       begin                             
         ck_num := This_Item.AddPa2; 
         ck_gold := ck_value[ck_kind];
          if (ck_num > 0) and (ck_num < 12) then //有添加的话这里相应改到10，下同
         begin
           for i:= 1 to ck_num  do
           begin
           ck_gold := ck_gold*2;
           end;
         end;  
       end;
     end;
     
     if ck_num >= 4 then
     begin
     This_Player.NotifyClientCommitItem(0,'无法鉴定：你的'+This_Item.ItemName+'已激发出所有属性！');  
     end else
     if ck_gold > 0 then
     begin ck_rand := random(100)                                                                                       
       if This_Player.YBNum>= ck_gold then
        case ck_num of 
        0 : 
        begin  
         if ck_rand < 35then
            begin
         This_Player.PsYBConsum(This_NPC,'xin',20152,ck_gold,1);  
         This_Player.NotifyClientCommitItem(0,'升级失败，你的项链属性未发生变化');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：'); 
           end else
           if ck_rand < 99 then
            begin 
         This_Item.AddPa2 := This_Item.AddPa2 + 1;   //幸运+1
           This_Player.PsYBConsum(This_NPC,'xin',20153,ck_gold,1);  
         ck_str :='幸运+1';
         ck_red :='红字公告';
         This_Player.NotifyClientCommitItem(0,'升级成功：你的'+This_Item.ItemName+'提升到了'+ck_str+'！');
         This_NPC.NpcNotice('恭喜：'+This_Player.Name+'在幸运使者处把'+This_Item.ItemName+'的幸运提升到了'+ck_str+'！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：');
            end;
        end;
       1 : begin  
         if ck_rand < 75 then
            begin
         This_Player.PsYBConsum(This_NPC,'xin',20152,ck_gold,1);  
         This_Player.NotifyClientCommitItem(0,'升级失败，你的项链属性未发生变化');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：'); 
           end else
         if ck_rand < 99 then
         begin 
         This_Item.AddPa2 := This_Item.AddPa2 + 1;   //幸运+2
           This_Player.PsYBConsum(This_NPC,'xin',20153,ck_gold,1);  
         ck_str :='幸运+2';
         ck_red :='红字公告';
         This_Player.NotifyClientCommitItem(0,'升级成功：你的'+This_Item.ItemName+'提升到了'+ck_str+'！');
         This_NPC.NpcNotice('恭喜：'+This_Player.Name+'在幸运使者处把'+This_Item.ItemName+'的幸运提升到了'+ck_str+'！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：');
         end;
         end;
         2:begin  ck_rand := random(100);
      
           if ck_rand < 85 then
            begin
         This_Player.PsYBConsum(This_NPC,'xin',20152,ck_gold,1);  
         This_Player.NotifyClientCommitItem(0,'升级失败，你的项链属性未发生变化');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：'); 
           end else
           if ck_rand < 99 then
            begin 
         This_Item.AddPa2 := This_Item.AddPa2 + 1;   //幸运+3
         This_Player.PsYBConsum(This_NPC,'xin',20153,ck_gold,1);  
         ck_str :='幸运+3';
         ck_red :='红字公告';
         This_Player.NotifyClientCommitItem(0,'升级成功：你的'+This_Item.ItemName+'提升到了'+ck_str+'！');
         This_NPC.NpcNotice('恭喜：'+This_Player.Name+'在幸运使者处把'+This_Item.ItemName+'的幸运提升到了'+ck_str+'！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：');
            end;
         end;
      end else
       begin
         This_Player.NotifyClientCommitItem(0,'无法鉴定：你的元宝不足，需要'+inttostr(ck_gold)+'元宝。！'); 
       end;
      end else 
     begin
        This_Player.NotifyClientCommitItem(0,'该物品不可升级，请投入可升级的装备！');   
     end;     
  end;


function xin(price, num: Integer):boolean; 
begin
   result := true; 
 
end;


begin
  This_Npc.NpcDialog(This_Player,
  '|你好，在我这里可以升级项链属性。\'
  +'|只能升级55级以上的项链！。\'
  +'|升级也有几率失败！！\'
  +'                                                           \'
  +'|{cmd}<开始鉴定/@Checkup>\' 
  );
end.
   