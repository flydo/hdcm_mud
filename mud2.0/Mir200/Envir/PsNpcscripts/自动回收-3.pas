program mir2;



procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

Procedure domain;

var 
 AA,AB,AC,AD,AE,AF,AG,AH,AJ,AK,AO,AP : String; 
begin
if (This_Player.GetV(26,1)=1) and (This_Player.GetV(88,1)>0) then begin
   AA := '已开';
   end else begin 
   AA := '关闭';
   end;

if (This_Player.GetV(26,2)=1) and (This_Player.GetV(88,1)>0) then begin
   AB := '已开';
   end else begin 
   AB := '关闭';
   end;

if (This_Player.GetV(26,3)=1) and (This_Player.GetV(88,1)>0) then begin
   AC := '已开';
   end else begin 
   AC := '关闭';
   end;

if (This_Player.GetV(26,4)=1) and (This_Player.GetV(88,1)>0) then begin
   AD := '已开';
   end else begin 
   AD := '关闭';
   end;

if (This_Player.GetV(26,5)=1) and (This_Player.GetV(88,1)>0) then begin
   AE := '已开';
   end else begin 
   AE := '关闭';
   end;

if (This_Player.GetV(26,6)=1) and (This_Player.GetV(88,1)>0) then begin
   AF := '已开';
   end else begin 
   AF := '关闭';
   end;
if (This_Player.GetV(26,7)=1) and (This_Player.GetV(88,1)>0) then begin
   AG := '已开';
   end else begin 
   AG := '关闭';
   end;
if (This_Player.GetV(26,8)=1) and (This_Player.GetV(88,1)>0) then begin
   AH := '已开';
   end else begin 
   AH := '关闭';
   end;
   if (This_Player.GetV(26,9)=1) and (This_Player.GetV(88,1)>0) then begin
   AJ := '已开';
   end else begin 
   AJ := '关闭';
   end;
   if (This_Player.GetV(26,10)=1) and (This_Player.GetV(88,1)>0) then begin
   AK := '已开';
   end else begin 
   AK := '关闭';
   end;
   if (This_Player.GetV(26,11)=1) and (This_Player.GetV(88,1)>0) then begin
   AO := '已开';
   end else begin 
   AO := '关闭';
   end;
   if (This_Player.GetV(26,12)=1) and (This_Player.GetV(88,1)>0) then begin
   AP := '已开';
   end else begin 
   AP := '关闭';
   end;
		This_NPC.NpcDialog(This_Player,
		
		'|<祖玛系列:/c=red>每件0元宝+3W经验^                      <' + AA + '/SCOLOR=224><  开启/@kqmy1>'+
		'|<赤月系列:/c=red>每件12元宝+6W经验^                      <' + AB + '/SCOLOR=224><  开启/@kqmy2>'+
		'|<40级系列:/c=red>每件60元宝+15W经验^                      <' + AC + '/SCOLOR=224><  开启/@kqmy3>'+
		'|<45级系列:/c=red>每件100元宝+35W经验^                      <' + AD + '/SCOLOR=224><  开启/@kqmy4>'+
		'|<50级系列:/c=red>每件140元宝+80W经验^                      <' + AE + '/SCOLOR=224><  开启/@kqmy5>'+
		'|<55级系列:/c=red>每件300元宝+150W经验^                      <' + AF + '/SCOLOR=224><  开启/@kqmy6>'+
		'|<65级系列:/c=red>每件600元宝+300W经验^                      <' + AG + '/SCOLOR=224><  开启/@kqmy7>'+
        '|<70级系列:/c=red>每件1200元宝+400W经验^                      <' + AH + '/SCOLOR=224><  开启/@kqmy8>'+
        '|<80级系列:/c=red>每件2000元宝+700W经验^                      <' + AJ + '/SCOLOR=224><  开启/@kqmy9>'+
        '|<90级系列:/c=red>每件2800元宝+1000W经验^                      <' + AK + '/SCOLOR=224><  开启/@kqmy10>'+
        '|<100级系列:/c=red>每件3600元宝+1500W经验^                      <' + AO + '/SCOLOR=224><  开启/@kqmy11>'+
    //    '|<荣耀金牛系列:/c=red>每件4000元宝+1500W经验^                      <' + AP + '/SCOLOR=224><  开启/@kqmy12>'+
		'|<【刷新状态】/@main>^<【关闭所有】/@gmy>'
		);		 	 
end;

function getZBnameById(ZBid : integer) : string;
var ZBlv , ZBlvId : integer;

begin
    ZBlv := ZBid div 100;
    ZBlvId := ZBid mod 100;
    result := '';
    case ZBlv of
1 : 
        begin
            case ZBlvId of
                1 : result := '绿色项链';
                2 : result := '骑士手镯';
                3 : result := '力量戒指';
                4 : result := '恶魔铃铛';
                5 : result := '龙之手镯';
                6 : result := '紫碧螺';
                7 : result := '灵魂项链';
                8 : result := '三眼手镯';
                9 : result := '泰坦戒指';
                10 : result := '黑铁头盔';
                11 : result := '青铜腰带';
                12 : result := '紫绸靴';
				13 : result := '龙纹剑';
                14 : result := '骨玉权杖';
                15 : result := '裁决之杖';
            end;
        end;
        2 : 
        begin
            case ZBlvId of
                1 : result := '圣战头盔';
                2 : result := '圣战项链';
                3 : result := '圣战手镯';
                4 : result := '圣战戒指';
                5 : result := '法神头盔';
                6 : result := '法神项链';
                7 : result := '法神手镯';
                8 : result := '法神戒指';
                9 : result := '天尊头盔';
                10 : result := '天尊项链';
                11 : result := '天尊手镯';
                12 : result := '天尊戒指';
                13 : result := '钢铁腰带';
                14 : result := '避魂靴';
				15 : result := '逍遥扇';
                16 : result := '屠龙';
                17 : result := '嗜魂法杖';
				18 : result := '天魔神甲';
				19 : result := '天尊道袍';
				20 : result := '圣战宝甲';
				21 : result := '霓裳羽衣';
				22 : result := '天师长袍';
				23 : result := '法神披风';
            end;
        end;
		        3 : 
        begin
            case ZBlvId of
                1 : result := '勇士头盔';
                2 : result := '魔幻项链';
                3 : result := '魔幻手镯';
                4 : result := '魔幻戒指';
                5 : result := '骷髅骨';
                6 : result := '道域头盔';
                7 : result := '白银项链';
                8 : result := '白银手镯';
                9 : result := '白银戒指';
                10 : result := '吉娃娃';
                11 : result := '宙神头盔';
                12 : result := '浮游手镯';
                13 : result := '浮游戒指';
                14 : result := '浮游项链';
                15 : result := '情人花';
				16 : result := '圣龙魔袍(男)';
                17 : result := '圣龙魔袍(女)';
                18 : result := '白玉法杖';
                19 : result := '泰坦道衣(男)';
				20 : result := '泰坦道衣(女)';
				21 : result := '黑铁银蛇';
				22 : result := '钢盔战甲(男)';
				23 : result := '钢盔战甲(女)';
				24 : result := '绿玉裁决';
            end;
        end;
	   4 : 
        begin
            case ZBlvId of
                1 : result := '朱雀头盔';
                2 : result := '摄魂项链';
                3 : result := '摄魂手镯';
                4 : result := '摄魂戒指';
                5 : result := '魔法勋章';
                6 : result := '贝迪头盔';
                7 : result := '道尊项链';
                8 : result := '道尊手镯';
                9 : result := '道尊戒指';
                10 : result := '真爱钻石';
                11 : result := '黑暗头盔';
                12 : result := '铁血项链';
                13 : result := '铁血手镯';
                14 : result := '铁血戒指';
                15 : result := '灵魂钻石';
				16 : result := '朱雀神袍(男)';
                17 : result := '朱雀神袍(女)';
                18 : result := '蓝灵法杖';
                19 : result := '青龙道衣(男)';
				20 : result := '青龙道衣(女)';
				21 : result := '金域无极';
				22 : result := '白虎战甲(女)';
				23 : result := '白虎战甲(男)';
				24 : result := '绿玉屠龙';
		   end;
		   end;
		  5 : 
        begin
            case ZBlvId of
                1 : result := '风铃火山';
                2 : result := '魔天项链';
                3 : result := '魔天手镯';
                4 : result := '魔天戒指';
                5 : result := '卦';
                6 : result := '蓝灵头盔';
                7 : result := '蓝灵项链';
                8 : result := '蓝灵手环';
                9 : result := '蓝灵戒指';
                10 : result := '凤';
				11 : result := '将军头盔';
				12 : result := '将军项链';
				13 : result := '将军手镯';
				14 : result := '将军戒指';
				15 : result := '龙';
				16 : result := '猩猩魔袍(男)';
                17 : result := '猩猩魔袍(女)';
                18 : result := '摄魂之剑';
                19 : result := '麒麟道衣(男)';
				20 : result := '麒麟道衣(女)';
				21 : result := '仙人蒲扇';
				22 : result := '铁骑战甲(男)';
				23 : result := '铁骑战甲(女)';
				24 : result := '战神之刃';
		   end;
		   end;
		 6 : 
        begin
            case ZBlvId of
                1 : result := '龙皇霸气戒(战)';
                2 : result := '龙皇霸气戒(法)';
                3 : result := '龙皇霸气戒(道)';
                4 : result := '龙皇霸气镯(战)';
                5 : result := '龙皇霸气镯(法)';
                6 : result := '龙皇霸气镯(道)';
                7 : result := '龙皇霸气链(战)';
                8 : result := '龙皇霸气链(法)';
                9 : result := '龙皇霸气链(道)';
                10 : result := '龙皇霸气盔(战)';
				11 : result := '龙皇霸气盔(法)';
				12 : result := '龙皇霸气盔(道)';
				13 : result := '龙皇霸气心';
				14 : result := '龙皇霸龙甲(法)';
                15 : result := '龙皇霸凤铠(法)';
                16 : result := '龙皇霸气法剑';
				17 : result := '龙皇霸龙甲(道)';
                18 : result := '龙皇霸凤铠(道)';
                19 : result := '龙皇霸气道剑';
				20 : result := '龙皇霸龙甲(战)';
                21 : result := '龙皇霸凤铠(战)';
                22 : result := '龙皇霸气战剑';
		   end;
		   end;
		   7 : 
        begin
            case ZBlvId of
                1 : result := '不灭V神戒(战)';
                2 : result := '不灭V神镯(战)';
                3 : result := '不灭V神盔(战)';
                4 : result := '不灭V神链(战)';
                5 : result := '不灭V神灵';
				6 : result := '不灭V神戒(道)';
                7 : result := '不灭V神镯(道)';
                8 : result := '不灭V神链(道)';
                9 : result := '不灭V神盔(道)';
                10 : result := '不灭V神戒(法)';
				11 : result := '不灭V神镯(法)';
				12 : result := '不灭V神链(法)';
				13 : result := '不灭V神盔(法)';
				14 : result := '不灭V冰火战刀';
                15 : result := '不灭V紫神甲(战)';
                16 : result := '不灭V紫神铠(战)';
				17 : result := '不灭V冰火道刀';
                18 : result := '不灭V紫神甲(道)';
                19 : result := '不灭V紫神铠(道)';
				20 : result := '不灭V冰火法刀';
                21 : result := '不灭V紫神甲(法)';
                22 : result := '不灭V紫神铠(法)';
				
		   end;
		   end;
		   8 : 
        begin
            case ZBlvId of
                1 : result := '辉煌V神戒(战)';
                2 : result := '辉煌V神镯(战)';
                3 : result := '辉煌V神链(战)';
                4 : result := '辉煌V神冠(战)';
                5 : result := '辉煌神魄心';
				6 : result := '辉煌V神戒(道)';
                7 : result := '辉煌V神镯(道)';
                8 : result := '辉煌V神链(道)';
                9 : result := '辉煌V神冠(道)';
                10 : result := '辉煌V神戒(法)';
				11 : result := '辉煌V神镯(法)';
				12 : result := '辉煌V神链(法)';
				13 : result := '辉煌V神冠(法)';
				14 : result := '辉煌炽火战剑';
                15 : result := '辉煌寒冰甲(战)';
                16 : result := '辉煌寒冰铠(战)';
				17 : result := '辉煌炽火法剑';
                18 : result := '辉煌炽火道剑';
                19 : result := '辉煌寒冰甲(道)';
				20 : result := '辉煌寒冰甲(法)';
                21 : result := '辉煌寒冰铠(道)';
                22 : result := '辉煌寒冰铠(法)';
		   end;
		   end;
		   9 : 
        begin
            case ZBlvId of
				1 : result := '灭世神威戒';
                2 : result := '灭世神威镯';
                3 : result := '灭世神威链';
                4 : result := '灭世神威盔';
                5 : result := '灭世神威勋';
                6 : result := '灭世天神掌';
                7 : result := '灭世蝴蝶甲';
				8 : result := '灭世蝴蝶铠';

				
		   end;
		   end;
		   10 : 
        begin
            case ZBlvId of
				1 : result := '凤凰の飞戒';
                2 : result := '凤凰の飞镯';
                3 : result := '凤凰の飞链';
                4 : result := '凤凰の飞盔';
                5 : result := '凤凰の飞勋';
                6 : result := '破馆珍剑';
                7 : result := '凤凰の飞甲';
				8 : result := '凤凰の飞铠';
		   end;
		   end;
		   11 : 
        begin
            case ZBlvId of
                1 : result := '诅咒の戒';
                2 : result := '诅咒の镯';
                3 : result := '诅咒の链';
                4 : result := '诅咒の盔';
                5 : result := '诅咒の勋';
                6 : result := '魅影之刃';
                7 : result := '诅咒の甲'; 		
                8 : result := '诅咒の铠';
	   								
		   end;
		   end;
		   12 : 
        begin
            case ZBlvId of
                1 : result := '荣耀神剑';
                2 : result := '荣耀神甲(男)';
                3 : result := '荣耀神甲(女)';
                4 : result := '金牛战剑';
                5 : result := '金牛道扇';
                6 : result := '金牛魔杖';
                7 : result := '金牛战甲(男)';
                8 : result := '金牛战甲(女)';
                9 : result := '金牛魔衣(男)';
               10 : result := '金牛魔衣(女)';
               11 : result := '金牛道袍(男)';
               12 : result := '金牛道袍(女)';
				
		   end;
		   end;
    end;
end;



function getZexpNum(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 30000;
        2 : result := 60000;
        3 : result := 150000;
	    4 : result := 350000;
        5 : result := 800000;
        6 : result := 1500000;
		7 : result := 3000000;
        8 : result := 4000000;
	    9 : result := 7000000;
       10 : result := 10000000;
       11 : result := 15000000;
	   12 : result := 15000000;

    end;
end;

function getZYBNum(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 0;
        2 : result := 12;
        3 : result := 60;
	    4 : result := 100;
        5 : result := 140;
        6 : result := 300;
		7 : result := 600;
        8 : result := 1200;
	    9 : result := 2000;
       10 : result := 2800;
       11 : result := 3600;
	   12 : result := 4000;
    end;
end;

procedure  zdhs1();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
				                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(1));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(1));								
            end;
        end; 
    end;
end;
end;

procedure  zdhs2();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(2));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(2));								
            end;
        end; 
    end;
end;
end;

procedure  zdhs3();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(3));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(3));								
            end;
        end; 
    end;
end;
end;

procedure  zdhs4();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(4));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(4));								
            end;
        end; 
    end;
end;
end;

procedure  zdhs5();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(5));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(5));								
            end;
        end; 
    end;
end;
end;

procedure  zdhs6();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(6));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(6));								
            end;
        end; 
    end;
end;
end;
procedure  zdhs7();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(7));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(7));								
            end;
        end; 
    end;
end;
end;
procedure  zdhs8();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(8));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(8));								
            end;
        end; 
    end;
end;
end;
procedure  zdhs9();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(900 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(9));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(9));								
            end;
        end; 
    end;
end;
end;

procedure  zdhs10();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(1000 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(9));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(9));								
            end;
        end; 
    end;
end;
end;
	
procedure  zdhs11();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(1100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(9));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(9));								
            end;
        end; 
    end;
end;
end;
	
procedure  zdhs12();
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 50 do
    begin
        Iname := getZBnameById(1200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(9));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(9));								
            end;
        end; 
    end;
end;
end;
	
procedure _kqmy1;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,1,1);
		This_Player.SetV(88,1,1);
		This_Player.CallOut(This_NPC,1,'zdmyk1');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
end;

procedure zdmyk1;
begin
	if This_Player.GetV(88,1) = 1 then
		begin
		zdhs1();
		This_Player.CallOut(This_NPC,1,'zdmyk1');
	end	
end;


procedure _kqmy2;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,2,1);
		This_Player.SetV(88,1,2);
		This_Player.CallOut(This_NPC,1,'zdmyk2');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
end;		


 procedure bianse;
var color : Integer;
begin
	color := random(256);
begin

	This_Player.SetS(1,2,color);//设置名字颜色
end;
   This_Player.CallOut(This_Npc,1,'bianse');

   end;


procedure zdmyk2;
begin
	if This_Player.GetV(88,1) = 2 then
		begin
		zdhs1();
		zdhs2();
		This_Player.CallOut(This_NPC,1,'zdmyk2');
	end	
end;


procedure _kqmy3;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,3,1);
		This_Player.SetV(88,1,3);
		This_Player.CallOut(This_NPC,1,'zdmyk3');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
		end;
 
procedure zdmyk3;
begin
	if This_Player.GetV(88,1) = 3 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		This_Player.CallOut(This_NPC,1,'zdmyk3');
	end	
end;


procedure _kqmy4;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,4,1);
		This_Player.SetV(88,1,4);
		This_Player.CallOut(This_NPC,1,'zdmyk4');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
		end;
 
procedure zdmyk4;
begin
	if This_Player.GetV(88,1) = 4 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		This_Player.CallOut(This_NPC,1,'zdmyk4');
	end	
end;


procedure _kqmy5;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,5,1);
		This_Player.SetV(88,1,5);
		This_Player.CallOut(This_NPC,1,'zdmyk5');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);		
		end;

procedure zdmyk5;
begin
	if This_Player.GetV(88,1) = 5 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		zdhs5();
		This_Player.CallOut(This_NPC,1,'zdmyk5');
	end	
end;


procedure _kqmy6;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,6,1);
		This_Player.SetV(88,1,6);
		This_Player.CallOut(This_NPC,1,'zdmyk6');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
end;		

procedure zdmyk6;
begin
	if This_Player.GetV(88,1) = 6 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		zdhs5();
		zdhs6();
		This_Player.CallOut(This_NPC,1,'zdmyk6');
	end	
end;


procedure _kqmy7;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,7,1);
		This_Player.SetV(88,1,7);
		This_Player.CallOut(This_NPC,1,'zdmyk7');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
		end;

procedure zdmyk7;
begin
	if This_Player.GetV(88,1) = 7 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		zdhs5();
		zdhs6();
		zdhs7();
		This_Player.CallOut(This_NPC,1,'zdmyk7');
	end	
end;

procedure _kqmy8;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,8,1);
		This_Player.SetV(88,1,8);
		This_Player.CallOut(This_NPC,1,'zdmyk8');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
		end;

procedure zdmyk8;
begin
	if This_Player.GetV(88,1) = 8 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		zdhs5();
		zdhs6();
		zdhs7();
		zdhs8();
		This_Player.CallOut(This_NPC,1,'zdmyk8');
	end	
end;

procedure _kqmy9;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,9,1);
		This_Player.SetV(88,1,9);
		This_Player.CallOut(This_NPC,1,'zdmyk9');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
		end;

procedure zdmyk9;
begin
	if This_Player.GetV(88,1) = 9 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		zdhs5();
		zdhs6();
		zdhs7();
		zdhs8();
		zdhs9();
		This_Player.CallOut(This_NPC,1,'zdmyk9');
	end	
end;

procedure _kqmy10;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,10,1);
		This_Player.SetV(88,1,10);
		This_Player.CallOut(This_NPC,1,'zdmyk10');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
		end;

procedure zdmyk10;
begin
	if This_Player.GetV(88,1) = 10 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		zdhs5();
		zdhs6();
		zdhs7();
		zdhs8();
		zdhs9();
		zdhs10();
		This_Player.CallOut(This_NPC,1,'zdmyk10');
	end	
end;

procedure _kqmy11;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,11,1);
		This_Player.SetV(88,1,11);
		This_Player.CallOut(This_NPC,1,'zdmyk11');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
		end;

procedure zdmyk11;
begin
	if This_Player.GetV(88,1) = 11 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		zdhs5();
		zdhs6();
		zdhs7();
		zdhs8();
		zdhs9();
		zdhs10();
		zdhs11();
		This_Player.CallOut(This_NPC,1,'zdmyk11');
	end	
end;

procedure _kqmy12;
begin
	if This_Player.GetBagItemCount ('回收信物') >= 1 then
		begin
		This_Player.SetV(26,12,1);
		This_Player.SetV(88,1,12);
		This_Player.CallOut(This_NPC,1,'zdmyk12');
		This_NPC.NpcDialog(This_Player,
		'自动回收已开启。<默认开启自动回收/c=red>\ \'+
		'|{cmd}<【返回上页】/@main>'
		);
		end else
	 	This_Npc.NpcDialog(This_Player,
	   '|<包裹内未检测到物品[回收信物]无法使用此功能/c=254>\'+
       '|\'+
       '|<注意:土豪礼包领取物品[回收信物]，方可使用/c=254> \'
	   
		);	
		end;

procedure zdmyk12;
begin
	if This_Player.GetV(88,1) = 12 then
		begin
		zdhs1();
		zdhs2();
		zdhs3();
		zdhs4();
		zdhs5();
		zdhs6();
		zdhs7();
		zdhs8();
		zdhs9();
		zdhs10();
		zdhs11();
		zdhs12();
		This_Player.CallOut(This_NPC,1,'zdmyk12');
	end	
end;

procedure _gmy;
begin
		This_Player.SetV(26,1,0);
		This_Player.SetV(26,2,0);
		This_Player.SetV(26,3,0);
		This_Player.SetV(26,4,0);
		This_Player.SetV(26,5,0);
		This_Player.SetV(26,6,0);
		This_Player.SetV(26,7,0);
        This_Player.SetV(26,8,0);
        This_Player.SetV(26,9,0);
        This_Player.SetV(26,10,0);
        This_Player.SetV(26,11,0);
        This_Player.SetV(26,12,0);
		This_Player.SetV(88,1,0);
end;

begin
  domain;
end.