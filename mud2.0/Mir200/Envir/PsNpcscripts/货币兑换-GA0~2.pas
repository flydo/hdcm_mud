program Mir2;
Procedure _doexit;
begin
This_Npc.CloseDialog(This_Player);
end;


procedure _1; 
begin
if This_Player.YBNum >= 10000 then
begin 
if  This_Player.FreeBagNum >= 1 then     //检测背包
begin
This_Player.ScriptRequestSubYBNum(10000);
This_Player.Give('10000元宝' , 1);
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'背包空格数量不足1！！！  ');
end ; 
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'你的元宝不足1万！！！  ');
end ; 
end ; 

procedure _2; 
begin
if This_Player.GoldNum >= 5000000 then
begin 
if  This_Player.FreeBagNum >= 1 then     //检测背包
begin
This_Player.DecGold(5000000);
This_Player.Give('金砖' , 1);
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'背包空格数量不足1！！！  ');
end ; 
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'你的金币不足500万！！！  ');
end ; 
end ;


procedure _3; 
begin
if This_Player.GoldNum >= 10000000 then
begin 
if  This_Player.FreeBagNum >= 1 then     //检测背包
begin
This_Player.DecGold(10000000);
This_Player.Give('金盒' , 1);
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'背包空格数量不足1！！！  ');
end ; 
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'你的金币不足1000万！！！  ');
end ; 
end ;

procedure _4; 
begin
if This_Player.YBNum >= 200 then
begin 
if  This_Player.FreeBagNum >= 1 then     //检测背包
begin
This_Player.ScriptRequestSubYBNum(200);
This_Player.AddLF(0,100);
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'背包空格数量不足1！！！  ');
end ; 
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'你的元宝不足200！！！  ');
end ; 
end ;

procedure _5; 
begin
if This_Player.YBNum >= 2000 then
begin 
if  This_Player.FreeBagNum >= 1 then     //检测背包
begin
This_Player.ScriptRequestSubYBNum(2000);
This_Player.AddLF(0,1000);
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'背包空格数量不足1！！！  ');
end ; 
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'你的元宝不足1万！！！  ');
end ; 
end ;

procedure _6; 
begin
if This_Player.MyLFnum >= 1000 then
begin 
if  This_Player.FreeBagNum >= 1 then     //检测背包
begin
This_Player.DecLF(0, 1000, false);
This_Player.Give('金刚石' , 100);
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'背包空格数量不足1！！！  ');
end ; 
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'你的灵符不足1000！！！  ');
end ; 
end ;

procedure _7; 
begin
if This_Player.GetBagItemCount('金砖') >= 1 then    //检测物品
begin
This_Player.Take('金砖' , 1);
This_Player.AddGold(5000000);
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'你没有金砖，想蒙骗我啊！！！  ');
end ; 
end ;


procedure _8; 
begin
if This_Player.GetBagItemCount('金盒') >= 1 then    //检测物品
begin
This_Player.Take('金盒' , 1);
This_Player.AddGold(10000000);
end else  //检测提示 
begin
This_Npc.NpcDialog(This_Player,
'你没有金盒，想蒙骗我啊！！！  ');
end ; 
end ;


begin
       This_Npc.NpcDialog( This_Player,
  '你要兑换货币吗，我这里可以兑换各种货币,\'+
  
  '|{cmd}<兑换金砖/@2>\'+
  '|{cmd}<金砖兑换金币/@7>    ^<金盒兑换金币/@8>\' 
    );
end.