{

/************************************************************************}


//战神引擎装备回收元宝+经验完整脚本
//作者:by佰盛,QQ：10302235(更多脚本定制请咨询)，战神交流群93394989





PROGRAM Mir2;



{$I common.pas}



Procedure _doexit;

begin

   This_Npc.CloseDialog(This_Player);

end;



procedure domain;

begin

  This_Npc.NpcDialog(This_Player,

   '看来你需要我的帮助，\'+

   '我这里可以回收祖玛以上的装备。\'+

   '如果你有不要的装备，我这里可以给你兑换成经验+元宝\ \'+

   '|{cmd}<装备回收/@ZhuangBack>\'+

   '|{cmd}<退出/@doexit>');

end;



function getZBnameById(ZBid : integer) : string;

var ZBlv , ZBlvId : integer;

begin

    ZBlv := ZBid div 100;

    ZBlvId := ZBid mod 100;

    result := '';

    case ZBlv of

        1 : 

        begin

            case ZBlvId of

                1 : result := '绿色项链';

                2 : result := '骑士手镯';

                3 : result := '力量戒指';

                4 : result := '恶魔铃铛';

                5 : result := '龙之手镯';

                6 : result := '紫碧螺';

                7 : result := '灵魂项链';

                8 : result := '三眼手镯';

                9 : result := '泰坦戒指';

                10 : result := '黑铁头盔';

                11 : result := '青铜腰带';

                12 : result := '紫绸靴';

				13 : result := '骨玉权杖';

				14 : result := '裁决之杖';

				15 : result := '龙纹剑';

				16 : result := '斗笠43号';

				17 : result := '斗笠44号';

				18 : result := '斗笠45号';

				19 : result := '荣誉勋章43号';

				20 : result := '荣誉勋章44号';

				21 : result := '荣誉勋章45号';

            end;

        end;

		

        2 : 

        begin

            case ZBlvId of

                1 : result := '圣战头盔';

                2 : result := '圣战项链';

                3 : result := '圣战手镯';

                4 : result := '圣战戒指';

                5 : result := '法神头盔';

                6 : result := '法神项链';

                7 : result := '法神手镯';

                8 : result := '法神戒指';

                9 : result := '天尊头盔';

                10 : result := '天尊项链';

                11 : result := '天尊手镯';

                12 : result := '天尊戒指';

                13 : result := '钢铁腰带';

                14 : result := '避魂靴';

				15 : result := '霓裳羽衣';

                16 : result := '天师长袍';

                17 : result := '圣战宝甲';

                18 : result := '天魔神甲';

                19 : result := '法神披风';

                20 : result := '天尊道袍';



            end;

        end;

		

		

		        3 : 

        begin

            case ZBlvId of

                1 : result := '宙神头盔';
				2 : result := '勇士头盔';
				3 : result := '道域头盔';
				4 : result := '浮游项链';
				5 : result := '魔幻项链';
				6 : result := '白银项链';
	            7 : result := '浮游手镯';
                8 : result := '魔幻手镯';
                9 : result := '白银手镯';
                10 : result := '浮游戒指';
                11 : result := '魔幻戒指';				
				12 : result := '白银戒指';
				13 : result := '情人花';
				14 : result := '骷髅骨';
				15 : result := '吉娃娃';
				16 : result := '绿玉裁决';
				17 : result := '白玉法杖';
				18 : result := '黑铁银蛇';
				19 : result := '钢盔战甲';
				20 : result := '圣龙魔袍';
				21 : result := '泰坦道衣';
            end;

        end;

       

	   4 : 

        begin

            case ZBlvId of

                1 : result := '黑暗头盔';
				2 : result := '朱雀头盔';
				3 : result := '贝迪头盔';
				4 : result := '铁血项链';
				5 : result := '摄魂项链';
				6 : result := '道尊项链';
	            7 : result := '铁血手镯';
                8 : result := '摄魂手镯';
                9 : result := '道尊手镯';
                10 : result := '铁血戒指';
                11 : result := '摄魂戒指';
				12 : result := '道尊戒指';
				13 : result := '灵魂勋章';
				14 : result := '魔法勋章';
				15 : result := '真爱勋章';
				16 : result := '绿玉屠龙';
				17 : result := '蓝灵法杖';
				18 : result := '金域无极';
				19 : result := '白虎战甲';
				20 : result := '朱雀神袍';
				21 : result := '青龙道衣';
		   end;

		   end;

		 

		 

		  5 : 

        begin

            case ZBlvId of

				1 : result := '将军头盔';
				2 : result := '风铃火山';
				3 : result := '蓝灵头盔';
				4 : result := '将军项链';
				5 : result := '魔天项链';
				6 : result := '蓝灵项链';
	            7 : result := '将军手镯';
                8 : result := '魔天手镯';
                9 : result := '蓝灵手环';
                10 : result := '将军戒指';
                11 : result := '魔天戒指';
				12 : result := '蓝灵戒指';
				13 : result := '龙';
				14 : result := '凤';
				15 : result := '卦';
				16 : result := '战神之刃';
				17 : result := '摄魂之杖';
				18 : result := '仙人蒲扇';
				19 : result := '铁骑战甲';
				20 : result := '猩猩魔袍';
				21 : result := '麒麟道衣';


		   end;

		   end;

		   



		   

		 6 : 

        begin

            case ZBlvId of

                1 : result := '毁灭战盔';
				2 : result := '破魔禁忌';
				3 : result := '圣者神冠';
				4 : result := '毁灭神扣';
				5 : result := '破魔臣链';
				6 : result := '圣者仁心';
	            7 : result := '毁灭护腕';
                8 : result := '破魔封印';
                9 : result := '圣者臂环';
                10 : result := '毁灭の戒';
                11 : result := '破魔の戒';
				12 : result := '圣者の戒';
				13 : result := '十方俱灭';
				14 : result := '四大皆空';
				15 : result := '三界无敌';
				16 : result := '毁灭之刃';
				17 : result := '破魔叛决';
				18 : result := '圣者之剑';
				19 : result := '毁天灭地';
				20 : result := '破妖除魔';
				21 : result := '圣者无敌';

		   end;

		   end;

		   

    end;

end;



function getZexpNum(Zlv : integer) : integer;

begin

    case Zlv of

        1 : result := 20000;

        2 : result := 80000;

        3 : result := 160000;

		4 : result := 320000;

        5 : result := 640000;

        6 : result := 1280000;
		
        
		
    end;

end;





procedure _ZhuangBack;

begin

    This_NPC.NpcDialog(This_Player,

    '请选择你要回收的装备种类：\'

    +'|{cmd}<祖玛装备/@zhuangDlg~1>           ^<赤月装备/@zhuangDlg~2>\'

    +'|{cmd}<奴隶装备/@zhuangDlg~3>           ^<真天装备/@zhuangDlg~4>\'

	+'|{cmd}<蛮荒装备/@zhuangDlg~5>           ^<火龙装备/@zhuangDlg~6>'

	

	);

end;



procedure _zhuangDlg(LvidStr : string);

var ZlvId , i: integer;

DlgStr , ExpStr ,Iname,Ybstr : string;

begin

    ZlvId := strToIntDef(LvidStr,-1);

    

    ExpStr := inttostr(getZexpNum(ZlvId) div 10000) + '万';

	  Ybstr  := inttostr(getZexpNum(ZlvId) div 10000) + '个';

    DlgStr := '';

    

    for i := 1 to 39 do

    begin

        Iname := getZBnameById(ZlvId * 100 + i);

        if Iname <> '' then

        begin

            DlgStr := DlgStr + '<' + Iname + '/@ZhBackTrue~' + inttostr(ZlvId * 100 + i) + '>';

            if (i mod 3) = 0 then

            DlgStr := DlgStr + '|\'

            else

            DlgStr := DlgStr + '^';

        end; 

    end; 

    

    case ZlvId of

        1 : DlgStr := DlgStr + '<一键回收祖玛装备/@ZUma_all>';

        2 : DlgStr := DlgStr + '<一键回收赤月装备/@ChiYue_all>';

        3 : DlgStr := DlgStr + '<一键回收奴隶装备/@tianzhichiyue_all>';

		4 : DlgStr := DlgStr + '<一键回收真天装备/@molong_all>';

		5 : DlgStr := DlgStr + '<一键回收蛮荒装备/@tianzhimolong_all>';

		6 : DlgStr := DlgStr + '<一键回收火龙装备/@kaitian_all>';



    end;

    

    This_NPC.NpcDialog(This_Player,

    '请选择装备进行回收，或者一键全部回收！ \'

    +'|{cmd}' + DlgStr

    );      

end;



procedure _ZhBackTrue(ZBidStr : string);

var ZBid : integer;

ZBname : string;

begin

    ZBid := StrToIntDef(ZBidStr,-1);

    ZBname := getZBnameById(ZBid);

    if ZBname <> '' then

    begin

        if This_Player.GetBagItemCount(ZBname) > 0 then

        begin

            This_Player.Take(ZBname,1);

            This_Player.Give('经验',getZexpNum(ZBid div 100));

            This_NPC.NpcDialog(This_Player,

            '恭喜你成功回收！'

            +'|{cmd}<继续回收' + ZBname + '/@ZhBackTrue~' + inttostr(ZBid) + '>'

            +'|{cmd}<返回/@zhuangDlg~' + inttostr(ZBid div 100) + '>');

        end else

        This_NPC.NpcDialog(This_Player,

        '装备不足，无法回收！\'

        +'|{cmd}<返回/@zhuangDlg~' + inttostr(ZBid div 100) + '>'

        );  

    end;

end;



procedure _ZUma_all;

var ZMnum , i : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 21 do

    begin

        Iname := getZBnameById(100 + i);

        if Iname <> '' then

        begin

            if This_Player.GetBagItemCount(Iname) > 0 then

            begin

                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);

            end;

        end; 

    end;

    

    This_NPC.NpcDialog(This_Player,

     '你的包裹中有' + inttostr(ZMnum) + '件祖玛装备，回收可获得:' + inttostr(getZexpNum(1) div 10000 * ZMnum) + '万经验，你确定回收祖玛装备吗？\|' 

    +'|{cmd}<确认回收所有祖玛装备/@ZUma_True>'

    +'|{cmd}<返回/@zhuangDlg~1>');

end;



procedure  _ZUma_True;

var ZMnum , i , j , itemNum : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 21 do

    begin

        Iname := getZBnameById(100 + i);

        if Iname <> '' then

        begin

            itemNum := This_Player.GetBagItemCount(Iname);

            if itemNum > 0 then

            begin

                ZMnum := ZMnum + itemNum;

                This_Player.Take(Iname, itemNum);

                

                for j := 1 to itemNum do

                This_Player.Give('经验', getZexpNum(1));

            end;

        end; 

    end;

    This_NPC.NpcDialog(This_Player,

    '恭喜你成功回收！' 

    +'|{cmd}<返回/@zhuangDlg~1>');



end;





procedure _Chiyue_all;

var ZMnum , i : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 20 do

    begin

        Iname := getZBnameById(200 + i);

        if Iname <> '' then

        begin

            if This_Player.GetBagItemCount(Iname) > 0 then

            begin

                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);

            end;

        end; 

    end;

    

    This_NPC.NpcDialog(This_Player,

    '你的包裹中有' + inttostr(ZMnum) + '件赤月装备，回收可获得:' + inttostr(getZexpNum(2) div 10000 * ZMnum) + '万经验，' + inttostr(getZexpNum(2) div 80000 * ZMnum) + '个元宝，你确定回收所有赤月装备吗？\|'

    +'|{cmd}<确认回收所有赤月装备/@Chiyue_True>'

    +'|{cmd}<返回/@zhuangDlg~2>');

end;





procedure  _Chiyue_True;

var ZMnum , i , j , itemNum : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 20 do

    begin

        Iname := getZBnameById(200 + i);

        if Iname <> '' then

        begin

            itemNum := This_Player.GetBagItemCount(Iname);

            if itemNum > 0 then

            begin

                ZMnum := ZMnum + itemNum;

                This_Player.Take(Iname, itemNum);

                

                for j := 1 to itemNum do

                This_Player.Give('经验', getZexpNum(2));

			          This_Player.ScriptRequestAddYBNum(getZexpNum(2) div 80000 * ZMnum);

            end;

        end; 

    end;

    This_NPC.NpcDialog(This_Player,

    '恭喜你成功回收'

    +'|{cmd}<返回/@zhuangDlg~2>');



end;



procedure _tianzhichiyue_all;

var ZMnum , i : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 12 do

    begin

        Iname := getZBnameById(300 + i);

        if Iname <> '' then

        begin

            if This_Player.GetBagItemCount(Iname) > 0 then

            begin

                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);

            end;

        end; 

    end;

    

    This_NPC.NpcDialog(This_Player,

    '你的包裹中有' + inttostr(ZMnum) + '件奴隶装备装备，回收可获得:' + inttostr(getZexpNum(3) div 10000 * ZMnum) + '万经验，' + inttostr(getZexpNum(3) div 80000 * ZMnum) + '个元宝，你确定回收所有奴隶装备装备吗？\|'

    +'|{cmd}<确认回收所有奴隶装备装备/@tianzhichiyue_True>'

    +'|{cmd}<返回/@zhuangDlg~3>');

end;



procedure  _tianzhichiyue_True;

var ZMnum , i , j , itemNum : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 12 do

    begin

        Iname := getZBnameById(300 + i);

        if Iname <> '' then

        begin

            itemNum := This_Player.GetBagItemCount(Iname);

            if itemNum > 0 then

            begin

                ZMnum := ZMnum + itemNum;

                This_Player.Take(Iname, itemNum);

                

                for j := 1 to itemNum do

                This_Player.Give('经验', getZexpNum(3));

                This_Player.ScriptRequestAddYBNum(getZexpNum(3) div 80000 * ZMnum);

            end;

        end; 

    end;

    This_NPC.NpcDialog(This_Player,

    '恭喜你成功回收！'

    +'|{cmd}<返回/@zhuangDlg~3>');



end;











procedure _molong_all;

var ZMnum , i : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 39 do

    begin

        Iname := getZBnameById(400 + i);

        if Iname <> '' then

        begin

            if This_Player.GetBagItemCount(Iname) > 0 then

            begin

                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);

            end;

        end; 

    end;

    

    This_NPC.NpcDialog(This_Player,

    '你的包裹中有' + inttostr(ZMnum) + '件真天装备，回收可获得:' + inttostr(getZexpNum(4) div 10000 * ZMnum) + '万经验，' + inttostr(getZexpNum(4) div 80000 * ZMnum) + '个元宝，你确定回收所有真天装备吗？\|'

    +'|{cmd}<确认回收所有真天装备/@molong_True>'

    +'|{cmd}<返回/@zhuangDlg~4>');

end;



procedure  _molong_True;

var ZMnum , i , j , itemNum : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 39 do

    begin

        Iname := getZBnameById(400 + i);

        if Iname <> '' then

        begin

            itemNum := This_Player.GetBagItemCount(Iname);

            if itemNum > 0 then

            begin

                ZMnum := ZMnum + itemNum;

                This_Player.Take(Iname, itemNum);

                

                for j := 1 to itemNum do

                This_Player.Give('经验', getZexpNum(4));

				        This_Player.ScriptRequestAddYBNum(getZexpNum(4) div 80000 * ZMnum);

            end;

        end; 

    end;

    This_NPC.NpcDialog(This_Player,

    '恭喜你获得:' + inttostr(getZexpNum(4) div 10000 * ZMnum) + '万经验！' + inttostr(getZexpNum(4) div 80000 * ZMnum) + '个元宝！\|'

    +'|{cmd}<返回/@zhuangDlg~4>');



end;













procedure _tianzhimolong_all;

var ZMnum , i : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 21 do

    begin

        Iname := getZBnameById(500 + i);

        if Iname <> '' then

        begin

            if This_Player.GetBagItemCount(Iname) > 0 then

            begin

                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);

            end;

        end; 

    end;

    

    This_NPC.NpcDialog(This_Player,

    '你的包裹中有' + inttostr(ZMnum) + '件蛮荒装备装备，回收可获得:' + inttostr(getZexpNum(5) div 10000 * ZMnum) + '万经验，' + inttostr(getZexpNum(5) div 64000 * ZMnum) + '个元宝，你确定回收所有蛮荒装备装备吗？\|'

    +'|{cmd}<确认回收所有蛮荒装备装备/@tianzhimolong_True>'

    +'|{cmd}<返回/@zhuangDlg~5>');

end;



procedure  _tianzhimolong_True;

var ZMnum , i , j , itemNum : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 21 do

    begin

        Iname := getZBnameById(500 + i);

        if Iname <> '' then

        begin

            itemNum := This_Player.GetBagItemCount(Iname);

            if itemNum > 0 then

            begin

                ZMnum := ZMnum + itemNum;

                This_Player.Take(Iname, itemNum);

                

                for j := 1 to itemNum do

                This_Player.Give('经验', getZexpNum(5));

                This_Player.ScriptRequestAddYBNum(getZexpNum(5) div 64000 * ZMnum);

            end;

        end; 

    end;

    This_NPC.NpcDialog(This_Player,

    '恭喜你获得:' + inttostr(getZexpNum(5) div 10000 * ZMnum) + '万经验！' + inttostr(getZexpNum(5) div 64000 * ZMnum) + '个元宝！\|'

    +'|{cmd}<返回/@zhuangDlg~5>');



end;









procedure _kaitian_all;

var ZMnum , i : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 14 do

    begin

        Iname := getZBnameById(600 + i);

        if Iname <> '' then

        begin

            if This_Player.GetBagItemCount(Iname) > 0 then

            begin

                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);

            end;

        end; 

    end;

    

    This_NPC.NpcDialog(This_Player,

    '你的包裹中有' + inttostr(ZMnum) + '件火龙装备，回收可获得:' + inttostr(getZexpNum(6) div 10000 * ZMnum) + '万经验，' + inttostr(getZexpNum(6) div 25600 * ZMnum) + '个灵符，你确定回收所有开天装备吗？\|'

    +'|{cmd}<确认回收所有火龙装备/@kaitian_True>'

    +'|{cmd}<返回/@zhuangDlg~6>');

end;




procedure  _kaitian_True;

var ZMnum , i , j , itemNum : integer;

Iname : string;

begin

    ZMnum := 0;

    for i := 1 to 14 do

    begin

        Iname := getZBnameById(600 + i);

        if Iname <> '' then

        begin

            itemNum := This_Player.GetBagItemCount(Iname);

            if itemNum > 0 then

            begin

                ZMnum := ZMnum + itemNum;

                This_Player.Take(Iname, itemNum);

                

                for j := 1 to itemNum do

                This_Player.Give('经验', getZexpNum(6));

				This_Player.AddLF(0,getZexpNum(6) div 25600 * ZMnum);

            end;

        end; 

    end;

    This_NPC.NpcDialog(This_Player,

    '恭喜你获得:' + inttostr(getZexpNum(6) div 10000 * ZMnum) + '万经验！' + inttostr(getZexpNum(6) div 25600 * ZMnum) + '个灵符！\|'

    +'|{cmd}<返回/@zhuangDlg~6>');



end;





Begin

  domain;

end.