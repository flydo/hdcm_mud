{ ***********************
土城跑酷
作者：开心就好
内容：在一个特定的时间段开始在土城一定范围内跑动随机得到一定的物品奖励，比如以下脚本
会在每天15：00-15：05 及19：00 -19：05开放 
******************************}
program Mir2;
var
WpName : string;
Rdm_int,px, py :Integer;

Procedure _doexit;
begin
   This_Npc.CloseDialog(This_Player);
end;

procedure _tcpk;
   begin
if This_Player.Level >1 then
  begin                 
    This_Player.CallOut(This_Npc, 1, 'tcpk2');
    This_Player.FlyTo('3',333,333);
    This_Npc.NpcDialog(This_Player,
   '请好好把握这幸福的时光。'
   ); 
   
   end;
    end;

 procedure tcpk2;
 
begin
Rdm_int := random(100); 
px:= This_player.My_X;
py:= This_Player.My_Y;

if  ((GetHour = 15) and (GetMin < 5)) or 
((GetHour = 19) and (GetMin < 5)) then begin  //自己修改开放时间    
if (This_Player.MapName = '3') and ( 323 < px ) and (px < 343 ) 
and (323 < py ) and (py < 343) then  //在土城坐标333.333 周围10格内跑动有效 
    begin if (This_Player.GetV(77,1)  <> px) or  
    (This_Player.GetV(77,2)  <> py )then   // 移动才会继续触犯以下代码 
    begin 
      if This_Player.FreeBagNum >= 2 then  
       begin
	      if Rdm_int < 5 then  // 5%机会获得以下物品 
                begin
                case random(10) of    // 5%机会获得以下25个物品中的一个 
                    0 : WpName := '裁决之杖';
					1 : WpName := '圣战头盔';
					2 : WpName := '圣战项链';
					3 : WpName := '圣战手镯';
					4 : WpName := '圣战戒指';
					5 : WpName := '骨玉权杖';
					6 : WpName := '法神项链';
					7 : WpName := '法神手镯';
					8 : WpName := '法神戒指';
					9 : WpName := '龙纹剑';
					10 : WpName := '天尊项链';
					11 : WpName := '天尊手镯';
					12 : WpName := '天尊戒指';
					13: WpName := '防御戒指';
					14: WpName := '天师长袍';
					15 :WpName := '天尊道袍';
					16: WpName := '霓裳羽衣';
                    17 :WpName := '法神披风';
                    18 :WpName := '圣战宝甲';
					19: WpName := '天魔神甲';

					end;
               
             end else if Rdm_int < 10 then  
                begin
                case random(16) of
                    0 : WpName := '井中月';
                    1 : WpName := '黑铁头盔';
                    2 : WpName := '骑士手镯';
					4 : WpName := '力量戒指';
                    5 : WpName := '无极棍';
                    6 : WpName := '灵魂项链';
					7 : WpName := '三眼手镯';
                    8 : WpName := '泰坦戒指';
                    9 : WpName := '恶魔铃铛';
					10: WpName := '龙之手镯';
					11 :WpName := '紫碧螺';
					12: WpName := '魔杖';
					13: WpName := '绿色项链';
				
            end;
               
             end    
             else if Rdm_int < 20 then 
                begin
                case random(10) of
                    0 : WpName := '炼狱';
                    1 : WpName := '幽灵项链';
                    2 : WpName := '幽灵手套';
					3 : WpName := '龙之戒指';
                    4 : WpName := '魔杖';
                    5 : WpName := '生命项链';
                    6 : WpName := '思贝儿手镯';
                    7 : WpName := '红宝石戒指';
                    8 : WpName := '银蛇';
                    9 : WpName := '天珠项链';
					10 : WpName := '心灵手镯';
					11 : WpName := '铂金戒指';
             end;
               
             end  
             else if Rdm_int < 50 then 
                begin
                case random(50) of
                    0 : WpName := '裁决之杖';
                    1 : WpName := '骨玉权杖';
                    2 : WpName := '无极棍';
					3 : WpName := '龙纹剑';
                    4 : WpName := '祝福油';
                    5 : WpName := '金刚石';
					6 : WpName := '1元宝';
					7 : WpName := '2元宝';
					8 : WpName := '10元宝';
					9 : WpName := '5元宝';
					10 : WpName := '2灵符';
					11 : WpName := '5灵符';
					12 : WpName := '1灵符';
           end;
               
             end    
        else if Rdm_int < 99 then 
                begin
                case random(4) of
                    0 : begin 
					This_Player.Give('经验',5000);
					This_Player.PlayerNotice('你获得5000经验！' , 1);
					end;
                    1 : begin 
          This_Player.Give('经验',10000);
					This_Player.PlayerNotice('你获得10000经验！' , 1);
					end;
                    2 : begin This_Player.Give('经验',20000);
					This_Player.PlayerNotice('你获得20000经验！' , 1);
					end;
					3 :  begin This_Player.Give('经验',25000);
					This_Player.PlayerNotice('你获得25000经验！' , 1);
					end;
                end;
                end;
               
                
	This_Player.Give(WpName , 1);
	This_Player.PlayerNotice(
            WpName + '已放入您的包裹!\',1); 
     This_Player.setV(77,1,px);
      This_Player.setV(77,2,py) ;
      This_Player.CallOut(This_Npc, 2, 'tcpk2');  // 2秒后循环运行 
      end else
        This_Player.PlayerNotice(
           '你的包裹空间不够!',1);
     This_Player.CallOut(This_Npc, 2, 'tcpk2');   //  再次符合条件后继续运行 
   end else 
   This_Player.PlayerNotice(
           '跑起来才有东西!',1);
    This_Player.CallOut(This_Npc, 2, 'tcpk2');   //再次符合条件后继续运行
   end else 
    This_Player.PlayerNotice(
           '你跑得太远了!',1);
    This_Player.CallOut(This_Npc, 2, 'tcpk2');   // 再次符合条件后继续运行
   end else 
   This_Player.PlayerNotice(
           '不在活动时间!',1);
   end; 
      
begin   
This_NPC.NpcDialog(This_Player,
   '土城跑酷：\ \'+
   '欢迎来到开心就好土城跑酷.\ \'+
   '在土城一定范围内不停移动能随机获得多种物品.\ \'+
   '想要强大就得快人一步、可以在这里跑酷.\ \'+
   '每天15:00-15:05及19:00 -19:05开放 .\ \'+
   '|{cmd}<◆开始跑酷/@tcpk>');
end.    