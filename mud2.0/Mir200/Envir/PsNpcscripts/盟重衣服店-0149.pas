{********************************************************************

*******************************************************************}


program mir2;

{$I common.pas}
{$I ActiveValidateCom.pas}

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure NormalDialog;
begin

   This_Npc.NpcDialog(This_Player,
   '什么？您想知道什么？是谁告诉您我知道这些消息的。\哎，看来这个秘密是守不住了。\国王为了防止宝藏被盗，他将藏宝图分别交给了几大将领。\这些将领最近都在封魔谷一带活动，您可以去那里打探一下哦。\'
   //+'|{cmd}<打听天工神剪的故事/@gongdian>\' 
   +'|{cmd}<合成雷霆战甲、烈焰魔衣、光芒道袍/@hecheng>\' 
   +'|{cmd}<强化雷霆战甲、烈焰魔衣、光芒道袍/@@StrengthenCloth>\'  //@@StrengthenCloth   qianghua
   +'|{cmd}<买/@buy>衣服'+ addSpace('', 16) + '^<修复/@repair>衣服\'           
   +'|{cmd}<卖/@sell>衣服'+ addSpace('', 16) + '^<特殊修理/@s_repair>\'
   +'|{cmd}<退出/@doexit>'
   );

end;


procedure _hecheng;
begin
  This_Npc.NpcDialog(This_Player,
  '合成雷霆战甲、烈焰魔衣、光芒道袍，需要一些原料，\除了相应的藏宝图，还需要金刚石188颗，\同时还需要相应的圣战、天尊、法神系列的物品，\我会自己去看你包裹里有多少这些物品，它们的多少很重要，\给我越多这些物品，对合成帮助越大，越有可能合成成功。\如果失败，金刚石和圣战等物品将会消耗，只有藏宝图能还给您。\'
  +'|{cmd}<合成雷霆战甲、烈焰魔衣、光芒道袍/@hecheng1>\'
  +'|{cmd}<查看升级所需物品/@chaxun>\'
  +'|{cmd}<返回/@main>'
  );
end;

procedure _hecheng1;
begin
  This_Npc.NpcDialog(This_Player,
  '合成需要的圣战、法神、天尊系列的物品，\'+
  '只要一个系列的任意物品1-10件就可以了，\'+
  '如果合成失败，只有藏宝图还能返还给你，你想合成什么呢？\ \'+
  '|{cmd}<雷霆战甲(男)/@ComposeDsOne>           ^<雷霆战甲(女)/@ComposeDsTwo>\'+
  '|{cmd}<烈焰魔衣(男)/@ComposeDsThr>           ^<烈焰魔衣(女)/@ComposeDsFou>\'+
  '|{cmd}<光芒道袍(男)/@ComposeDsFiv>           ^<光芒道袍(女)/@ComposeDsSix>\ \'+
  '|{cmd}<返回/@main>');
end;

{Procedure _ComposeDsOne;
begin
   This_NPC.ClickComposeDress(This_Player,'1');  
end;

Procedure _ComposeDsTwo;
begin
   This_NPC.ClickComposeDress(This_Player,'2');
end;

Procedure _ComposeDsThr;
begin
   This_NPC.ClickComposeDress(This_Player,'3');
end;

Procedure _ComposeDsFou;
begin
   This_NPC.ClickComposeDress(This_Player,'4');
end;

Procedure _ComposeDsFiv;
begin
   This_NPC.ClickComposeDress(This_Player,'5');
end;

Procedure _ComposeDsSix;
begin
   This_NPC.ClickComposeDress(This_Player,'6');
end;  
}
Procedure _ComposeDsOne;
begin
   This_NPC.ClickComposeDress(This_Player,'@ComposeDs1');  //按老脚本的接口，这里传送的字符串需保证第11位为对应的数字（1，2，3，4，5，6） 
end;

Procedure _ComposeDsTwo;
begin
   This_NPC.ClickComposeDress(This_Player,'@ComposeDs2');
end;

Procedure _ComposeDsThr;
begin
   This_NPC.ClickComposeDress(This_Player,'@ComposeDs3');
end;

Procedure _ComposeDsFou;
begin
   This_NPC.ClickComposeDress(This_Player,'@ComposeDs4');
end;

Procedure _ComposeDsFiv;
begin
   This_NPC.ClickComposeDress(This_Player,'@ComposeDs5');
end;

Procedure _ComposeDsSix;
begin
   This_NPC.ClickComposeDress(This_Player,'@ComposeDs6');
end;

procedure _chaxun;
begin
  This_Npc.NpcDialog(This_Player,
  '你想查看哪个物品的升级配方呢？\ \'
  +'|{cmd}<雷霆战甲(男)/@leiman>'+ addSpace('', 16) + '^<雷霆战甲(女)/@leiwoman>\'
  +'|{cmd}<烈焰魔衣(男)/@lieman>'+ addSpace('', 16) + '^<烈焰魔衣(女)/@liewoman>\'
  +'|{cmd}<光芒道袍(男)/@guaman>'+ addSpace('', 16) + '^<光芒道袍(女)/@guawoman>\'
  );
end;

procedure _leiman;
begin
  This_Npc.NpcDialog(This_Player,
  '合成雷霆战甲(男)所需物品：\金刚石：188颗，\藏宝图301、藏宝图302、藏宝图303、藏宝图304，\藏宝图305、藏宝图306、藏宝图307、藏宝图308，\圣战头盔、圣战项链、圣战手镯、圣战戒指中的任意1-10件。\'
  +'|{cmd}<返回查看配方/@chaxun>\'
  +'|{cmd}<进行合成/@ComposeDsOne>'+ addSpace('', 16) + '|{cmd}<关闭/@doexit>'
  );
end;

procedure _leiwoman;
begin
  This_Npc.NpcDialog(This_Player,
  '合成雷霆战甲(女)所需物品：\金刚石：188颗，\藏宝图301、藏宝图302、藏宝图309、藏宝图310，\藏宝图311、藏宝图312、藏宝图313、藏宝图314，\圣战头盔、圣战项链、圣战手镯、圣战戒指中的任意1-10件。\'
  +'|{cmd}<返回查看配方/@chaxun>\'
  +'|{cmd}<进行合成/@ComposeDsTwo>'+ addSpace('', 16) + '|{cmd}<关闭/@doexit>'
  );
end;

procedure _lieman;
begin
  This_Npc.NpcDialog(This_Player,
  '合成烈焰魔衣(男)所需物品：\金刚石：188颗，\藏宝图315、藏宝图316、藏宝图317、藏宝图318，\藏宝图305、藏宝图306、藏宝图307、藏宝图308，\法神头盔、法神项链、法神手镯、法神戒指中的任意1-10件。\'
  +'|{cmd}<返回查看配方/@chaxun>\'
  +'|{cmd}<进行合成/@ComposeDsThr>'+ addSpace('', 16) + '|{cmd}<关闭/@doexit>'
  );
end;

procedure _liewoman;
begin
  This_Npc.NpcDialog(This_Player,
  '合成烈焰魔衣(女)所需物品：\金刚石：188颗，\藏宝图315、藏宝图316、藏宝图319、藏宝图320，\藏宝图311、藏宝图312、藏宝图313、藏宝图314，\法神头盔、法神项链、法神手镯、法神戒指中的任意1-10件。\'
  +'|{cmd}<返回查看配方/@chaxun>\'
  +'|{cmd}<进行合成/@ComposeDsFou>'+ addSpace('', 16) + '|{cmd}<关闭/@doexit>'
  );
end;

procedure _guaman;
begin
  This_Npc.NpcDialog(This_Player,
  '合成光芒道袍(男)所需物品：\金刚石：188颗，\藏宝图321、藏宝图322、藏宝图323、藏宝图324，\藏宝图305、藏宝图306、藏宝图307、藏宝图308，\天尊头盔、天尊项链、天尊手镯、天尊戒指中的任意1-10件。\'
  +'|{cmd}<返回查看配方/@chaxun>\'
  +'|{cmd}<进行合成/@ComposeDsFiv>'+ addSpace('', 16) + '|{cmd}<关闭/@doexit>'
  );
end;

procedure _guawoman;
begin
  This_Npc.NpcDialog(This_Player,
  '合成光芒道袍(女)所需物品：\金刚石：188颗，\藏宝图321、藏宝图322、藏宝图325、藏宝图326，\藏宝图311、藏宝图312、藏宝图313、藏宝图314，\天尊头盔、天尊项链、天尊手镯、天尊戒指中的任意1-10件。\'
  +'|{cmd}<返回查看配方/@chaxun>\'
  +'|{cmd}<进行合成/@ComposeDsSix>'+ addSpace('', 16) + '|{cmd}<关闭/@doexit>'
  );
end;

//强化
procedure _qianghua;
begin
  This_Npc.NpcDialog(This_Player,
  '强化雷霆战甲、烈焰魔衣、光芒道袍，必须穿戴上这些衣服，而且\'
  +'需要一些材料，除了888颗金刚石，同时还需要相应的3件圣战、天\'
  +'尊、法神系列物品。我会扣除你包裹中3件这些物品。强化时成败由\'
  +'天，如果失败，金刚石和圣战等物品将会消失，只有你穿戴的衣服\'
  +'能还给您。另外，每件衣服最多只能成功强化3次，每次强化成功，除\'
  +'了增加1点对应的攻击、魔法、道术外，还会增加魔御和防御各1点。\ \'
  //+'|{cmd}<强化雷霆战甲、烈焰魔衣、光芒道袍/@qianghua1>\'
  +'|{cmd}<强化雷霆战甲、烈焰魔衣、光芒道袍/@@StrengthenCloth>\'
  +'|{cmd}<返回/@main>'
  );
end;

procedure _qianghua1;
begin
  This_Npc.NpcDialog(This_Player,
  '每次强化需要穿戴所需强化的衣服，并且需要圣战、法神、天尊系\'+
  '列的物品3件和888颗金刚石，如果强化失败，只有衣服能返还给你，\'+
  '你想强化什么呢？\ \'+
  '|{cmd}<雷霆战甲(男)/@StrengthenGo~1>           ^<雷霆战甲(女)/@StrengthenGo~2>\'+
  '|{cmd}<烈焰魔衣(男)/@StrengthenGo~3>           ^<烈焰魔衣(女)/@StrengthenGo~4>\'+
  '|{cmd}<光芒道袍(男)/@StrengthenGo~5>           ^<光芒道袍(女)/@StrengthenGo~6>\ \'+
  '|{cmd}<返回/@main>');
end;
 
Procedure _StrengthenGo(str:String);
var
temp:integer; 
str_not,str_att:string; 
begin
   temp := strtointdef(str,-1);
   if (temp < 1) or (temp > 6) then exit;
   case temp of
     1:
     begin
     str:='雷霆战甲(男)';
     str_att:='攻击';
     end;
     2:
     begin
     str:='雷霆战甲(女)';
     str_att:='攻击';
     end;
     3:
     begin
     str:='烈焰魔衣(男)';
     str_att:='魔法';
     end;
     4:
     begin
     str:='烈焰魔衣(女)';
     str_att:='魔法';
     end;
     5:
     begin
     str:='光芒道袍(男)';
     str_att:='道术';
     end;
     6:
     begin
     str:='光芒道袍(女)';
     str_att:='道术';
     end;
   end;
  // temp := This_NPC.ClickStrengthenClothes(This_Player,str);
   case temp of
     0,2:str_not:='未穿戴对应衣服，不可强化。'; 
     1:
     begin 
     str_not:='恭喜你，强化成功。';
     This_NPC.NpcNotice('恭喜'+ This_Player.Name+'在盟重服装店成功强化了'+str+'！');
     end;
     -1,3,7:str_not:='该装备不可强化。';
     4:str_not:='金刚石不足，不可强化。';
     5:str_not:='缺乏必要首饰，不可强化。';
     6:str_not:='很遗憾，强化失败，除衣服外其他材料均已消耗。';
     8:str_not:='你穿戴的衣服已经强化过3次了，不可再次强化。';
   end;
   This_NPC.NpcDialog(This_Player,str_not);
end;

procedure _buy;
begin
  This_Npc.NpcDialog(This_Player, 
    '你想买什么样的衣服？\ \ \<返回/@main>'
  );
  This_Npc.Click_Buy(This_Player);
end;

procedure _Sell;
begin
  This_Npc.NpcDialog(This_Player, 
    '把你要卖的衣服给我看看，我会给你个估价。\ \<返回/@main>'
  );
  This_Npc.Click_Sell(This_Player);
end;

procedure _Repair;
begin
  This_Npc.NpcDialog(This_Player,
    '请放上去要修补的衣服。\ \ \<返回/@main>'
  );
  This_Npc.Click_Repair(This_Player);
end;

procedure RepairDone;
begin
  This_Npc.NpcDialog(This_Player,
    '看来修补得很不错。\ \ \ <返回/@main>'
  );
end;

procedure _S_Repair;
begin
  This_Npc.NpcDialog(This_Player,
    '你这家伙，你可太幸运了，我正好有材料做特殊修补\但费用是普通的3倍！'
  );
  This_Npc.Click_SRepair(This_Player);
end;

procedure SRepairDone;
begin
  This_Npc.NpcDialog(This_Player,
    '看上去它已经修好了...\请好好的使用它.\ \<返回/@main>'
  );
end;

//初始化操作
procedure OnInitialize;
begin
  This_Npc.AddStdMode(10);
  This_Npc.AddStdMode(11);
  This_Npc.AddStdMode(27);
  This_Npc.AddStdMode(28);
  This_Npc.FillGoods('布衣(男)',50,1);
  This_Npc.FillGoods('布衣(女)',50,1);
  This_Npc.FillGoods('轻型盔甲(男)',50,1); 
  This_Npc.FillGoods('轻型盔甲(女)',50,1);
  This_Npc.FillGoods('中型盔甲(男)',50,1);
  This_Npc.FillGoods('中型盔甲(女)',50,1);
  This_Npc.FillGoods('幽灵战衣(男)',50,1);
  This_Npc.FillGoods('幽灵战衣(女)',50,1);
  This_Npc.FillGoods('恶魔长袍(男)',30,1);
  This_Npc.FillGoods('恶魔长袍(女)',30,1);
  This_Npc.FillGoods('战神盔甲(男)',30,1);
  This_Npc.FillGoods('战神盔甲(女)',30,1);
  This_Npc.SetRebate(100);
end;

//脚本执行的入口
begin
  //domain;
  NormalDialog;
end.