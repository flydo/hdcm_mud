{
*******************************************************************}

program Mir2;

 

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;


procedure _givecai;
var
num,duramax,duraav : integer;
begin
   if (This_Player.GoldNum < 110) and (This_Player.GetBagItemCount('花叶包鸡') < 1) then
   begin
      This_Player.CheckBagItemEx('鸡肉',num,duramax,duraav);
      if (num >= 1) and (duramax > 3500) then
      begin
         This_Npc.NpcDialog(This_Player,
         '你找到材料了啊，这只是第一步而已哦。\'+
         '我最近迷上了猜拳游戏，你必须和我猜上一拳，如果你赢了我，\'+
         '我就马上给你做。不管输赢，材料我都要收了的哦。\ \'+
         '|{cmd}<好，开始猜拳/@caiquan>\ \'+
         '|{cmd}<让我想一会/@doexit>'); 
      end else
      begin
         This_Npc.NpcDialog(This_Player,
         '你没有我需要的材料啊！'); 
      end;
   end;
end;

procedure _caiquan;
var
num,duramax,duraav : integer;
begin
   This_Player.CheckBagItemEx('鸡肉',num,duramax,duraav);
   if (num >= 1) and (duramax > 3500) then
   begin
      This_Npc.NpcDialog(This_Player,
      '好，有勇气，来吧，看看谁的运气更好！\'+ 
      '不过事先说明了，打平也是你输哦，材料也会被收的哦！\ \'+
      '|{cmd}<出剪刀/@cjd>       ^<出石头/@cst>       ^<出布/@cbp>\ \'+
      '|{cmd}<让我想一会/@doexit>'); 
   end else
   begin
      This_Npc.NpcDialog(This_Player,
      '你没有我需要的材料啊！'); 
   end;
end;

procedure _cbp;
var
num,duramax,duraav ,R : integer;
begin
   This_Player.CheckBagItemEx('鸡肉',num,duramax,duraav);
   if (num >= 1) and (duramax > 3500) then
   begin
      R := random(3)+1;
      if R = 1 then
      begin
         This_Player.Take('鸡肉',num);
         R := 0; 
         This_Npc.NpcDialog(This_Player,
         '你出布……我出布……打平，还是你输，呵呵！');
      end else if R = 2 then
      begin
         This_Player.Take('鸡肉',num);
         R := 0;
         This_Npc.NpcDialog(This_Player,
         '你出布……我出剪刀……哈哈，你输了！');
      end else if R = 3 then
      begin
         This_Npc.NpcDialog(This_Player,
         '你出布……我出石头……哎呀，我输了！\'+
         '你是不是现在就要花叶包鸡呢？\ \'+
         '|{cmd}<给我东西吧/@giveji1>\'+
         '|{cmd}<算了，还是下次吧/@nexthua>');
      end; 
   end else
   begin
      This_Npc.NpcDialog(This_Player,
      '你没有我需要的材料啊！'); 
   end;
end;

procedure _cst;
var
num,duramax,duraav ,R : integer;
begin
   This_Player.CheckBagItemEx('鸡肉',num,duramax,duraav);
   if (num >= 1) and (duramax > 3500) then
   begin
      R := random(3)+1;
      if R = 3 then
      begin
         This_Player.Take('鸡肉',num);
         R := 0; 
         This_Npc.NpcDialog(This_Player,
         '你出石头……我出布……哈哈，你输了！');
      end else if R = 2 then
      begin
         This_Player.Take('鸡肉',num);
         R := 0;
         This_Npc.NpcDialog(This_Player,
         '你出石头……我也出石头……打平，还是你输，呵呵！');
      end else if R = 1 then
      begin
         This_Npc.NpcDialog(This_Player,
         '你出石头……我出剪刀……哎呀，我输了！\'+
         '你是不是现在就要花叶包鸡呢？\ \'+
         '|{cmd}<给我东西吧/@giveji1>\'+
         '|{cmd}<算了，还是下次吧/@nexthua>');
      end; 
   end else
   begin
      This_Npc.NpcDialog(This_Player,
      '你没有我需要的材料啊！'); 
   end;
end;

procedure _cjd;
var
num,duramax,duraav ,R : integer;
begin
   This_Player.CheckBagItemEx('鸡肉',num,duramax,duraav);
   if (num >= 1) and (duramax > 3500) then
   begin
      R := random(3)+1;
      if R = 3 then
      begin
         This_Player.Take('鸡肉',num);
         R := 0; 
         This_Npc.NpcDialog(This_Player,
         '你出剪刀……我也是剪刀……打平，还是你输，呵呵！');
      end else if R = 2 then
      begin
         This_Player.Take('鸡肉',num);
         R := 0;
         This_Npc.NpcDialog(This_Player,
         '你出剪刀……我出石头……哈哈，你输了！');
      end else if R = 1 then
      begin
         This_Npc.NpcDialog(This_Player,
         '你出剪刀……我出布……哎呀，我输了！\'+
         '你是不是现在就要花叶包鸡呢？\ \'+
         '|{cmd}<给我东西吧/@giveji1>\'+
         '|{cmd}<算了，还是下次吧/@nexthua>');
      end; 
   end else
   begin
      This_Npc.NpcDialog(This_Player,
      '你没有我需要的材料啊！'); 
   end;
end; 

procedure _giveji1;
var
num,duramax,duraav ,R : integer;
begin
   This_Player.CheckBagItemEx('鸡肉',num,duramax,duraav);
   if (num >= 1) and (duramax > 3500) then
   begin
      This_Player.Take('鸡肉',num);
      This_Player.Give('花叶包鸡',1);
      This_Npc.CloseDialog(This_Player);
   end else
   begin
      This_Npc.NpcDialog(This_Player,
      '你没有我需要的材料啊！'); 
   end;
end;

procedure _nexthua;
begin
   This_Npc.NpcDialog(This_Player,
   '告诉你，庄园的花匠还知道鸡肉其它的用途，\'+
   '不过他最近有事情，向比奇国王请假回家了，\'+
   '他好像知道一些鸡肉的神秘用途，不过一直不肯说\'+
   '|{cmd}<离开/@doexit>'); 
end;

procedure _giveenshi;
begin
  This_Npc.NpcDialog(This_Player,
  '如果你有一个“恩师帖”就可以在我这里换取海量经验奖励，\'
  +'每个人只有4次机会。\ \'
  +'|{cmd}<我要换取奖励/@giveenshi_1>'
  );
end;

procedure _giveenshi_1;
begin
  if This_Player.Level >= 40 then
  begin
    if This_Player.GetS(20,2) < 400 then
    begin
      if This_Player.GetBagItemCount('恩师帖') > 0 then
      begin
        if This_Player.GetS(20, 2) = 300 then
        begin
          This_Player.Take('恩师帖',1);
          This_Npc.GiveConfigPrize(This_Player,14,'祝贺：“' + This_Player.Name + '”递交恩师帖获得了经验：800万经验奖励！');
          This_Player.SetS(20,2,400); 
        end
        else if This_Player.GetS(20, 2) = 200 then
        begin
          This_Player.Take('恩师帖',1);
          This_Npc.GiveConfigPrize(This_Player,14,'祝贺：“' + This_Player.Name + '”递交恩师帖获得了经验：800万经验奖励！');
          This_Player.SetS(20,2,300); 
        end
        else if This_Player.GetS(20, 2) = 100 then
        begin
          This_Player.Take('恩师帖',1);
          This_Npc.GiveConfigPrize(This_Player,14,'祝贺：“' + This_Player.Name + '”递交恩师帖获得了经验：800万经验奖励！');
          This_Player.SetS(20,2,200); 
        end
        else if This_Player.GetS(20, 2) = -1 then
        begin
          This_Player.Take('恩师帖',1);
          This_Npc.GiveConfigPrize(This_Player,14,'祝贺：“' + This_Player.Name + '”递交恩师帖获得了经验：800万经验奖励！');
          This_Player.SetS(20,2,100); 
        end
      end
      else
      begin
        This_Npc.NpcDialog(This_Player,
        '你的恩师帖在哪里？\没有恩师帖可不能兑换奖励！');
      end; 
    end
    else
    begin
      This_Npc.NpcDialog(This_Player,
      '您已经领取过恩师帖活动的奖励，不能重复领取！');
    end;
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '对不起，你的等级未超过40级，不能换取奖励。\');
  end;
end;

procedure _jinzhen;
begin
  This_Npc.NpcDialog(This_Player,
  '修炼内功经络的奥秘渐渐流传开来，庄主吩咐过了，\'
  +'让我收集一些金针碎片，以锻造金针来加强庄园护卫们的实力。\'
  +'如果你有金针碎片的话，就尽管交给我吧，我会拿一些\'
  +'好东西来交换的。不过我只跟等级至少为<45级/c=red>的人做交易……\ \'
  +'|{cmd}<我有金针碎片/@jinzhen_start>		                     ^<我要来进行特训/@jinzhen_training>\'
  //+'|{cmd}<我想使用金针来修炼内功/@jinzhen_forcemagic>\'            //该选项暂不开放（2009-05-07） 
  +'|{cmd}<任务奖励规则/@Jinzhen_Guize>                       ^<任务奖励/@JZJiangli>\ \'     //添加活动规则 
  +'|{cmd}<离开/@doexit>'
  );
end;

procedure _JZJiangli;
begin
   This_Npc.NpcDialog(This_Player,
   '如果你运气好的话，通过不断的交换物品，\'+
   '就可以把一枚小小的金针碎片换成<12000000经验/c=red>或者一枚<4级金针/c=red>。\'+
   '即使你在运气最差的情况下，也可以至少换到200000的经验奖励。\ \'+
   '|{cmd}<返回/@jinzhen>\');
end;

procedure _Jinzhen_Guize;
begin
  This_Npc.NpcDialog(This_Player,
  '1、每天可以拿金针碎片找我随机换取半兽人头骨、普通军刀、\'+
  '   残破的古籍中任意一样东西。\'+
  '2、盟重酒馆的鉴宝散人在收集怪物的身体部分做研究，盟重指挥官\'+
  '   时常需求和军队有关的物品，比奇书店老板在研究各色书籍，\'+
  '   如果你有和这三类有关的物品，可以去找他们。\'+
  '3、从鉴宝散人、指挥官和比奇书店老板手中，可以换到更好的物品，\'+
  '   当然也有可能他们什么都不给你。\ \'+
  '|{cmd}<下一页/@GZNextpage>\');
end;

procedure _GZNextpage;
begin
  This_Npc.NpcDialog(This_Player,
  '4、如果你不愿意冒险的话，你可以随时将这些物品从他们三人\'+
  '   手中换得奖励。记住，鉴宝散人需要怪物身体上的部分，\'+
  '   指挥官需要军事物品，比奇书店老板需要书籍。\'+
  '5、如果你拿到了统领佩剑或将军令牌，你还可以凭这两样东西，\'+
  '   获得一次额外的训练赚取大量经验。\'+
  '6、越好的物品，所能换得的奖励越好。\ \'+
  '|{cmd}<我知道了/@jinzhen>');
end;

procedure _jinzhen_start;
var
  temp,datenum: integer;
begin
  datenum := GetDateNum(GetNow);
  if This_Player.Level < 45 then
  begin
    This_Npc.NpcDialog(This_Player,
    '你的等级还没有达到45级，我不会和你做交易的.');  
  end else if This_Player.GetV(21,2) = datenum then
  begin
    This_Npc.NpcDialog(This_Player,
    '今天你已经使用金针碎片兑换过物品了，不能再次兑换！'); 
  end
  else
  begin
    if This_Player.GetBagItemCount('金针碎片') >= 1 then
    begin
      temp := random(100) + 1;
      if temp <= 40 then
      begin
        This_Npc.NpcDialog(This_Player,
        '非常感谢你的金针碎片。\'
        +'前段时间庄园的卫队击退了一次半兽人的进攻，\'
        +'少庄主一向嫉恶如仇，对于肆虐玛法的半兽人深恶痛绝，\'
        +'他下令将半兽人的头骨取出作为战利品，来鼓舞玛法人抗击半兽人，\'
        +'请收下这块<半兽人头骨/c=red>吧。\'
        +'酒馆的鉴宝散人好像在研究生物，也许他需要半兽人头骨。\ \'
        +'|{cmd}<好的/@doexit>'
        );
        This_Player.Take('金针碎片',1);
        This_Player.Give('半兽人头骨',1);
        This_Player.SetV(21,2,datenum);
      end
      else if temp <= 80 then
      begin
        This_Npc.NpcDialog(This_Player,
        '非常感谢你的金针碎片。\'
        +'在庄园的地下书库中，有很多祖辈们留下的书籍，\'
        +'不过由于年代久远，多数已经残破不堪，\'
        +'这本<残破的古籍/c=red>就给你吧，希望对你有所帮助。\'
        +'比奇书店的老板一直在研究各种古代书籍。也许需要这本残破古籍。\ \'
        +'|{cmd}<好的/@doexit>'
        );
        This_Player.Take('金针碎片',1);
        This_Player.Give('残破的古籍',1);
        This_Player.SetV(21,2,datenum);
      end
      else
      begin
        This_Npc.NpcDialog(This_Player,
        '非常感谢你的金针碎片。\'
        +'这坐庄园一直是协助国家防守半兽人进攻的重要根据地，\'
        +'也藏有大量的兵器，这把<普通军刀/c=red>就送你了，\'
        +'希望能够为你带来幸运。\'
        +'盟重的指挥官一直在招募新兵，一定欠缺兵器。\ \'
        +'|{cmd}<好的/@doexit>'
        );
        This_Player.Take('金针碎片',1);
        This_Player.Give('普通军刀',1);
        This_Player.SetV(21,2,datenum);
      end;
    end
    else
    begin
       This_Npc.NpcDialog(This_Player,
       '你没有<金针碎片/c=red>呀！\ \|{cmd}<好的/@doexit>'); 
    end; 
  end;
end;

procedure _jinzhen_training;
begin
  This_Npc.NpcDialog(This_Player,
  '庄园庄主与盟重指挥官相约，只要持有他的信物，\'
  +'我们便可以为其进行一次特训，来增长经验，\'
  +'请问你有他的信物么？\ \'
  +'|{cmd}<我有统领佩剑/@jinzhen_train1>                      	<我有将军令牌/@jinzhen_train2>\ \'
  +'|{cmd}<离开/@doexit>'
  );
end;

procedure _jinzhen_train1;
var
  temp : integer;
begin
  temp := This_Player.GetV(21,6);
  if temp > 0 then
  begin
    This_Player.Give('经验', 3000000);
    This_Player.SetV(21,6,temp-1);
    This_Npc.NpcDialog(This_Player,
    '你获得了3000000经验，你使用统领佩剑进行特训的机会还有：' + inttostr(This_Player.GetV(21,6)) +'!');  
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '对不起，你已经不再有使用统领佩剑进行特训的机会！'); 
  end;
end;

procedure _jinzhen_train2;
var
  temp : integer;
begin
  temp := This_Player.GetV(21,7);
  if temp > 0 then
  begin
    This_Player.Give('经验', 8000000);
    This_Player.SetV(21,7,temp-1);
    This_Npc.NpcDialog(This_Player,
    '你获得了8000000经验，\你使用将军令牌进行特训的机会还有：' + inttostr(This_Player.GetV(21,7)) +'！');  
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '对不起，你已经不再有使用将军令牌进行特训的机会！'); 
  end;
end;

procedure _jinzhen_forcemagic;
begin
   This_Npc.NpcDialog(This_Player,
   '我们研究出了一套针灸的秘法，使用各个等级的金针，\'
   +'来刺激身体的各个部位，便可以激发人体的潜能，\'
   +'使内功经验大增，你要使用哪个等级的金针？\ \'
   +'|{cmd}<一级金针/@jinzhen_force1>		    ^<二级金针/@jinzhen_force2>		    ^<三级金针/@jinzhen_force3>\ \'	
   +'|{cmd}<四级金针/@jinzhen_force4>		    ^<五级金针/@jinzhen_force5>\ \'
   +'|{cmd}<离开/@doexit>'
   );
end;

procedure _jinzhen_force1;
begin
  if This_Player.GetBagItemCount('一级金针') > 0 then
  begin
    This_Player.Take('一级金针', 1);
    This_Player.Give('内功经验', 2220000);
    This_Npc.NpcDialog(This_Player,
    '你使用一级金针获得内功经验：2220000！\' 
    +'你还要使用哪个等级的金针？\ \'
    +'|{cmd}<一级金针/@jinzhen_force1>		    ^<二级金针/@jinzhen_force2>		    ^<三级金针/@jinzhen_force3>\ \'	
    +'|{cmd}<四级金针/@jinzhen_force4>		    ^<五级金针/@jinzhen_force5>\ \'
    +'|{cmd}<离开/@doexit>'
    );
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '如果你已经带来了一级金针，就赶快拿出来吧！'
    ); 
  end; 
end;

procedure _jinzhen_force2;
begin
  if This_Player.GetBagItemCount('二级金针') > 0 then
  begin
    This_Player.Take('二级金针', 1);
    This_Player.Give('内功经验', 8820000);
    This_Npc.NpcDialog(This_Player,
    '你使用二级金针获得内功经验：8820000！\' 
    +'你还要使用哪个等级的金针？\ \'
    +'|{cmd}<一级金针/@jinzhen_force1>		    ^<二级金针/@jinzhen_force2>		    ^<三级金针/@jinzhen_force3>\ \'	
    +'|{cmd}<四级金针/@jinzhen_force4>		    ^<五级金针/@jinzhen_force5>\ \'
    +'|{cmd}<离开/@doexit>'
    );
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '如果你已经带来了二级金针，就赶快拿出来吧！'
    ); 
  end; 
end;

procedure _jinzhen_force3;
begin
  if This_Player.GetBagItemCount('三级金针') > 0 then
  begin
    This_Player.Take('三级金针', 1);
    This_Player.Give('内功经验', 24120000);
    This_Npc.NpcDialog(This_Player,
    '你使用三级金针获得内功经验：24120000！\' 
    +'你还要使用哪个等级的金针？\ \'
    +'|{cmd}<一级金针/@jinzhen_force1>		    ^<二级金针/@jinzhen_force2>		    ^<三级金针/@jinzhen_force3>\ \'	
    +'|{cmd}<四级金针/@jinzhen_force4>		    ^<五级金针/@jinzhen_force5>\ \'
    +'|{cmd}<离开/@doexit>'
    );
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '如果你已经带来了三级金针，就赶快拿出来吧！' 
    );
  end; 
end;

procedure _jinzhen_force4;
begin
  if This_Player.GetBagItemCount('四级金针') > 0 then
  begin
    This_Player.Take('四级金针', 1);
    This_Player.Give('内功经验', 67680000);
    This_Npc.NpcDialog(This_Player,
    '你使用四级金针获得内功经验：67680000！\' 
    +'你还要使用哪个等级的金针？\ \'
    +'|{cmd}<一级金针/@jinzhen_force1>		    ^<二级金针/@jinzhen_force2>		    ^<三级金针/@jinzhen_force3>\ \'	
    +'|{cmd}<四级金针/@jinzhen_force4>		    ^<五级金针/@jinzhen_force5>\ \'
    +'|{cmd}<离开/@doexit>'
    );
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '如果你已经带来了四级金针，就赶快拿出来吧！'
    ); 
  end; 
end;

procedure _jinzhen_force5;
begin
  if This_Player.GetBagItemCount('五级金针') > 0 then
  begin
    This_Player.Take('五级金针', 1);
    This_Player.Give('内功经验', 199680000);
    This_Npc.NpcDialog(This_Player,
    '你使用五级金针获得内功经验：199680000！\' 
    +'你还要使用哪个等级的金针？\ \'
    +'|{cmd}<一级金针/@jinzhen_force1>		    ^<二级金针/@jinzhen_force2>		    ^<三级金针/@jinzhen_force3>\ \'	
    +'|{cmd}<四级金针/@jinzhen_force4>		    ^<五级金针/@jinzhen_force5>\ \'
    +'|{cmd}<离开/@doexit>'
    );
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,
    '如果你已经带来了五级金针，就赶快拿出来吧！'
    ); 
  end; 
end; 

Begin
   if (This_Player.GoldNum < 110) and (This_Player.GetBagItemCount('花叶包鸡') < 1) then
   begin
      This_Npc.NpcDialog(This_Player,
      '是仓库管理员叫你来的吧，肯定是求我做花叶包鸡吧，他已经求\'+
      '过我好几次了，不过我太忙，一直没答应他，\'+
      '既然你来求我，我肯定要给你面子，我就抽空做一碗，\'+
      '要做菜要材料，要麻烦你给我找一些<纯度4以上/c=red>的鸡肉就可以了。\ \'+
      '|{cmd}<我有材料，先交给你/@givecai>              ^<好吧，我去找材料/@doexit>\'
      );
   end else
   begin
      This_Npc.NpcDialog(This_Player,
      '感谢来到庄园！大家可以先欣赏一下这里美丽的景色，\'+
      '目前庄园只在中庭开放元宝锻造和元宝交易的功能，\'+
      '其他的各种神秘功能将会逐渐开放，别忘了常来看看哦！\ \'
      );
   end;
end.