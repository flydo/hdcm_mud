{*************************************************************************************
** 淡淡的哀愁  独家制作 拍卖行功能 QQ 253353132                                      *
请将《Auction.txt》放到目录D:\mud2.0\Mir200\Share\config\下	                         *
使用变量 G(11,11) 拍卖总流水号   G(11,12) 总拍卖物计数。                             *
私人变量 V(11,1~10)对应1~10个拍品的流水号 11 卖家临时标记 12 卖出物总数 13提取极品   *
私人变量 S(11,1~10)对应1~10个参与竞拍的流水号 11 买家临时标记 12竞价物总数 13扣费凭证*
重要：1,进行变量查重; 2,添加NPC定时执行参数 10-30秒。
**************************************************************************************}
program mir2;


procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;


var
pm_cost,pm_maxman,pm_level,pm_max,pm_cancel,pm_lose,pm_add,pm_limit,limit_gold,limit_YB,limit_hb,limit_LF:integer;
pm_find,pm_kind,pm_wphb:string;
procedure OnInitialize;
begin


pm_max:=100; 		//服务器最多同时拍卖的数量
pm_limit:=5;		//单人最高同时拍卖的数量(推荐5,最高不超过10)
pm_level:=40;		//卖家等级限制
pm_cost:=2; 		//成交手续费%(按最高出价)
pm_cancel:=10;		//拍卖中强制取消手续费%(按最高出价)
pm_lose:=2;     	//流拍手续费(按起拍价)
pm_add:=5;			//每次叫价按起拍价递增的百分比
pm_wphb:=''; 		//物品类型的货币名称,留空则为不使用(需保留单引号'')。
limit_gold:=10000; 	//金币的最低起拍价 
limit_YB:=1; 		//元宝的最低起拍价 
limit_LF:=10; 		//灵符的最低起拍价
limit_hb:=1;		//物品货币最低起拍价
pm_maxman:=50;		//单一物品最多参与拍卖的人数

end; 


function getf(x : String) : String;
begin
	result := copy(x, 0, pos(',',x)-1); 	//COPY函数：返回字符串x中从第0个字符起共 pos函数个字符
end;										//POS函数： 返回字符串x中最早出现,的位置

function getb(x : String) : String;
begin
	result := copy(x, pos(',',x)+1, length(x));
end;

function getpmitem(zb : String) : String;
begin

//职业类型除'叠加'表示可叠加物品外 其他可以根据情况自行编写

{物品类型固定12种，乱写的话无法分类查看 			不同装备的极品类别按顺序注释		
                                                			武器：攻,魔,道,运,诅咒  
        '武器';                                 			防具：防,魔防,攻,魔,道  
        '衣服';                                 (stdmode=19)项链：魔躲,运,攻,魔,道  
        '项链';                                 (stdmode=20)项链：准,敏,攻,魔,道    
        '戒指';                                 (stdmode=21)项链：体复,魔复,攻,魔,道
        '手镯';                                 (stdmode=22)戒指：防,魔防,攻,魔,道	
		'材料';		                            (stdmode=23)戒指：毒躲,毒复,攻,魔,道
        '鞋子';                                 (stdmode=24)手镯：准,敏,攻,魔,道    
        '头盔';                                 (stdmode=26)手镯：防,魔防,攻,魔,道	
		'宝石';		   
        '勋章';
        '斗笠';
        '消耗';
}
								//职业,类型,最大数量(叠加物品有效),极品类别按顺序注释							   
	case zb of
//武器示例								   
		'怒斩'			: result := '战士,武器,1,攻,魔,道,运,诅咒';
		'龙牙' 			: result := '法师,武器,1,攻,魔,道,运,诅咒';
		'逍遥扇' 		: result := '道士,武器,1,攻,魔,道,运,诅咒';
		'屠龙' 			: result := '战士,武器,1,攻,魔,道,运,诅咒';
		'嗜魂法杖' 		: result := '法师,武器,1,攻,魔,道,运,诅咒';
		'黑铁银蛇' 		: result := '道士,武器,1,攻,魔,道,运,诅咒';
		'绿玉裁决'		: result := '战士,武器,1,攻,魔,道,运,诅咒';
		'白玉法杖' 		: result := '法师,武器,1,攻,魔,道,运,诅咒';
		'金域无极' 		: result := '道士,武器,1,攻,魔,道,运,诅咒';
		'绿玉屠龙'   	: result := '战士,武器,1,攻,魔,道,运,诅咒';
        '蓝灵法杖' 		: result := '法师,武器,1,攻,魔,道,运,诅咒';		
		'金域无极' 		: result := '道士,武器,1,攻,魔,道,运,诅咒';
		'战神之刃'   	: result := '战士,武器,1,攻,魔,道,运,诅咒';
        '仙人蒲扇' 		: result := '法师,武器,1,攻,魔,道,运,诅咒';
		'龙皇霸气法剑' 		: result := '法师,武器,1,攻,魔,道,运,诅咒';
		'龙皇霸气道剑' 		: result := '道士,武器,1,攻,魔,道,运,诅咒';
		'龙皇霸气战剑' 		: result := '战士,武器,1,攻,魔,道,运,诅咒';
		'不灭V冰火法刀' 		: result := '法师,武器,1,攻,魔,道,运,诅咒';
		'不灭V冰火道刀' 		: result := '道士,武器,1,攻,魔,道,运,诅咒';
		'不灭V冰火战刀' 		: result := '战士,武器,1,攻,魔,道,运,诅咒';
		'辉煌炽火法剑' 		: result := '法师,武器,1,攻,魔,道,运,诅咒';
		'辉煌炽火道剑' 		: result := '道士,武器,1,攻,魔,道,运,诅咒';
		'辉煌炽火战剑' 		: result := '战士,武器,1,攻,魔,道,运,诅咒';
		'灭世天神掌' 		: result := '通用,武器,1,攻,魔,道,运,诅咒';
		'破馆珍剑' 		    : result := '通用,武器,1,攻,魔,道,运,诅咒';
		'魅影之刃' 		    : result := '通用,武器,1,攻,魔,道,运,诅咒';
		'赤血魔剑[神]' 		: result := '通用,武器,1,攻,魔,道,运,诅咒';
		'返朴归真斩[天]' 	: result := '通用,武器,1,攻,魔,道,运,诅咒';
//头盔,衣服,四格物品示例
		'宙神头盔'		: result := '战士,头盔,1,防,魔防,攻,魔,道';
		'勇士头盔'  	: result := '法师,头盔,1,防,魔防,攻,魔,道';
		'道域头盔'  	: result := '道士,头盔,1,防,魔防,攻,魔,道';
		'黑暗头盔'  	: result := '战士,头盔,1,防,魔防,攻,魔,道';
        '朱雀头盔'		: result := '法师,头盔,1,防,魔防,攻,魔,道';
	    '贝迪头盔'  	: result := '道士,头盔,1,防,魔防,攻,魔,道';
		'将军头盔'  	: result := '战士,头盔,1,防,魔防,攻,魔,道';
        '风铃火山'		: result := '法师,头盔,1,防,魔防,攻,魔,道';
	    '蓝灵头盔'  	: result := '道士,头盔,1,防,魔防,攻,魔,道';
		'灵魂钻石'  	: result := '道士,勋章,1,防,魔防,攻,魔,道';
		'真爱钻石'  	: result := '战士,勋章,1,防,魔防,攻,魔,道';
		'魔法勋章'  	: result := '法师,勋章,1,防,魔防,攻,魔,道';
		
	    '天魔神甲'		: result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'圣战宝甲'      : result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'法神披风'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
		'霓裳羽衣'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
        '天师长袍'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'天尊道袍'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		
		'白虎战甲(男)'		: result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'白虎战甲(女)'      : result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'朱雀神袍(男)'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
		'朱雀神袍(女)'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
        '青龙道衣(男)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'青龙道衣(女)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';	
	    '赤月腰带(战)'	: result := '战士,腰带,1,防,魔防,攻,魔,道';
		'赤月腰带(法)'	: result := '法师,腰带,1,防,魔防,攻,魔,道';
		'赤月腰带(道)'	: result := '道士,腰带,1,防,魔防,攻,魔,道';
		'赤月靴子(战)'	: result := '战士,靴子,1,防,魔防,攻,魔,道';
		'赤月靴子(法)'	: result := '法师,靴子,1,防,魔防,攻,魔,道';
		'赤月靴子(道)'	: result := '道士,靴子,1,防,魔防,攻,魔,道';
		
		

//普通首饰类示例		
        '浮游项链'  	: result := '战士,项链,1,防,魔防,攻,魔,道';
		'浮游手镯'  	: result := '战士,手镯,1,防,魔防,攻,魔,道';
		'浮游戒指'  	: result := '战士,戒指,1,防,魔防,攻,魔,道';
		'铁血项链'  	: result := '战士,项链,1,防,魔防,攻,魔,道';
		'铁血手镯'  	: result := '战士,手镯,1,防,魔防,攻,魔,道';
		'铁血戒指'  	: result := '战士,戒指,1,防,魔防,攻,魔,道';
		'魔幻项链'  	: result := '法师,项链,1,防,魔防,攻,魔,道';
		'魔幻手镯'  	: result := '法师,手镯,1,防,魔防,攻,魔,道';
        '魔幻戒指'  	: result := '法师,戒指,1,防,魔防,攻,魔,道';
		'摄魂项链'  	: result := '法师,项链,1,防,魔防,攻,魔,道';
		'摄魂手镯'  	: result := '法师,手镯,1,防,魔防,攻,魔,道';
		'摄魂戒指'  	: result := '法师,戒指,1,防,魔防,攻,魔,道';
		'白银项链'  	: result := '道士,项链,1,防,魔防,攻,魔,道';
		'白银手镯'  	: result := '道士,手镯,1,防,魔防,攻,魔,道';
		'白银戒指'  	: result := '道士,戒指,1,防,魔防,攻,魔,道';
		'道尊项链'  	: result := '道士,项链,1,防,魔防,攻,魔,道';
		'道尊手镯'  	: result := '道士,手镯,1,防,魔防,攻,魔,道';
		'道尊戒指'  	: result := '道士,戒指,1,防,魔防,攻,魔,道';
//50级首饰类示例

		'铁骑战甲(男)'		: result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'铁骑战甲(女)'      : result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'猩猩魔袍(男)'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
		'猩猩魔袍(女)'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
        '麒麟道衣(男)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'麒麟道衣(女)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'卦'  	: result := '道士,勋章,1,防,魔防,攻,魔,道';
		'龙'  	: result := '战士,勋章,1,防,魔防,攻,魔,道';
		'凤'  	: result := '法师,勋章,1,防,魔防,攻,魔,道';	
		'魔天项链'  	: result := '法师,项链,1,防,魔防,攻,魔,道';
		'魔天手镯'  	: result := '法师,手镯,1,防,魔防,攻,魔,道';
		'魔天戒指'  	: result := '法师,戒指,1,防,魔防,攻,魔,道';
		'蓝灵项链'  	: result := '道士,项链,1,防,魔防,攻,魔,道';
		'蓝灵手环'  	: result := '道士,手镯,1,防,魔防,攻,魔,道';
		'蓝灵戒指'  	: result := '道士,戒指,1,防,魔防,攻,魔,道';
		'将军项链'  	: result := '战士,项链,1,防,魔防,攻,魔,道';
		'将军手镯'  	: result := '战士,手镯,1,防,魔防,攻,魔,道';
		'将军戒指'  	: result := '战士,戒指,1,防,魔防,攻,魔,道';
//55级首饰类示例
		'龙皇霸气盔(战)'	  : result := '战士,头盔,1,防,魔防,攻,魔,道';
		'龙皇霸气盔(法)'  	  : result := '法师,头盔,1,防,魔防,攻,魔,道';
		'龙皇霸气盔(道)'      : result := '道士,头盔,1,防,魔防,攻,魔,道';
        '龙皇霸龙甲(战)'	  : result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'龙皇霸凤铠(战)'      : result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'龙皇霸龙甲(法)'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
		'龙皇霸凤铠(法)'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
        '龙皇霸龙甲(道)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'龙皇霸凤铠(道)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'龙皇霸气心'      	: result := '通用,勋章,1,防,魔防,攻,魔,道';
		'龙皇霸气链(法)'  	: result := '法师,项链,1,防,魔防,攻,魔,道';
		'龙皇霸气镯(法)'  	: result := '法师,手镯,1,防,魔防,攻,魔,道';
		'龙皇霸气戒(法)'  	: result := '法师,戒指,1,防,魔防,攻,魔,道';
		'龙皇霸气链(道)'  	: result := '道士,项链,1,防,魔防,攻,魔,道';
		'龙皇霸气镯(道)'  	: result := '道士,手镯,1,防,魔防,攻,魔,道';
		'龙皇霸气戒(道)'  	: result := '道士,戒指,1,防,魔防,攻,魔,道';
		'龙皇霸气链(战)'  	: result := '战士,项链,1,防,魔防,攻,魔,道';
		'龙皇霸气镯(战)'  	: result := '战士,手镯,1,防,魔防,攻,魔,道';
		'龙皇霸气戒(战)'  	: result := '战士,戒指,1,防,魔防,攻,魔,道';
//65级首饰类示例
		'不灭V神盔(战)'	  : result := '战士,头盔,1,防,魔防,攻,魔,道';
		'不灭V神盔(法)'  	  : result := '法师,头盔,1,防,魔防,攻,魔,道';
		'不灭V神盔(道)'      : result := '道士,头盔,1,防,魔防,攻,魔,道';
        '不灭V紫神甲(战)'		: result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'不灭V紫神铠(战)'      : result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'不灭V紫神甲(法))'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
		'不灭V紫神铠(法)'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
        '不灭V紫神甲(道)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'不灭V紫神铠(道)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'不灭V神灵'      	: result := '通用,勋章,1,防,魔防,攻,魔,道';
		'不灭V神链(法)'  	: result := '法师,项链,1,防,魔防,攻,魔,道';
		'不灭V神镯(法)'  	: result := '法师,手镯,1,防,魔防,攻,魔,道';
		'不灭V神戒(法)'  	: result := '法师,戒指,1,防,魔防,攻,魔,道';
		'不灭V神链(道)'  	: result := '道士,项链,1,防,魔防,攻,魔,道';
		'不灭V神镯(道)'  	: result := '道士,手镯,1,防,魔防,攻,魔,道';
		'不灭V神戒(道)'  	: result := '道士,戒指,1,防,魔防,攻,魔,道';
		'不灭V神链(战)'  	: result := '战士,项链,1,防,魔防,攻,魔,道';
		'不灭V神镯(战)'  	: result := '战士,手镯,1,防,魔防,攻,魔,道';
		'不灭V神戒(战))'  	: result := '战士,戒指,1,防,魔防,攻,魔,道';
//70级首饰类示例
		'辉煌V神冠(战)'	  : result := '战士,头盔,1,防,魔防,攻,魔,道';
		'辉煌V神冠(法)'  	  : result := '法师,头盔,1,防,魔防,攻,魔,道';
		'辉煌V神冠(道)'      : result := '道士,头盔,1,防,魔防,攻,魔,道';
        '辉煌寒冰甲(战)'		: result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'辉煌寒冰铠(战)'      : result := '战士,衣服,1,防,魔防,攻,魔,道'; 
		'辉煌寒冰甲(法))'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
		'辉煌寒冰铠(法)'      : result := '法师,衣服,1,防,魔防,攻,魔,道'; 
        '辉煌寒冰甲(道)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'辉煌寒冰铠(道)'      : result := '道士,衣服,1,防,魔防,攻,魔,道';
		'辉煌神魄心灵'      	: result := '通用,勋章,1,防,魔防,攻,魔,道';
		'辉煌V神链(法)'  	: result := '法师,项链,1,防,魔防,攻,魔,道';
		'辉煌V神镯(法)'  	: result := '法师,手镯,1,防,魔防,攻,魔,道';
		'辉煌V神戒(法)'  	: result := '法师,戒指,1,防,魔防,攻,魔,道';
		'辉煌V神链(道)'  	: result := '道士,项链,1,防,魔防,攻,魔,道';
		'辉煌V神镯(道)'  	: result := '道士,手镯,1,防,魔防,攻,魔,道';
		'辉煌V神戒(道)'  	: result := '道士,戒指,1,防,魔防,攻,魔,道';
		'辉煌V神链(战)'  	: result := '战士,项链,1,防,魔防,攻,魔,道';
		'辉煌V神镯(战)'  	: result := '战士,手镯,1,防,魔防,攻,魔,道';
		'辉煌V神戒(战))'  	: result := '战士,戒指,1,防,魔防,攻,魔,道';
		
//80级首饰类示例
        '灭世神威盔'	 : result := '通用,头盔,1,防,魔防,攻,魔,道';
        '灭世蝴蝶甲'	: result := '通用,衣服,1,防,魔防,攻,魔,道'; 
		'灭世蝴蝶铠'    : result := '通用,衣服,1,防,魔防,攻,魔,道'; 
		'灭世神威勋'    : result := '通用,勋章,1,防,魔防,攻,魔,道';
		'灭世神威链'  	: result := '通用,项链,1,防,魔防,攻,魔,道';
		'灭世神威镯'  	: result := '通用,手镯,1,防,魔防,攻,魔,道';
		'灭世神威戒'  	: result := '通用,戒指,1,防,魔防,攻,魔,道';
		
//90级首饰类示例
        '凤凰の飞盔'	 : result := '通用,头盔,1,防,魔防,攻,魔,道';
        '凤凰の飞甲'	 : result := '通用,衣服,1,防,魔防,攻,魔,道'; 
		'凤凰の飞铠'     : result := '通用,衣服,1,防,魔防,攻,魔,道'; 
		'凤凰の飞勋'     : result := '通用,勋章,1,防,魔防,攻,魔,道';
		'凤凰の飞链'  	 : result := '通用,项链,1,防,魔防,攻,魔,道';
		'凤凰の飞镯' 	 : result := '通用,手镯,1,防,魔防,攻,魔,道';
		'凤凰の飞戒'  	 : result := '通用,戒指,1,防,魔防,攻,魔,道';
//100级首饰类示例
        '诅咒の盔'	    : result := '通用,头盔,1,防,魔防,攻,魔,道';
        '诅咒の甲'		: result := '通用,衣服,1,防,魔防,攻,魔,道'; 
		'诅咒の铠'      : result := '通用,衣服,1,防,魔防,攻,魔,道'; 
		'诅咒の勋'      : result := '通用,勋章,1,防,魔防,攻,魔,道';
		'诅咒の链'  	: result := '通用,项链,1,防,魔防,攻,魔,道';
		'诅咒の镯'    	: result := '通用,手镯,1,防,魔防,攻,魔,道';
		'诅咒の戒'  	: result := '通用,戒指,1,防,魔防,攻,魔,道';
				
//特殊首饰类示例
        '记忆项链'  	: result := '通用,项链,1,魔躲,运,攻,魔,道';
		'狂风项链'  	: result := '通用,手镯,1,体复,魔复,攻,魔,道';
		'祈祷戒指'  	: result := '通用,戒指,1,毒躲,毒复,攻,魔,道';
		'夏普儿手镯'  	: result := '通用,项链,1,准,敏,攻,魔,道';

//叠物品示例
		'祝福油'  		    : result := '叠加,材料,99,';	//可叠加 材料,每次上架的最高数量为10 无极品注释因此可以不写，但是必须以,号结尾
		'死亡坟章'  		: result := '叠加,材料,99,';
		'天外飞石'  		: result := '叠加,材料,99,';
		'武士之星'  		: result := '叠加,材料,99,';
		'法师之星'  		: result := '叠加,材料,99,';
		'道士之星'  		: result := '叠加,材料,99,';
		'书页'  		    : result := '叠加,材料,99,';
		'白虎巨牙'  		: result := '叠加,材料,99,';
		'真龙天骨'  		: result := '叠加,材料,99,';
		'万年玄冰'  		: result := '叠加,材料,99,';
		'千年暖玉'  		: result := '叠加,材料,99,';
		'会员兑换证'  		: result := '叠加,材料,1,';
		'轮回碎片'  		: result := '叠加,材料,99,';
		
		
		else
			result := '';
end;
end;

procedure _myinfo(id:string);
var wpidx,wpname,wpstate,wpstr,wptype,cmdstr,perstr,astr,color,lqzt,jpstr,statestr,pmcmd:string;
i,ppos,wpnum,bgold,sellgold:integer;
begin
	cmdstr:='';
	astr:='';
	
	if id = '2' then
	begin
		perstr:='卖家';
		astr:='|{cmd}<拍卖大厅/@main>^<买家中心/@myinfo~1>';
		for i:=1  to pm_limit do
		begin
			wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(i)+'',This_Player.Name);
			if length(wpidx) > 0 then
			begin
				color:='254';//默认颜色
				wpname:=ReadIniSectionStr('Auction.txt',wpidx,'物品名');
				wpstate:=ReadIniSectionStr('Auction.txt',wpidx,'拍卖状态');
				wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'物品数量'),1);
				wptype:=ReadIniSectionStr('Auction.txt',wpidx,'职业类型');
				bgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'起拍价'),0);
				jpstr:=ReadIniSectionStr('Auction.txt',wpidx,'极品');
				sellgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'一口价'),-1);
				pmcmd:='<管理/@wupininfo~'+inttostr(i)+'>|';
				if ((jpstr='是') and ((wpstate='已取消') or (wpstate = '流拍已领'))) then
				begin
					pmcmd:='<提取/@jipin~'+wpidx+'>|';
					wpstate:='极品提取';
					This_Player.SetV(11,11,i);
				end
				
				statestr:='筹备中';
				if wpstate='已成交' then statestr:='<'+wpstate+'/fcolor=250>';
				if wpstate='已流拍' then statestr:='<'+wpstate+'/fcolor=247>';
				if wpstate='竞拍中' then statestr:='<'+wpstate+'/fcolor=168>';
				if wpstate='极品提取' then statestr:='<'+wpstate+'/fcolor=249>';
				
				if sellgold > bgold then color:='249';//一口价颜色
				if jpstr= '是' then color:='253';//极品颜色
				wpstr:='<'+wpname+'/fcolor='+color+'>';
				if wptype='叠加' then wpstr:='<'+wpname+'*'+inttostr(wpnum)+'/fcolor='+color+'>';
				cmdstr:=cmdstr+'<拍卖货架/fcolor=242> ^'+wpstr+'  ^'+statestr+'  ^'+pmcmd;
			end else
			begin
				cmdstr:=cmdstr+'<拍卖货架/fcolor=242> ^<空空如也,去上架物品/@paimai~'+inttostr(i)+'>^|';
			end
		end;
	end else
	begin
		perstr:='买家';
		astr:='|{cmd}<拍卖大厅/@main>^<卖家中心/@myinfo~2>';
		for i:=1 to pm_limit do
		begin
			ppos:=This_Player.GetS(11,i);
			if ppos >= 100000 then
			begin
				color:='254';//默认颜色
				wpname:=ReadIniSectionStr('Auction.txt',inttostr(ppos),'物品名');
				wpstate:=ReadIniSectionStr('Auction.txt',inttostr(ppos),'拍卖状态');
				wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',inttostr(ppos),'物品数量'),1);
				wptype:=ReadIniSectionStr('Auction.txt',inttostr(ppos),'职业类型');
				bgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',inttostr(ppos),'起拍价'),0);
				sellgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',inttostr(ppos),'一口价'),-1);
				jpstr:=ReadIniSectionStr('Auction.txt',inttostr(ppos),'极品');
				lqzt:=ReadIniSectionStr('Auction.txt',inttostr(ppos),'领取状态');
				pmcmd:='<查看/@paipin~'+inttostr(ppos)+'>|';
				if (jpstr='是') and (lqzt='已领取') then
				begin
					pmcmd:='<提取/@jipin~'+inttostr(ppos)+'>|';
					wpstate:='极品提取';
					This_Player.SetS(11,11,i);					
				end
				
				statestr:='已取消';				
				if wpstate='已成交' then statestr:='<'+wpstate+'/fcolor=250>';
				if wpstate='已流拍' then statestr:='<'+wpstate+'/fcolor=247>';
				if wpstate='竞拍中' then statestr:='<'+wpstate+'/fcolor=168>';
				if wpstate='极品提取' then statestr:='<'+wpstate+'/fcolor=249>';
				
				if sellgold > bgold then color:='249';//一口价颜色
				if jpstr= '是' then color:='253';//极品颜色				
				wpstr:='<'+wpname+'/fcolor='+color+'>';
				if wptype='叠加' then wpstr:='<'+wpname+'*'+inttostr(wpnum)+'/fcolor='+color+'>';
				cmdstr:=cmdstr+'<购物车/fcolor=242> ^'+wpstr+'  ^'+statestr+'  ^'+pmcmd;
			end else
			begin
				cmdstr:=cmdstr+'<购物车/fcolor=242> ^<空空如也,去拍卖大厅/@main>^|';
			end;
		end
	end
	
	This_NPC.NpcDialog(This_Player,
	'|^<★拍卖行·'+perstr+'中心★/fcolor=250>^^'
	+'|<————————————————————/fcolor=247>|'
//	+astr
//	+'|<————————————————————/fcolor=247>|'
	+cmdstr
	+'<————————————————————/fcolor=247>'
	+astr
	);
end;

procedure _paimai(id : string);
var i:integer;
begin
	for i:=1 to 12 do
	begin
		if This_Player.GetV(11,i) < 0 then This_Player.SetV(11,i,0); //卖家变量初始化
		if This_Player.GetS(11,i) < 0 then This_Player.SetS(11,i,0); //买家变量初始化
	end;

	if (This_Player.Level >= pm_level) or (This_Player.GMLevel > 0) then //普通玩家限制等级使用，GM不限制
	begin
		if This_Player.GetV(11,12) < pm_limit then //小于拍卖限制
		begin
			This_Npc.Click_CommitItem(This_Player, 1, '请放入你想拍卖的物品');
			This_Player.SetV(11,11,strtoint(id)); //拍卖物品栏位标记
			if This_Player.GetV(11,13) > 0 then  This_Player.SetV(11,13,0);//上架时极品提取标识清零
		end else 
			This_Npc.NpcDialog(This_Player, '每人最多只能同时拍卖'+inttostr(pm_limit)+'个物品|{cmd}<返回/@main>');
	end else
		This_Npc.NpcDialog(This_Player, '需要'+inttostr(pm_level)+'级以上才能拍卖物品|{cmd}<返回/@main>');
end;

procedure _jipin(id:string);
var wpname,jpstr,sxstr,wptype,zb:string;
abil:array [1..5] of integer;abilstr:array [1..5] of string;
i:integer;
begin

	wpname:=ReadIniSectionStr('Auction.txt',id,'物品名');
	wptype:=ReadIniSectionStr('Auction.txt',id,'物品类型');
	jpstr:=ReadIniSectionStr('Auction.txt',id,'极品');

	if jpstr= '是' then
	begin
		for i:=1 to 5 do
		begin
			abil[i]:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'Abil'+inttostr(i)),0);
			abilstr[i]:='';
		end

		zb := getpmitem(wpname);		
		if abil[1] >0 then 	abilstr[1]:=''+getf(getb(getb(getb(zb))))+' '+inttostr(abil[1])+' ';//极品注释1
		if abil[2] >0 then 	abilstr[2]:=''+getf(getb(getb(getb(getb(zb)))))+' '+inttostr(abil[2])+' ';//极品注释2
		if abil[3] >0 then 	abilstr[3]:=''+getf(getb(getb(getb(getb(getb(zb))))))+' '+inttostr(abil[3])+' ';//极品注释3
		if abil[4] >0 then 	abilstr[4]:=''+getf(getb(getb(getb(getb(getb(getb(zb)))))))+' '+inttostr(abil[4])+' ';//极品注释4
		if abil[5] >0 then 	abilstr[5]:=''+getb(getb(getb(getb(getb(getb(getb(getb(zb))))))))+' '+inttostr(abil[5])+' ';//极品注释5
			
		sxstr:='';
		for i:=1 to 5 do
		begin
			sxstr:=sxstr+abilstr[i];
		end
		
		This_NPC.NpcDialog(This_Player,
		'你有一件<'+wpname+'/fcolor=253>的拍卖物品可以提取:'
		+'|<极品属性:/fcolor=242> <'+sxstr+'/fcolor=253>'
		+'|{cmd}<提取属性/@getjipin~'+id+'> ^<返回/@myinfo~1>'
		);

	end else
		This_NPC.NpcDialog(This_Player,'未知错误!'+id);
end;

procedure _getjipin(id:string);
var wpname:string;
begin
	This_Player.SetV(11,13,strtoint(id));
	wpname:=ReadIniSectionStr('Auction.txt',id,'物品名');	
	This_NPC.Click_CommitItem(This_Player,1,'请放入'+wpname);
end;

procedure _maijiainfo(namestr:string);
var i:integer;
wp_idx,wp_name,wp_state,wp_gold:array [1..10] of string;
begin
	for i:= 1 to pm_limit do
	begin
		wp_idx[i]:=ReadIniSectionStr('Auction.txt','物品'+inttostr(i)+'',This_Player.Name); //读取是否有拍卖物品
		
		if length(wp_idx[i]) > 0 then
		begin
			wp_name[i]:=ReadIniSectionStr('Auction.txt',wp_idx[i],'物品名');
			wp_state[i]:=ReadIniSectionStr('Auction.txt',wp_idx[i],'拍卖状态');
			wp_gold[i]:=ReadIniSectionStr('Auction.txt',wp_idx[i],'拍卖状态');
		end
	end
end;

procedure _wupininfo(id:string);
var wpidx,wptype,wpname,wpstate,wpgold,wpadd,wpjptime,wpmoneytype,losestr,donestr,xxstr,typestr,highgold,buyer,cmdstr,wpstr,numstr,sellstr,jpstr,sxstr,zb:string;
addgold,basegold,ngold,jptime,losegold,donegold,lesstime,starttime,wpnum,sellgold,i:integer;
abil:array [1..5] of integer; abilstr:array [1..5] of string;
begin
	This_Player.SetV(11,11,strtoint(id));//栏位标记
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+id+'',This_Player.Name);
	if length(wpidx) >0 then
	begin
		wpstate:=ReadIniSectionStr('Auction.txt',wpidx,'拍卖状态');
		wpmoneytype:=ReadIniSectionStr('Auction.txt',wpidx,'货币类型');
		ngold:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'成交价'),0);
		jptime:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'竞拍时长'),0);
		basegold:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'起拍价'),0);
		sellgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'一口价'),-1);
		wpname:=ReadIniSectionStr('Auction.txt',wpidx,'物品名');
		wptype:=ReadIniSectionStr('Auction.txt',wpidx,'职业类型');
		wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'物品数量'),1);
		jpstr:=ReadIniSectionStr('Auction.txt',wpidx,'极品');
		if jpstr= '是' then
		begin
			for i:= 1 to 5 do
			begin
				abilstr[i]:='';
				abil[i]:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'Abil'+inttostr(i)),0);
			end
			
			zb := getpmitem(wpname);		
			if abil[1] >0 then 	abilstr[1]:=''+getf(getb(getb(getb(zb))))+' '+inttostr(abil[1])+' ';//极品注释1
			if abil[2] >0 then 	abilstr[2]:=''+getf(getb(getb(getb(getb(zb)))))+' '+inttostr(abil[2])+' ';//极品注释2
			if abil[3] >0 then 	abilstr[3]:=''+getf(getb(getb(getb(getb(getb(zb))))))+' '+inttostr(abil[3])+' ';//极品注释3
			if abil[4] >0 then 	abilstr[4]:=''+getf(getb(getb(getb(getb(getb(getb(zb)))))))+' '+inttostr(abil[4])+' ';//极品注释4
			if abil[5] >0 then 	abilstr[5]:=''+getb(getb(getb(getb(getb(getb(getb(getb(zb))))))))+' '+inttostr(abil[5])+' ';//极品注释5
			
			sxstr:='|<极品属性:/fcolor=242> ^';
			for i:= 1 to 5 do
			begin
				sxstr:=sxstr+'<'+abilstr[i]+'/fcolor=253>';
			end
				sxstr:=sxstr+'^^';
		end else
		sxstr:='';		
		addgold:=(basegold*pm_add div 100);
		losegold:=(basegold*pm_lose div 100);
		donegold:=(ngold*pm_cost div 100);
		if addgold <=0 then addgold:=1; if losegold <=0  then losegold:=1;if donegold<=0 then donegold:=1;
		numstr:=''; if  wptype='叠加' then numstr:='<数量/@setnum~'+wpname+'>';
		
		wpstr:='<'+wpname+'/fcolor=254>';
		if wptype='叠加' then  wpstr:='<'+wpname+'*'+inttostr(wpnum)+'/fcolor=254>';
		
		losestr:='|<流拍费用:/fcolor=242> ^<'+inttostr(losegold)+'/fcolor=254>(<起拍价的'+inttostr(pm_lose)+'%/fcolor=247>)^^';
		donestr:='|<成交费用:/fcolor=242> ^<'+inttostr(donegold)+'/fcolor=254>(<成交价'+inttostr(pm_cost)+'%/fcolor=247>)^^';
		if wpstate = '筹备中' then
		begin		
			case wpmoneytype of
			'金币' : begin typestr:='<金币/fcolor=254> ^<重设/@resetmtype>^';  end;
			'元宝' : begin typestr:='<元宝/fcolor=254> ^<重设/@resetmtype>^';  end;
			'灵符' : begin typestr:='<灵符/fcolor=254> ^<重设/@resetmtype>^';  end;	
			pm_wphb : begin typestr:='<'+pm_wphb+'/fcolor=254> ^<重设/@resetmtype>^';  end;
			end;
			
			case jptime of
			0 : begin wpjptime:='<必选/fcolor=168>  <2/@settime~2>  <4/@settime~4>  <6/@settime~6>  <12/@settime~12>  <24/@settime~24>  <[小时]/fcolor=247>^^';  end;
			2 : begin wpjptime:='<2小时/fcolor=254> ^<重设/@resettime>^';  end;
			4 : begin wpjptime:='<4小时/fcolor=254> ^<重设/@resettime>^';  end;
			6 : begin wpjptime:='<6小时/fcolor=254> ^<重设/@resettime>^';  end;
			12 : begin wpjptime:='<12小时/fcolor=254> ^<重设/@resettime>^';  end;
			24 : begin wpjptime:='<24小时/fcolor=254> ^<重设/@resettime>^';  end;
			
			end;
			
			if length(wpmoneytype) <=0 then typestr:='<必选/fcolor=168> <金币/@setmtype~1> <元宝/@setmtype~2> <灵符/@setmtype~3> <'+pm_wphb+'/@setmtype~4>^^';
		

			wpadd:='<'+inttostr(addgold)+'/fcolor=254>(<起拍价的'+inttostr(pm_add)+'%/fcolor=247>)^^';
			wpgold:='<'+inttostr(basegold)+'/fcolor=254> ^<重设/@setbase>^';
			sellstr:='<非必选/fcolor=247>  <启用一口价/@sellgold>^^';
			if sellgold >= basegold then sellstr:='<'+inttostr(sellgold)+'/fcolor=254> ^<重设/@sellgold>^';
			
			if basegold <= 0 then
			begin
				wpgold:='<选择货币类型后设置/@setbase>^^'
				wpadd:='<起拍价的'+inttostr(pm_add)+'%,最低为1/fcolor=247>^^';
				losestr:='|<流拍费用:/fcolor=242> ^<起拍价的'+inttostr(pm_lose)+'%,最低为1/fcolor=247>^^';
			end
			
			wpname:=ReadIniSectionStr('Auction.txt',wpidx,'物品名');
			This_Npc.NpcDialog(This_Player,
			'|^<#'+wpidx+'#/fcolor=241> <'+wpstate+'/fcolor=243>^^'+
			+'|<————————————————————/fcolor=247>'
			+'|<拍卖物品:/fcolor=242> ^'+wpstr+' ^'+numstr+'^'
			+'|<货币类型:/fcolor=242> ^'+typestr
			+'|<起拍价格:/fcolor=242> ^'+wpgold
			+'|<加价幅度:/fcolor=242> ^'+wpadd
			+losestr
			+'|<竞拍时长:/fcolor=242> ^'+wpjptime
			+'|<一  口  价:/fcolor=242> ^'+sellstr
			+sxstr
			+'|<————————————————————/fcolor=247>'
			+'|<开始拍卖后不允许修改拍品的任何设置>'
			+'|<请务必确认拍品信息准确无误后再拍卖！>'
//			+'|<---------------------------------------------------------/fcolor=249>'
			+'|{cmd}<取消拍卖/@cancel~1> ^<立即拍卖/@pm_start> ^<返回/@myinfo~2>'
			); 
		
		end else	
//		if wpstate='竞拍中' then
		begin
			cmdstr:='|{cmd}<取消拍卖/@cancel_qr~'+wpidx+'> ^<返回/@myinfo~2>';
			if wpstate='已成交' then
			begin
				cmdstr:='|{cmd}<领取'+wpmoneytype+'/@jiesuan~'+wpidx+'> ^<返回/@myinfo~2>';
				xxstr:=donestr;
			end
			
			if wpstate='已流拍' then
			begin
				cmdstr:='|{cmd}<领回'+wpname+'/@jiesuan~'+wpidx+'> ^<返回/@myinfo~2>';
				xxstr:=losestr;
			end
			
			highgold:=ReadIniSectionStr('Auction.txt',wpidx,'当前价');
			if length(highgold) <=0 then highgold:=inttostr(basegold);
			buyer:=ReadIniSectionStr('Auction.txt',wpidx,'买家');
			if length(buyer) <= 0 then buyer:='暂无';
			starttime:=strtoint(ReadIniSectionStr('Auction.txt',wpidx,'开拍时间'));
			lesstime:=jptime*3600-(minusDataTime(GetNow,ConvertDBToDateTime(starttime)));
			sellstr:='|<一  口  价:/fcolor=242> ^<未启用/fcolor=255>^^';
			if sellgold >= basegold then sellstr:='|<一  口  价:/fcolor=242> ^<'+inttostr(sellgold)+'/fcolor=243>^^';
			
			if lesstime <=0 then lesstime:=0;
			This_Npc.NpcDialog(This_Player,
//			'|^<★物品'+id+'·竞拍中★/fcolor=249>^^'+
			'|^<#'+wpidx+'#/fcolor=241> <'+wpstate+'/fcolor=244>^^'+
			+'|<————————————————————/fcolor=247>'
			+'|<拍卖物品:/fcolor=242> ^<'+wpname+' * '+inttostr(wpnum)+'/fcolor=254> ^ ^'
			+'|<货币类型:/fcolor=242> ^<'+wpmoneytype+'/fcolor=254>^^'
			+sellstr
			+sxstr
			+'|<当前价格:/fcolor=242> ^<'+highgold+'/fcolor=254>^^'
			+'|<当前买家:/fcolor=242> ^<'+buyer+'/fcolor=254>^^'
			+'|<剩余时间:/fcolor=242> ^<'+inttostr(lesstime div 60)+'分钟/fcolor=254>^^'
			+xxstr
			+'|<————————————————————/fcolor=247>'
			+'|<拍卖成功将扣除成交价'+inttostr(pm_cost)+'%的费用>'
			+'|<取消拍卖将扣除当前价'+inttostr(pm_cancel)+'%的费用>'			
//			+'|<---------------------------------------------------------/fcolor=249>'
			+cmdstr
			);
		end 
		
	end else
		This_Npc.NpcDialog(This_Player,'你没有拍卖物品'+id+'');
end;

procedure _setnum(id:string);
begin
	This_NPC.InputDialog(This_Player,'请输入本次'+id+'的拍卖数量',0,8003);
end;

procedure P8003;
var wpidx,state,wpname:string;Inum,maxnum:integer;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11)),This_Player.Name);
	wpname:=ReadIniSectionStr('Auction.txt',wpidx,'物品名');
	state:=ReadIniSectionStr('Auction.txt',wpidx,'拍卖状态');
	maxnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'堆叠上限'),1);
	
	Inum:=StrToIntDef(This_Npc.InputStr,-1);
	if state='筹备中' then
	begin
		if (Inum > 0) and (Inum <= maxnum) then
		begin
			if This_Player.GetBagItemCount(wpname) >= Inum - 1 then
			begin
				WriteIniSectionStr('Auction.txt',wpidx,'物品数量',inttostr(Inum));
				_wupininfo(inttostr(This_Player.GetV(11,11)));
			end else
			begin
				This_Player.PlayerNotice('少骗人了！你身上没有这么多东西！',0);
			end;
		end else
			This_Player.PlayerNotice('限制输入1-'+inttostr(maxnum)+'的数字',0);
	end else
		This_Player.PlayerNotice('当前物品不可编辑',0);
end;

procedure _goldcost(idxxx:string);
var cgold:integer;id:string;
begin
	This_Player.SetS(11,13,0);//扣除标记初始化。
	
	id:=copy(idxxx, 0, pos(',', idxxx)-1);
	cgold:=strtoint(copy(idxxx, pos(',', idxxx)+1, length(idxxx)));
	if id='金币' then
	begin
		if This_Player.GoldNum >= cgold then
		begin
			This_Player.DecGold(cgold);
			This_Player.SetS(11,13,1);//扣除成功标记
		end else
		begin
			This_Player.PlayerNotice('你的金币不足'+inttostr(cgold)+'',0);
			exit;
		end
	end else
	if id='元宝' then
	begin
		if This_Player.YBNum >= cgold then
		begin
			This_Player.ScriptRequestSubYBNum(cgold);
			This_Player.SetS(11,13,1);//扣除成功标记
		end	else
		begin
			This_Player.PlayerNotice('你的元宝不足'+inttostr(cgold)+'',0);
			exit;
		end
	end else
	if id='灵符' then
	begin
		if This_Player.MyLFnum >= cgold then
		begin
			This_Player.DecLF(18001,cgold,false);
			This_Player.SetS(11,13,1);//扣除成功标记
		end else
		begin
			This_Player.PlayerNotice('你的灵符不足'+inttostr(cgold)+'',0);
			exit;
		end	
	end else
	if id=pm_wphb then
	begin
		if This_Player.GetBagItemCount(pm_wphb) >= cgold then
		begin
			This_Player.take(pm_wphb,cgold);
			This_Player.SetS(11,13,1);//扣除成功标记
		end else
		begin
			This_Player.PlayerNotice('你的'+pm_wphb+'不足'+inttostr(cgold)+'',0);
			exit;
		end	
	end else	
	begin
		This_Player.PlayerNotice('未知错误！',0);
		exit;
	end
end;

procedure _jiesuan(id:string);
var wpidx,oname,bname,lname,mtype,state,lqzt,jpstr,wpname:string;
zgold,lgold,wgold,bgold,wpnum,lpgold,i,ppos:integer;
begin
	state:=ReadIniSectionStr('Auction.txt',id,'拍卖状态');
	oname:=ReadIniSectionStr('Auction.txt',id,'卖家');
	bname:=ReadIniSectionStr('Auction.txt',id,'买家');
	mtype:=ReadIniSectionStr('Auction.txt',id,'货币类型');
	wpname:=ReadIniSectionStr('Auction.txt',id,'物品名');
	wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'物品数量'),1);
	bgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'起拍价'),0);	
	zgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'成交价'),0);
	jpstr:=ReadIniSectionStr('Auction.txt',id,'极品');
	lqzt:=ReadIniSectionStr('Auction.txt',id,'领取状态');
	wgold:=zgold*(100-pm_cost) div 100; //卖家收益
	lpgold:=bgold*pm_lose  div 100;//流拍费用
	if lpgold < 1 then lpgold:=1;

	if state = '已成交' then
	begin
		if This_Player.Name = oname then
		begin
			if mtype = '金币' then This_Player.AddGold(wgold);
			if mtype = '元宝' then This_Player.ScriptRequestAddYBNum(wgold);
			if mtype = '灵符' then This_Player.AddLF(0,wgold);
			if mtype =pm_wphb then This_Player.Give(pm_wphb,wgold);
			for i:=1 to pm_limit do
			begin
				wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(i),This_Player.Name);
				ppos:=This_Player.GetV(11,i);
				if wpidx=id then
				begin
					WriteIniSectionStr('Auction.txt','物品'+inttostr(i),This_Player.Name,''); //删除文本关联记录
				end
				if ppos=strtoint(id) then
				begin
					This_Player.SetV(11,i,0);		//清理关联变量
					This_Player.SetV(11,12,This_Player.GetV(11,12)-1);//总拍卖物减一
				end
			end

			WriteIniSectionStr('Auction.txt',id,'卖家',oname+' 已结算 '+MirDateTimeToStr('MM-dd hh:mm' , GetNow));
			_myinfo('2');
		end else
		begin
			if This_Player.Name = bname then //买家结算
			begin
				if (length(lqzt) <=0) and (jpstr='是') then
				begin
					if This_Player.FreeBagNum >= 3 then
					begin
						This_Player.Give(wpname,wpnum);
						WriteIniSectionStr('Auction.txt',id,'领取状态','已领取');
						This_Player.PlayerNotice('获得拍卖的物品,请提取属性！',2);
						_jipin(id);
						exit;
					end else
					begin
						This_Player.PlayerNotice('请至少预留出3个以上的背包空格再领取！',0);
						exit;
					end
				end else
				begin
					if (length(lqzt) <=0) then
					begin
						if This_Player.FreeBagNum >= 3 then
						begin
							This_Player.Give(wpname,wpnum);
							WriteIniSectionStr('Auction.txt',id,'领取状态','已领取');
						end else
						begin
							This_Player.PlayerNotice('请至少预留出3个以上的背包空格再领取！',0);
							exit;
						end;
					end
					
					if (lqzt = '极品属性已提取') or (jpstr <> '是') then
					begin
						for i:=1 to pm_limit do
						begin
							ppos:=This_Player.GetS(11,i);
							if ppos=strtoint(id) then
							begin
								This_Player.SetS(11,i,0);		//清理关联变量
								This_Player.SetS(11,12,This_Player.GetS(11,12)-1);//总拍卖物减一
								WriteIniSectionStr('Auction.txt',id,'买家',This_Player.Name+' 已结算 '+MirDateTimeToStr('MM-dd hh:mm' , GetNow));		
								break;
							end
						end
					end
				end
			end else   //竞价者结算
			begin
				for i:=1 to pm_limit do
				begin
					ppos:=This_Player.GetS(11,i);
					if ppos=strtoint(id) then
					begin
						This_Player.SetS(11,i,0);		//清理关联变量
						This_Player.SetS(11,12,This_Player.GetS(11,12)-1);//总拍卖物减一
						break;
					end
				end
				
				for i:=1 to pm_maxman do
				begin
					lname:=ReadIniSectionStr('Auction.txt',id,'出价人'+inttostr(i));
					if This_Player.Name = lname then
					begin
						lgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'出价'+inttostr(i)),0); 
						if lgold >0 then
						begin
							if mtype = '金币' then This_Player.AddGold(lgold);
							if mtype = '元宝' then This_Player.ScriptRequestAddYBNum(lgold);
							if mtype = '灵符' then This_Player.AddLF(0,lgold);
							if mtype =pm_wphb then This_Player.give(pm_wphb,lgold);
							WriteIniSectionStr('Auction.txt',id,'出价人'+inttostr(i),This_Player.Name+' 已结算 '+MirDateTimeToStr('MM-dd hh:mm' , GetNow));							
							break;
						end
					end
				end	
			end
			_myinfo('1');
		end
	end else
	if state ='已流拍' then
	begin
		if This_Player.Name = oname then
		begin
			if This_Player.FreeBagNum >= 3 then
			begin
				if (length(lqzt) <=0) and (jpstr='是') then
				begin
					_goldcost(mtype+','+inttostr(lpgold)); //流拍费用
					if 	This_Player.GetS(11,13)=1 then
					begin
						This_Player.Give(wpname,wpnum);
						WriteIniSectionStr('Auction.txt',id,'领取状态','已领取');
						WriteIniSectionStr('Auction.txt',id,'拍卖状态','流拍已领');
						This_Player.PlayerNotice('获得拍卖的物品,请提取属性！',2);
						_jipin(id);
						exit;
					end
				end else
				begin
					_goldcost(mtype+','+inttostr(lpgold)); //流拍费用
					if 	This_Player.GetS(11,13)=1 then
					begin
						This_Player.Give(wpname,wpnum);	//退回物品
						WriteIniSectionStr('Auction.txt',id,'拍卖状态','流拍已领');					
						for i:=1 to pm_limit do
						begin
							wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(i),This_Player.Name);
							ppos:=This_Player.GetV(11,i);
							if wpidx=id then
							begin
								WriteIniSectionStr('Auction.txt','物品'+inttostr(i),This_Player.Name,''); //删除文本关联记录
							end
							if ppos=strtoint(id) then
							begin
								This_Player.SetV(11,i,0);		//清理关联变量
								This_Player.SetV(11,12,This_Player.GetV(11,12)-1);//总拍卖物减一
							end
						end
						WriteIniSectionStr('Auction.txt',id,'卖家',oname+' 已结算 '+MirDateTimeToStr('MM-dd hh:mm' , GetNow));
						_myinfo('2');
					end
				end
			end else
			begin
				This_Player.PlayerNotice('请至少预留出3个以上的背包空格再领取！',0);
				exit;
			end					
		end;	
	end else
	if state ='已取消' then
	begin
		for i:=1 to pm_limit do
		begin
			ppos:=This_Player.GetS(11,i);
			if ppos=strtoint(id) then
			begin
				This_Player.SetS(11,i,0);		//清理关联变量
				This_Player.SetS(11,12,This_Player.GetS(11,12)-1);//总拍卖物减一
				break;
			end
		end
		
		for i:=1 to pm_maxman do
		begin
			lname:=ReadIniSectionStr('Auction.txt',id,'出价人'+inttostr(i));
			if This_Player.Name = lname then
			begin
				lgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'出价'+inttostr(i)),0); 
				if lgold >0 then
				begin
					if mtype = '金币' then This_Player.AddGold(lgold);
					if mtype = '元宝' then This_Player.ScriptRequestAddYBNum(lgold);
					if mtype = '灵符' then This_Player.AddLF(0,lgold);
					if mtype =pm_wphb then This_Player.give(pm_wphb,lgold);
					WriteIniSectionStr('Auction.txt',id,'出价人'+inttostr(i),This_Player.Name+' 已结算 '+MirDateTimeToStr('MM-dd hh:mm' , GetNow));							
					break;
				end
			end
		end
		if This_Player.Name = oname then 
		begin
			_myinfo('2');
		end else
			_myinfo('1');
	end
end;

procedure _cancel_qr(id:string);
var wpname,wpstate,mtype:string;
ngold,cgold:integer;
begin
	wpstate:=ReadIniSectionStr('Auction.txt',id,'拍卖状态');
	wpname:=ReadIniSectionStr('Auction.txt',id,'物品名');
	mtype:=ReadIniSectionStr('Auction.txt',id,'货币类型');
	ngold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'当前价'),0);
	cgold:=ngold*pm_cancel div 100;
	if cgold<=0 then cgold:=1;
	
	if wpstate='竞拍中' then
	begin
		This_NPC.NpcDialog(This_Player,
		'当前<#'+id+'>号拍品<'+wpname+'/fcolor=250>正在竞拍中,强行取消将会扣除<'+inttostr(cgold)+mtype+'>(<当前价的'+inttostr(pm_cancel)+'%/fcolor=247>),你一定要取消拍卖吗?'
		+'|{cmd}<确认取消/@cancel~2> <返回拍卖/@wupininfo~'+inttostr(This_Player.GetV(11,11))+'>'
		);
	end else
	begin
		This_Player.PlayerNotice('物品状态已改变，已为你刷新,请重新操作！',0);
		_myinfo('2');
	end
	
end;


procedure _cancel(id:string);
var wpidx,wpname,mtype,PM_LIST,oname,jpstr:string;
highgold,wpnum,cancelgold,i:integer;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	if length(wpidx) > 0 then
	begin
		wpname:=ReadIniSectionStr('Auction.txt',wpidx,'物品名');
		wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'物品数量'),1);
		oname:=ReadIniSectionStr('Auction.txt',wpidx,'卖家');
		highgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'当前价'),1);
		jpstr:=ReadIniSectionStr('Auction.txt',wpidx,'极品');
		mtype:=ReadIniSectionStr('Auction.txt',wpidx,'货币类型');
		cancelgold:=highgold*pm_cancel div 100; //强制取消
		if cancelgold <= 0 then cancelgold:=1;
	
		if jpstr = '是' then
		begin
			if id= '2' then
			begin
				_goldcost(mtype+','+inttostr(cancelgold));
				if 	This_Player.GetS(11,13)=1 then//扣除成功标记
				begin
					for i:=1 to pm_max do
					begin
						PM_LIST:=ReadIniSectionStr('Auction.txt','正在拍卖',inttostr(i));
						if PM_LIST = wpidx then
						begin
							WriteIniSectionStr('Auction.txt','正在拍卖',inttostr(i),''); //从正在拍卖物品列表中删除
							break;
						end
					end;					
					WriteIniSectionStr('Auction.txt',wpidx,'拍卖状态','已取消');//
					This_Player.Give(wpname,wpnum);//返回物品
					This_Player.PlayerNotice('已取消物品拍卖,极品属性请重新提取',0);
					SerVerSay('【拍卖行】:编号为['+wpidx+']的['+wpname+'*'+inttostr(wpnum)+']被卖家['+oname+']强行撤回了！系统已对其作出处罚！',5);					
					_myinfo('2');						
				end				
			end else
			begin
				WriteIniSectionStr('Auction.txt',wpidx,'拍卖状态','已取消');//
				This_Player.Give(wpname,1);//返回物品
				This_Player.PlayerNotice('已取消物品拍卖,极品属性请重新提取',0);
				_myinfo('2');				
			end
		end else
		begin
			if id= '2' then	
			begin
				_goldcost(mtype+','+inttostr(cancelgold));
				if 	This_Player.GetS(11,13)=1 then//扣除成功标记
				begin
					for i:=1 to pm_max do
					begin
						PM_LIST:=ReadIniSectionStr('Auction.txt','正在拍卖',inttostr(i));
						if PM_LIST = wpidx then
						begin
							WriteIniSectionStr('Auction.txt','正在拍卖',inttostr(i),''); //从正在拍卖物品列表中删除
							break;
						end
					end;					
						WriteIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name,'');//取消拍品和人物
						This_Player.SetV(11,This_Player.GetV(11,11),0);//清除对应变量值
						This_Player.SetV(11,12,This_Player.GetV(11,12)-1);//竞拍物品减一 
						This_Player.Give(wpname,wpnum);//返回物品
						WriteIniSectionStr('Auction.txt',wpidx,'拍卖状态','已取消');//						
						This_Player.PlayerNotice('已取消物品拍卖',0);
						SerVerSay('【拍卖行】:编号为['+wpidx+']的['+wpname+'*'+inttostr(wpnum)+']被卖家['+oname+']强行撤回了！系统已对其作出处罚！',5);					
						_myinfo('2');						
				end;
			end else
			begin
				WriteIniSectionStr('Auction.txt',wpidx,'拍卖状态','已取消');//
				WriteIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name,'');//取消拍品和人物
				for i:=1 to pm_max do
				begin
					PM_LIST:=ReadIniSectionStr('Auction.txt','正在拍卖',inttostr(i));
					if PM_LIST = wpidx then
					begin
						WriteIniSectionStr('Auction.txt','正在拍卖',inttostr(i),''); //从正在拍卖物品列表中删除
						break;
					end
				end;
				This_Player.SetV(11,This_Player.GetV(11,11),0);//清除对应变量值
				This_Player.SetV(11,12,This_Player.GetV(11,12)-1);//竞拍物品减一 
				This_Player.Give(wpname,1);//返回物品
				This_Player.PlayerNotice('已取消物品拍卖',0);
				_myinfo('2');		
			end
		end 
	end;
end;

procedure _pm_start;
var wpidx,wpname,wpstate,wpgold,wptime,mtype:string;
wpnum:integer;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	wpname:=ReadIniSectionStr('Auction.txt',wpidx,'物品名');
	wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'物品数量'),1);
	wpstate:=ReadIniSectionStr('Auction.txt',wpidx,'拍卖状态');
	mtype:=ReadIniSectionStr('Auction.txt',wpidx,'货币类型');
	wptime:=ReadIniSectionStr('Auction.txt',wpidx,'竞拍时长');
	wpgold:=ReadIniSectionStr('Auction.txt',wpidx,'起拍价');
	if (length(wpidx) > 0) and  (length(wpname) > 0) and  (length(mtype) > 0)  and  (length(wptime) > 0)  and  (length(wpgold) > 0) then
	begin
		This_Npc.NpcDialog(This_Player,
		'你将以<'+wpgold+mtype+'/fcolor=250>的起拍价格拍卖<'+wpname+'*'+inttostr(wpnum)+'/fcolor=254>,如果主动取消拍卖，将会扣除起拍价的<'+inttostr(pm_cancel)+'%>，是否确定以此价格拍卖？'
		+'|{cmd}<返回修改/@wupininfo~'+inttostr(This_Player.GetV(11,11))+'> ^<确认拍卖/@pm_start_ok>'
		);
	end else
		This_Player.PlayerNotice('拍品信息不全，禁止拍卖！',0);
end;

procedure _pm_start_ok;
var wpidx,wpname,wpstate,wpgold,wptime,mtype,PM_LIST,re:string;
basegold,wpnum,losegold,i:integer;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	wpname:=ReadIniSectionStr('Auction.txt',wpidx,'物品名');
	wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'物品数量'),1);
	wpstate:=ReadIniSectionStr('Auction.txt',wpidx,'拍卖状态');
	mtype:=ReadIniSectionStr('Auction.txt',wpidx,'货币类型');
	wptime:=ReadIniSectionStr('Auction.txt',wpidx,'竞拍时长');
	wpgold:=ReadIniSectionStr('Auction.txt',wpidx,'起拍价');
	if (length(wpidx) > 0) and  (length(wpname) > 0) and  (length(mtype) > 0)  and  (length(wptime) > 0)  and  (length(wpgold) > 0) then
	begin
		re:='';
		for i:=1 to pm_max do
		begin
			PM_LIST:=ReadIniSectionStr('Auction.txt','正在拍卖',inttostr(i));
			if 	length(PM_LIST) <=0 then
			begin
				if This_Player.GetBagItemCount(wpname) >= wpnum-1 then
				begin
					This_Player.Take(wpname,wpnum-1);
					WriteIniSectionStr('Auction.txt','正在拍卖',inttostr(i),wpidx);//写入正在拍卖物品列表
					WriteIniSectionStr('Auction.txt',wpidx,'开拍时间',inttostr(ConvertDateTimeToDB(GetNow)));
					WriteIniSectionStr('Auction.txt',wpidx,'拍卖状态','竞拍中');
					SerVerSay('【拍卖行】:编号为['+wpidx+']的['+wpname+']上线了，起拍价['+wpgold+mtype+']限时['+wptime+']小时',5);
					_wupininfo(inttostr(This_Player.GetV(11,11)));
					re:='ok'
					exit;
				end else
				begin
					This_Player.PlayerNotice('拍品数量不足，禁止拍卖！',0);
					_wupininfo(inttostr(This_Player.GetV(11,11)));
					exit;
				end
			end			
		end
		
		if re='' then
		begin
			This_Player.PlayerNotice('系统拍卖名额已满,请稍后再试！',0);
			_wupininfo(inttostr(This_Player.GetV(11,11)));
		end
	end else
		This_Player.PlayerNotice('拍品信息不全，禁止拍卖！',0);
end;

procedure _sellgold;
begin
	This_Npc.InputDialog(This_Player,'请输入一口价(输入值需要大于起拍价,否则为关闭该功能)',0,8004);
	
end;

procedure P8004;
var wpidx:string;
bgold,igold:integer;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	bgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'起拍价'),0);
	igold:=StrToIntDef(This_Npc.InputStr,-1);
	
	if bgold > 0 then
	begin
		if igold > bgold then
		begin
			WriteIniSectionStr('Auction.txt',wpidx,'一口价',inttostr(igold));
			_wupininfo(inttostr(This_Player.GetV(11,11)));
			This_Player.PlayerNotice('成功设置一口价！如需关闭请重设一个比起拍价低的值。',2);
		end else
		begin
			WriteIniSectionStr('Auction.txt',wpidx,'一口价','');
			_wupininfo(inttostr(This_Player.GetV(11,11)));		
			This_Player.PlayerNotice('已经关闭一口价功能！如需使用请设置比起拍价高的值。',0);
		end
	end else
		This_Player.PlayerNotice('请先设置起拍价！',0);
end;

procedure _settime(id:string);
var wpidx:string;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	WriteIniSectionStr('Auction.txt',wpidx,'竞拍时长',inttostr(strtoint(id)));	
	_wupininfo(inttostr(This_Player.GetV(11,11)));
end;

procedure _resettime;
var wpidx:string;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	WriteIniSectionStr('Auction.txt',wpidx,'竞拍时长','');	
	_wupininfo(inttostr(This_Player.GetV(11,11)));
end;

procedure _setmtype(id:string);
var wpidx,mtype,times:string;defgold:integer;
begin
	if id='1' then begin mtype:='金币'; defgold:=limit_gold; end
	if id='2' then begin mtype:='元宝'; defgold:=limit_YB; end
	if id='3' then begin mtype:='灵符'; defgold:=limit_LF; end
	if id='4' then begin mtype:=pm_wphb; defgold:=limit_hb; end
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	times:=ReadIniSectionStr('Auction.txt',wpidx,'竞拍时长');
	WriteIniSectionStr('Auction.txt',wpidx,'货币类型',mtype);
	WriteIniSectionStr('Auction.txt',wpidx,'起拍价',inttostr(defgold));//初始化默认起拍价
	if length(times) <= 0 then WriteIniSectionStr('Auction.txt',wpidx,'竞拍时长','12');//初始化默认时间
	_wupininfo(inttostr(This_Player.GetV(11,11)));
end;

procedure _resetmtype;
var wpidx:string;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	WriteIniSectionStr('Auction.txt',wpidx,'货币类型','');
	WriteIniSectionStr('Auction.txt',wpidx,'起拍价','');
	WriteIniSectionStr('Auction.txt',wpidx,'当前价','');
	_wupininfo(inttostr(This_Player.GetV(11,11)));
end;

procedure _setbase;
var wpidx,mtype:string;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	mtype:=ReadIniSectionStr('Auction.txt',wpidx,'货币类型');
	if length(mtype) > 0 then
	begin
		This_Npc.InputDialog(This_Player,'请输入起拍价格',0,8001);
	end else
		This_Player.PlayerNotice('请先选择货币类型',0);
end;

procedure P8001;
var wpidx,mtype:string;
maxcost:integer;
begin
	wpidx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(This_Player.GetV(11,11))+'',This_Player.Name);
	mtype:=ReadIniSectionStr('Auction.txt',wpidx,'货币类型');
	if mtype='金币' then maxcost:=limit_gold;
	if mtype='元宝' then maxcost:=limit_YB;
	if mtype='灵符' then maxcost:=limit_LF;
	if mtype=pm_wphb then maxcost:=limit_hb;
	
	if (StrToIntDef(This_Npc.InputStr,-1) >= maxcost) and (StrToIntDef(This_Npc.InputStr,-1) <=10000000) then
	begin
		WriteIniSectionStr('Auction.txt',wpidx,'起拍价',This_Npc.InputStr);
		WriteIniSectionStr('Auction.txt',wpidx,'当前价',This_Npc.InputStr);
		_wupininfo(inttostr(This_Player.GetV(11,11)));
	end else
		This_Player.PlayerNotice(mtype+'类型物品的最低起拍价为'+inttostr(maxcost)+',最高不超过1000W,请输入正确的值',0);
		
end;


procedure ThreeItem(id:string);
begin
	This_Npc.NpcDialog(This_Player,
	'你正准备拍卖的<'+id+'/fcolor=250>是可叠加物品,默认只上架一个，上架后可调整数量'
	+'|{cmd}<确定拍卖/@diyitem~'+id+'> <取消拍卖/@exit>'
	)
end;


procedure _diyitem(id:string);
var wpidx,wppos,zb,zy,lx,sl:string;
begin
	zb := getpmitem(id);//名称
	zy := getf(zb); //职业
	lx := getf(getb(zb));//类型

	if This_Player.GetBagItemCount(id) > 0 then
	begin
		This_Player.Take(id,1);
		wppos:=inttostr(This_Player.GetV(11,11));//拍品上架位置
		wpidx:=inttostr(GetG(11,11)+1);
		WriteIniSectionStr('Auction.txt','物品'+wppos+'',This_Player.Name,wpidx); //写入拍品流水号与人物对应关系。
		WriteIniSectionStr('Auction.txt',wpidx,'物品名',id); //按拍品流水号创建字段并写入属性
		WriteIniSectionStr('Auction.txt',wpidx,'物品类型',lx); 
		WriteIniSectionStr('Auction.txt',wpidx,'职业类型',zy); 
		WriteIniSectionStr('Auction.txt',wpidx,'卖家',This_Player.Name);
		WriteIniSectionStr('Auction.txt',wpidx,'拍卖状态','筹备中');
		SetG(11,11,GetG(11,11)+1); //全局拍卖流水号+1;
		This_Player.SetV(11,strtoint(wppos),strtoint(wpidx));//拍品信息写入玩家变量
		This_Player.SetV(11,12,This_Player.GetV(11,12)+1);//卖家上架物品总数+1;
//		This_Player.SetV(11,11,0); //栏位标识清零
		_wupininfo(wppos);//跳转到拍卖物品设定页面	
	end else
		This_Npc.NpcDialog(This_Player,'你背包里没有<'+id+'>！|{cmd}<返回卖家中心/@myinfo~2>')

end;

procedure CommitItem(AType:word);
var
zb,zy,lx,sl,pm_pos,lwpos,pm_idx,wpname,wpstate,wpidx,lqzt,oname,bname:string;
abil:array [1..5] of integer;
i,jp:integer;
begin
	zb := getpmitem(This_Item.ItemName);//名称
	zy := getf(zb); //职业
	lx := getf(getb(zb));//类型
	sl := getf(getb(getb(zb)));//最大数量
	
	if zb <> '' then
	begin
		if This_Player.GetV(11,13) < 100000 then  //大于10W为提取极品
		begin
				pm_pos:=inttostr(This_Player.GetV(11,11));//拍品上架位置
				pm_idx:=inttostr(GetG(11,11)+1);
			
				if zy = '叠加' then 
				begin
					This_Player.NotifyClientCommitItem(0,'');
					This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
					ThreeItem(This_Item.ItemName);
					WriteIniSectionStr('Auction.txt',pm_idx,'堆叠上限',sl);
				end else
				begin
					jp:=0;
					abil[1]:=This_Item.AddPa1;
					abil[2]:=This_Item.AddPa2;
					abil[3]:=This_Item.AddPa3;
					abil[4]:=This_Item.AddPa4;
					abil[5]:=This_Item.AddPa5;
					
					for i:=1 to 5 do
					begin
						if abil[i] > 0 then
						begin
							WriteIniSectionStr('Auction.txt',pm_idx,'Abil'+inttostr(i),inttostr(abil[i]));//写极品值
							jp:=jp+abil[i];
						end
					end
		
					if jp > 0 then WriteIniSectionStr('Auction.txt',pm_idx,'极品','是');
		
					
			
				WriteIniSectionStr('Auction.txt','物品'+pm_pos+'',This_Player.Name,pm_idx); //写入拍品流水号与人物对应关系。
				WriteIniSectionStr('Auction.txt',pm_idx,'物品名',This_Item.ItemName); //按拍品流水号创建字段并写入属性
				WriteIniSectionStr('Auction.txt',pm_idx,'物品类型',lx); 
				WriteIniSectionStr('Auction.txt',pm_idx,'职业类型',zy); 
				WriteIniSectionStr('Auction.txt',pm_idx,'卖家',This_Player.Name);
				WriteIniSectionStr('Auction.txt',pm_idx,'拍卖状态','筹备中');
				
				This_Player.TakeByClientID(This_Item.ClientItemID);//成功收走物品
				This_Player.NotifyClientCommitItem(1,'提交成功');
				This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
				
				SetG(11,11,GetG(11,11)+1); //全局拍卖流水号+1;
				This_Player.SetV(11,strtoint(pm_pos),strtoint(pm_idx));//拍品信息写入玩家变量
				This_Player.SetV(11,12,This_Player.GetV(11,12)+1);//卖家上架物品总数+1;
		//		This_Player.SetV(11,11,0); //栏位标识清零
				_wupininfo(pm_pos);//跳转到拍卖物品设定页面
				end
		end else
		begin
			wpidx:=inttostr(This_Player.GetV(11,13));
			wpname:=ReadIniSectionStr('Auction.txt',wpidx,'物品名');
			bname:=ReadIniSectionStr('Auction.txt',wpidx,'买家');
			oname:=ReadIniSectionStr('Auction.txt',wpidx,'卖家')
			wpstate:=ReadIniSectionStr('Auction.txt',wpidx,'拍卖状态');
			lqzt:=ReadIniSectionStr('Auction.txt',wpidx,'领取状态');
			if (wpname = This_Item.ItemName) and (((bname = This_Player.Name) and (lqzt = '已领取')) or ((oname = This_Player.Name) and ((wpstate='已取消') or (wpstate = '流拍已领')))) then
			begin
				for i:= 1 to 5 do
				begin
					abil[i]:=StrToIntDef(ReadIniSectionStr('Auction.txt',wpidx,'Abil'+inttostr(i)),0);
					pm_idx:=ReadIniSectionStr('Auction.txt','物品'+inttostr(i)+'',This_Player.Name);
					if pm_idx = wpidx then lwpos:=inttostr(i);
				end
				This_Item.AddPa1:=abil[1];
				This_Item.AddPa2:=abil[2];
				This_Item.AddPa3:=abil[3];
				This_Item.AddPa4:=abil[4];
				This_Item.AddPa5:=abil[5];
				This_Player.NotifyClientCommitItem(0,'极品属性提取成功');
				This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);	
				if This_Player.Name = bname then
				begin
					WriteIniSectionStr('Auction.txt',wpidx,'领取状态','极品属性已提取');
				end else
				if This_Player.Name = oname then
				begin
					WriteIniSectionStr('Auction.txt',wpidx,'领取状态','极品属性已提取');				
					WriteIniSectionStr('Auction.txt','物品'+lwpos+'',This_Player.Name,'');//取消拍品和人物
					This_Player.SetV(11,This_Player.GetV(11,11),0);//清除对应变量值
					This_Player.SetV(11,12,This_Player.GetV(11,12)-1);//竞拍物品减一 
				end
				This_Player.SetV(11,13,0);//
				if wpstate = '流拍已领' then 
				begin
					_myinfo('2');
				end else
						_jiesuan(wpidx);				
			end else
			begin
				This_Player.NotifyClientCommitItem(0,'极品提取非法！请核对姓名,装备名字是否相符！以及是否提取过该极品属性');
			end
		end;
	end else
	begin							
		This_Player.NotifyClientCommitItem(0,'非法物品！');
		This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
		This_NPC.Click_CommitItem(This_Player,1,'请放入你想拍卖的物品');
	end
end;

procedure Execute; //拍卖计时
var sytime,ktime,stime,xx,pgold,bgold,lspos,wpnum:integer;
pmidx,bname,wpname,oname,mtype:string;
donelog:array [1..102] of string;
begin

	for xx:=1 to 100 do
	begin
		donelog[xx]:=ReadIniSectionStr('Auction.txt','历史记录',inttostr(xx));
		if length(donelog[xx]) < 0 then
		begin
			lspos:=xx;
			break
		end
	end
	
	if length(donelog[100]) > 0 then
	begin
		for xx:=1 to 100 do
		begin
			donelog[xx]:=donelog[xx+1];
		end
		lspos:=100;
	end

	for xx:=1 to pm_max do
	begin
		pmidx:=ReadIniSectionStr('Auction.txt','正在拍卖',inttostr(xx));
		if length(pmidx) > 0 then
		begin
			wpname:=ReadIniSectionStr('Auction.txt',pmidx,'物品名');
			wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',pmidx,'物品数量'),1);
			bname:=ReadIniSectionStr('Auction.txt',pmidx,'买家');
			oname:=ReadIniSectionStr('Auction.txt',pmidx,'卖家')
			mtype:=ReadIniSectionStr('Auction.txt',pmidx,'货币类型');
			ktime:=strtoint(ReadIniSectionStr('Auction.txt',pmidx,'开拍时间'));
			stime:=strtoint(ReadIniSectionStr('Auction.txt',pmidx,'竞拍时长'));
			pgold:=strtoint(ReadIniSectionStr('Auction.txt',pmidx,'当前价'));
			bgold:=strtoint(ReadIniSectionStr('Auction.txt',pmidx,'起拍价'));
			sytime:=((stime*3600-(minusDataTime(GetNow,ConvertDBToDateTime(ktime)))) div 60); //剩余时间分
			if sytime <=0 then
			begin
				if pgold > bgold then
				begin
					WriteIniSectionStr('Auction.txt',pmidx,'拍卖状态','已成交');
					WriteIniSectionStr('Auction.txt','历史记录',inttostr(lspos),'<'+wpname+'*'+inttostr(wpnum)+'/fcolor=250> '+inttostr(pgold)+mtype+' <已成交/fcolor=250> <'+MirDateTimeToStr('MM-dd hh:mm' , GetNow)+'/fcolor=247>');					
					SerVerSay('【拍卖行】:编号为['+pmidx+']的['+wpname+']竞价结束，由['+bname+']以['+inttostr(pgold)+mtype+']的价格拍得！',5);
					WriteIniSectionStr('Auction.txt',pmidx,'成交价',inttostr(pgold));
				end else
					WriteIniSectionStr('Auction.txt',pmidx,'拍卖状态','已流拍');
					
				WriteIniSectionStr('Auction.txt','正在拍卖',inttostr(xx),''); //从正在拍卖物品列表中删除
			end;
		end;
	end

	if GetG(11,11) < 100000 then SetG(11,11,100000);//流水号初始化 6位数
end;

procedure _dating(ipage:string);
var wpname,wpjob,wpgold,wptime,mtype,wptype,PM_InDX,liststr,starttime,wpstr,pagestr,color,jpstr,kindstr:string;
lesstime,i,wpnum,sellgold,bgold:integer;
begin
	pm_find:='';
	liststr:='';
	kindstr:='|<分类/fcolor=242>  <全部/fcolor=254>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';

	case pm_kind of 
	'武器': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/fcolor=254>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'衣服': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/fcolor=254>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'项链': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/fcolor=254>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'戒指': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/fcolor=254>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'手镯': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/fcolor=254> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'材料': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5>|<材料/fcolor=254>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'鞋子': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/fcolor=254>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'头盔': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@fcolor=254>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'宝石': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@fcolor=254>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'勋章': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@fcolor=254>  <斗笠/@itemkind~11>  <消耗/@itemkind~12>';end;
	'斗笠': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/fcolor=254>  <消耗/@itemkind~12>';end;
	'消耗': begin  kindstr:='|<分类/fcolor=242>  <全部/@itemkind~0>  <武器/@itemkind~1>  <防具/@itemkind~2>  <项链/@itemkind~3>  <戒指/@itemkind~4>  <手镯/@itemkind~5> |<材料/@itemkind~6>  <鞋子/@itemkind~7>  <头盔/@itemkind~8>  <宝石/@itemkind~9>  <勋章/@itemkind~10>  <斗笠/@itemkind~11>  <消耗/fcolor=254>';end;
	end;

	pagestr:='<P'+ipage+'/fcolor=254> <→/@dating~'+inttostr(strtoint(ipage)+1)+'>||'
	if strtoint(ipage) > 1 then pagestr:='<←/@dating~'+inttostr(strtoint(ipage)-1)+'><P'+ipage+'/fcolor=254> <→/@dating~'+inttostr(strtoint(ipage)+1)+'>||'
	
	for i:= 1+(strtoint(ipage)-1)*10 to strtoint(ipage)*10 do
	begin
		PM_InDX:=ReadIniSectionStr('Auction.txt','大厅列表',inttostr(i));

		if length(PM_InDX) > 0 then
		begin
			wpname:=ReadIniSectionStr('Auction.txt',PM_InDX,'物品名');
			wpjob:=ReadIniSectionStr('Auction.txt',PM_InDX,'职业类型');
			mtype:=ReadIniSectionStr('Auction.txt',PM_InDX,'货币类型');
			wptype:=ReadIniSectionStr('Auction.txt',PM_InDX,'物品类型');
			wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',PM_InDX,'物品数量'),1);
			wptime:=ReadIniSectionStr('Auction.txt',PM_InDX,'竞拍时长');
			wpgold:=ReadIniSectionStr('Auction.txt',PM_InDX,'当前价');
			jpstr:=ReadIniSectionStr('Auction.txt',PM_InDX,'极品');
			bgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',PM_InDX,'起拍价'),0);
			sellgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',PM_InDX,'一口价'),-1);			
			starttime:=ReadIniSectionStr('Auction.txt',PM_InDX,'开拍时间');

			color:='254';//普通物品颜色
			if sellgold > bgold then color:='249'; //一口价颜色
			if jpstr='是' then color:='253';//极品颜色
			
			lesstime:=((strtoint(wptime)*3600-(minusDataTime(GetNow,ConvertDBToDateTime(strtoint(starttime))))) div 60); //剩余时间分
			
			wpstr:='<'+wpname+'/fcolor='+color+'>';
			if wpjob='叠加' then  wpstr:='<'+wpname+'*'+inttostr(wpnum)+'/fcolor='+color+'>';
			
			if wpjob='战士' then wpjob:='<'+wpjob+'/fcolor=242>';
			if wpjob='法师' then wpjob:='<'+wpjob+'/fcolor=241>';
			if wpjob='道士' then wpjob:='<'+wpjob+'/fcolor=243>';
			
			if mtype='金币' then mtype:='<'+mtype+'/fcolor=251>';
			if mtype='元宝' then mtype:='<'+mtype+'/fcolor=191>';
			if mtype='灵符' then mtype:='<'+mtype+'/fcolor=168>';
			if mtype=pm_wphb then mtype:='<'+mtype+'/fcolor=244>';		

			if length(pm_kind) > 0 then
			begin
				if wptype = pm_kind then
				begin
				liststr:=liststr+wpstr+'^'+mtype+' <'+wpgold+'/fcolor=250>  ^<竞价/@paipin~'+PM_InDX+'>|';
				end
			end else
			liststr:=liststr+wpstr+'^'+mtype+' <'+wpgold+'/fcolor=250>  ^<竞价/@paipin~'+PM_InDX+'>|';
		end
	end;
	
	This_Npc.NpcDialog(This_Player,
	+'|^<★拍卖行·拍卖大厅★/fcolor=243>^^'
	+kindstr
	+'|<————————————————————/fcolor=247>'
	+'|<【名称】/fcolor=90> <查找/@finditem> ^<【当前价】/fcolor=90> ^'+pagestr
	+liststr
	+'<————————————————————/fcolor=247>'
	+'|<红色的支持一口价>^<紫色的带极品属性/fcolor=253>'
	+'|{cmd}<大厅首页/@main> ^<历史行情/@donelog~1>'
	+'|{cmd}<买家中心/@myinfo~1> ^<卖家中心/@myinfo~2>'
	); 
end;

procedure _itemkind(id:string);
begin
	if id='0'  then pm_kind:='';
	if id='1'  then pm_kind:='武器';
	if id='2'  then pm_kind:='衣服';
	if id='3'  then pm_kind:='项链';
	if id='4'  then pm_kind:='戒指';
	if id='5'  then pm_kind:='手镯';
	if id='6'  then pm_kind:='材料';
	if id='7'  then pm_kind:='鞋子';
	if id='8'  then pm_kind:='头盔';
	if id='9'  then pm_kind:='宝石';
	if id='10' then pm_kind:='勋章';
	if id='11' then pm_kind:='斗笠';
	if id='12' then pm_kind:='消耗';
	_dating('1');
end;

procedure _finditem;
begin
	This_NPC.InputDialog(This_Player,'请输入要查找的物品名字，支持模糊搜索',0,8005);
end;

procedure _dating2(idxx:string);
var Iname,wpname,wpjob,wpgold,wptime,mtype,wptype,PM_InDX,liststr,starttime,wpstr,pagestr,color,jpstr,ipage:string;
lesstime,i,wpnum,sellgold,bgold:integer;
begin
	
	Iname:=copy(idxx, 0, pos(',', idxx)-1);
	ipage:=copy(idxx, pos(',', idxx)+1, length(idxx));

	liststr:='';
	pagestr:='<P'+ipage+'/fcolor=254> <→/@dating2~'+Iname+','+inttostr(strtoint(ipage)+1)+'>||'
	if strtoint(ipage) > 1 then pagestr:='<←/@dating2~'+Iname+','+inttostr(strtoint(ipage)-1)+'><P'+ipage+'/fcolor=254> <→/@dating2~'+Iname+','+inttostr(strtoint(ipage)+1)+'>||'
	
	for i:= 1+(strtoint(ipage)-1)*10 to strtoint(ipage)*10 do
	begin
		PM_InDX:=ReadIniSectionStr('Auction.txt','大厅列表',inttostr(i));

		if length(PM_InDX) > 0 then
		begin
			wpname:=ReadIniSectionStr('Auction.txt',PM_InDX,'物品名');
			wpjob:=ReadIniSectionStr('Auction.txt',PM_InDX,'职业类型');
			mtype:=ReadIniSectionStr('Auction.txt',PM_InDX,'货币类型');
			wptype:=ReadIniSectionStr('Auction.txt',PM_InDX,'物品类型');
			wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',PM_InDX,'物品数量'),1);
			wptime:=ReadIniSectionStr('Auction.txt',PM_InDX,'竞拍时长');
			wpgold:=ReadIniSectionStr('Auction.txt',PM_InDX,'当前价');
			jpstr:=ReadIniSectionStr('Auction.txt',PM_InDX,'极品');
			bgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',PM_InDX,'起拍价'),0);
			sellgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',PM_InDX,'一口价'),-1);			
			starttime:=ReadIniSectionStr('Auction.txt',PM_InDX,'开拍时间');
		
			color:='254';//普通物品颜色
			if sellgold > bgold then color:='249'; //一口价颜色
			if jpstr='是' then color:='253';//极品颜色
			
			lesstime:=((strtoint(wptime)*3600-(minusDataTime(GetNow,ConvertDBToDateTime(strtoint(starttime))))) div 60); //剩余时间分
			
			wpstr:='<'+wpname+'/fcolor='+color+'>';
			if wptype='叠加' then  wpstr:='<'+wpname+'*'+inttostr(wpnum)+'/fcolor='+color+'>';
			
			if wpjob='战士' then wpjob:='<'+wpjob+'/fcolor=242>';
			if wpjob='法师' then wpjob:='<'+wpjob+'/fcolor=241>';
			if wpjob='道士' then wpjob:='<'+wpjob+'/fcolor=243>';
			
			if mtype='金币' then mtype:='<'+mtype+'/fcolor=251>';
			if mtype='元宝' then mtype:='<'+mtype+'/fcolor=191>';
			if mtype='灵符' then mtype:='<'+mtype+'/fcolor=168>';
			if mtype=pm_wphb then mtype:='<'+mtype+'/fcolor=244>';
			
			if pos(Iname,wpname) > 0 then
			begin		
				liststr:=liststr+wpstr+'^'+mtype+' <'+wpgold+'/fcolor=250>  ^<竞价/@paipin~'+PM_InDX+'>|';
			end else
		end
	end;
	
	This_Npc.NpcDialog(This_Player,
	+'|^<★拍卖行/fcolor=243>·查找<'+Iname+'/fcolor=250>^^'
	+'|<————————————————————/fcolor=247>'
	+'|<【名称】/fcolor=90> <查找/@finditem> ^<【当前价】/fcolor=90> ^'+pagestr
	+liststr
	+'<————————————————————/fcolor=247>'
	+'|<红色的支持一口价>^<紫色的带极品属性/fcolor=253>'
	+'|{cmd}<大厅首页/@main> ^<买家中心/@myinfo~1> ^<卖家中心/@myinfo~2> '
	); 
end;

procedure P8005;
begin
	pm_find:=This_Npc.InputStr;
	_dating2(This_Npc.InputStr+',1');
	
end;

procedure _paipin(id:string);
var wpname,wpstate,wpgold,wpadd,wpjptime,wpjob,wptype,wpstr,mtype,losestr,typestr,highgold,buyer,bname,oname,cmdstr,smstr,sellstr,sxstr,color,jpstr,zb,czstr:string;
addgold,basegold,jptime,losegold,lesstime,starttime,wpnum,k,pgold,sellgold,i:integer;
abil:array [1..5] of integer;abilstr:array [1..5] of string;
begin
		cmdstr:='';smstr:='';color:='254';
		This_Player.SetS(11,11,strtoint(id));
		wpname:=ReadIniSectionStr('Auction.txt',id,'物品名');
		wpstate:=ReadIniSectionStr('Auction.txt',id,'拍卖状态');
		mtype:=ReadIniSectionStr('Auction.txt',id,'货币类型');
		wpjob:=ReadIniSectionStr('Auction.txt',id,'职业类型');
		wptype:=ReadIniSectionStr('Auction.txt',id,'物品类型');	
		oname:=ReadIniSectionStr('Auction.txt',id,'卖家');
		wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'物品数量'),1);		
		jptime:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'竞拍时长'),0);
		basegold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'起拍价'),0);
		sellgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'一口价'),-1);
		jpstr:=ReadIniSectionStr('Auction.txt',id,'极品');
		czstr:='';if length(pm_find)>0 then czstr:='|<返回查找“'+pm_find+'”/@dating2~'+pm_find+',1>';
		if jpstr= '是' then
		begin
			for i:= 1 to 5 do
			begin
				abilstr[i]:='';
				abil[i]:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'Abil'+inttostr(i)),0);
			end

			zb := getpmitem(wpname);		
			if abil[1] >0 then 	abilstr[1]:=''+getf(getb(getb(getb(zb))))+' '+inttostr(abil[1])+' ';//极品注释1
			if abil[2] >0 then 	abilstr[2]:=''+getf(getb(getb(getb(getb(zb)))))+' '+inttostr(abil[2])+' ';//极品注释2
			if abil[3] >0 then 	abilstr[3]:=''+getf(getb(getb(getb(getb(getb(zb))))))+' '+inttostr(abil[3])+' ';//极品注释3
			if abil[4] >0 then 	abilstr[4]:=''+getf(getb(getb(getb(getb(getb(getb(zb)))))))+' '+inttostr(abil[4])+' ';//极品注释4
			if abil[5] >0 then 	abilstr[5]:=''+getb(getb(getb(getb(getb(getb(getb(getb(zb))))))))+' '+inttostr(abil[5])+' ';//极品注释5

			sxstr:='|<附加:/fcolor=242> ';
			for i:= 1 to 5 do
			begin
				sxstr:=sxstr+'<'+abilstr[i]+'/fcolor=253>';
			end
		end else
		sxstr:='';		

		if sellgold > basegold then color:='249';
		if length(jpstr)<=0 then jpstr:='否'; 
		if jpstr='是' then color:='253';
		
		sellstr:='';
		if sellgold > basegold then sellstr:='|<支持一口价,'+inttostr(sellgold)+mtype+'立即成交/fcolor=254>'		

		addgold:=(basegold*pm_add div 100);
		if addgold <= 0 then addgold:=1;
		losegold:=(basegold*pm_cancel div 100);
		wpstr:=wpname;
		if wpjob='叠加' then wpstr:=wpname+'*'+inttostr(wpnum);
		
		for k:=1 to pm_maxman do
		begin
			bname:=ReadIniSectionStr('Auction.txt',id,'出价人'+inttostr(k)+'');
			if bname= This_Player.Name then
			begin
				pgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'出价'+inttostr(k)+''),0);
				break;
			end
		end

			highgold:=ReadIniSectionStr('Auction.txt',id,'当前价');
			if length(highgold) <=0 then highgold:=inttostr(basegold);
			buyer:=ReadIniSectionStr('Auction.txt',id,'买家');
			if length(buyer) <= 0 then buyer:='暂无';
			starttime:=strtointDef(ReadIniSectionStr('Auction.txt',id,'开拍时间'),0);
			lesstime:=jptime*3600-(minusDataTime(GetNow,ConvertDBToDateTime(starttime)));
			if lesstime<=0 then lesstime:=0;

		if wpstate='竞拍中' then
		begin
			cmdstr:=cmdstr+'|{cmd}<一键出价/@buynow~'+id+',0> ^<自定出价/@diybuy~'+id+'> |{cmd}<拍卖大厅/@main> ^<买家中心/@myinfo~1> '
			smstr:='|<一键出价为现价+加价-我的出价/fcolor=249>'
		end else
		begin
			cmdstr:=cmdstr+'|{cmd}<一键结算/@jiesuan~'+id+'> |{cmd}<拍卖大厅/@main> ^<买家中心/@myinfo~1> '
			smstr:='|<一键结算可以根据拍卖结果自动处理/fcolor=249>'			
		end
			This_Npc.NpcDialog(This_Player,
			+czstr
			+'|^<#'+id+'#/fcolor=241> <'+wpstr+'/fcolor='+color+'>^^'+
			+'|<————————————————————/fcolor=247>'
			+'|<类型/fcolor=242> <'+wpjob+wptype+'/fcolor=254> ^<卖家/fcolor=242> <'+oname+'/fcolor=254> '
			+'|<底价/fcolor=242> <'+inttostr(basegold)+mtype+'/fcolor=254> ^<加价/fcolor=242> <'+inttostr(addgold)+mtype+'/fcolor=254>'
			+'|<现价/fcolor=242> <'+highgold+mtype+'/fcolor=244> ^<买家/fcolor=242> <'+buyer+'/fcolor=244>'
			+'|<限时/fcolor=242> <'+inttostr(lesstime div 60)+'分钟/fcolor=251>^<极品/fcolor=242> <'+jpstr+'/fcolor=251> '//<拍品/fcolor=242> <'+wpstr+'/fcolor='+color+'>'
			+sxstr
			+'|<————————————————————/fcolor=247>'
			+'|我的出价 【<'+inttostr(pgold)+'/fcolor=249>】^出价记录 <查看/@buylogs~'+id+'>'
			+'|<————————————————————/fcolor=247>'
			+sellstr
			+smstr
//			+'|<---------------------------------------------------------/fcolor=249>'
			+cmdstr
			);
end;


procedure _buynow(idxx:string);
var wpname,wpstate,mtype,bname,buyname,oname,id,xpos:string;
i,j,k,ngold,bgold,agold,wpnum,newgold,sellgold,cgold,pgold,bpos,lpos,lspos,ppos:integer;
buylog,donelog:array [1..102] of string;
begin
	id:=copy(idxx, 0, pos(',', idxx)-1);
	cgold:=strtoint(copy(idxx, pos(',', idxx)+1, length(idxx)));

	wpstate:=ReadIniSectionStr('Auction.txt',id,'拍卖状态');
	oname:=ReadIniSectionStr('Auction.txt',id,'卖家');
	
if This_Player.Name <> oname then
begin
	if (wpstate='竞拍中') and (This_Player.GetS(11,12) <= pm_limit) then
	begin
		ngold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'当前价'),0);
		bgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'起拍价'),0);
		sellgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'一口价'),-1);
		mtype:=ReadIniSectionStr('Auction.txt',id,'货币类型');
		wpname:=ReadIniSectionStr('Auction.txt',id,'物品名');
		wpnum:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'物品数量'),1)
		agold:=(bgold*pm_add div 100); //加价
		if agold <=0 then agold:=1;
		ppos:=0; //变量号
		bpos:=0;//出价号
		lpos:=0;//记录号
		
		for i:=1 to pm_limit do
		begin
			if This_Player.GetS(11,i) = strtoint(id) then
			begin
				ppos:=i;
				break;
			end
		end
		
		if ppos = 0 then
		begin
			for i:=1 to pm_limit do
			begin
				if This_Player.GetS(11,i) <= 0 then
				begin
					This_Player.SetS(11,i,strtoint(id));
					This_Player.SetS(11,12,This_Player.GetS(11,12)+1);
					ppos:=i;
					break;
				end;
			end
		end
	
		for k:=1 to pm_maxman do
		begin
			bname:=ReadIniSectionStr('Auction.txt',id,'出价人'+inttostr(k)+'');
			if bname= This_Player.Name then
			begin
				pgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'出价'+inttostr(k)+''),0);
				bpos:=k;
			end
		end
		
					
		if bpos = 0 then
		begin
			for k:=1 to pm_maxman do
			begin
				bname:=ReadIniSectionStr('Auction.txt',id,'出价人'+inttostr(k)+'');
				if length(bname) <=0 then
				begin
					pgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',id,'出价'+inttostr(k)+''),0);
					bpos:=k;
					break;
				end		
			end
		end

		for j:=1 to 10 do
		begin
			buylog[j]:=ReadIniSectionStr('Auction.txt',id,'出价记录'+inttostr(j)+'');
			if length(buylog[j]) <=0 then
			begin
				lpos:=j;
				break;
			end
		end

		if length(buylog[10]) > 0 then
		begin
			for i:= 1 to 10 do
			begin
				buylog[i]:=buylog[i+1];
				WriteIniSectionStr('Auction.txt',id,'出价记录'+inttostr(i)+'',buylog[i]);
			end
			lpos:=10;
		end
		
		for i:=1 to 100 do
		begin
			donelog[i]:=ReadIniSectionStr('Auction.txt','历史记录',inttostr(i));
			if length(donelog[i]) <=0 then
			begin
				lspos:=i;
				break;
			end
		end
		
		if length(donelog[100]) > 0 then
		begin
			for i:=1 to 100 do
			begin
				donelog[i]:=donelog[i+1];
				WriteIniSectionStr('Auction.txt',id,'历史记录',donelog[i]);
			end
			lspos:=100;
		end
		
	
		if cgold >=agold+ngold then
		begin
			newgold:=cgold;
		end else
			newgold:=agold+ngold;

		
		if newgold > 0 then
		begin	
			_goldcost(mtype+','+inttostr(newgold-pgold));
			if This_Player.GetS(11,13) = 1 then
			begin
				WriteIniSectionStr('Auction.txt',id,'当前价',inttostr(newgold));
				WriteIniSectionStr('Auction.txt',id,'买家',This_Player.Name);
				WriteIniSectionStr('Auction.txt',id,'出价人'+inttostr(bpos)+'',This_Player.Name);
				WriteIniSectionStr('Auction.txt',id,'出价'+inttostr(bpos)+'',inttostr(newgold));			
				WriteIniSectionStr('Auction.txt',id,'出价记录'+inttostr(lpos)+'','<'+This_Player.Name+'/fcolor=191> 出价 <'+inttostr(newgold)+mtype+'/fcolor=250> <'+MirDateTimeToStr('MM-dd hh:mm' , GetNow)+'/fcolor=247>');
				buyname:=ReadIniSectionStr('Auction.txt',id,'买家');
				if (newgold >= sellgold) and (sellgold > bgold) then //一口价判断
				begin
					WriteIniSectionStr('Auction.txt',id,'拍卖状态','已成交');
					WriteIniSectionStr('Auction.txt','历史记录',inttostr(lspos),'<'+wpname+'*'+inttostr(wpnum)+'/fcolor=250> '+inttostr(newgold)+mtype+' <已成交/fcolor=250> <'+MirDateTimeToStr('MM-dd hh:mm' , GetNow)+'/fcolor=247>');
					SerVerSay('【拍卖行】:编号为['+id+']的['+wpname+']竞价结束，由['+buyname+']以['+inttostr(newgold)+mtype+']的价格拍得！',5);
					WriteIniSectionStr('Auction.txt',id,'成交价',inttostr(newgold));
					for i:=1 to 100 do
					begin
						xpos:=ReadIniSectionStr('Auction.txt','正在拍卖',inttostr(i));
						if xpos=id then
						begin
							WriteIniSectionStr('Auction.txt','正在拍卖',inttostr(i),'');
							break
						end
					end
				end
				_paipin(id);	
			end			
		end else
			This_Player.PlayerNotice('未知错误！',0);
	end else
	begin
		This_Player.PlayerNotice(id+'号物品'+wpname+'已经结束拍卖！或者你参与竞拍的数量超过'+inttostr(pm_limit)+'',0);
		_paipin(id);
	end
end else
	This_Player.PlayerNotice('自抬身价是不被允许的作弊行为！',0);
end;

procedure _diybuy(id:string);
begin
	This_Player.SetS(11,11,strtoint(id));
	This_NPC.InputDialog(This_Player,'请输入你的叫价',0,8002);
end;

procedure P8002;
var igold,ngold,agold,bgold,i,ppos,ids:integer;
mtype:string;
begin
	igold:=StrToIntDef(This_Npc.InputStr,-1);
	ids:=This_Player.GetS(11,11);
	ngold:=StrToIntDef(ReadIniSectionStr('Auction.txt',inttostr(ids),'当前价'),0);
	bgold:=StrToIntDef(ReadIniSectionStr('Auction.txt',inttostr(ids),'起拍价'),0);
	mtype:=ReadIniSectionStr('Auction.txt',inttostr(ids),'货币类型');
	agold:=bgold*pm_add div 100;
	if agold <=0 then agold:=1;
	ppos:=0;

	if igold >= ngold+agold then
	begin		
		_buynow(inttostr(ids)+','+inttostr(igold));
	end else
		This_Player.PlayerNotice('请输入大于'+inttostr(ngold+agold)+'的数字',0);
end;


procedure _buylogs(id:string);
var buylog:array [1..12] of string;
j:integer;buyrec:string;
begin
		buyrec:='';
		for j:=1 to 10 do
		begin
			buylog[j]:=ReadIniSectionStr('Auction.txt',id,'出价记录'+inttostr(j)+'');
			
			if length(buylog[j]) >0 then
			begin
				buyrec:=buyrec +buylog[j]+'|';
			end
		end
	
	This_Npc.NpcDialog(This_Player,
	'|^<#'+id+'#/fcolor=241> <出价记录/fcolor=250>^^'+
	+'|<————————————————————/fcolor=247>|'
	+buyrec
	+'<————————————————————/fcolor=247>'
	+'|<最多显示10条记录/fcolor=242>'
	+'|{cmd}<返回大厅/@main> ^<返回竞价/@paipin~'+id+'>'
	);
	
end;

procedure _donelog(ipage:string);
var donelog:array [1..102] of string;
j:integer;donerec,donecmd:string;
begin
		donerec:='';
		for j:=1+(strtoint(ipage)-1)*10 to strtoint(ipage)*10 do
		begin
			donelog[j]:=ReadIniSectionStr('Auction.txt','历史记录',inttostr(j));
			
			if length(donelog[j]) > 0 then
			begin
				donerec:=donerec +donelog[j]+'|';
			end
		end
		
		if length(donelog[100]) > 0 then
		begin
			for j:=1+(strtoint(ipage)-1)*10 to strtoint(ipage)*10 do
			begin
				donelog[j]:=donelog[j+1];
			end
		end
	donecmd:='|{cmd}<下一页/@donelog~'+inttostr(strtoint(ipage)+1)+'> ^<返回大厅/@main>'
	if strtoint(ipage) > 1 then donecmd:='|{cmd}<上一页/@donelog~'+inttostr(strtoint(ipage)-1)+'> ^<下一页/@donelog~'+inttostr(strtoint(ipage)+1)+'> ^<返回大厅/@main>'
	This_Npc.NpcDialog(This_Player,
	'|^<拍卖行/fcolor=241> <历史行情/fcolor=250>^^'
	+'|<————————————————————/fcolor=247>|'
	+donerec
	+'<————————————————————/fcolor=247>'
	+'|<最多显示100条记录/fcolor=242>'
	+donecmd
	);
	
end;

procedure getlist;
var ix,jx,count:integer;
pmindx:string;
begin
	count:=0;
	for ix:=1 to pm_max do
	begin
		pmindx:=ReadIniSectionStr('Auction.txt','正在拍卖',inttostr(ix));
		if length(pmindx) > 0 then
		begin
			count:=count+1;
			WriteIniSectionStr('Auction.txt','大厅列表',inttostr(count),pmindx);
		end
	end
	
	if count < pm_max then
	begin
		for jx:=count+1 to pm_max  do
		begin
			WriteIniSectionStr('Auction.txt','大厅列表',inttostr(jx),'');
		end
	end;

end;



begin
	if This_Player.GetS(11,12) < 0 then This_Player.SetS(11,12,0); //买家物品总数初始化
	if This_Player.GetV(11,12) < 0 then This_Player.SetV(11,12,0); //卖家物品总数初始化
	if GetG(11,12) < 0 then SetG(11,12,0); //全局拍卖总数初始化
getlist;
pm_kind:='';
_dating('1');
end.