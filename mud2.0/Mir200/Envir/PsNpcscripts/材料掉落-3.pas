{****************************************************************
** 淡淡的哀愁  独家制作 在线玩家管理功能(完整版) QQ 253353132   *
请将《在线管理.txt》放到目录D:\mud2.0\Mir200\Share\config\下	*
该脚本采用数据库读取+txt存取的方式写成，减少变量使用，普适性高	*
使用时只需核对技能名称和小黑屋所在地图代码即可*
*****************************************************************}
 
program Mir2;

procedure _exit;
begin
  This_Npc.CloseDialog(This_Player);
end;

function SuperGM(price, num: Integer):boolean;
begin                                               
  result := true;
end;

function CheckGM:Boolean; //权限检查
begin
	if This_Player.GMLevel >  0 then
	begin
		result:=true;
	end else
	begin
		SerVerSay(This_Player.Name+'非法使用GM功能，请管理员立即查看!!',0);
		SerVerSay(This_Player.Name+'非法使用GM功能，请管理员立即查看!!',0);
		SerVerSay(This_Player.Name+'非法使用GM功能，请管理员立即查看!!',0);
		result:=false;
	end
end;

var BASEGOLD,BASEYB,BASELF,BASEJGS,BASEJF,BASELV,BASESW,BASEPK,BASEABIL,BASETIME,setmax,jnidx:integer;
Pname,wp_name,wpidx,wp_num,jn_lv,wpndx,setidx,delskill,addskill,offline,offline2,killpet,Page,orders,Total,CountTime,BLCAK_ROOM:string;
ITEMNUM,PersonZ:array [1..100] of integer;
BASEITEM,MapStr,MapName,PersonA,PersonB,PersonX,PersonY:array [1..100] of string; 
heroskill,humskill:array [1..100] of string;
procedure OnInitialize();							//请自行检查技能名称是否对应
begin

BLCAK_ROOM:='SD000';//小黑屋代码

heroskill[1]:= '白日门火球术';heroskill[2]:= '白日门治愈术';heroskill[3]:= '白日门剑术';heroskill[4]:= '白日门战法';heroskill[5]:= '白日门大火球';heroskill[6]:= '白日门施毒术';heroskill[7]:= '白日门攻杀';heroskill[8]:= '白日门抗拒';heroskill[9]:= '白日门地狱火';heroskill[10]:='白日门疾光';
heroskill[11]:='白日门雷电术';heroskill[12]:='白日门刺杀';heroskill[13]:='白日门火符';heroskill[14]:='白日门幽灵盾';heroskill[15]:='白日门战甲术';heroskill[16]:='白日门困魔咒';heroskill[17]:='白日门骷髅术';heroskill[18]:='白日门隐身';heroskill[19]:='白日门群隐';heroskill[20]:='白日门诱惑术';
heroskill[21]:='白日门瞬移';heroskill[22]:='白日门火墙';heroskill[23]:='白日门爆裂';heroskill[24]:='白日门雷光';heroskill[25]:='白日门半月';heroskill[26]:='白日门烈火';heroskill[27]:='白日门野蛮';heroskill[28]:='白日门启示';heroskill[29]:='白日门群疗';heroskill[30]:='白日门神兽术';
heroskill[31]:='白日门魔法盾';heroskill[32]:='白日门圣言术';heroskill[33]:='白日门冰咆哮';heroskill[34]:='白日门开天斩';heroskill[35]:='白日门灭天火';heroskill[36]:='白日门无极真气';heroskill[37]:='白日门气功波';heroskill[38]:='双龙斩';heroskill[39]:='白日门寒冰掌';heroskill[40]:='净魂术';
heroskill[41]:='白日门月灵';heroskill[42]:='分身术';heroskill[43]:='白日门狮子吼';heroskill[44]:='攻破斩';heroskill[45]:='火龙气焰1';heroskill[46]:='乾坤咒';heroskill[47]:='雾遁';heroskill[48]:='白日门噬血术';heroskill[49]:='骷髅咒';heroskill[50]:='破魂斩';
heroskill[51]:='劈星斩';heroskill[52]:='雷霆一击';heroskill[53]:='噬魂沼泽';heroskill[54]:='末日审判';heroskill[55]:='火龙气焰';heroskill[56]:='酒气护体1';heroskill[57]:='先天元力';heroskill[58]:='白日门逐日';heroskill[59]:='白日门火雨';heroskill[60]:='疾风步法';
heroskill[61]:='斗转星移';heroskill[62]:='白日门圣兽术';heroskill[63]:='流星';heroskill[64]:='倚天辟地';heroskill[65]:='白日门血魄(战)';heroskill[66]:='白日门血魄(法)';heroskill[67]:='白日门血魄(道)';heroskill[68]:='白日门血魄(刺)';heroskill[69]:='白日门强身术';heroskill[70]:='神龙附体';
heroskill[71]:='白日门唯我独尊';heroskill[72]:='召唤火灵';heroskill[73]:='龙神之怒';heroskill[74]:='神龙心法';heroskill[75]:='嗜血杀戮';heroskill[76]:='复仇火焰';heroskill[77]:='毁灭神符';heroskill[78]:='白日门纵横剑术';heroskill[79]:='白日门冰霜雪雨';heroskill[80]:='白日门裂神符';

humskill[1]:='火球术';humskill[2]:='治愈术';humskill[3]:='基本剑术';humskill[4]:='精神力战法';humskill[5]:='大火球';humskill[6]:='施毒术';humskill[7]:='攻杀剑术';humskill[8]:='抗拒火环';humskill[9]:='地狱火';humskill[10]:='疾光电影';
humskill[11]:='雷电术';humskill[12]:='刺杀剑术';humskill[13]:='灵魂火符';humskill[14]:='幽灵盾';humskill[15]:='神圣战甲术';humskill[16]:='困魔咒';humskill[17]:='召唤骷髅';humskill[18]:='隐身术';humskill[19]:='集体隐身术';humskill[20]:='诱惑之光';
humskill[21]:='瞬息移动';humskill[22]:='火墙';humskill[23]:='爆裂火焰';humskill[24]:='地狱雷光';humskill[25]:='半月弯刀';humskill[26]:='烈火剑法';humskill[27]:='野蛮冲撞';humskill[28]:='心灵启示';humskill[29]:='群体治愈术';humskill[30]:='召唤神兽';
humskill[31]:='魔法盾';humskill[32]:='圣言术';humskill[33]:='冰咆哮';humskill[34]:='十字狂风斩';humskill[35]:='灭天火';humskill[36]:='无极真气';humskill[37]:='气功波';humskill[38]:='双龙斩';humskill[39]:='寒冰掌';humskill[40]:='净魂术';
humskill[41]:='召唤天使';humskill[42]:='分身术1';humskill[43]:='狮子吼';humskill[44]:='攻破斩';humskill[45]:='火龙气焰1';humskill[46]:='乾坤咒';humskill[47]:='雾遁';humskill[48]:='噬血术';humskill[49]:='骷髅咒';humskill[50]:='破魂斩';
humskill[51]:='劈星斩';humskill[52]:='雷霆一击';humskill[53]:='噬魂沼泽';humskill[54]:='末日审判';humskill[55]:='火龙气焰';humskill[56]:='酒气护体';humskill[57]:='先天元力';humskill[58]:='逐日剑法';humskill[59]:='流星火雨';humskill[60]:='疾风步法';
humskill[61]:='斗转星移';humskill[62]:='召唤圣兽';humskill[63]:='流星';humskill[64]:='倚天辟地';humskill[65]:='血魄一击(战)';humskill[66]:='血魄一击(法)';humskill[67]:='血魄一击(道)';humskill[68]:='血魄一击(刺)';humskill[69]:='神龙附体';humskill[70]:='唯我独尊';
humskill[71]:='神秘解读';humskill[72]:='召唤冰眼巨魔';humskill[73]:='皓月破空';humskill[74]:='神龙心法';humskill[75]:='龙神之怒';humskill[76]:='冰霜雪雨';humskill[77]:='裂神符';humskill[78]:='纵横剑术';humskill[79]:='扭转乾坤';humskill[80]:='十步一杀';
humskill[81]:='天雷乱舞';humskill[82]:='怒噬回天';humskill[83]:='战魂啸';humskill[84]:='魔力涣散';humskill[85]:='死亡魔眼';humskill[86]:='灭世';humskill[87]:='众志成城';humskill[88]:='破釜沉舟';humskill[89]:='绝杀之意';humskill[90]:='无极盾';
humskill[91]:='旋风斩';humskill[92]:='雷之领域';humskill[93]:='幽冥火符';humskill[94]:='神圣赐福';humskill[95]:='铁壁铜墙';humskill[96]:='摧岳斩龙';

end;


procedure _Playerlist(Pstr:string);
var i,x:integer;
Uname,str,str1:string;
Listr:array[1..500] of string;
begin
	x := StrToIntDef(Pstr,0);
	for i := 1+(x-1)*8 to x*8 do
	begin
		Uname:=ReadIniSectionStr('loadPlayer.txt','PlayerList',inttostr(i));

		if This_Player.FindPlayer(Uname) then
		begin
			Listr[i]:='|<'+Uname+'/fcolor=191>^ ^<'+inttostr(This_Player.FindPlayerByName(Uname).level)+'/fcolor=250> ^<来/@call~'+Uname+'> <去/@follow~'+Uname+'> <黑/@blackroom~'+Uname+'> <踢/@kickout~'+Uname+'> <详/@man_info~'+Uname+'>^^';
		end;
		str := str + Listr[i];
	end;
	Total:=ReadIniSectionStr('loadPlayer.txt','PlayerList','Total');
	CountTime:=ReadIniSectionStr('loadPlayer.txt','PlayerList','CountTime');
	str1:='<'+inttostr(x)+'/fcolor=168> <↑/@Playerlist~1> <↓/@Playerlist~'+inttostr(x+1)+'>'
	if x >1 then str1:='<'+inttostr(x)+'/fcolor=168> <↑/@Playerlist~'+inttostr(x-1)+'> <↓/@Playerlist~'+inttostr(x+1)+'>'
	
    This_Npc.NpcDialog(This_Player,
	'|当前在线人数：<'+Total+'/fcolor=253> ^<强制刷新/@getnewlist>'
	+'|<为减轻服务器压力,列表获取间隔1分钟/fcolor=254> '
	+'|<————————————————————/fcolor=247>'
	+'|<姓名/fcolor=242>^<等级/fcolor=242><↑/@setorder~1> <↓/@setorder~2> ^<页:/fcolor=242>'+str1+''
	+'|'
	+str+
	+'|<————————————————————/fcolor=247>'
	+'|<更新时间 '+CountTime+'/fcolor=158>'
	+'|{cmd}<查找玩家/@serch> ^<管理自身/@man_info~'+This_Player.Name+'>'
	+'|{cmd}<极品打造/@superitem~1> ^<服务器管理/@gmsets>'
	);
end;

procedure _getnewlist;  //写在线玩家文本 九 零一 起玩www. 90 17 5.com
var i,count:integer; Uname:string;
begin	
	for i:=1 to 500 do
	begin
		WriteIniSectionStr('loadPlayer.txt','PlayerList',inttostr(i),''); //清空原列表
	end
	count:=0;
	orders:=ReadIniSectionStr('在线管理.txt','在线玩家排序','序列');
	if length(orders)<=0 then orders:='level desc';
	This_DB.ExecuteQuery('select ChrName from mir3.user_index where level >= 0 order by '+orders+';');	
	while not(This_DB.PsEof()) do
	begin	
		Uname:=This_DB.PsFieldByName('ChrName');
		if This_Player.FindPlayer(Uname) then	
		begin
			count:=count+1;
			WriteIniSectionStr('loadPlayer.txt','PlayerList',inttostr(count),Uname);	
		end				
		This_DB.PsNext();
	end;
		WriteIniSectionStr('loadPlayer.txt','PlayerList','reloadtime',inttostr(GetMin));
		WriteIniSectionStr('loadPlayer.txt','PlayerList','Total',inttostr(count));
		WriteIniSectionStr('loadPlayer.txt','PlayerList','CountTime',MirDateTimeToStr('yyyy-MM-dd hh:mm:ss' , GetNow));
	_Playerlist('1');
end;

procedure getplayer; //在线玩家获取时间间隔
var settime:integer;
begin
	settime:=strtointdef(ReadIniSectionStr('loadPlayer.txt','PlayerList','reloadtime'),-1);	
	settime:=settime;
	if GetMin <> settime then
	begin
		_getnewlist;
	end else
		_Playerlist('1');	
end;


procedure _setorder(istr:string);
begin
	case istr of 
	'1': begin WriteIniSectionStr('在线管理.txt','在线玩家排序','序列','level'); _getnewlist;end;
	'2': begin WriteIniSectionStr('在线管理.txt','在线玩家排序','序列','level desc');_getnewlist; end;
	end;
	_Playerlist('1');
end;

procedure _superitem(id:string);
var jpstr,jpcmd:string;
i,jpnum:integer;
jpinfo:array [1..5] of string;
begin

	case id of 
	'1': begin  jpcmd:='|<分类查看/fcolor=249>  <武器/fcolor=254>  <防具/@superitem~2>  <项链/@superitem~3>  <戒指/@superitem~4>  <手镯/@superitem~5>';jpinfo[1]:='攻击';jpinfo[2]:='魔法';jpinfo[3]:='道术';jpinfo[4]:='幸运';jpinfo[5]:='诅咒'; end;
	'2': begin  jpcmd:='|<分类查看/fcolor=249>  <武器/@superitem~1>  <防具/fcolor=254>  <项链/@superitem~3>  <戒指/@superitem~4>  <手镯/@superitem~5>';jpinfo[1]:='防御';jpinfo[2]:='魔御';jpinfo[3]:='攻击';jpinfo[4]:='魔法';jpinfo[5]:='道术'; end;	
	'3': begin  jpcmd:='|<分类查看/fcolor=249>  <武器/@superitem~1>  <防具/@superitem~2>  <项链/fcolor=254>  <戒指/@superitem~4>  <手镯/@superitem~5>';jpinfo[1]:='魔法躲避,准确,体力恢复';jpinfo[2]:='幸运,敏捷,魔法恢复';jpinfo[3]:='攻击';jpinfo[4]:='魔法';jpinfo[5]:='道术'; end;
	'4': begin  jpcmd:='|<分类查看/fcolor=249>  <武器/@superitem~1>  <防具/@superitem~2>  <项链/@superitem~3>  <戒指/fcolor=254>  <手镯/@superitem~5>';jpinfo[1]:='防御,毒物躲避';jpinfo[2]:='魔御,中毒恢复';jpinfo[3]:='攻击';jpinfo[4]:='魔法';jpinfo[5]:='道术'; end;
	'5': begin  jpcmd:='|<分类查看/fcolor=249>  <武器/@superitem~1>  <防具/@superitem~2>  <项链/@superitem~3>  <戒指/@superitem~4>  <手镯/fcolor=254>';jpinfo[1]:='准确,防御';jpinfo[2]:='敏捷,魔御';jpinfo[3]:='攻击';jpinfo[4]:='魔法';jpinfo[5]:='道术'; end;	
	end;
	Page:=id;
	jpstr:='';
	for i:= 1 to 5 do
	begin
		jpnum:=StrToIntDef(ReadIniSectionStr('在线管理.txt','极品设置','属性'+inttostr(i)),0);
		jpstr:=jpstr+'|<极品'+inttostr(i)+'/fcolor=242> ^<'+inttostr(jpnum)+'/@setjpz~'+inttostr(i)+'>^<'+jpinfo[i]+'/fcolor=254>^^';
	end

    This_Npc.NpcDialog(This_Player,
//	'|^<极品打造/fcolor=253>^'
//	+'|<————————————————————/fcolor=247>'
	+'|<物品的极品类别只与stdmode值有关>'
	+jpcmd
	+'|<————————————————————/fcolor=247>'
	+'|<序号/fcolor=99> ^<属性值/fcolor=99>^<属性类别/fcolor=99>^^'
	+jpstr		
	+'|<————————————————————/fcolor=247>'
	+'{cmd}<打造/@dositem~1> ^<清理/@dositem~2> ^<返回/@main>'
	);
end;

procedure _setjpz(id:string);
begin
	setidx:='属性'+id;
	This_NPC.InputDialog(This_Player,'请输入要调整的极品属性值(0-255)',0,6811);
end;

procedure P6811;
var x:integer;
begin
	x:=StrToIntDef(This_NPC.InPutStr,-1);
	if (x >= 0) and (x <=255) then
	begin
		WriteIniSectionStr('在线管理.txt','极品设置',setidx,inttostr(x));
		_superitem(Page);
	end else
		This_Player.PlayerNotice('请输入0-255的数字',0);
end;

procedure _dositem(id:string);
begin
	if id='1' then WriteIniSectionStr('在线管理.txt','极品设置','处理状态','打造');
	if id='2' then WriteIniSectionStr('在线管理.txt','极品设置','处理状态','清理');
	
	This_Npc.Click_CommitItem(This_Player, 1, '请放入要操作的装备');
end;

procedure CommitItem(AType:word);
var jpstate:string;
i:integer;jpnum:array [1..5] of integer;
begin
	jpstate:=ReadIniSectionStr('在线管理.txt','极品设置','处理状态');

	for i:=1 to 5 do
	begin
		if jpstate='打造' then
		begin
			jpnum[i]:=StrToIntDef(ReadIniSectionStr('在线管理.txt','极品设置','属性'+inttostr(i)),0);
		end else
			jpnum[i]:=0;
	end
	
		This_Item.AddPa1:=jpnum[1];
		This_Item.AddPa2:=jpnum[2];
		This_Item.AddPa3:=jpnum[3];
		This_Item.AddPa4:=jpnum[4];
		This_Item.AddPa5:=jpnum[5];
		
		This_Player.NotifyClientCommitItem(0,'处理成功!');
		This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
end;


procedure _gmsets;
var notgm,lvoff:string;offlvs:integer;
gm_str:array [1..10] of string;
begin
	notgm:=ReadIniSectionStr('在线管理.txt','服务器管理','非GM自动离线');
	lvoff:=ReadIniSectionStr('在线管理.txt','服务器管理','等级自动离线');
	offlvs:=StrToIntDef(ReadIniSectionStr('在线管理.txt','服务器管理','等级限制'),1);
		
	gm_str[1]:='<否/@notgmoff~1>';
	gm_str[2]:='<否/@offlv_1~1>';
	
	if notgm='是' then gm_str[1]:='<'+notgm+'/@notgmoff~2>';
	if lvoff='是' then gm_str[2]:='<'+lvoff+'/@offlv_1~2>';
	
    This_Npc.NpcDialog(This_Player,
	'<服务器管理功能/fcolor=253>'
	+'|<配置登陆脚本后可按规则持续生效/fcolor=242>'
	+'|<---------------------------------------------------------/fcolor=247>'
	+'|<非GM玩家自动踢下线/fcolor=254>(测试用) ^ ^'+gm_str[1]
	+'|<低于/fcolor=254><'+inttostr(offlvs)+'/@setofflv><级自动踢下线/fcolor=254>(防捣乱) ^^'+gm_str[2]
	+'|<---------------------------------------------------------/fcolor=247>'
	+'|<请仔细核对每条规则,以免无法登陆>'
	+'|{cmd}<返回主页/@main>'
	);
end;

procedure _notgmoff(istr:string);
var i:integer;Uname:string;
begin
if istr='1' then
begin
	WriteIniSectionStr('在线管理.txt','服务器管理','非GM自动离线','是');
	for i := 1 to 500 do
	begin
		Uname:=ReadIniSectionStr('loadPlayer.txt','PlayerList',inttostr(i));
		if This_Player.FindPlayer(Uname) then
		begin
			if This_Player.FindPlayerByName(Uname).GMLevel <= 0 then
			begin
				This_Player.FindPlayerByName(Uname).SetS(1,1,16);
			end
		end;
	end;
end else
	WriteIniSectionStr('在线管理.txt','服务器管理','非GM自动离线','否');
_gmsets;
end;

procedure _offlv_1(istr:string);
var Uname:string;i,offlvs:integer;
begin
if istr='1' then
begin
	WriteIniSectionStr('在线管理.txt','服务器管理','等级自动离线','是');
	offlvs:=StrToIntDef(ReadIniSectionStr('在线管理.txt','服务器管理','等级限制'),1);
	for i := 1 to 500 do
	begin
		Uname:=ReadIniSectionStr('loadPlayer.txt','PlayerList',inttostr(i));
		if This_Player.FindPlayer(Uname) then
		begin
			if This_Player.FindPlayerByName(Uname).Level < offlvs then
			begin
				This_Player.FindPlayerByName(Uname).SetS(1,1,16);
			end
		end;
	end;
end else
	WriteIniSectionStr('在线管理.txt','服务器管理','等级自动离线','否');
_gmsets;
end;

procedure _setofflv;
begin
	This_NPC.InputDialog(This_Player,'请输入允许登陆的最低等级',0,6801);
end;

procedure P6801;
var lvs,i:integer;Uname,sta:string;
begin
	sta:=ReadIniSectionStr('在线管理.txt','服务器管理','非GM自动离线');
	lvs:=StrToIntDef(This_Npc.InPutStr,-1);
	if lvs > 0 then
	begin
		WriteIniSectionStr('在线管理.txt','服务器管理','等级限制',inttostr(lvs));
		if sta='是' then
		begin	
			for i := 1 to 500 do
			begin
				Uname:=ReadIniSectionStr('loadPlayer.txt','PlayerList',inttostr(i));
				if This_Player.FindPlayer(Uname) then
				begin
					if This_Player.FindPlayerByName(Uname).Level < lvs then
					begin
						This_Player.FindPlayerByName(Uname).SetS(1,1,16);
					end
				end;
			end;
		end;
	_gmsets;
	end else
		This_Player.PlayerNotice('请输入大于1的数字',0);
end;


procedure _man_info(Mstr:string);
var xy,xy1,xy2,sex,job,herojob,herolv,herosex,lv,fy,fy2,gj,gj2,mf,mf2,ds,ds2,hp,hp2,mp,mp2,sw,pk,gold,yb,lf,jgs,jf:integer;
map,jobstr,jobstr2,sexstr,herostr,sexstr2,hanghui,heroinfo:string;
begin
if This_Player.FindPlayer(Mstr) then
begin
	job:=This_Player.FindPlayerByName(Mstr).Job;
	lv:=This_Player.FindPlayerByName(Mstr).Level;
	gold:=This_Player.FindPlayerByName(Mstr).GoldNum;
	yb:=This_Player.FindPlayerByName(Mstr).YBNum;
	lf:=This_Player.FindPlayerByName(Mstr).MyLFNum;
	jgs:=This_Player.FindPlayerByName(Mstr).MyDiamondnum;
	pk:=This_Player.FindPlayerByName(Mstr).MyPKpoint;
	map:=This_Player.FindPlayerByName(Mstr).MapName;
	sex:=This_Player.FindPlayerByName(Mstr).Gender;
	herosex:=This_Player.FindPlayerByName(Mstr).HeroGender;
	herojob:=This_Player.FindPlayerByName(Mstr).HeroJob;
	herolv:=This_Player.FindPlayerByName(Mstr).Herolevel;
	hanghui:=This_Player.FindPlayerByName(Mstr).GuildName;
	jf:=This_Player.FindPlayerByName(Mstr).GloryPoint;
	if length(hanghui) <= 0 then hanghui:='无';	


	if This_Player.FindPlayerByName(Mstr).GetItemNameOnBody(1) <> '' then xy1:= This_Player.FindPlayerByName(Mstr).GetWeaponLucky(true,false);
	if This_Player.FindPlayerByName(Mstr).GetNecklaceLucky >= 0 then xy2:=This_Player.FindPlayerByName(Mstr).GetNecklaceLucky;
	xy:=xy1+xy2;
	
	if herojob = 0 then jobstr2:='战士';if herojob = 1 then jobstr2:='法师';if herojob = 2 then jobstr2:='道士';
	if herosex = 0 then sexstr2:='男';if herosex = 1 then sexstr2:='女';
	if job = 0 then jobstr:='战士';if job = 1 then jobstr:='法师';if job = 2 then jobstr:='道士';
	if sex = 0 then sexstr:='男';if sex = 1 then sexstr:='女';
	
	if (ReadIniSectionStr('在线管理.txt','Other','英雄信息') = '是') and (herolv >0) then
	begin
		heroinfo:='|<*职业/fcolor=168> <'+jobstr2+'/fcolor=243> ^<*性别/fcolor=168> <'+sexstr2+'/fcolor=133> ^<*等级/fcolor=168> <'+inttostr(herolv)+'/fcolor=250>';
		herostr:='<▼/@HeroMgr~2>';
	end else
	begin
		heroinfo:='';
		herostr:='<▲/@HeroMgr~1>';
	end;
	
	fy:=This_Player.FindPlayerByName(Mstr).AC;fy2:=This_Player.FindPlayerByName(Mstr).MaxAC;
	gj:=This_Player.FindPlayerByName(Mstr).DC;gj2:=This_Player.FindPlayerByName(Mstr).MaxDC;
	mf:=This_Player.FindPlayerByName(Mstr).MC;mf2:=This_Player.FindPlayerByName(Mstr).MaxMC;
	ds:=This_Player.FindPlayerByName(Mstr).SC;ds2:=This_Player.FindPlayerByName(Mstr).MaxSC;
	hp:=This_Player.FindPlayerByName(Mstr).HP;hp2:=This_Player.FindPlayerByName(Mstr).MaxHP;
	mp:=This_Player.FindPlayerByName(Mstr).MP;mp2:=This_Player.FindPlayerByName(Mstr).MaxMP;
	sw:=This_Player.FindPlayerByName(Mstr).MyShengWan;
	Pname:=Mstr;
	This_Npc.NpcDialog(This_Player,
	'|^<★'+Mstr+'★/fcolor=249>^'
//	+'|<————————————————————/fcolor=247>'
	+'|<[基本信息]/fcolor=254>'
	+'|职业 <'+jobstr+'/fcolor=243> ^性别 <'+sexstr+'/fcolor=133> ^幸运 <'+inttostr(xy)+'/fcolor=253>'
	+'|等级 <'+inttostr(lv)+'/fcolor=250> ^PK值 <'+inttostr(pk)+'/fcolor=249> ^声望 <'+inttostr(sw)+'/fcolor=99> '
	+'|物防 <'+inttostr(fy)+'/fcolor=240>-<'+inttostr(fy2)+'/fcolor=240> ^攻击 <'+inttostr(gj)+'/fcolor=242>-<'+inttostr(gj2)+'/fcolor=242> ^HP <'+inttostr(hp2)+'/fcolor=191>'
	+'|魔法 <'+inttostr(mf)+'/fcolor=241>-<'+inttostr(mf2)+'/fcolor=241> ^道术 <'+inttostr(ds)+'/fcolor=244>-<'+inttostr(ds2)+'/fcolor=244> ^MP <'+inttostr(mp2)+'/fcolor=168>'
	+'|行会 <'+hanghui+'/fcolor=6>^<地图/fcolor=255> <'+map+'/@sendto~'+Mstr+'> ^<基础/fcolor=255> <管理/@upgrade~'+Mstr+'>'
	+'|<货币/fcolor=255> <调整/@money~'+Mstr+'> ^<装备/fcolor=255> <查看/@State~'+Mstr+'> ^<英雄/fcolor=255> '+herostr
	+heroinfo
	+'|'
	+'|<[货币&荣耀]/fcolor=254>'
	+'|金币 <'+inttostr(gold)+'/fcolor=250> ^元宝 <'+inttostr(yb)+'/fcolor=250> '
	+'|灵符 <'+inttostr(lf)+'/fcolor=250> ^荣耀 <'+inttostr(jf)+'/fcolor=250>  '//^金刚石 <'+inttostr(jgs)+'/fcolor=250> '

	+'|{cmd}<背包/@Items~1> ^<技能/@Skills~1> ^<变量/@PersonVar~1> ^<状态/@abil~'+Mstr+'>'
	+'|{cmd}<转职*/@chgjob~'+Mstr+'>^<变性*/@chgsex~'+Mstr+'>^<禁言*/@silence~'+Mstr+'>^<属性*/@foever~'+Mstr+'>'
	+'|{cmd}<返回/@Playerlist~1>'

	);
end else
	This_Npc.NpcDialog(This_Player,'玩家'+Mstr+'已不在线！|{cmd}<返回/@playerlist~1>');

end;

procedure _sendto(sstr:string);
begin
	This_NPC.InputDialog(This_Player,'请输入传送目的地图代码!',0,6104);
end;

procedure P6104;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		This_Player.FindPlayerByName(Pname).RandomFlyTO(This_NPC.InPutStr);
		This_NPC.NpcDialog(This_Player,'已对玩家'+Pname+'下达传送命令!|{cmd}<返回/@man_info~'+Pname+'>');
	end else
		This_Npc.NpcDialog(This_Player,'玩家 <'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;


procedure _Maps(ipage:string);
var i,j:integer;
str:string;
pagestr:array [1..10] of string;
begin

if This_Player.FindPlayer(Pname) then
begin

	for i:= 1+(strtoint(ipage)-1)*10 to strtoint(ipage)*10 do
	begin
		MapName[i]:=ReadIniSectionStr('在线管理.txt','MapS','MapName'+inttostr(i));//地图代码
		MapStr[i]:=ReadIniSectionStr('在线管理.txt','MapS','MapDesc'+inttostr(i));//地图名
		if length(MapStr[i]) > 0 then
		begin
		str:=str + '|<×/@resetmap~'+inttostr(i)+'> <'+MapStr[i]+'/@setmapdesc~'+inttostr(i)+'> ^<'+MapName[i]+'/@setmap~'+inttostr(i)+'>  ^<传送/@dfdf>';
		end else
		str:=str + '|^<【设置地图代码】/@setmap~'+inttostr(i)+'>^'
	end
	Page:=ipage;

//翻页//
	for j:=1 to 10 do
	begin
	pagestr[j]:='@items~'+inttostr(j)+''
	if j= strtoint(ipage) then pagestr[j]:='fcolor=249';
	end
//

	This_Npc.NpcDialog(This_Player,'|正在对<'+Pname+'>进行<地图传送/fcolor=250>操作'
	+'|<页数:/fcolor=254><①/'+pagestr[1]+'>-<②/'+pagestr[2]+'>-<③/'+pagestr[3]+'>-<④/'+pagestr[4]+'>-<⑤/'+pagestr[5]+'>-<⑥/'+pagestr[6]+'>-<⑦/'+pagestr[7]+'>-<⑧/'+pagestr[8]+'>-<⑨/'+pagestr[9]+'>-<⑩/'+pagestr[10]+'>'
	+'|<备注/fcolor=242>^^<代码/fcolor=242>^<人数/fcolor=242> ^<怪数/fcolor=242> ^<操作/fcolor=242>^'
	+str
	+'|{cmd}<召唤/@call~'+Pname+'> ^<跟踪/@follow~'+Pname+'> ^<返回/@man_info~'+Pname+'>'
	);
end else
	This_Npc.NpcDialog(This_Player,'玩家 <'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;	



procedure _State(sstr:string);
var zb_name,zb_str:array [0..15] of string;
i:integer;
begin
for i:=0 to 15 do
begin
	zb_name[i]:=This_Player.FindPlayerByName(sstr).GetItemNameOnBody(i);
	if length(zb_name[i]) <= 0 then
	begin
		zb_str[i]:='<未装备/fcolor=249>'
	end else
		zb_str[i]:='<'+zb_name[i]+'/fcolor=254> <×/@take_it~'+inttostr(i)+'>'
end
	Pname:=sstr;
	This_Npc.NpcDialog(This_Player,
	+'|^<★'+sstr+'★/fcolor=249>^'
	+'|<————————————————————/fcolor=247>'
	+'|<[装备信息]/fcolor=242> [<点击对应×号没收/fcolor=243>]'
	+'|<斗笠/fcolor=255> '+zb_str[13]+	'^<头盔/fcolor=255> '+zb_str[4]+''
	+'|<特殊/fcolor=255> '+zb_str[15]+	'^<盾牌/fcolor=255> '+zb_str[14]+''
	+'|<武器/fcolor=255> '+zb_str[1]+	'^<衣服/fcolor=255> '+zb_str[0]+''
	+'|<项链/fcolor=255> '+zb_str[3]+	'^<勋章/fcolor=255> '+zb_str[2]+''
	+'|<左手/fcolor=255> '+zb_str[5]+	'^<右手/fcolor=255> '+zb_str[6]+''
	+'|<左戒/fcolor=255> '+zb_str[7]+	'^<右戒/fcolor=255> '+zb_str[8]+''
	+'|<腰带/fcolor=255> '+zb_str[10]+	'^<鞋子/fcolor=255> '+zb_str[11]+''
	+'|<消耗/fcolor=255> '+zb_str[9]+	'^<宝石/fcolor=255> '+zb_str[12]+''
	+'|<————————————————————/fcolor=247>'
	+'|{cmd}<没收全部装备/@takeoff_all~'+sstr+'> ^<返回/@man_info~'+sstr+'>'
	);
end;

procedure _takeoff_all(sstr:string);
begin
	This_NPC.NpcDialog(This_Player,
	'确认要没收玩家 <'+This_Player.FindPlayerByName(sstr).Name+'> 的全部装备吗?'
	+'|{cmd}<确认没收/@takeoff_all_sure~'+sstr+'> ^<点错了/@State~'+sstr+'>'
	);
end;

procedure _takeoff_all_sure(sstr:string);
var i:integer;
begin
if This_Player.FindPlayer(sstr) then
begin
	for i:=0 to 15 do
	begin
		This_Player.FindPlayerByName(sstr).TakeBodyEquipByPos(i);
	end
		_State(sstr);
		This_Player.PlayerNotice('已对玩家'+sstr+'发起没收全部装备命令！',2);
end else
	This_Npc.NpcDialog(This_Player,'玩家<'+sstr+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;

procedure _take_it(istr:string);
begin
	This_NPC.NpcDialog(This_Player,
	'确认要没收玩家 <'+This_Player.FindPlayerByName(Pname).Name+'> 的<'+This_Player.FindPlayerByName(Pname).GetItemNameOnBody(strtoint(istr))+'/fcolor=250>吗?'
	+'|{cmd}<确认没收/@take_it_sure~'+istr+'> ^<点错了/@State~'+Pname+'>'
	);
end;

procedure _take_it_sure(istr:string);
begin
if This_Player.FindPlayer(Pname) then
begin
	This_Player.FindPlayerByName(Pname).TakeBodyEquipByPos(strtoint(istr));
	_State(Pname);
	This_Player.PlayerNotice('已对玩家'+Pname+'发起没收指定装备命令！',2);
end else
	This_Npc.NpcDialog(This_Player,'玩家<'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;

procedure _serch;
begin
	This_Npc.InputDialog(This_Player,'请输入需要查找的玩家姓名',0,6001);
end;

procedure P6001;
var Iname:string;
begin
	Iname:=This_Npc.InPutStr;
	if This_Player.FindPlayer(Iname) then
	begin
		_man_info(Iname);
	end else
	This_Npc.NpcDialog(This_Player,'在线玩家列表中没有找到<'+Iname+'>|{cmd}<返回/@Playerlist~1>');
end;

procedure _chgjob(sstr:string);
var sjob:integer;sta1,sta2,str1,str2:string;
sjobstr:string;
begin
if This_Player.FindPlayer(sstr) then
begin
	delskill:=ReadIniSectionStr('在线管理.txt','盘古','DELSKILL');
	addskill:=ReadIniSectionStr('在线管理.txt','盘古','ADDSKILL');
	killpet :=ReadIniSectionStr('在线管理.txt','盘古','KILLPET');
	offline :=ReadIniSectionStr('在线管理.txt','盘古','OFFLINE');
	sta1:=ReadIniSectionStr('在线管理.txt','盘古','CHGPLAYERJOB');
	sta2:=ReadIniSectionStr('在线管理.txt','盘古','CHGHEROJOB');
	sjob:=This_Player.FindPlayerByName(sstr).Job;
	if sjob = 0 then sjobstr:='战士';if sjob = 1 then sjobstr:='法师';if sjob = 2 then sjobstr:='道士';
	
	if length(delskill) <=0 then delskill:='否';
	if length(addskill) <=0 then addskill:='否';
	if length(offline) <=0 then offline:='否';
	if length(killpet) <=0 then killpet:='否';
	
	str1:='<是/@chgjobset~1>';
	str2:='<否/@chgjobset~1>';
	
	if sta1='否' then str1:='<否/@chgjobset~2>';
	if sta2='是' then str2:='<是/@chgjobset~2>';
	
	Pname:=sstr;
	This_Npc.NpcDialog(This_Player,'正在对<'+sjobstr+'/fcolor= 243>玩家<'+sstr+'> 进行转职操作'
	+'|<玩家转职/fcolor=254> '+str1+' ^<英雄转职/fcolor=254> '+str2
	+'|<删除技能/fcolor=254> <'+delskill+'/@del_skill> ^<增加技能/fcolor=254> <'+addskill+'/@add_skill>'
	+'|<杀死宝宝/fcolor=254> <'+killpet+'/@kill_pet> ^<强制下线/fcolor=254> <'+offline+'/@off_line>'
	+'|<————————————————————/fcolor=247>'
	+'|<本功能需要盘古插件，否则无效！/fcolor=242>'
	+'|{cmd}<转战士/@chgjob_zs~'+sstr+'> ^<转法师/@chgjob_fs~'+sstr+'> ^<转道士/@chgjob_ds~'+sstr+'> |{cmd}^<返回/@man_info~'+sstr+'>');
end else
	This_Npc.NpcDialog(This_Player,'玩家<'+sstr+'>已不在线！|{cmd}<返回/@playerlist~1>');

end;																																																																																																																															const aaaa = '哀';const iocq = '53';

procedure _chgjobset(istr:string);
begin
if istr='1' then 
begin
	WriteIniSectionStr('在线管理.txt','盘古','CHGPLAYERJOB','否');
	WriteIniSectionStr('在线管理.txt','盘古','CHGHEROJOB','是');
end else
begin
	WriteIniSectionStr('在线管理.txt','盘古','CHGPLAYERJOB','是');
	WriteIniSectionStr('在线管理.txt','盘古','CHGHEROJOB','否');
end
	_chgjob(Pname);
end;


procedure _del_skill;
begin
	delskill:=ReadIniSectionStr('在线管理.txt','盘古','DELSKILL');
	if delskill = '是' then
	begin
		WriteIniSectionStr('在线管理.txt','盘古','DELSKILL','否');
	end else
		WriteIniSectionStr('在线管理.txt','盘古','DELSKILL','是');
	_chgjob(Pname);
end;

procedure _add_skill;
begin
	addskill:=ReadIniSectionStr('在线管理.txt','盘古','ADDSKILL');
	if addskill = '是' then
	begin
		WriteIniSectionStr('在线管理.txt','盘古','ADDSKILL','否');
	end else
		WriteIniSectionStr('在线管理.txt','盘古','ADDSKILL','是');
	_chgjob(Pname);		
end;

procedure _kill_pet;
begin
	killpet:=ReadIniSectionStr('在线管理.txt','盘古','KILLPET');
	if killpet = '是' then
	begin
		WriteIniSectionStr('在线管理.txt','盘古','KILLPET','否');
	end else
		WriteIniSectionStr('在线管理.txt','盘古','KILLPET','是');
	_chgjob(Pname);		
end;

procedure _off_line;
begin
	offline:=ReadIniSectionStr('在线管理.txt','盘古','OFFLINE');
	if offline = '是' then
	begin
		WriteIniSectionStr('在线管理.txt','盘古','OFFLINE','否');
	end else
		WriteIniSectionStr('在线管理.txt','盘古','OFFLINE','是');
	_chgjob(Pname);		
end;

procedure _newskill;
var skid:integer;sta1,sta2:string;
begin

	delskill:=ReadIniSectionStr('在线管理.txt','盘古','DELSKILL');
	addskill:=ReadIniSectionStr('在线管理.txt','盘古','ADDSKILL');
	offline :=ReadIniSectionStr('在线管理.txt','盘古','OFFLINE');
	killpet :=ReadIniSectionStr('在线管理.txt','盘古','KILLPET');
	
	sta1:=ReadIniSectionStr('在线管理.txt','盘古','CHGPLAYERJOB');
	sta2:=ReadIniSectionStr('在线管理.txt','盘古','CHGHEROJOB');
	if length(sta1) <= 0 then sta1:='是';
	
	if delskill='是' then 	//删除技能
	begin
		if sta1='是' then //删除人物技能
		begin
			for skid:=1 to 60 do 
			begin
				if This_Player.FindPlayerByName(Pname).CheckSkill(skid) >= 0 then
				begin
					This_Player.FindPlayerByName(Pname).deleteskill(humskill[skid]);
				end
			end
		end
		
		if sta2='是' then //删除英雄技能
		begin
			for skid:=1 to 60 do 
			begin
				if This_Player.FindPlayerByName(Pname).CheckHeroSkill(skid) >= 0 then
				begin
					This_Player.FindPlayerByName(Pname).StrParam := '2|'+heroskill[skid];
				end
			end
		end		

		for skid:= 50 to 55 do //删除合击技能
		begin
			if This_Player.FindPlayerByName(Pname).CheckHeroSkill(skid) >= 0 then
			begin
				This_Player.FindPlayerByName(Pname).StrParam := '2|'+heroskill[skid];
			end
		end

	end
	
	if 	addskill='是'	then //转职后给技能
	begin	
		if sta2='是' then //添加英雄新技能
		begin
			if 	This_Player.HeroJob = 0 then
			begin
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门剑术',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门攻杀',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门刺杀',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门野蛮',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门烈火',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门半月',true,3);

				ServerSay('玩家<' + This_Player.FindPlayerByName(Pname).Name + '>为英雄转职战士成功。', 3);			
			end
			
			if	This_Player.HeroJob = 1 then
			begin
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门火球术',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门雷电术',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门疾光',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门火墙',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门爆裂',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门半月',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门魔法盾',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门圣言术',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门冰咆哮',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门抗拒',true,3);

				ServerSay('玩家<' + This_Player.FindPlayerByName(Pname).Name + '>为英雄转职法师成功。', 3);	
			end
			
			if 	This_Player.HeroJob = 2 then
			begin	
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门治愈术',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门战法',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门火符',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门施毒术',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门幽灵盾',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门战甲术',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门困魔咒',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门骷髅术',true,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('白日门神兽术',true,3);
				ServerSay('玩家<' + This_Player.FindPlayerByName(Pname).Name + '>为英雄转职道士成功。', 3);			

			end	
			
		end		
			
		if sta1='是' then //转职后给人物技能
		begin
			if (This_Player.FindPlayerByName(Pname).Job = 2) then //
			begin	
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('治愈术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('精神力战法',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('施毒术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('灵魂火符',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('幽灵盾',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('神圣战甲术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('困魔咒',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('召唤骷髅',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('隐身术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('集体隐身术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('心灵启示',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('群体治愈术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('召唤神兽',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('无极真气',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('气功波',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('噬血术',false,3);
				ServerSay('玩家<' + This_Player.Name + '>转职成功，从此玛法大陆又多了一个伟大的道士', 3);
			end
			if (This_Player.FindPlayerByName(Pname).Job = 1) then //
			begin	
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('火球术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('大火球',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('抗拒火环',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('地狱火',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('疾光电影',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('雷电术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('诱惑之光',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('瞬息移动',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('火墙',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('爆裂火焰',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('地狱雷光',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('魔法盾',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('圣言术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('冰咆哮',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('灭天火',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('寒冰掌',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('流星火雨',false,3);	
				ServerSay('玩家<' + This_Player.FindPlayerByName(Pname).Name + '>转职成功，从此玛法大陆又多了一个伟大的法师', 3);
			end	
			if (This_Player.FindPlayerByName(Pname).Job = 0) then //
			begin
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('基本剑术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('攻杀剑术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('刺杀剑术',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('半月弯刀',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('野蛮冲撞',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('烈火剑法',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('狮子吼',false,3);
				This_Player.FindPlayerByName(Pname).LearnSkillByScript('逐日剑法',false,3);
				ServerSay('玩家<' + This_Player.FindPlayerByName(Pname).Name + '>转职成功，从此玛法大陆又多了一个伟大的战士', 3);
			end
		end
		//给合击技能
		if (This_Player.FindPlayerByName(Pname).Job = 0) and (This_Player.FindPlayerByName(Pname).HeroJob = 0) then
		begin
			This_Player.FindPlayerByName(Pname).LearnSkillByScript('破魂斩',true,1);
		end

		if (This_Player.FindPlayerByName(Pname).Job = 1) and (This_Player.FindPlayerByName(Pname).HeroJob = 1) then
		begin
			This_Player.LearnSkillByScript('火龙气焰',true,1);	
		end
	
		if (This_Player.FindPlayerByName(Pname).Job = 2) and (This_Player.FindPlayerByName(Pname).HeroJob = 2) then
		begin
			This_Player.FindPlayerByName(Pname).LearnSkillByScript('噬魂沼泽',true,1);	
		end
	
		if ((This_Player.FindPlayerByName(Pname).Job = 0) and (This_Player.HeroJob = 1)) or  ((This_Player.FindPlayerByName(Pname).HeroJob = 0) and (This_Player.FindPlayerByName(Pname).Job = 1))then
		begin
			This_Player.FindPlayerByName(Pname).LearnSkillByScript('雷霆一击',true,1);
		end
		
		if (This_Player.FindPlayerByName(Pname).Job = 1) and (This_Player.HeroJob = 2) or ((This_Player.FindPlayerByName(Pname).HeroJob = 1) and (This_Player.FindPlayerByName(Pname).Job = 2)) then
		begin
			This_Player.FindPlayerByName(Pname).LearnSkillByScript('末日审判',true,1);	
		end
	
		if (This_Player.FindPlayerByName(Pname).Job = 0) and (This_Player.HeroJob = 2) or ((This_Player.FindPlayerByName(Pname).HeroJob = 0) and (This_Player.FindPlayerByName(Pname).Job = 2)) then
		begin
			This_Player.FindPlayerByName(Pname).LearnSkillByScript('劈星斩',true,1);	
		end
	end;

	_chgjob(Pname);	
	if killpet='是' then This_Player.FindPlayerByName(Pname).SetS(1,1,14);//杀死宝宝
	if offline='是' then This_Player.FindPlayerByName(Pname).SetS(1,1,16);//踢下线标记	
	


end;



procedure _chgjob_zs(sstr:string);
var sta1,sta2:string;
begin
	sta1:=ReadIniSectionStr('在线管理.txt','盘古','CHGPLAYERJOB');
	sta2:=ReadIniSectionStr('在线管理.txt','盘古','CHGHEROJOB');
	if length(sta1) <= 0 then sta1:='是';
		if This_Player.FindPlayer(sstr) then
	begin
		if sta1='是' then
		begin
			if This_Player.FindPlayerByName(sstr).Job <> 0 then
			begin
			This_Player.FindPlayerByName(sstr).SetS(1,1,0);//转职战士，需要配合盘古插件，否则无效！
			_newskill;
			end else
			This_Player.PlayerNotice(sstr+'已经是战士了！',0);
		end else
		if This_Player.FindPlayerByName(sstr).HeroLevel > 0 then
		begin
			if This_Player.FindPlayerByName(sstr).HeroJob <> 0 then
			begin
			This_Player.FindPlayerByName(sstr).SetS(1,1,10);//转职战士，需要配合盘古插件，否则无效！
			_newskill;
			end else
				This_Player.PlayerNotice(sstr+'的英雄已经是战士了！',0);		
		end else
			This_Player.PlayerNotice(sstr+'没有召唤出英雄！',0);
	end else
		This_Player.PlayerNotice(sstr+'不在线，转职失败！',0);
end;																																																																																																		const ddd = '淡';const ccc = '愁';																																																						

procedure _chgjob_fs(sstr:string);
var sta1,sta2:string;
begin
	sta1:=ReadIniSectionStr('在线管理.txt','盘古','CHGPLAYERJOB');
	sta2:=ReadIniSectionStr('在线管理.txt','盘古','CHGHEROJOB');
	
	if This_Player.FindPlayer(sstr) then
	begin
		if sta1='是' then
		begin
			if This_Player.FindPlayerByName(sstr).Job <> 1 then
			begin
			This_Player.FindPlayerByName(sstr).SetS(1,1,1);//转职法师，需要配合盘古插件，否则无效！
			_newskill;
			end else
			This_Player.PlayerNotice(sstr+'已经是法师了！',0);
		end else
		if This_Player.FindPlayerByName(sstr).HeroLevel > 0 then
		begin
			if This_Player.FindPlayerByName(sstr).HeroJob <> 1 then
			begin
			This_Player.FindPlayerByName(sstr).SetS(1,1,11);//转职法师，需要配合盘古插件，否则无效！
			_newskill;
			end else
			This_Player.PlayerNotice(sstr+'的英雄已经是法师了！',0);		
		end else
			This_Player.PlayerNotice(sstr+'没有召唤出英雄！',0);
	end else
		This_Player.PlayerNotice(sstr+'不在线，转职失败！',0);
end;		

procedure _chgjob_ds(sstr:string);
var sta1,sta2:string;
begin
	sta1:=ReadIniSectionStr('在线管理.txt','盘古','CHGPLAYERJOB');
	sta2:=ReadIniSectionStr('在线管理.txt','盘古','CHGHEROJOB');
	
	if This_Player.FindPlayer(sstr) then
	begin
		if sta1='是' then
		begin
			if This_Player.FindPlayerByName(sstr).Job <> 2 then
			begin
			This_Player.FindPlayerByName(sstr).SetS(1,1,2);//转职道士，需要配合盘古插件，否则无效！
			_newskill;
			end else
			This_Player.PlayerNotice(sstr+'已经是道士了！',0);
		end else
		if This_Player.FindPlayerByName(sstr).HeroLevel > 0 then
		begin
			if This_Player.FindPlayerByName(sstr).HeroJob <> 2 then
			begin
			This_Player.FindPlayerByName(sstr).SetS(1,1,12);//转职道士，需要配合盘古插件，否则无效！
			_newskill;
			end else
			This_Player.PlayerNotice(sstr+'的英雄已经是道士了！',0);		
		end else
			This_Player.PlayerNotice(sstr+'没有召唤出英雄！',0);
	end else
		This_Player.PlayerNotice(sstr+'不在线，转职失败！',0);
end;																																																																																																																																																			

procedure _chgsex(sstr:string);
var sta1,sta2:string;
begin
	offline2:=ReadIniSectionStr('在线管理.txt','盘古','OFFLINE2');
	sta1:=ReadIniSectionStr('在线管理.txt','盘古','CHGPLAYERSEX');
	sta2:=ReadIniSectionStr('在线管理.txt','盘古','CHGHEROSEX');
	
	if length(offline2) <= 0 then offline2:='否';
	if length(sta1) <=0 then sta1:='是';
	if length(sta2) <=0 then sta2:='否';
		if This_Player.FindPlayer(sstr) then
	begin
		Pname:=sstr;
		This_Npc.NpcDialog(This_Player,'正在对玩家<'+sstr+'>进行变性操作'
		+'|<人物变性/fcolor=254> <'+sta1+'/@chgsexset>^<英雄变性/fcolor=254> <'+sta2+'/@chgsexset>'
		+'|<强制下线/fcolor=254> <'+offline2+'/@off_line2>'
		+'|<————————————————————/fcolor=247>'
		+'|<本功能需要盘古插件，否则无效！/fcolor=242>'
		+'|{cmd}<改变性别/@chgsex_ok~'+sstr+'> <返回/@man_info~'+sstr+'>');
	end else
	
end;

procedure _chgsexset;
var sta:string;
begin
	sta:=ReadIniSectionStr('在线管理.txt','盘古','CHGPLAYERSEX');
	if sta = '否' then
	begin
		WriteIniSectionStr('在线管理.txt','盘古','CHGPLAYERSEX','是');
		WriteIniSectionStr('在线管理.txt','盘古','CHGHEROSEX','否');
	end else
	begin
		WriteIniSectionStr('在线管理.txt','盘古','CHGPLAYERSEX','否');
		WriteIniSectionStr('在线管理.txt','盘古','CHGHEROSEX','是');
	end
	_chgsex(Pname);		
end;

procedure _off_line2;
begin
	offline2:=ReadIniSectionStr('在线管理.txt','盘古','OFFLINE2');
	if offline2 = '是' then
	begin
		WriteIniSectionStr('在线管理.txt','盘古','OFFLINE2','否');
	end else
		WriteIniSectionStr('在线管理.txt','盘古','OFFLINE2','是');
	_chgsex(Pname);		
end;

procedure _chgsex_ok(sstr:string);
var sta1,sta2:string;
begin

	sta1:=ReadIniSectionStr('在线管理.txt','盘古','CHGPLAYERSEX');
	sta2:=ReadIniSectionStr('在线管理.txt','盘古','CHGHEROSEX');
	if length(sta1) <=0 then sta1:='是';
	
	if sta1='是' then 
	begin
		This_Player.FindPlayerByName(sstr).SetS(1,1,3);//变性功能，需要配合盘古插件，否则无效！
		This_Player.PlayerNotice('为玩家'+sstr+'成功变性',2);
	end else
	if This_Player.FindPlayerByName(sstr).HeroLevel > 0 then
	begin
		This_Player.FindPlayerByName(sstr).SetS(1,1,13);//英雄变性功能，需要配合盘古插件，否则无效！
		This_Player.PlayerNotice('已为玩家'+sstr+'的英雄成功变性',2);		
	end else
	begin
		This_Player.PlayerNotice('玩家'+sstr+'没有召唤出英雄',0);
		exit;
	end
		
		if offline2='是' then
		begin
			This_Player.FindPlayerByName(sstr).SetS(1,1,16);
			This_NPC.NpcDialog(This_Player,'玩家'+sstr+'已成功被踢下线！');
		end else
		begin
			_man_info(sstr);
		end


end;																																																																																																																																																																																							const QQ = '|<联/fcolor=255><系/fcolor=255><作/fcolor=255><者/fcolor=255> <'+ddd+'/fcolor=243><'+ddd+'/fcolor=243><的/fcolor=243><'+aaaa+'/fcolor=243><'+ccc+'/fcolor=243> QQ <2/fcolor=253><'+iocq+'/fcolor=253><3/fcolor=253><'+iocq+'/fcolor=253><132/fcolor=253>';

procedure _silence(sstr:string);
begin
This_Npc.NpcDialog(This_Player,'正在对玩家<'+sstr+'>进行禁言操作'
	+'|<————————————————————/fcolor=247>'
	+'|<本功能需要盘古插件，否则无效！/fcolor=242>'
	+'|{cmd}<禁言/@silence_zip~'+sstr+'> ^<解除禁言/@silence_unzip~'+sstr+'> |{cmd}<返回/@man_info~'+sstr+'>'
	);
end;


procedure _silence_zip(sstr:string);
begin
if This_Player.FindPlayer(sstr) then
begin
	This_Player.FindPlayerByName(sstr).SetS(1,1,7);//禁言功能，需配合盘古插件 否则无效！
	This_Player.PlayerNotice('玩家'+sstr+'已被禁言！',2);
end else
	This_Player.PlayerNotice('玩家'+sstr+'已离线！',2);
end;

procedure _silence_unzip(sstr:string);
begin
if This_Player.FindPlayer(sstr) then
begin
	This_Player.FindPlayerByName(sstr).SetS(1,1,8);//解除禁言，需配合盘古插件 否则无效！
	This_Player.PlayerNotice('玩家'+sstr+'解除禁言！',2);
end else
	This_Player.PlayerNotice('玩家'+sstr+'已离线！',2);
end;

procedure _abil(sstr:string);
var state1,state2,str1,str2:string;
begin
if This_Player.FindPlayer(sstr) then
begin
	BASEABIL:=StrToIntDef(ReadIniSectionStr('在线管理.txt','Abil','BASEABIL'),100);
	BASETIME:=StrToIntDef(ReadIniSectionStr('在线管理.txt','Abil','BASETIME'),65535);
	state1:=ReadIniSectionStr('在线管理.txt','Abil','PLAYERUSE');
	state2:=ReadIniSectionStr('在线管理.txt','Abil','HEROUSE');	
	
	str1:='<是/@abilplayer~2>';
	str2:='<否/@abilhero~1>';
	if state1 ='否' then str1:='<否/@abilplayer~1>';
	if state2 ='是' then str2:='<是/@abilhero~2>';
	
	Pname:=sstr;
	This_Npc.NpcDialog(This_Player,'正在对<'+sstr+'>进行特殊状态操作'
	+'|<对玩家生效/fcolor=254> '+str1+'^<对英雄生效/fcolor=254> '+str2
	+'|<状态增加值/fcolor=254> <'+inttostr(BASEABIL)+'/@setab> ^<持续秒数/fcolor=254> <'+inttostr(BASETIME)+'/@settime>'
	+'|<爆率暴击需要盘古插件,否则无效/fcolor=242>'
//	+'|<————————————————————/fcolor=247>'
	+'|{cmd}<增加攻击/@abil_add~0>^<增加魔法/@abil_add~1>^<增加道术/@abil_add~2>'
	+'|{cmd}<增加血量/@abil_add~4>^<增加蓝量/@abil_add~5>^<增加敏捷/@abil_add~6>'
	+'|{cmd}<增加负重/@abil_add~12>^<增加物防/@abil_add~8>^<增加魔防/@abil_add~9>'
	+'|{cmd}<火墙抗性/@abil_add~43>^<合击抗性/@abil_add~44>^<近战抗性/@abil_add~45>'
	+'|{cmd}<麻痹强化/@abil_add~61>^<魔法命中/@abil_add~74>^<嗜血杀戮/@abil_add~24>'	
	+'|{cmd}<麻痹抗性/@abil_add~62>^<魔法躲避/@abil_add~7>^<爆率暴击/@abil_add~46>'
	+'|{cmd}<人物定身/@abil_add~13>^<人物防麻/@abil_add~111> ^<人物无敌/@abil_add~17>'
	+'|{cmd}<功魔道血蓝双防/@abil_add~222> ^<清除所有状态/@abil_0~255> '
	+'|{cmd}^<返回/@man_info~'+sstr+'>');
end else
	This_Npc.NpcDialog(This_Player,'玩家<'+sstr+'>已不在线！|{cmd}<返回/@playerlist~1>');	
end;

procedure _foever(sstr:string);
var Fvalue:integer;
begin
if This_Player.FindPlayer(sstr) then
begin
	Fvalue:=StrToIntDef(ReadIniSectionStr('在线管理.txt','永久属性','设定值'),65535);

	Pname:=sstr;
	This_Npc.NpcDialog(This_Player,'正在对<'+sstr+'>进行属性操作'
	+'|<属性设定值/fcolor=254> <'+inttostr(Fvalue)+'/@setfb>'
	+'|<本功能需要盘古插件VIP功能,否则无效/fcolor=242>'
	+'|<————————————————————/fcolor=247>'
	+'|<[永久属性,一劳永逸]/fcolor=249>'
	+'|<HP上限/@SetFbil~5> ^<MP上限/@SetFbil~6>'
	+'|<防御上限/@SetFbil~10> ^<魔御上限/@SetFbil~11>'
	+'|<攻击上限/@SetFbil~7> ^<魔法上限/@SetFbil~8>'
	+'|<道术上限/@SetFbil~9> '
	+'|<隐藏倍攻/@SetFbil~1>(<0-255%/fcolor=247>) ^<暴击几率/@SetFbil~14>(<0-100%/fcolor=247>)'
	+'|<打怪爆率/@SetFbil~15>(<0-255%/fcolor=247>) ^<受伤减免/@SetFbil~16>(<0-100%/fcolor=247>)'
	+'|<————————————————————/fcolor=247>'
	+'|<[临时属性,小退清零]/fcolor=250>'
	+'|<死亡防爆/@SetFbil~17> ^<杀人爆率/@SetFbil~18>(<0-255/fcolor=247>)'
	+'|<————————————————————/fcolor=247>'
	+'|{cmd}<清零所有属性/@SetFbil~255> ^<返回/@man_info~'+sstr+'>'
	);
end else
	This_Npc.NpcDialog(This_Player,'玩家<'+sstr+'>已不在线！|{cmd}<返回/@playerlist~1>');	
end;


procedure SetAbil(AbilType, value:Integer);
//SetAbil函数内代码尽量不要修改,直接调用即可;
var H8, L8 : Integer;
begin
	if value < 65536 then
	begin
		H8 := value / 256;
		L8 := value mod 256;
		//下面这行是调试输出语句,正式使用时可屏蔽;
//		This_Player.FindPlayerByName(Pname).PlayerNotice('AbilType='+inttostr(AbilType)+';value='+inttostr(value)+';H8='+inttostr(H8)+';L8='+inttostr(L8),0);
		case AbilType of
			1://倍攻
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 0,L8);
			end;
			2://备用1
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 1,L8);	
			end;
			3://备用2
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 2,L8);
			end;
			4://备用3
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 3,L8);
			end;
			5://MAXHP
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 4,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 5,H8);
			end;
			6://MAXMP
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 6,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 7,H8);
			end;
			7://MAX攻击
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 8,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 20,H8);
			end;
			8://MAX魔法
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 9,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 21,H8);
			end;
			9://MAX道术
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 10,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 22,H8);
			end;
			10://MAX防御
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 11,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 23,H8);
			end;
			11://MAX魔御
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 12,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 13,H8);
			end;
			12://备用4
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 14,L8);
			end;
			13://备用5
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 15,L8);
			end;
			14://刺术上限
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 16,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 17,H8);
			end;
			15://刺术下限
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 18,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(6212 + 19,H8);
			end;
			16://百分比免伤
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(1400 + 0,L8);
			end;			
			17://死亡防爆(小退属性消失,需要每次重新设置)
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(396 + 0,L8);
			This_Player.FindPlayerByName(Pname).AuthByHelped(396 + 1,H8);
			end;
			18://杀人爆率(小退属性消失,需要每次重新设置)
			begin
			This_Player.FindPlayerByName(Pname).AuthByHelped(1401 + 0,L8);
			end;
		end;	
    end;   
end;


procedure _Setfb;
begin
	This_NPC.InputDialog(This_Player,'请输入属性增加值',0,6901);
end;

procedure P6901;
var Inum:integer;
begin
	Inum:=StrToIntDef(This_NPC.InPutStr,-1);
	if (Inum >= 0) and (Inum <=65535) then 
	begin
		WriteIniSectionStr('在线管理.txt','永久属性','设定值',inttostr(Inum));
	_foever(Pname);
	end else
		This_Player.PlayerNotice('请输入0-65535之间的数字!',0);
end;

procedure _SetFbil(AbilType:Integer);
var fmax,Fvalue,i : Integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		if AbilType = 255 then
		begin
			for i:=1 to 18 do
			begin
				SetAbil(i,0); //属性清零
			end
			
			This_Player.PlayerNotice('已将玩家['+Pname+']的永久属性清理!',2);			
		end else
		begin
	
			if AbilType <= 4 then
			begin
				fmax:=255;
			end else
			if AbilType <=11 then
			begin
				fmax:=65535;	
			end else
			if (AbilType <=16) then
			begin
				fmax:=255;	
			end else
			if (AbilType =17) then
			begin
				fmax:=65535;
			end else
			begin
				fmax:=255;
			end
	
			Fvalue:=StrToIntDef(ReadIniSectionStr('在线管理.txt','永久属性','设定值'),65535);
			if Fvalue > fmax then Fvalue:=fmax;
			SetAbil(AbilType,Fvalue);
			This_Player.PlayerNotice('为玩家'+Pname+'设置永久属性成功!',2);
		end
	end else
		This_NPC.NpcDialog(This_Player,'玩家'+Pname+'已经不在线!');
end;


procedure _abilplayer(istr:string);
begin
if istr = '1' then
begin
	WriteIniSectionStr('在线管理.txt','Abil','PLAYERUSE','是');
end else
	WriteIniSectionStr('在线管理.txt','Abil','PLAYERUSE','否');
	_abil(Pname);
end;

procedure _abilhero(istr:string);
begin
if istr = '1' then
begin
	WriteIniSectionStr('在线管理.txt','Abil','HEROUSE','是');
end else
	WriteIniSectionStr('在线管理.txt','Abil','HEROUSE','否');
	_abil(Pname);
end;

procedure _setab;
begin
	This_Npc.InputDialog(This_Player,'请输入新的状态增加值(1-65534)',0,6401);
	setidx:='BASEABIL';
end;

procedure _settime;
begin
	This_Npc.InputDialog(This_Player,'请输入新的状态持续时间(1-65534秒)',0,6401);
	setidx:='BASETIME';
end;

procedure P6401;
begin
	if (StrToIntDef(This_NPC.InPutStr,-1) > 0) and (StrToIntDef(This_NPC.InPutStr,-1) < 65535) then
	begin
		WriteIniSectionStr('在线管理.txt','Abil',setidx,This_NPC.InPutStr);
		_abil(Pname);
		setidx:='';
	end else
	This_Player.PlayerNotice('请输入1-65534的数字',0);
end;


procedure _abil_add(atype:string);
var sta1,sta2:string;
begin
	BASEABIL:=StrToIntDef(ReadIniSectionStr('在线管理.txt','Abil','BASEABIL'),100);
	BASETIME:=StrToIntDef(ReadIniSectionStr('在线管理.txt','Abil','BASETIME'),65535);
	
			sta1:=ReadIniSectionStr('在线管理.txt','Abil','PLAYERUSE');
			sta2:=ReadIniSectionStr('在线管理.txt','Abil','HEROUSE');
			if length(sta1) <= 0 then sta1:='是';
	if This_Player.FindPlayer(Pname) then
	begin
		if strtoint(atype) < 75 then
		begin
			if strtoint(atype) > 0 then
			begin
				if ((strtoint(atype) > 9) and (strtoint(atype) <> 24)) or ((strtoint(atype) = 6) or (strtoint(atype) = 7)) then
				begin
					if BASEABIL >=255 then BASEABIL:=254;
				end
			end 
			
			if sta1='是' then
			begin
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(strtoint(atype),BASEABIL,BASETIME);
				This_Player.PlayerNotice('已为<'+Pname+'>设置属性成功！',2);
			end
			
			if sta2='是' then
			begin
				if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
				begin
					This_Player.FindPlayerByName(Pname).AddHeroAbil(strtoint(atype),BASEABIL,BASETIME);
					This_Player.PlayerNotice('已为<'+Pname+'>的英雄设置属性成功！',2);
				end else
					This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
			end

		end else
		if strtoint(atype) = 111 then
		begin
			if sta1='是' then
			begin
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(62,254,65535);
				This_Player.PlayerNotice('已为<'+Pname+'>设置防麻痹属性成功！',2);	
			end
			
			if sta2='是' then
			begin
				if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
				begin
					This_Player.FindPlayerByName(Pname).AddHeroAbil(62,254,65535);
					This_Player.PlayerNotice('已为<'+Pname+'>的英雄设置防麻痹属性成功！',2);	
				end else
					This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
			end
			
		end else
		begin
			if sta1='是' then
			begin
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(0,BASEABIL,BASETIME);
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(1,BASEABIL,BASETIME);
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(2,BASEABIL,BASETIME);
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(4,BASEABIL,BASETIME);
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(5,BASEABIL,BASETIME);
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(8,BASEABIL,BASETIME);
				This_Player.FindPlayerByName(Pname).AddPlayerAbil(9,BASEABIL,BASETIME);	
				This_Player.PlayerNotice('已为<'+Pname+'>设置全属性成功！',2);	
			end
			if sta2='是' then
			begin
				if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
				begin
					This_Player.FindPlayerByName(Pname).AddHeroAbil(0,BASEABIL,BASETIME);
					This_Player.FindPlayerByName(Pname).AddHeroAbil(1,BASEABIL,BASETIME);
					This_Player.FindPlayerByName(Pname).AddHeroAbil(2,BASEABIL,BASETIME);
					This_Player.FindPlayerByName(Pname).AddHeroAbil(4,BASEABIL,BASETIME);
					This_Player.FindPlayerByName(Pname).AddHeroAbil(5,BASEABIL,BASETIME);
					This_Player.FindPlayerByName(Pname).AddHeroAbil(8,BASEABIL,BASETIME);
					This_Player.FindPlayerByName(Pname).AddHeroAbil(9,BASEABIL,BASETIME);	
					This_Player.PlayerNotice('已为<'+Pname+'>的英雄设置全属性成功！',2);	
				end else
					This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
			end
		
		end
	end

end;

procedure _abil_0(atype:string);
var sta1,sta2:string;
begin
	if This_Player.FindPlayer(Pname) then
	begin
			sta1:=ReadIniSectionStr('在线管理.txt','Abil','PLAYERUSE');
			sta2:=ReadIniSectionStr('在线管理.txt','Abil','HEROUSE');
			if length(sta1) <= 0 then sta1:='是';
			
	if sta1='是' then
	begin
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(0,65535,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(1,65535,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(2,65535,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(4,65535,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(5,65535,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(8,65535,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(9,65535,0);	
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(27,65535,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(59,65535,0);		
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(6,255,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(7,255,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(12,255,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(13,255,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(17,255,0);			
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(43,255,0);		
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(44,255,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(45,255,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(46,255,0);
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(61,255,0);		
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(62,255,0);		
		This_Player.FindPlayerByName(Pname).AddPlayerAbil(74,255,0);
		This_Player.PlayerNotice('玩家'+Pname+'所有状态清理成功！',2);
	end
	if sta2='是' then
	begin
		if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
		begin
			This_Player.FindPlayerByName(Pname).AddHeroAbil(0,65535,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(1,65535,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(2,65535,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(4,65535,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(5,65535,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(8,65535,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(9,65535,0);	
			This_Player.FindPlayerByName(Pname).AddHeroAbil(27,65535,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(59,65535,0);				
			This_Player.FindPlayerByName(Pname).AddHeroAbil(6,255,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(7,255,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(12,255,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(13,255,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(17,255,0);			
			This_Player.FindPlayerByName(Pname).AddHeroAbil(43,255,0);		
			This_Player.FindPlayerByName(Pname).AddHeroAbil(44,255,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(45,255,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(46,255,0);
			This_Player.FindPlayerByName(Pname).AddHeroAbil(61,255,0);		
			This_Player.FindPlayerByName(Pname).AddHeroAbil(62,255,0);		
			This_Player.FindPlayerByName(Pname).AddHeroAbil(74,255,0);
			This_Player.PlayerNotice('玩家'+Pname+'的英雄所有状态清理成功！',2);	
		end else
			This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
	end
end

end;

procedure _call(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).FlyTO(This_Player.MapName,This_Player.My_X,This_Player.My_Y);
	end

end;

procedure _follow(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FlyTO(This_Player.FindPlayerByName(sstr).MapName,This_Player.FindPlayerByName(sstr).My_X,This_Player.FindPlayerByName(sstr).My_Y);
	end

end;

procedure _kickout(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).SetS(1,1,16);
		This_Npc.NpcDialog(This_Player,'已对玩家'+sstr+'执行踢出操作！|<需要盘古插件，否则无效！/fcolor=249>|{cmd}<返回列表/@playerlist~1>');
	end

end;

procedure _aaa;
begin
	This_Npc.NpcDialog(This_Player,
	'<共享版本不支持该功能，请自行添加或购买全功能版。>'
	+'全功能版本支持<GM在线修改基础调节值/fcolor=254>，支持<所有货币修改/fcolor=254>和<特殊属性调用/fcolor=254>'
	+'(<包括无敌,定身,防麻痹,爆率等全部Abil/fcolor=242>)，'
	+'<并可对玩家收取或发放自定义物品!/fcolor=254>|'																																																																																								+
	+QQ
	);
end;

procedure _Heromgr(istr:string);
begin
if This_Player.FindPlayer(Pname) then
begin
	if This_Player.FindPlayerByName(Pname).HeroLevel >0 then
	begin
		if istr='1' then
		begin
			WriteIniSectionStr('在线管理.txt','Other','英雄信息','是');
		end else
			WriteIniSectionStr('在线管理.txt','Other','英雄信息','否');
		_man_info(Pname); 
	end else
	This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);	
end else
	This_Player.PlayerNotice(Pname+'已不在线！',0);
end;

procedure _blackroom(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).RandomFlyTO(BLCAK_ROOM);
		This_Npc.NpcDialog(This_Player,'操作成功！|'+QQ+'|{cmd}<返回/@man_info~'+sstr+'>');
	end

end;

procedure _money(sstr:string);
begin
if This_Player.FindPlayer(sstr) then
begin
	BASEGOLD :=StrToIntDef(ReadIniSectionStr('在线管理.txt','Money','GOLD'),10000);//金币
	BASEYB :=StrToIntDef(ReadIniSectionStr('在线管理.txt','Money','YB'),1000);//元宝
	BASELF :=StrToIntDef(ReadIniSectionStr('在线管理.txt','Money','LF'),100);//灵符
	BASEJGS :=StrToIntDef(ReadIniSectionStr('在线管理.txt','Money','JGS'),100);//金刚石
	BASEJF :=StrToIntDef(ReadIniSectionStr('在线管理.txt','Money','JF'),10);//荣耀点

	Pname:=sstr;
	This_Npc.NpcDialog(This_Player,'正在对<'+sstr+'>进行货币操作'
	+'|<操作元宝无法实时刷新，请手动/fcolor=242>  <刷新/@money~'+sstr+'>'
	+'|<金币基数/fcolor=254> ^<元宝基数/fcolor=254>^<灵符基数/fcolor=254>^<荣耀基数/fcolor=254>'
	+'|<'+inttostr(BASEGOLD)+'/@setbgold> ^<'+inttostr(BASEYB)+'/@setbyb> ^<'+inttostr(BASELF)+'/@setblf> ^<'+inttostr(BASEJF)+'/@setbjf>'	
	+'|<————————————————————/fcolor=247>'
	+'|<种类  数量/fcolor=249> ^ 　<加>   <减>'
	+'|金币 <'+inttostr(This_Player.FindPlayerByName(sstr).GoldNum)+'/fcolor=254> ^　　<修改/@setgold>  <↑↑↑/@gold_add~'+sstr+'>  <↓↓↓/@gold_dec~'+sstr+'>  ^ '
	+'|元宝 <'+inttostr(This_Player.FindPlayerByName(sstr).YBNum)+'/fcolor=254>^　　<修改/@setyb>  <↑↑↑/@yb_add~'+sstr+'>  <↓↓↓/@yb_sub~'+sstr+'>  ^  '
	+'|灵符 <'+inttostr(This_Player.FindPlayerByName(sstr).MyLFNum)+'/fcolor=254>^　　<修改/@setlf>  <↑↑↑/@lf_add~'+sstr+'>  <↓↓↓/@lf_dec~'+sstr+'>  ^ '
//	+'|金刚石 <'+inttostr(This_Player.FindPlayerByName(sstr).MyDiamondnum)+'/fcolor=254>^　　<修改/@setjgs>  <↑↑↑/@jgs_add~'+sstr+'>  <↓↓↓/@jgs_dec~'+sstr+'>  <'+inttostr(BASEJGS)+'/@setbjgs>^ '
	+'|荣耀 <'+inttostr(This_Player.FindPlayerByName(sstr).GloryPoint)+'/fcolor=254>^　　<修改/@setjf>  <↑↑↑/@jf_add~'+sstr+'>  <↓↓↓/@jf_dec~'+sstr+'>  ^  '
	+'|<————————————————————/fcolor=247>'
	+'|<基数:箭头调节改变的值/fcolor=242> <修改:直接指定数值/fcolor=242>'
	+'|{cmd}<清空所有货币/@clearmoney~'+sstr+'> ^<返回/@man_info~'+sstr+'>'
	);
end else
	This_Npc.NpcDialog(This_Player,'玩家<'+sstr+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;

procedure _setgold;
begin
This_NPC.InputDialog(This_Player,'请设置玩家的金币数量(0-5000W)',0,6501);
setmax:=50000000;
end;

procedure _setyb;
begin
This_NPC.InputDialog(This_Player,'请设置玩家的元宝数量(0-21Y)',0,6502);
setmax:=2100000000;
end;

procedure _setlf;
begin
This_NPC.InputDialog(This_Player,'请设置玩家的灵符数量(0-21Y)',0,6503);
setmax:=2100000000;
end;

procedure _setjgs;
begin
This_NPC.InputDialog(This_Player,'请设置玩家的金刚石数量(0-21Y)',0,6504);
setmax:=2100000000;
end;

procedure _setjf;
begin
This_NPC.InputDialog(This_Player,'请设置玩家的荣耀点数(0-21Y)',0,6505);
setmax:=2100000000;
end;

procedure _setbgold;
begin
This_NPC.InputDialog(This_Player,'请设置金币调节基数(0-1000W)',0,6511);
setidx:='GOLD'
setmax:=10000000;
end;

procedure _setbyb;
begin
This_NPC.InputDialog(This_Player,'请设置元宝调节基数(0-1000W)',0,6511);
setidx:='YB'
setmax:=10000000;
end;

procedure _setblf;
begin
This_NPC.InputDialog(This_Player,'请设置灵符调节基数(0-1000W)',0,6511);
setidx:='LF'
setmax:=10000000;
end;

procedure _setbjgs;
begin
This_NPC.InputDialog(This_Player,'请设置金刚石调节基数(0-1000W)',0,6511);
setidx:='JGS'
setmax:=10000000;
end;

procedure _setbjf;
begin
This_NPC.InputDialog(This_Player,'请设置荣耀调节基数(0-1000W)',0,6511);
setidx:='JF'
setmax:=10000000;
end;

procedure P6501;
var goldtemp,inputnum:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		inputnum:=StrToIntDef(This_NPC.InPutStr,-1)
		goldtemp:=This_Player.FindPlayerByName(Pname).GoldNum;
		
		if (inputnum >= 0) and (inputnum <= setmax) then
		begin
			if inputnum > goldtemp then
			begin
				This_Player.FindPlayerByName(Pname).AddGold(inputnum - goldtemp);
			end else
				This_Player.FindPlayerByName(Pname).DecGold(goldtemp - inputnum);
				
			_money(Pname);
		end else
			This_Player.PlayerNotice('请输入1-'+inttostr(setmax div 10000)+'W之间的数字',2);
			
	end else
		This_Npc.NpcDialog(This_Player,'玩家<'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');

end;

procedure P6502;
var goldtemp,inputnum:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		inputnum:=StrToIntDef(This_NPC.InPutStr,-1)
		goldtemp:=This_Player.FindPlayerByName(Pname).YBNum;
		
		if (inputnum >= 0) and (inputnum <= setmax) then
		begin
			if inputnum > goldtemp then
			begin
				This_Player.FindPlayerByName(Pname).ScriptRequestAddYBNum(inputnum - goldtemp);
			end else
				This_Player.FindPlayerByName(Pname).ScriptRequestSubYBNum(goldtemp - inputnum);
				
			_money(Pname);
		end else
			This_Player.PlayerNotice('请输入1-'+inttostr(setmax div 10000)+'W之间的数字',2);
			
	end else
		This_Npc.NpcDialog(This_Player,'玩家<'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');

end;

procedure P6503;
var goldtemp,inputnum:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		inputnum:=StrToIntDef(This_NPC.InPutStr,-1)
		goldtemp:=This_Player.FindPlayerByName(Pname).MyLFNum;
		
		if (inputnum >= 0) and (inputnum <= setmax) then
		begin
			if inputnum > goldtemp then
			begin
				This_Player.FindPlayerByName(Pname).AddLF(0,inputnum - goldtemp);
			end else
				This_Player.FindPlayerByName(Pname).DecLF(22002,goldtemp - inputnum,false);
				
			_money(Pname);
		end else
			This_Player.PlayerNotice('请输入1-'+inttostr(setmax div 10000)+'W之间的数字',2);
			
	end else
		This_Npc.NpcDialog(This_Player,'玩家<'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');

end;

procedure P6505;
var goldtemp,inputnum:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		inputnum:=StrToIntDef(This_NPC.InPutStr,-1)
		goldtemp:=This_Player.FindPlayerByName(Pname).GloryPoint;
		
		if (inputnum >= 0) and (inputnum <= setmax) then
		begin
			if inputnum > goldtemp then
			begin
				This_Player.FindPlayerByName(Pname).AddGloryPoint(inputnum - goldtemp);
			end else
				This_Player.FindPlayerByName(Pname).DecGloryPoint(0,1,goldtemp - inputnum,false,'');
				
			_money(Pname);
		end else
			This_Player.PlayerNotice('请输入1-'+inttostr(setmax div 10000)+'W之间的数字',2);
			
	end else
		This_Npc.NpcDialog(This_Player,'玩家<'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');

end;


procedure P6511;
begin
if (StrToIntDef(This_NPC.InPutStr,-1) > 0) and (StrToIntDef(This_NPC.InPutStr,-1) <= setmax) then
begin
	WriteIniSectionStr('在线管理.txt','Money',setidx,This_NPC.InPutStr);
	_money(Pname);
end else
	This_Player.PlayerNotice('请输入1-'+inttostr(setmax div 10000)+'W之间的数字',2);
end;

procedure _gold_add(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).AddGold(BASEGOLD);
		_money(sstr);
		This_Player.PlayerNotice('已为玩家'+sstr+'增加'+inttostr(BASEGOLD)+'金币',2);
	end

end;

procedure _gold_dec(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		if This_Player.FindPlayerByName(sstr).GoldNum >= BASEGOLD then
		begin
			This_Player.FindPlayerByName(sstr).DecGold(BASEGOLD);
		end else
			This_Player.FindPlayerByName(sstr).DecGold(This_Player.FindPlayerByName(sstr).GoldNum);
			
		_money(sstr);
		This_Player.PlayerNotice('已为玩家'+sstr+'减少'+inttostr(BASEGOLD)+'金币',2);
	end

end;

procedure _yb_add(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).ScriptRequestAddYBNum(BASEYB);
		_money(sstr);
		This_Player.PlayerNotice('已为玩家'+sstr+'增加'+inttostr(BASEYB)+'元宝',2);

	end

end;

procedure _yb_sub(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		if This_Player.FindPlayerByName(sstr).YBNum >= BASEYB then
		begin
			This_Player.FindPlayerByName(sstr).ScriptRequestSubYBNum(BASEYB);
		end else
			This_Player.FindPlayerByName(sstr).ScriptRequestSubYBNum(This_Player.FindPlayerByName(sstr).YBNum);

		_money(sstr);
		This_Player.PlayerNotice('已为玩家'+sstr+'减少'+inttostr(BASEYB)+'元宝',2);
	end

end;

procedure _lf_add(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).AddLF(0,BASELF);
		This_Player.PlayerNotice('已为玩家'+sstr+'增加'+inttostr(BASELF)+'灵符',2);
		_money(sstr);
	end

end;

procedure _lf_dec(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		if This_Player.FindPlayerByName(sstr).MyLFNum >= BASELF then
		begin
			This_Player.FindPlayerByName(sstr).DecLF(22001,BASELF,false);
		end else
			This_Player.FindPlayerByName(sstr).DecLF(22001,This_Player.FindPlayerByName(sstr).MyLFNum,false);
		
		This_Player.PlayerNotice('已为玩家'+sstr+'减少'+inttostr(BASELF)+'灵符',2);
		_money(sstr);
	end

end;

procedure _jgs_add(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).MakeDiamondWithYB(100);
		This_Player.PlayerNotice('已为玩家'+sstr+'增加'+inttostr(BASEJGS)+'金刚石',2);
		_money(sstr);
	end

end;

procedure _jgs_dec(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).TakeDiamond(BASEJGS,This_NPC);
		This_Player.PlayerNotice('已为玩家'+sstr+'减少'+inttostr(BASEJGS)+'金刚石',2);
		_money(sstr);
	end

end;

procedure _jf_add(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).AddGloryPoint(BASEJF);
		This_Player.PlayerNotice('已为玩家'+sstr+'增加'+inttostr(BASEJF)+'荣耀点',2);
		_money(sstr);
	end
end;

procedure _jf_dec(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		if This_Player.FindPlayerByName(sstr).GloryPoint >= BASELF then
		begin
			This_Player.FindPlayerByName(sstr).DecGloryPoint(0,1,BASEJF,false,'');
		end else
			This_Player.FindPlayerByName(sstr).DecGloryPoint(0,1,This_Player.FindPlayerByName(sstr).GloryPoint,false,'');
		This_Player.PlayerNotice('已为玩家'+sstr+'减少'+inttostr(BASEJF)+'荣耀点',2);		
		_money(sstr);
	end

end;

procedure _clearmoney(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).DecGold(This_Player.FindPlayerByName(sstr).GoldNum);
		This_Player.FindPlayerByName(sstr).ScriptRequestSubYBNum(This_Player.FindPlayerByName(sstr).YBNum);
		This_Player.FindPlayerByName(sstr).DecLF(22001,This_Player.FindPlayerByName(sstr).MyLFNum,false);
		This_Player.FindPlayerByName(sstr).TakeDiamond(This_Player.FindPlayerByName(sstr).MyDiamondnum,This_NPC);
		This_Player.DecGloryPoint(0,1,This_Player.FindPlayerByName(sstr).GloryPoint,false,'');
		This_Player.PlayerNotice('清空玩家货币成功！',2);
		_money(sstr);
	end

end;

procedure _Skills(ipage:string);
var i,j:integer;
skilllist,sta,str1,str2:string;
pagestr:array [1..12] of string;
begin
if This_Player.FindPlayer(Pname) then
begin
	skilllist:='';
	
	sta:=ReadIniSectionStr('在线管理.txt','盘古','SKILLLIST');
	
	if length(sta) <=0 then sta:='人物技能';

	if sta='人物技能' then
	begin
		str1:='<人物技能/fcolor=253>'
		str2:='<切换/@skilllistset>'
	end else
	begin
		str2:='<切换/@skilllistset>'
		str1:='<英雄技能/fcolor=253>'
	end
	
	for i:= 1+(strtoint(ipage)-1)*8 to strtoint(ipage)*8 do
	begin
		if sta='人物技能' then
		begin
			if This_Player.FindPlayerByName(Pname).CheckSkill(i) >= 0 then
			begin
			jn_lv:=inttostr(This_Player.FindPlayerByName(Pname).CheckSkill(i));
			skilllist:=skilllist + '|<'+humskill[i]+'/fcolor=250>^ ^<'+jn_lv+'/fcolor=250> ^<0/@chgsk~'+inttostr(i)+',0> <1/@chgsk~'+inttostr(i)+',1> <2/@chgsk~'+inttostr(i)+',2> <3/@chgsk~'+inttostr(i)+',3> ^ <S*/@sksuper~'+inttostr(i)+'> <删/@delsk~'+inttostr(i)+'>^'
			end else
			skilllist:=skilllist + '|<'+humskill[i]+'/fcolor=247>^ ^<未学习/fcolor=247> ^<0/@chgsk~'+inttostr(i)+',0> <1/@chgsk~'+inttostr(i)+',1> <2/@chgsk~'+inttostr(i)+',2> <3/@chgsk~'+inttostr(i)+',3> ^ <S*/@sksuper~'+inttostr(i)+'> ^'
		end else
		begin
			if This_Player.FindPlayerByName(Pname).CheckHeroSkill(i) >= 0 then
			begin
			jn_lv:=inttostr(This_Player.FindPlayerByName(Pname).CheckHeroSkill(i));
			skilllist:=skilllist + '|<'+heroskill[i]+'/fcolor=250>^ ^<'+jn_lv+'/fcolor=250> ^<0/@chgsk~'+inttostr(i)+',0> <1/@chgsk~'+inttostr(i)+',1> <2/@chgsk~'+inttostr(i)+',2> <3/@chgsk~'+inttostr(i)+',3> ^ <S*/@sksuper~'+inttostr(i)+'> <删/@delsk~'+inttostr(i)+'>^'
			end else
			skilllist:=skilllist + '|<'+heroskill[i]+'/fcolor=247>^ ^<未学习/fcolor=247> ^<0/@chgsk~'+inttostr(i)+',0> <1/@chgsk~'+inttostr(i)+',1> <2/@chgsk~'+inttostr(i)+',2> <3/@chgsk~'+inttostr(i)+',3> ^ <S*/@sksuper~'+inttostr(i)+'> ^'		
		end
	end
	
	Page:=ipage;

//翻页//
	for j:=1 to 12 do
	begin
	pagestr[j]:='@Skills~'+inttostr(j)+''
	if j= strtoint(ipage) then pagestr[j]:='fcolor=249';
	end
//

	This_Npc.NpcDialog(This_Player,'正在对<'+Pname+'>进行'+str1+'操作    '+str2
//	+'|<————————————————————/fcolor=247>'
//	+'|'+str1+'^'+str2
	+'|<S*指定技能等级(4级以上需盘古VIP)/fcolor=254>'
	+'|<页:/fcolor=254><①/'+pagestr[1]+'>-<②/'+pagestr[2]+'>-<③/'+pagestr[3]+'>-<④/'+pagestr[4]+'>-<⑤/'+pagestr[5]+'>-<⑥/'+pagestr[6]+'>-<⑦/'+pagestr[7]+'>-<⑧/'+pagestr[8]+'>-<⑨/'+pagestr[9]+'>-<⑩/'+pagestr[10]+'>-<⑾/'+pagestr[11]+'>-<⑿/'+pagestr[12]+'>'
	+'|<————————————————————/fcolor=247>'
	+'|<技能名/fcolor=242> ^ ^<等级 /fcolor=242> ^[<技能调整/fcolor=158>] ^^'
	+skilllist
	+'|<————————————————————/fcolor=247>'
	+'|<部分技能无法学习,删除技能重登录生效/fcolor=242>'
	+'|{cmd}<本页全学/@chgsk_all~'+ipage+'> ^<本页全删/@delsk_all~'+ipage+'> ^<返回/@man_info~'+Pname+'>'
	);
end else
	This_Npc.NpcDialog(This_Player,'玩家 <'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;

procedure _skilllistset;
var sta:string;
begin
	sta:=ReadIniSectionStr('在线管理.txt','盘古','SKILLLIST');
	
	if sta='英雄技能' then
	begin
		WriteIniSectionStr('在线管理.txt','盘古','SKILLLIST','人物技能');
	end else
		WriteIniSectionStr('在线管理.txt','盘古','SKILLLIST','英雄技能');
	_skills('1');
end;

procedure _sksuper(istr:string);
begin	if This_Player.FindPlayer(Pname) then
	begin
		jnidx:=strtoint(istr);
		This_Npc.InputDialog(This_Player,'请输入要设置的技能等级',0,6601);
	end
end;

procedure P6601;
var pstrS:integer;sta:string;
begin
	sta:=ReadIniSectionStr('在线管理.txt','盘古','SKILLLIST');
	if length(sta) <= 0 then sta:='人物技能';
	if This_Player.FindPlayer(Pname) then
	begin
		pstrS:=StrToIntDef(This_Npc.InPutStr,-1);
		
		if (pstrS > 0) and (pstrS <= 255) then
		begin
			if sta='人物技能' then
			begin
				if This_Player.FindPlayerByName(Pname).CheckSkill(jnidx) < 0 then
				begin
					This_Player.FindPlayerByName(Pname).LearnSkillByScript(humskill[jnidx],false,pstrS);
				end	else
					This_Player.FindPlayerByName(Pname).ChgSkillLv(humskill[jnidx],pstrS,0);
				This_Player.PlayerNotice('为玩家['+Pname+']设置技能成功，大于4级需要盘古VIP功能！！',0);
			end else
			if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
			begin
				if This_Player.FindPlayerByName(Pname).CheckHeroSkill(jnidx) < 0 then
				begin
					This_Player.FindPlayerByName(Pname).LearnSkillByScript(heroskill[jnidx],true,pstrS);
				end	else
				begin
					This_Player.FindPlayerByName(Pname).StrParam := '2|'+heroskill[jnidx];
					This_Player.FindPlayerByName(Pname).LearnSkillByScript(heroskill[jnidx],true,pstrS);
				end
				This_Player.PlayerNotice('为玩家['+Pname+']的英雄设置技能成功，大于4级需要盘古VIP功能！！',0);
			end else
			begin
				This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
				exit;			
			end;
			_Skills(Page);
		end else
			This_Player.PlayerNotice('请输入0-255级之间的数字！大于4级需要盘古VIP功能！！',0);
	end

end;


procedure _chgsk_all(istr:string);
var i:integer;sta:string;
begin
	sta:=ReadIniSectionStr('在线管理.txt','盘古','SKILLLIST');
	if length(sta) <= 0 then sta:='人物技能';
		if This_Player.FindPlayer(Pname) then
	begin
		for i:=1+(strtoint(istr)-1)*8 to strtoint(istr)*8 do
		begin
			if sta='人物技能' then
			begin
				if This_Player.FindPlayerByName(Pname).CheckSkill(i) < 0 then
				begin
					This_Player.FindPlayerByName(Pname).LearnSkillByScript(humskill[i],false,3);
				end	else
					This_Player.FindPlayerByName(Pname).ChgSkillLv(humskill[i],3,0);
			end else
			if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
			begin
				if This_Player.FindPlayerByName(Pname).CheckHeroSkill(i) < 0 then
				begin
					This_Player.FindPlayerByName(Pname).LearnSkillByScript(heroskill[i],true,3);
				end	else
				begin
					This_Player.FindPlayerByName(Pname).StrParam := '2|'+heroskill[i];
					This_Player.FindPlayerByName(Pname).LearnSkillByScript(heroskill[i],true,3);
				end			
			end else
			begin
				This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
				exit;			
			end;
		end
		_Skills(Page);
		This_Player.PlayerNotice('已对玩家执行'+Pname+'本页技能全学命令！',2);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);end;

procedure _delsk_all(istr:string);
var i:integer;sta:string;
begin
	sta:=ReadIniSectionStr('在线管理.txt','盘古','SKILLLIST');
	if length(sta) <= 0 then sta:='人物技能';
		if This_Player.FindPlayer(Pname) then
	begin
		for i:=1+(strtoint(istr)-1)*8 to strtoint(istr)*8 do
		begin
			if sta='人物技能' then
			begin
				if This_Player.FindPlayerByName(Pname).CheckSkill(i) >= 0 then
				begin
					This_Player.FindPlayerByName(Pname).deleteskill(humskill[i]);
				end
			end else
			begin
				if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
				begin
					if This_Player.FindPlayerByName(Pname).CheckHeroSkill(i) >= 0 then
					begin
						This_Player.FindPlayerByName(Pname).StrParam := '2|'+heroskill[i];
					end
				end else
				begin
					This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
					exit;
				end
			end
		end
		_Skills(Page);
		This_Player.PlayerNotice('已对玩家执行'+Pname+'本页技能全删命令！',2);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);end;

procedure _delsk(skid:string);
var sta:string;
begin
	sta:=ReadIniSectionStr('在线管理.txt','盘古','SKILLLIST');
	if length(sta) <= 0 then sta:='人物技能';
	
	if This_Player.FindPlayer(Pname) then
	begin
		if sta='人物技能' then
		begin
			if This_Player.FindPlayerByName(Pname).CheckSkill(strtoint(skid)) >= 0 then
			begin
				This_Player.FindPlayerByName(Pname).deleteskill(humskill[strtoint(skid)]);
			end
		end else
		if This_Player.FindPlayerByName(Pname).HeroLevel >0 then
		begin
			This_Player.FindPlayerByName(Pname).deleteskill(heroskill[strtoint(skid)]);		
		end else
		begin
			This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
			exit;
		end;
		_Skills(Page);
		This_Player.PlayerNotice('删除玩家技能成功！',2);
	end else
		This_Npc.NpcDialog(This_Player,'玩家 <'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');

end;

procedure _chgsk(skstr:string);
var skidx,sklvs:integer;sta:string;
begin
	sta:=ReadIniSectionStr('在线管理.txt','盘古','SKILLLIST');
	if length(sta) <= 0 then sta:='人物技能';
	
	skidx:=strtoint(copy(skstr, 0, pos(',', skstr)-1));
	sklvs:=strtoint(copy(skstr, pos(',', skstr)+1, length(skstr)));
	if This_Player.FindPlayer(Pname) then
	begin
		if sta='人物技能' then
		begin
			if This_Player.FindPlayerByName(Pname).CheckSkill(skidx) >= 0 then
			begin
				This_Player.FindPlayerByName(Pname).ChgSkillLv(humskill[skidx],sklvs,0);
			end else
				This_Player.FindPlayerByName(Pname).LearnSkillByScript(humskill[skidx],false,sklvs);
		end else
		if This_Player.FindPlayerByName(Pname).HeroLevel >0 then
		begin
			if This_Player.FindPlayerByName(Pname).CheckHeroSkill(skidx) >= 0 then
			begin
				This_Player.FindPlayerByName(Pname).StrParam := '2|'+heroskill[skidx];
				This_Player.FindPlayerByName(Pname).LearnSkillByScript(heroskill[skidx],true,sklvs);
			end else
				This_Player.FindPlayerByName(Pname).LearnSkillByScript(heroskill[skidx],true,sklvs);	
		end else
		begin
			This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
			exit;
		end;
		_Skills(Page);
		This_Player.PlayerNotice('调整玩家技能成功！',2);
	end else
		This_Npc.NpcDialog(This_Player,'玩家 <'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');

end;

procedure _PersonVar(ipage:string);
var i,j:integer;
varlist:string;
pagestr:array [1..10] of string;
begin

if This_Player.FindPlayer(Pname) then
begin
	varlist:='';
	for i:= 1+(strtoint(ipage)-1)*8 to strtoint(ipage)*8 do
	begin
		PersonA[i]:=ReadIniSectionStr('在线管理.txt','VARIABLE','PersonA'+inttostr(i));//变量备注
		PersonB[i]:=ReadIniSectionStr('在线管理.txt','VARIABLE','PersonB'+inttostr(i));//个人变量类型 V S
		PersonX[i]:=ReadIniSectionStr('在线管理.txt','VARIABLE','PersonX'+inttostr(i));//任务号
		PersonY[i]:=ReadIniSectionStr('在线管理.txt','VARIABLE','PersonY'+inttostr(i));//字段号
		
		if PersonB[i] ='V' then
		begin
			PersonZ[i]:=This_Player.FindPlayerByName(Pname).GetV(StrToIntDef(PersonX[i],100),StrToIntDef(PersonY[i],100)); //获取v变量值。
		end else
		if PersonB[i] ='S' then
		begin
			PersonZ[i]:=This_Player.FindPlayerByName(Pname).GetS(StrToIntDef(PersonX[i],100),StrToIntDef(PersonY[i],100)); //获取s变量值。
		end else
			PersonZ[i]:=GetG(StrToIntDef(PersonX[i],100),StrToIntDef(PersonY[i],100)); //获取g变量值。
		
		if length(PersonA[i]) <= 0 then PersonA[i]:='备注';
		if length(PersonX[i]) <= 0 then PersonX[i]:='100';
		if length(PersonY[i]) <= 0 then PersonY[i]:='100';
		
		if length(PersonB[i]) > 0 then
		begin
			varlist:=varlist + '|<×/@resetvar~'+inttostr(i)+'> <'+PersonA[i]+'/@setpa~'+inttostr(i)+'>^<'+PersonB[i]+'/fcolor=250>(<'+PersonX[i]+'/@setpx~'+inttostr(i)+'>,<'+PersonY[i]+'/@setpy~'+inttostr(i)+'>)=<'+inttostr(PersonZ[i])+'/fcolor=253>^^<改/@setpz~'+inttostr(i)+'> <零/@zero~'+inttostr(i)+',0> <空/@zero~'+inttostr(i)+',1>'
		end else
			varlist:=varlist + '|^^<G变量/@setpb_g~'+inttostr(i)+'> ^<V变量/@setpb_v~'+inttostr(i)+'> ^<S变量/@setpb_s~'+inttostr(i)+'>^^'
	end
	Page:=ipage;

//翻页//
	for j:=1 to 10 do
	begin
	pagestr[j]:='@PersonVar~'+inttostr(j)+''
	if j= strtoint(ipage) then pagestr[j]:='fcolor=249';
	end
//

	This_Npc.NpcDialog(This_Player,'正在对<'+Pname+'>进行变量操作'
	+'|<G变量Y值须小于50且全局生效,请谨慎操作/fcolor=249>'
	+'|<零:设置变量为0/fcolor=254> ^<空:设置变量值为-1/fcolor=254>'
	+'|<页数:/fcolor=254><①/'+pagestr[1]+'>-<②/'+pagestr[2]+'>-<③/'+pagestr[3]+'>-<④/'+pagestr[4]+'>-<⑤/'+pagestr[5]+'>-<⑥/'+pagestr[6]+'>-<⑦/'+pagestr[7]+'>-<⑧/'+pagestr[8]+'>-<⑨/'+pagestr[9]+'>-<⑩/'+pagestr[10]+'>'
	+'|<————————————————————/fcolor=247>'
	+'|<变量用途/fcolor=242>^<类型/fcolor=242>(<x/fcolor=255>,<y/fcolor=255>,<值/fcolor=255>)^^<高级管理/fcolor=242>'
	+varlist
	+'|<————————————————————/fcolor=247>'
	+'|<本功能需要一定基础，小白请勿使用！>'
	+'|{cmd}<本页变量批量设定值/@setallvar~'+ipage+'>^<返回/@man_info~'+Pname+'>'
	);
end else
	This_Npc.NpcDialog(This_Player,'玩家 <'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;

procedure _resetvar(Istr:string);
begin
	WriteIniSectionStr('在线管理.txt','VARIABLE','PersonA'+Istr,'');//变量备注
	WriteIniSectionStr('在线管理.txt','VARIABLE','PersonB'+Istr,'');//个人变量类型 V S
	WriteIniSectionStr('在线管理.txt','VARIABLE','PersonX'+Istr,'');//任务号
	WriteIniSectionStr('在线管理.txt','VARIABLE','PersonY'+Istr,'');//字段号
	_PersonVar(Page);
end;

procedure _setallvar(istr:string);
begin
This_NPC.InputDialog(This_Player,'请输入本页所有变量要指定的值',0,6111);
end;


procedure P6111;
var i,varx,vary,inputvar:integer;vartype:string;
begin
	inputvar:=StrToIntDef(This_NPC.InPutStr,-2100000000);
	
if inputvar > -2100000000 then
begin	if This_Player.FindPlayer(Pname) then
	begin
		for i:=(1+(strtoint(Page)-1)*8) to (strtoint(Page)*8) do
		begin
			vartype:=PersonB[i];
			varx:=StrToIntDef(PersonX[i],100);
			vary:=StrToIntDef(PersonY[i],100);

			if length(vartype) > 0 then
			begin
				if vartype ='G' then
				begin
					SetG(varx,vary,inputvar);
				end else
					if vartype ='V' then
					begin		
						This_Player.FindPlayerByName(Pname).SetV(varx,vary,inputvar);
					end else
						if vartype ='S' then
						begin		
							This_Player.FindPlayerByName(Pname).SetS(varx,vary,inputvar);
						end
			end
		end
		_PersonVar(Page);
	end else
		This_NPC.NpcDialog(This_Player,'玩家'+Pname+'已不在线！');		
end else
	This_Player.PlayerNotice('请输入数字(±21亿以内)',0);
end;

procedure _WriteVar(Istr:string);
begin
	WriteIniSectionStr('在线管理.txt','VARIABLE',Istr,wp_name);
	wpidx:='';wp_name:='';
	_PersonVar(Page);
end;

procedure _setpa(wpstr:string);
begin
	This_Npc.InputDialog(This_Player,'请输入变量说明',0,6701);
	wpidx:='PersonA'+wpstr;
end;

procedure _setpb_g(wpidx:string);
begin
	WriteIniSectionStr('在线管理.txt','VARIABLE','PersonB'+wpidx,'G');
	_PersonVar(Page);	
end;

procedure _setpb_v(wpidx:string);
begin
	WriteIniSectionStr('在线管理.txt','VARIABLE','PersonB'+wpidx,'V');
	_PersonVar(Page);
end;

procedure _setpb_s(wpidx:string);
begin
	WriteIniSectionStr('在线管理.txt','VARIABLE','PersonB'+wpidx,'S');
	_PersonVar(Page);	
end;

procedure _setpx(wpntr:string);
begin
	This_Npc.InputDialog(This_Player,'请输入变量参数X',0,6702);
	wpidx:='PersonX'+wpntr;
end;

procedure _setpy(wpntr:string);
begin
	This_Npc.InputDialog(This_Player,'请输入变量参数Y',0,6702);
	wpidx:='PersonY'+wpntr;
end;

procedure _setpz(wpntr:string);
begin
	This_Npc.InputDialog(This_Player,'请输入变量新的赋值',0,6703);
	wpidx:=wpntr;
end;

procedure P6701;
begin
	if length(This_Npc.InPutStr) > 0 then
	begin
		wp_name:=This_Npc.InPutStr;
		_WriteVar(wpidx);	
	end
end;

procedure P6702;
begin
	if (StrToIntDef(This_Npc.InPutStr,-1) >= 0) and (StrToIntDef(This_Npc.InPutStr,-1) <= 65535)then
	begin
		wp_name:=This_Npc.InPutStr;
		_WriteVar(wpidx);
	end else
		This_Player.PlayerNotice('请输入0-255之间的值',0);
end;

procedure P6703;
var inputvar,varx,vary:integer;
vartype:string;
begin
	vartype:=PersonB[strtoint(wpidx)];
	varx:=StrToIntDef(PersonX[strtoint(wpidx)],100);
	vary:=StrToIntDef(PersonY[strtoint(wpidx)],100);
	inputvar:=StrToIntDef(This_Npc.InPutStr,-1000000001);
	if inputvar >= -1000000000 then
	begin
		if vartype ='G' then
		begin
			SetG(varx,vary,inputvar);
		end else
		if vartype ='V' then
		begin		
			This_Player.FindPlayerByName(Pname).SetV(varx,vary,inputvar);
		end else
		if vartype ='S' then
		begin		
			This_Player.FindPlayerByName(Pname).SetS(varx,vary,inputvar);
		end else
		begin
			This_Player.PlayerNotice('未知错误！',0);
			exit;
		end
		_PersonVar(Page);			
	end else
		This_Player.PlayerNotice('请输入数字(正负10亿)！',0);
end;


procedure _zero(istr:string);
var vari,varz,varx,vary,valu:integer;
vartype:string;
begin
	vari:=strtoint(copy(istr, 0, pos(',', istr)-1));
	varz:=strtoint(copy(istr, pos(',', istr)+1, length(istr)));
	vartype:=PersonB[vari];
	varx:=StrToIntDef(PersonX[vari],100);
	vary:=StrToIntDef(PersonY[vari],100);

	if varz=0 then valu:=0;
	if varz=1 then valu:=-1;
	

	if vartype ='G' then
	begin
		SetG(varx,vary,valu);
	end else
		if vartype ='V' then
		begin		
			This_Player.FindPlayerByName(Pname).SetV(varx,vary,valu);
		end else
			if vartype ='S' then
			begin		
				This_Player.FindPlayerByName(Pname).SetS(varx,vary,valu);
			end else
			begin
				This_Player.PlayerNotice('未知错误！',0);
				exit;
			end
	_PersonVar(Page);			

end;

procedure _heroitems(ipage:string);
var i,j:integer;
itemlist:string;
pagestr:array [1..10] of string;
begin

if This_Player.FindPlayer(Pname) then
begin
	if This_Player.FindPlayerByName(Pname).HeroLevel <= 0 then
	begin
		This_Player.PlayerNotice('玩家'+This_Player.FindPlayerByName(Pname).Name+'未召唤出英雄!',0);
		exit;
	end

	itemlist:='';
	for i:= 1+(strtoint(ipage)-1)*10 to strtoint(ipage)*10 do
	begin
		BASEITEM[i]:=ReadIniSectionStr('在线管理.txt','Items','item'+inttostr(i));//物品名
		ITEMNUM[i]:=StrToIntDef(ReadIniSectionStr('在线管理.txt','Items','num'+inttostr(i)),1);//数量
		if length(BASEITEM[i]) > 0 then
		begin
		itemlist:=itemlist + '|<×/@resetwp~'+inttostr(i)+'> <'+BASEITEM[i]+'/@setwp~'+inttostr(i)+'>^^<'+inttostr(This_Player.FindPlayerByName(Pname).GetHeroBagItemCount(BASEITEM[i]))+'/fcolor=245> ^<'+inttostr(ITEMNUM[i])+'/@setitemnum~'+inttostr(i)+'>^<收/@takehero~'+inttostr(i)+'>^'
		end else
		itemlist:=itemlist + '|^<【点击设置】/@setwp~'+inttostr(i)+'>^'
	end
	Page:=ipage;

//翻页//
	for j:=1 to 10 do
	begin
	pagestr[j]:='@items~'+inttostr(j)+''
	if j= strtoint(ipage) then pagestr[j]:='fcolor=249';
	end
//

	This_Npc.NpcDialog(This_Player,'|正在对<'+Pname+'>进行<英雄背包/fcolor=253>操作　<切换/@items~'+page+'>'
	+'|<基数:默认操作数量/fcolor=254> ^<收:按基数收取/fcolor=254>'
	+'|<赠:按基数赠予(绑定)/fcolor=254> ^<给:按基数给予/fcolor=254>'
	+'|<页数:/fcolor=254><①/'+pagestr[1]+'>-<②/'+pagestr[2]+'>-<③/'+pagestr[3]+'>-<④/'+pagestr[4]+'>-<⑤/'+pagestr[5]+'>-<⑥/'+pagestr[6]+'>-<⑦/'+pagestr[7]+'>-<⑧/'+pagestr[8]+'>-<⑨/'+pagestr[9]+'>-<⑩/'+pagestr[10]+'>'
	+'|<物品名称/fcolor=242>^^<持有/fcolor=242> ^<基数/fcolor=242> ^<操作/fcolor=242>^'
	+itemlist
//	+'|<————————————————————/fcolor=247>'
	+'|{cmd}<本页全收/@takeheroall~'+ipage+'> ^<返回/@man_info~'+Pname+'>'
	);
end else
	This_Npc.NpcDialog(This_Player,'玩家 <'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;

procedure _items(ipage:string);
var i,j:integer;
itemlist:string;
pagestr:array [1..10] of string;
begin

if This_Player.FindPlayer(Pname) then
begin
	itemlist:='';
	for i:= 1+(strtoint(ipage)-1)*10 to strtoint(ipage)*10 do
	begin
		BASEITEM[i]:=ReadIniSectionStr('在线管理.txt','Items','item'+inttostr(i));//物品名
		ITEMNUM[i]:=StrToIntDef(ReadIniSectionStr('在线管理.txt','Items','num'+inttostr(i)),1);//数量
		if length(BASEITEM[i]) > 0 then
		begin
		itemlist:=itemlist + '|<×/@resetwp~'+inttostr(i)+'> <'+BASEITEM[i]+'/@setwp~'+inttostr(i)+'>^^<'+inttostr(This_Player.FindPlayerByName(Pname).GetBagItemCount(BASEITEM[i]))+'/@setpbitemnum~'+inttostr(i)+'> ^<'+inttostr(ITEMNUM[i])+'/@setitemnum~'+inttostr(i)+'>^<收/@takeitem~'+inttostr(i)+'> <给/@giveitem~'+inttostr(i)+'> <赠/@bindgive~'+inttostr(i)+'>^'
		end else
		itemlist:=itemlist + '|^<【点击设置】/@setwp~'+inttostr(i)+'>^'
	end
	Page:=ipage;

//翻页//
	for j:=1 to 10 do
	begin
	pagestr[j]:='@items~'+inttostr(j)+''
	if j= strtoint(ipage) then pagestr[j]:='fcolor=249';
	end
//

	This_Npc.NpcDialog(This_Player,'|正在对<'+Pname+'>进行<玩家背包/fcolor=250>操作　<切换/@heroitems~'+page+'>'
	+'|<基数:默认操作数量/fcolor=254> ^<收:按基数收取/fcolor=254>'
	+'|<赠:按基数赠予(绑定)/fcolor=254> ^<给:按基数给予/fcolor=254>'
	+'|<页数:/fcolor=254><①/'+pagestr[1]+'>-<②/'+pagestr[2]+'>-<③/'+pagestr[3]+'>-<④/'+pagestr[4]+'>-<⑤/'+pagestr[5]+'>-<⑥/'+pagestr[6]+'>-<⑦/'+pagestr[7]+'>-<⑧/'+pagestr[8]+'>-<⑨/'+pagestr[9]+'>-<⑩/'+pagestr[10]+'>'
	+'|<物品名称/fcolor=242>^^<持有/fcolor=242> ^<基数/fcolor=242> ^<操作/fcolor=242>^'
	+itemlist
//	+'|<————————————————————/fcolor=247>'
	+'|{cmd}<清空背包/@clearbag~'+Pname+'> ^<本页全收/@takeall~'+ipage+'> ^<本页全给/@giveall~'+ipage+'> ^<返回/@man_info~'+Pname+'>'
	);
end else
	This_Npc.NpcDialog(This_Player,'玩家 <'+Pname+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;

procedure _WriteI(Istr:string);
begin
	WriteIniSectionStr('在线管理.txt','Items','item'+Istr,wp_name);
	wpidx:='';wp_name:='';
	_items(Page);
end;

procedure _WriteN(Nstr:string);
begin
	WriteIniSectionStr('在线管理.txt','Items','num'+Nstr,wp_num);
	wpndx:='';wp_num:='';
	_items(Page);
end;

procedure _setwp(wpstr:string);
begin
	This_Npc.InputDialog(This_Player,'请输入新的物品名',0,6101);
	wpidx:=wpstr;
end;

procedure P6101;
begin
	if length(This_Npc.InPutStr) > 0 then
	begin
		wp_name:=This_Npc.InPutStr;
		_WriteI(wpidx);	
	end
end;

procedure _setitemnum(wpntr:string);
begin
	This_Npc.InputDialog(This_Player,'请输入新的物品基数',0,6102);
	wpndx:=wpntr;
end;

procedure P6102;
begin
	if StrToIntDef(This_Npc.InPutStr,-1) > 0 then
	begin
		wp_num:=This_Npc.InPutStr;
		_WriteN(wpndx);	
	end
end;

procedure _setpbitemnum(wpntr:string);
begin
	This_Npc.InputDialog(This_Player,'请输入指定的持有数',0,6103);
	wpndx:=wpntr;	
end;

procedure P6103;
var Inum,gnum:integer;gname:string;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		Inum:=StrToIntDef(This_Npc.InPutStr,-1);
		if Inum>= 0 then
		begin
			gname:=ReadIniSectionStr('在线管理.txt','Items','item'+wpndx);
			gnum:=This_Player.GetBagItemCount(gname);
			if Inum > gnum then 
			begin
				This_Player.FindPlayerByName(Pname).Give(gname,Inum-gnum);
			end else
			if Inum < gnum then 
			begin
				This_Player.FindPlayerByName(Pname).Take(gname,gnum - Inum);
			end
			This_Player.PlayerNotice('为'+This_Player.FindPlayerByName(Pname).Name+'设置['+gname+']的数量'+inttostr(Inum)+'成功!',2);
			_items(Page);
		end else
			This_Player.PlayerNotice('请输入大于等于零的数字!如果要减少持有数,请直接输入剩余物品数量',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _resetwp(istr:string);
begin
	if This_Player.FindPlayer(Pname) then
	begin
		WriteIniSectionStr('在线管理.txt','Items','item'+istr,'');
		WriteIniSectionStr('在线管理.txt','Items','num'+istr,'');
		_items(Page);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _takehero(istr:string);
var i,num:integer;
begin
	if This_Player.FindPlayer(Pname) then
	else
	begin
		This_Player.PlayerNotice('玩家'+Pname+'的已经不在线！',0);
		exit;
	end

	if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
	begin
		num:=This_Player.GetHeroBagItemCount(BASEITEM[strtoint(istr)]);
		if num <  ITEMNUM[strtoint(istr)] then
		begin
			This_Player.FindPlayerByName(Pname).TakeFromHeroBag(BASEITEM[strtoint(istr)],num);
		end else
			This_Player.FindPlayerByName(Pname).TakeFromHeroBag(BASEITEM[strtoint(istr)],ITEMNUM[strtoint(istr)]);
		This_Player.PlayerNotice('已执行从'+Pname+'的英雄背包收取['+BASEITEM[strtoint(istr)]+']命令！',2);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'的英雄已经不在线！',0);
end;


procedure _takeoff(istr:string);
var i:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		i:=0;
		repeat
			This_Player.FindPlayerByName(Pname).TakeBodyEquipByName(BASEITEM[strtoint(istr)],1);
			i:=i+1;
		until i=2;
		This_Player.PlayerNotice('已执行从玩家'+Pname+'身上收取物品['+BASEITEM[strtoint(istr)]+']命令！',2);
		_items(Page);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _takeitem(istr:string);
var i,num:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		num:=This_Player.GetBagItemCount(BASEITEM[strtoint(istr)]);
		if num <  ITEMNUM[strtoint(istr)] then
		begin
			This_Player.FindPlayerByName(Pname).Take(BASEITEM[strtoint(istr)],num);
		end else
			This_Player.FindPlayerByName(Pname).Take(BASEITEM[strtoint(istr)],ITEMNUM[strtoint(istr)]);
		This_Player.PlayerNotice('已执行从玩家'+Pname+'背包收取物品['+BASEITEM[strtoint(istr)]+']命令！',2);
		_items(Page);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _giveitem(istr:string);
begin
	if This_Player.FindPlayer(Pname) then
	begin
		This_Player.FindPlayerByName(Pname).give(BASEITEM[strtoint(istr)],ITEMNUM[strtoint(istr)]);
		This_Player.PlayerNotice('已执行给予玩家'+Pname+'物品['+BASEITEM[strtoint(istr)]+']命令！',2);
		_items(Page);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _bindgive(istr:string);
begin
	if This_Player.FindPlayer(Pname) then
	begin
		This_Player.FindPlayerByName(Pname).SysGiveGift(BASEITEM[strtoint(istr)],ITEMNUM[strtoint(istr)],true);
		This_Player.PlayerNotice('已执行给予玩家'+Pname+'赠与物品['+BASEITEM[strtoint(istr)]+']命令！',2);
		_items(Page);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _clearbag(sstr:string);
begin	if This_Player.FindPlayer(sstr) then
	begin
		This_Npc.NpcDialog(This_Player,
		'确定清空玩家<'+sstr+'>背包内的所有物品吗？|{cmd}<确认清空/@clearbag_ok~'+sstr+'> ^<返回/@items~1>');
	end

end;

procedure _clearbag_ok(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).DelBagItemOfAll;
		This_Npc.NpcDialog(This_Player,'清空玩家背包成功！|'+QQ+'|{cmd}<返回/@items~1>');
	end else
		This_Player.PlayerNotice('玩家'+sstr+'已经不在线！',0);
end;

procedure _giveall(istr:string);
var i:integer;
begin	if This_Player.FindPlayer(Pname) then
	begin
		for i:=1+(strtoint(istr)-1)*10 to strtoint(istr)*10 do
		begin
			This_Player.FindPlayerByName(Pname).Give(BASEITEM[i],ITEMNUM[i]);
		end				
		This_Player.PlayerNotice('已执行给予玩家'+Pname+'本页全部物品命令！',2);
		_items(Page);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _takeall(istr:string);
var i,num:integer;
begin	if This_Player.FindPlayer(Pname) then
	begin
		for i:=1+(strtoint(istr)-1)*10 to strtoint(istr)*10 do
		begin
			num:=This_Player.GetBagItemCount(BASEITEM[i]);
			if num <  ITEMNUM[i] then
			begin
				This_Player.FindPlayerByName(Pname).Take(BASEITEM[i],num);
			end else
				This_Player.FindPlayerByName(Pname).Take(BASEITEM[i],ITEMNUM[i]);
		end
		This_Player.PlayerNotice('已执行收取玩家'+Pname+'本页全部物品命令！',2);
		_items(Page);		
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _takeheroall(istr:string);
var i,num:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		if This_Player.FindPlayerByName(Pname).HeroLevel <= 0 then
		begin
			This_Player.PlayerNotice('玩家'+This_Player.FindPlayerByName(Pname).Name+'的英雄不在线!',0);
			exit;
		end
		for i:=1+(strtoint(istr)-1)*10 to strtoint(istr)*10 do
		begin
			num:=This_Player.GetHeroBagItemCount(BASEITEM[i]);
			if num <  ITEMNUM[i] then
			begin
				This_Player.FindPlayerByName(Pname).TakeFromHeroBag(BASEITEM[i],num);
			end else
				This_Player.FindPlayerByName(Pname).TakeFromHeroBag(BASEITEM[i],ITEMNUM[i]);
		end
		This_Player.PlayerNotice('已执行收取玩家'+Pname+'英雄本页全部物品命令！',2);
		_items(Page);		
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',0);
end;

procedure _upgrade(sstr:string);
var xy1,xy2,xy3,xy4,herolv:integer;
begin
if This_Player.FindPlayer(sstr) then
begin
	BASELV :=StrToIntDef(ReadIniSectionStr('在线管理.txt','Other','LV'),1);//等级
	BASEPK :=StrToIntDef(ReadIniSectionStr('在线管理.txt','Other','PK'),50);//PK值
	BASESW :=StrToIntDef(ReadIniSectionStr('在线管理.txt','Other','SW'),10);//声望

	xy1:= This_Player.FindPlayerByName(sstr).GetWeaponLucky(false,false);
	if xy1 = -9999 then //未佩戴武器
	begin
		xy1:=0;
		xy3:=0;
	end else
		xy3:=This_Player.FindPlayerByName(sstr).GetWeaponLucky(true,false);

	xy2:= This_Player.FindPlayerByName(sstr).GetWeaponLucky(false,true);
	if xy2 = -9999 then //英雄未佩戴武器
	begin
		xy2:=0;
		xy4:=0;
	end else
		xy3:=This_Player.FindPlayerByName(sstr).GetWeaponLucky(true,true);
		
	herolv:=This_Player.FindPlayerByName(sstr).HeroLevel;
	if herolv < 0 then herolv:=0;
	
	Pname:=sstr;
	
	
	This_Npc.NpcDialog(This_Player,'正在对<'+sstr+'>的基本信息操作'
	+'|<等级/fcolor=254> ^<PK/fcolor=254>^<声望/fcolor=254> '
	+'|<基数/fcolor=254> <'+inttostr(BASELV)+'/@setdjjs> ^<基数/fcolor=254> <'+inttostr(BASEPK)+'/@setpkjs> ^<基数/fcolor=254> <'+inttostr(BASESW)+'/@setswjs>'
	+'|<————————————————————/fcolor=247>'
	+'|<[玩家]/fcolor=249> ^ 　<加>   <减>'
	+'|人物等级 <'+inttostr(This_Player.FindPlayerByName(sstr).Level)+'/fcolor=250> ^　　<修改/@setdjz~1>  <↑↑↑/@levelup~1>  <↓↓↓/@leveldn~1>^'
	+'|人物PK值 <'+inttostr(This_Player.FindPlayerByName(sstr).MyPKpoint)+'/fcolor=250> ^　　<修改/@setpkz>  <↑↑↑/@pkadd~'+sstr+'>  <↓↓↓/@pkdec~'+sstr+'>^'
	+'|人物声望 <'+inttostr(This_Player.FindPlayerByName(sstr).MyShengWan)+'/fcolor=250> ^　　<修改/@setswz>  <↑↑↑/@swadd~'+sstr+'>  <↓↓↓/@swdec~'+sstr+'>^'
	+'|武器幸运 <'+inttostr(xy3)+'/fcolor=253> = 基础 <'+inttostr(xy3-xy1)+'/fcolor=250> + 附加 <'+inttostr(xy1)+'/@wqlucky~1>'
	+'|'
//	+'|<————————————————————/fcolor=247>'
	+'|<[英雄]/fcolor=249> ^ 　<加>   <减>'	
	+'|英雄等级 <'+inttostr(herolv)+'/fcolor=250>^　　<修改/@setdjz~2>  <↑↑↑/@levelup~2>  <↓↓↓/@leveldn~2>^'
	+'|武器幸运 <'+inttostr(xy4)+'/fcolor=253> = 基础 <'+inttostr(xy4-xy2)+'/fcolor=250> + 附加 <'+inttostr(xy2)+'/@wqlucky~2>'
	+'|<————————————————————/fcolor=247>'
	+'|<基数:箭头调节改变的值/fcolor=242> <修改:直接指定数值/fcolor=242>'
//	+'|<本页幸运值不包括武器自带属性>'
	+'|{cmd}<返回/@man_info~'+sstr+'>'
	);
end else
	This_Npc.NpcDialog(This_Player,'玩家<'+sstr+'>已不在线！|{cmd}<返回/@playerlist~1>');
end;

procedure _setdjz(istr:string);
begin
	if istr='1' then
	begin
		This_Npc.InputDialog(This_Player,'请输入新的等级值',0,6302);
	end else
		This_Npc.InputDialog(This_Player,'请输入新的等级值',0,6307);

end;

procedure _setpkz;
begin
	This_Npc.InputDialog(This_Player,'请输入新的PK值',0,6303);

end;

procedure _setswz;
begin
	This_Npc.InputDialog(This_Player,'请输入新的声望值',0,6304);
end;

procedure _wqlucky(istr:string);
begin
	if istr='1' then 
	begin
		This_Npc.InputDialog(This_Player,'请输入人物新的武器幸运值(-255~7)',0,6305);
	end else
		This_Npc.InputDialog(This_Player,'请输入英雄新的武器幸运值(-255~7)',0,6306);
end;


procedure P6302;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		if StrToIntDef(This_NPC.InPutStr,-1) > 0 then
		begin		
			This_Player.FindPlayerByName(Pname).SetPlayerLevel(strtoint(This_NPC.InPutStr));
			_upgrade(Pname);
			This_Player.PlayerNotice('玩家'+Pname+'等级设置为'+This_NPC.InPutStr+'级',2);
		end else
			This_Player.PlayerNotice('请输入大于0的数字',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',2);
end;

procedure P6307;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		if StrToIntDef(This_NPC.InPutStr,-1) > 0 then
		begin		
			if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
			begin
				This_Player.FindPlayerByName(Pname).SetHeroLevel(strtoint(This_NPC.InPutStr));
				_upgrade(Pname);
				This_Player.PlayerNotice('玩家'+Pname+'的英雄设置为'+This_NPC.InPutStr+'级',2);
			end else
				This_Player.PlayerNotice(Pname+'没有召唤出英雄！',0);
		end else
			This_Player.PlayerNotice('请输入大于0的数字',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',2);
end;

procedure P6303;
var pktemp:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		if StrToIntDef(This_NPC.InPutStr,-1) >= 0 then
		begin
			pktemp:=This_Player.FindPlayerByName(Pname).MyPKpoint;
			if  pktemp > StrToInt(This_NPC.InPutStr) then
			begin
				This_Player.FindPlayerByName(Pname).DecPkpoint(pktemp - strtoint(This_NPC.InPutStr));
			end else
				This_Player.FindPlayerByName(Pname).IncPkpoint(strtoint(This_NPC.InPutStr) - pktemp);
				_upgrade(Pname);
				This_Player.PlayerNotice('玩家'+Pname+'PK值设置为'+This_NPC.InPutStr+'点',2);
		end else
			This_Player.PlayerNotice('请输入大于0的数字',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',2);
end;

procedure P6304;
var swtemp:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		swtemp:=StrToIntDef(This_NPC.InPutStr,-1);
		if swtemp >= 0 then
		begin
			This_Player.FindPlayerByName(Pname).MyShengWan:=(swtemp);
			_upgrade(Pname);
			This_Player.PlayerNotice('玩家'+Pname+'PK值设置为'+inttostr(swtemp)+'点',2);
		end else
			This_Player.PlayerNotice('请输入大于0的数字',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',2);
end;

procedure P6305;
var swtemp:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		swtemp:=StrToIntDef(This_NPC.InPutStr,-1);
		if (swtemp > -256) and (swtemp <= 7) then
		begin
			if This_Player.FindPlayerByName(Pname).GetWeaponLucky(false,false) > -9999 then
			begin
				if swtemp >=0 then
				begin
					This_Player.FindPlayerByName(Pname).SetWeaponLucky(swtemp,false);
				end else
				begin
					This_Player.FindPlayerByName(Pname).SetWeaponLucky(-256-swtemp,false);
				end
				_upgrade(Pname);
				This_Player.PlayerNotice('玩家'+Pname+'武器幸运值设置为'+inttostr(swtemp)+'点已生效,重新佩戴显示新值',2);			
			end else
				This_Player.PlayerNotice('玩家'+Pname+'没有佩戴武器',0);
		end else
			This_Player.PlayerNotice('请输入-255~7之间的数字',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',2);
end;

procedure P6306;
var swtemp:integer;
begin
	if This_Player.FindPlayer(Pname) then
	begin
		swtemp:=StrToIntDef(This_NPC.InPutStr,-1);
		if (swtemp >= -256) and (swtemp <= 7) then
		begin
			if This_Player.FindPlayerByName(Pname).GetWeaponLucky(false,true) > -9999 then
			begin
				if swtemp >=0 then
				begin
					This_Player.FindPlayerByName(Pname).SetWeaponLucky(swtemp,true);
				end else
				begin
					This_Player.FindPlayerByName(Pname).SetWeaponLucky(-256-swtemp,true);
				end				
				_upgrade(Pname);
				This_Player.PlayerNotice('玩家'+Pname+'的英雄的武器幸运值设置为'+inttostr(swtemp)+'点已生效,重新佩戴显示新值',2);			
			end else
				This_Player.PlayerNotice('玩家'+Pname+'的英雄没有佩戴武器',0);
		end else
			This_Player.PlayerNotice('请输入0-7之间的数字',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',2);
end;

procedure _setdjjs;
begin
	This_Npc.InputDialog(This_Player,'请输入新的等级调节基数',0,6301);
	setidx:='LV'
end;

procedure _setpkjs;
begin
	This_Npc.InputDialog(This_Player,'请输入新的PK值调节基数',0,6301);
	setidx:='PK'
end;

procedure _setswjs;
begin
	This_Npc.InputDialog(This_Player,'请输入新的声望值调节基数',0,6301);
	setidx:='SW'
end;

procedure P6301;
begin
	if StrToIntDef(This_NPC.InPutStr,-1) >= 1 then
	begin
		WriteIniSectionStr('在线管理.txt','Other',setidx,This_NPC.InPutStr);
		_upgrade(Pname);
		setidx:='';
	end	
end;


procedure _levelup(istr:string);
begin
	if This_Player.FindPlayer(Pname) then
	begin
		if istr='1' then
		begin
			This_Player.FindPlayerByName(Pname).SetPlayerLevel(This_Player.FindPlayerByName(Pname).Level+BASELV);
			_upgrade(Pname);
			This_Player.PlayerNotice('玩家'+Pname+'等级增加'+inttostr(BASELV)+'级',2);
		end else
		if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
		begin
			This_Player.FindPlayerByName(Pname).SetHeroLevel(This_Player.FindPlayerByName(Pname).HeroLevel+BASELV);
			_upgrade(Pname);
			This_Player.PlayerNotice('玩家'+Pname+'的英雄等级增加'+inttostr(BASELV)+'级',2);
		end else
			This_Player.PlayerNotice('玩家'+Pname+'没有召唤出英雄！',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',2);
end;

procedure _leveldn(istr:string);
begin
	if This_Player.FindPlayer(Pname) then
	begin
		if istr='1' then
		begin
			This_Player.FindPlayerByName(Pname).SetPlayerLevel(This_Player.FindPlayerByName(Pname).Level-BASELV);
			_upgrade(Pname);
			This_Player.PlayerNotice('玩家'+Pname+'等级降低'+inttostr(BASELV)+'级',2);
		end else
		if This_Player.FindPlayerByName(Pname).HeroLevel > 0 then
		begin
			This_Player.FindPlayerByName(Pname).SetHeroLevel(This_Player.FindPlayerByName(Pname).HeroLevel-BASELV);
			_upgrade(Pname);
			This_Player.PlayerNotice('玩家'+Pname+'的英雄等级降低'+inttostr(BASELV)+'级',2);
		end else
			This_Player.PlayerNotice('玩家'+Pname+'没有召唤出英雄！',0);
	end else
		This_Player.PlayerNotice('玩家'+Pname+'已经不在线！',2);
end;

procedure _pkadd(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).IncPkpoint(BASEPK);
		_upgrade(sstr);
		This_Player.PlayerNotice('玩家'+sstr+'PK值增加'+inttostr(BASEPK)+'点',2);
	end else
		This_Player.PlayerNotice('玩家'+sstr+'已经不在线！',2);

end;

procedure _pkdec(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).DecPkPoint(BASEPK);
		_upgrade(sstr);
		This_Player.PlayerNotice('玩家'+sstr+'PK值降低'+inttostr(BASEPK)+'点',2);
	end else
		This_Player.PlayerNotice('玩家'+sstr+'已经不在线！',2);

end;

procedure _swadd(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).MyShengWan:=This_Player.FindPlayerByName(sstr).MyShengWan + (BASESW);
		_upgrade(sstr);
		This_Player.PlayerNotice('玩家'+sstr+'声望值增加'+inttostr(BASESW)+'点',2);
	end else
		This_Player.PlayerNotice('玩家'+sstr+'已经不在线！',2);

end;

procedure _swdec(sstr:string);
begin
	if This_Player.FindPlayer(sstr) then
	begin
		This_Player.FindPlayerByName(sstr).MyShengWan:=This_Player.FindPlayerByName(sstr).MyShengWan - (BASESW);
		_upgrade(sstr);
		This_Player.PlayerNotice('玩家'+sstr+'声望值降低'+inttostr(BASESW)+'点',2);
	end else
		This_Player.PlayerNotice('玩家'+sstr+'已经不在线！',2);

end;


begin
if This_Player.GMLevel > 0 then
begin
	_playerlist(inttostr(1));
end else

    This_NPC.NpcDialog(This_Player,
	'☆☆☆欢迎光临【寒刀沉默】☆☆☆\|'              
     +' 【奴隶洞，地王宫殿】掉落【修炼石】\|'
     +' 【火龙洞，水上世界】掉落【天外飞石】\|'
	 +' 【蚂蚁洞，幽 灵 船】掉落【死亡坟章】\|'
     +' 【沉  默   魔   穴】掉落【真龙天骨】\|'
	 +' 【真     天     宫】掉落【白虎巨牙】\|'
	 +' 【冰  雪   之   城】掉落【千年暖玉】\|'
	 +' 【愤  怒   神   殿】掉落【万年玄冰】\|'
	 +' 【武士之星，法师之星，道士之星】各大高级地图掉落\|'
	);
end.