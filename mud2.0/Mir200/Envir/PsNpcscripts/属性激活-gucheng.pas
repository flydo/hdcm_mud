program Mir2;
Procedure _doexit;
begin
This_Npc.CloseDialog(This_Player);
end;



procedure _xianglian;

begin

       This_Npc.NpcDialog( This_Player,
    +'欢迎来到【<寒刀沉默/fcolor=243>】 <属性激活功能/fcolor=250>\'	
    +'|<─────────────────────>\' 
    +'|<注意:每次激活需><【2件】/fcolor=244><一样的手镯>|<【元宝】/fcolor=244>+<【嗜血圣石】/fcolor=244>\'
	   +'|<─────────────────────>\' 
    +'<激活80级手镯/@1>  <需要元宝4000+3块圣石/fcolor=210>\'	
	   +'|<─────────────────────>\' 
	       +'<激活90级手镯/@2>  <需要元宝6000+5块圣石/fcolor=210>\'	
	   +'|<─────────────────────>\'
	       +'<激活100级手镯/@3>  <需要元宝8000+8块圣石/fcolor=210>\'	
	   +'|<─────────────────────>\'
    );
end;


procedure _1;
begin
if This_Player.GetBagItemCount('嗜血圣石') >= 3 then
begin
if This_Player.GetBagItemCount('灭世神威镯') >= 2 then
begin
if This_Player.YBNum  >= 4000 then
begin
This_Player.Take('灭世神威镯' , 2);
This_Player.Take('嗜血圣石' , 3);
This_Player.ScriptRequestSubYBNum(4000); 
This_Player.Give('灭世神威镯-嗜血',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启灭世神威镯吸血属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件灭世神威镯，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'嗜血圣石不足！！！  ');	
end;



procedure _2;
begin
if This_Player.GetBagItemCount('嗜血圣石') >= 5 then
begin
if This_Player.GetBagItemCount('凤凰の飞链') >= 2 then
begin
if This_Player.YBNum  >= 6000 then
begin
This_Player.Take('凤凰の飞镯' , 2);
This_Player.Take('嗜血圣石' , 5);
This_Player.ScriptRequestSubYBNum(6000); 
This_Player.Give('凤凰の飞镯-嗜血',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启凤凰の飞镯吸血属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件凤凰の飞镯，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'嗜血圣石不足！！！  ');	
end;


procedure _3;
begin
if This_Player.GetBagItemCount('嗜血圣石') >= 8 then
begin
if This_Player.GetBagItemCount('诅咒の链') >= 2 then
begin
if This_Player.YBNum  >= 8000 then
begin
This_Player.Take('诅咒の镯' , 2);
This_Player.Take('嗜血圣石' , 8);
This_Player.ScriptRequestSubYBNum(8000); 
This_Player.Give('诅咒の镯-嗜血',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启诅咒の镯吸血属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件诅咒の镯，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'嗜血圣石不足！！！  ');	
end;





procedure _du1;
begin
if This_Player.GetBagItemCount('混沌之石[毒]') >= 5 then
begin
if This_Player.GetBagItemCount('破馆珍剑') >= 2 then
begin
if This_Player.YBNum  >= 8000 then
begin
This_Player.Take('破馆珍剑' , 2);
This_Player.Take('混沌之石[毒]' , 5);
This_Player.ScriptRequestSubYBNum(8000); 
This_Player.Give('破馆珍剑-毒',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启破馆珍剑的剧毒属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件破馆珍剑，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'混沌之石[毒]不足！！！  ');	
end;

procedure _du2;
begin
if This_Player.GetBagItemCount('混沌之石[毒]') >= 10 then
begin
if This_Player.GetBagItemCount('魅影之刃') >= 2 then
begin
if This_Player.YBNum  >= 15000 then
begin
This_Player.Take('魅影之刃' , 2);
This_Player.Take('混沌之石[毒]' , 10);
This_Player.ScriptRequestSubYBNum(15000); 
This_Player.Give('魅影之刃-毒',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启魅影之刃的剧毒属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件魅影之刃，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'混沌之石[毒]不足！！！  ');	
end;


procedure _du3;
begin
if This_Player.GetBagItemCount('混沌之石[毒]') >= 15 then
begin
if This_Player.GetBagItemCount('赤血魔剑') >= 2 then
begin
if This_Player.YBNum  >= 20000 then
begin
This_Player.Take('赤血魔剑' , 2);
This_Player.Take('混沌之石[毒]' , 15);
This_Player.ScriptRequestSubYBNum(20000); 
This_Player.Give('赤血魔剑-毒',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启赤血魔剑的剧毒属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件赤血魔剑，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'混沌之石[毒]！！！  ');	
end;






procedure _duwuqi;
begin

       This_Npc.NpcDialog( This_Player,
    +'欢迎来到【<寒刀沉默/fcolor=243>】 <属性激活功能/fcolor=250>\'	
    +'|<─────────────────────>\' 
    +'|<注意:每次激活需><【2件】/fcolor=244><一样的武器>|<【元宝】/fcolor=244>+<【混沌之石[毒]】/fcolor=244>\'
	   +'|<─────────────────────>\' 
    +'<激活90级武器/@du1>  <需要元宝8000+5块混沌之石/fcolor=210>\'	
	   +'|<─────────────────────>\' 
	       +'<激活100级武器/@du2>  <需要元宝15000+10块混沌之石/fcolor=210>\'	
	   +'|<─────────────────────>\'
	       +'<激活110级武器/@du3>  <需要元宝20000+15块混沌之石/fcolor=210>\'	
	   +'|<─────────────────────>\'
    );
end;








procedure _su1;
begin
if This_Player.GetBagItemCount('鸿蒙之石[速]') >= 5 then
begin
if This_Player.GetBagItemCount('破馆珍剑') >= 2 then
begin
if This_Player.YBNum  >= 8000 then
begin
This_Player.Take('破馆珍剑' , 2);
This_Player.Take('鸿蒙之石[速]' , 5);
This_Player.ScriptRequestSubYBNum(8000); 
This_Player.Give('破馆珍剑-速',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启破馆珍剑的攻速属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件破馆珍剑，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'鸿蒙之石[速]不足！！！  ');	
end;

procedure _su2;
begin
if This_Player.GetBagItemCount('鸿蒙之石[速]') >= 10 then
begin
if This_Player.GetBagItemCount('魅影之刃') >= 2 then
begin
if This_Player.YBNum  >= 15000 then
begin
This_Player.Take('魅影之刃' , 2);
This_Player.Take('鸿蒙之石[速]' , 10);
This_Player.ScriptRequestSubYBNum(15000); 
This_Player.Give('魅影之刃-速',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启魅影之刃的攻速属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件魅影之刃，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'鸿蒙之石[速]不足！！！  ');	
end;


procedure _su3;
begin
if This_Player.GetBagItemCount('鸿蒙之石[速]') >= 15 then
begin
if This_Player.GetBagItemCount('赤血魔剑') >= 2 then
begin
if This_Player.YBNum  >= 20000 then
begin
This_Player.Take('赤血魔剑' , 2);
This_Player.Take('鸿蒙之石[速]' , 15);
This_Player.ScriptRequestSubYBNum(20000); 
This_Player.Give('赤血魔剑-速',1)
This_Npc.NpcDialog(This_Player,
'激活成功！！！  ');
ServerSay('玩家<' + This_Player.Name + '>开启赤血魔剑的攻速属性成功！', 3);	
end else
This_Npc.NpcDialog(This_Player,
'元宝不足！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'你没有2件赤血魔剑，无法激活！！！  ');
end else
This_Npc.NpcDialog(This_Player,
'鸿蒙之石[速]！！！  ');	
end;





procedure _suwuqi;
begin

       This_Npc.NpcDialog( This_Player,
    +'欢迎来到【<寒刀沉默/fcolor=243>】 <属性激活功能/fcolor=250>\'	
    +'|<─────────────────────>\' 
    +'|<注意:每次激活需><【2件】/fcolor=244><一样的武器>|<【元宝】/fcolor=244>+<【鸿蒙之石[速]】/fcolor=244>\'
	   +'|<─────────────────────>\' 
    +'<激活90级武器/@su1>  <需要元宝8000+5块鸿蒙之石/fcolor=210>\'	
	   +'|<─────────────────────>\' 
	       +'<激活100级武器/@su2>  <需要元宝15000+10块鸿蒙之石/fcolor=210>\'	
	   +'|<─────────────────────>\'
	       +'<激活110级武器/@su3>  <需要元宝20000+15块鸿蒙之石/fcolor=210>\'	
	   +'|<─────────────────────>\'
    );
end;






begin
       This_Npc.NpcDialog( This_Player,
    +'欢迎来到【<寒刀沉默/fcolor=243>】 <属性激活功能/fcolor=250>\'	
   +'|<─────────────────────>\' 
   +'|<注意:激活吸血属性需要><【嗜血圣石】/fcolor=241>\'
   +'|<注意:激活剧毒属性需要><【混沌之石[毒]】/fcolor=241>\'
   +'|<注意:激活攻速属性需要><【鸿蒙之石[速]】/fcolor=241>\'
  +'|{cmd}<激活手镯吸血属性/@xianglian>\'
    +'|{cmd}<激活武器剧毒属性/@duwuqi>\'
	  +'|{cmd}<激活武器攻速属性/@suwuqi>\'
    );
end.


 