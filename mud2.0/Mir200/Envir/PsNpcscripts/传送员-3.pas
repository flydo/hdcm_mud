{
/************************************************************************}


PROGRAM Mir2;

{$I common.pas}
{$I ActiveValidateCom.pas}


Procedure _doexit;
begin
   This_Npc.CloseDialog(This_Player);
end;

procedure domain;
begin
    This_NPC.NpcDialog(This_Player,
    '欢迎来到寒刀沉默！：\'
    +'|{cmd}<安全传送/@anquan>        ^<菜鸟区域/@xiane>\'
	+'|{cmd}<打宝传送/@dabao>        ^<高级传送/@gaoji>\'
    +'|{cmd}<六大重装/@liuda>   ^  <战士免费领宠物/@zsmianfling>   \'
	   +ActiveValidateStr
	);
end;



procedure _zsmianfling;
begin
if This_Player.Job = 0 then           //判断职业
begin
 if This_Player.GetSlaveCount('') < 1 then
 begin
 if This_Player.Level <= 60  then
 begin
 This_Player.MakeSlaveEx('变异骷髅' , 1 ,0);
 This_Npc.NpcDialog(This_Player,          
    '召唤成功，快带它去溜溜！');
	end else
	This_Player.PlayerDialog(
              '闹呢？你已经60级以上了！！\');
	end else
		This_Player.PlayerDialog(
              '你已经召唤了一个宝宝了！！\');
	end else
			This_Player.PlayerDialog(
              '你不是战士！！\');
end;



procedure _liuda;
begin
  This_Npc.NpcDialog(This_Player,
  '欢迎来到寒刀沉默！六大重装，爆衣服，45级加20W金币。就可以传送哟！\ \'
  +'|{cmd}<堕落坟场/@DLFC>    ^<死亡神殿/@SWSD>    ^<深渊魔域/@SYMY>\'
  +'|{cmd}<钳虫巢穴/@QCCX>    ^<地狱烈焰/@DYLY>   ^<困惑殿堂/@KHDT>\'
  );
end;




procedure _dabao;
begin
  This_Npc.NpcDialog(This_Player,
  '欢迎来到寒刀沉默！打宝地图，只需要1万金币就可以传送哟！\ \'
  +'|{cmd}<封魔殿/@FMD>    ^<石墓七层/@SMQ>    ^<荒原血域/@HYXY>\'
  +'|{cmd}<骨魔洞穴/@GMDX>    ^<抉择之地/@JZZD>   \'
  );
end;

procedure _gaoji;
begin
  This_Npc.NpcDialog(This_Player,
  '欢迎来到寒刀沉默！中级打宝，只需要25万金币，50级。就可以传送哟！\ \'
  +'|{cmd}<水上世界/@SSSJ>    ^<蚂蚁洞/@MYD>    ^<奴隶洞穴/@NLDX>\'
  +'|{cmd}<火龙洞/@HLD>    ^<地王宫殿/@DWGD>   \'
  );
end;

procedure _gaoji323;
begin
  This_Npc.NpcDialog(This_Player,
  '欢迎来到寒刀沉默！高级打宝，只需要60万金币，60级。就可以传送哟！\ \'
  +'|{cmd}<真天宫/@ZTG>    ^<幽灵船/@YLC>    ^<冰雪之城/@BXZC>\'
  +'|{cmd}<沉默魔穴/@CMMX>     \'
  );
end;



procedure _xiane;
begin
  This_Npc.NpcDialog(This_Player,
  '欢迎来到寒刀沉默！\ \'
  +'|{cmd}<菜鸟矿区【掉书的地图】/@CNKQ>    \'
  +'|{cmd}<菜鸟乐园【掉落初级装备】/@CNLY1>  \'
  +'|{cmd}<菜鸟乐园二【掉落初级装备】/@CNLY2>  \'
    
  );
end;

procedure _CNKQ; 
begin
This_Player.Flyto('D401~2',0,0);
end;

procedure _CNLY1; 
begin
This_Player.Flyto('D022~2',0,0);
end;



procedure _CNLY2; 
begin
This_Player.Flyto('D2071~2',0,0);
end;




procedure _FMD; 
begin
if This_Player.GoldNum >= 10000 then    //检测金币
begin
This_Player.DecGold(10000);
This_Player.Flyto('D2013',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足6万'   );
end ; 
end; 

procedure _SMQ; 
begin
if This_Player.GoldNum >= 10000 then    //检测金币
begin
This_Player.DecGold(10000);
This_Player.Flyto('D717',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足6万'   );
end ; 
end; 

procedure _HYXY; 
begin
if This_Player.GoldNum >= 10000 then    //检测金币
begin
This_Player.DecGold(10000);
This_Player.Flyto('66',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足6万'   );
end ; 
end; 


procedure _GMDX; 
begin
if This_Player.GoldNum >= 10000 then    //检测金币
begin
This_Player.DecGold(10000);
This_Player.Flyto('D2067',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足6万'   );
end ; 
end;


procedure _JZZD; 
begin
if This_Player.GoldNum >= 10000 then    //检测金币
begin
This_Player.DecGold(10000);
This_Player.Flyto('D1004',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足6万'   );
end ; 
end;


procedure _SSSJ; 
begin
if This_Player.Level >= 50  then    //检测等级 
begin
if This_Player.GoldNum >= 250000 then    //检测金币
begin
This_Player.DecGold(250000);
This_Player.Flyto('sssj',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足25万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _MYD; 
begin
if This_Player.Level >= 50  then    //检测等级 
begin
if This_Player.GoldNum >= 250000 then    //检测金币
begin
This_Player.DecGold(250000);
This_Player.Flyto('mydx1',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足25万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _NLDX; 
begin
if This_Player.Level >= 50  then    //检测等级 
begin
if This_Player.GoldNum >= 250000 then    //检测金币
begin
This_Player.DecGold(250000);
This_Player.Flyto('F004~1',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足25万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _HLD; 
begin
if This_Player.Level >= 50  then    //检测等级 
begin
if This_Player.GoldNum >= 250000 then    //检测金币
begin
This_Player.DecGold(250000);
This_Player.Flyto('d2081',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足25万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 



procedure _DWGD; 
begin
if This_Player.Level >= 50  then    //检测等级 
begin
if This_Player.GoldNum >= 250000 then    //检测金币
begin
This_Player.DecGold(250000);
This_Player.Flyto('D505~2',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足25万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 

//高级打宝

procedure _ZTG; 
begin
if This_Player.Level >= 60  then    //检测等级 
begin
if This_Player.GoldNum >= 600000 then    //检测金币
begin
This_Player.DecGold(600000);
This_Player.Flyto('ztg1',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足60万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _YLC; 
begin
if This_Player.Level >= 60  then    //检测等级 
begin
if This_Player.GoldNum >= 600000 then    //检测金币
begin
This_Player.DecGold(600000);
This_Player.Flyto('ylc1',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足60万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 




procedure _BXZC; 
begin
if This_Player.Level >= 60  then    //检测等级 
begin
if This_Player.GoldNum >= 600000 then    //检测金币
begin
This_Player.DecGold(600000);
This_Player.Flyto('bxzc1',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足60万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 



procedure _CMMX; 
begin
if This_Player.Level >= 60  then    //检测等级 
begin
if This_Player.GoldNum >= 600000 then    //检测金币
begin
This_Player.DecGold(600000);
This_Player.Flyto('bxzc2',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足60万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 



procedure _DLFC; 
begin
if This_Player.Level >= 45  then    //检测等级 
begin
if This_Player.GoldNum >= 200000 then    //检测金币
begin
This_Player.DecGold(200000);
This_Player.Flyto('T115',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足20万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _SWSD; 
begin
if This_Player.Level >= 45  then    //检测等级 
begin
if This_Player.GoldNum >= 200000 then    //检测金币
begin
This_Player.DecGold(200000);
This_Player.Flyto('T118',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足20万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _SYMY; 
begin
if This_Player.Level >= 45  then    //检测等级 
begin
if This_Player.GoldNum >= 200000 then    //检测金币
begin
This_Player.DecGold(200000);
This_Player.Flyto('T119',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足20万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _QCCX; 
begin
if This_Player.Level >= 45  then    //检测等级 
begin
if This_Player.GoldNum >= 200000 then    //检测金币
begin
This_Player.DecGold(200000);
This_Player.Flyto('T132',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足20万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _DYLY; 
begin
if This_Player.Level >= 45  then    //检测等级 
begin
if This_Player.GoldNum >= 200000 then    //检测金币
begin
This_Player.DecGold(200000);
This_Player.Flyto('T139',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足20万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 


procedure _KHDT; 
begin
if This_Player.Level >= 45  then    //检测等级 
begin
if This_Player.GoldNum >= 200000 then    //检测金币
begin
This_Player.DecGold(200000);
This_Player.Flyto('T140',0,0); //传送地图
end else  //检测不足 提示 
begin
This_Npc.NpcDialog(This_Player,
'金币不足20万'   );
end ; 
end else  //等级不足 提升下面 
begin
This_Npc.NpcDialog(This_Player,
'你的实力不足，就别去送死了吧？！！！  ');
end; 
end; 












procedure _anquan;
begin
  This_Npc.NpcDialog(This_Player,
  '欢迎来到散人之家传奇！\ \'
  +'|{cmd}<边界村/@MOVE01>     ^<银杏山谷/@MOVE02>    ^<比奇省/@MOVE0>\'
  +'|{cmd}<毒蛇山谷/@MOVE2>    ^<盟重省/@MOVE3>       ^<封魔谷/@MOVE4>\'
  +'|{cmd}<苍月岛/@MOVE5>      ^<白日门/@MOVE11>      ^<沙城区域/@shacheng>\' 
  +'|{cmd}<比奇皇宫/@hg>\' 
  );
end;

procedure _hg; 
begin
This_Player.Flyto('0122',23,31);
end;

procedure _MOVE01; 
begin
This_Player.Flyto('0',650,631);
end;

procedure _MOVE02; 
begin
This_Player.Flyto('0',289,618);
end;

procedure _MOVE0; 
begin
This_Player.Flyto('0',325,268);
end;


procedure _MOVE2; 
begin
This_Player.Flyto('2',503,483);
end;

procedure _MOVE3; 
begin
This_Player.Flyto('3',330,330);
end;

procedure _MOVE4; 
begin
This_Player.Flyto('4',240,200);
end;

procedure _MOVE5; 
begin
This_Player.Flyto('5',141,335);
end;

procedure _MOVE6; 
begin
This_Player.Flyto('6',124,156);
end;

procedure _MOVE11; 
begin
This_Player.Flyto('11',178,326);
end;

procedure _shacheng; 
begin
This_Player.Flyto('3',675,337);
end;

Begin
  domain;
end.e