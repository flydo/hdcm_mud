{********************************************************************

*******************************************************************}

program mir2;

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;
                                                                                                            
var
ck_name : array[1..30 ]of string;
ck_value : array[1..30] of integer; 

procedure OnInitialize;
begin
ck_name[1] :='龙皇霸气战剑';
ck_name[2] :='龙皇霸气道剑';
ck_name[3] :='龙皇霸气法剑';
ck_name[4] :='不灭V冰火战刀';
ck_name[5] :='不灭V冰火道刀';
ck_name[6] :='不灭V冰火法刀';
ck_name[7] :='辉煌炽火战剑';
ck_name[8] :='辉煌炽火道剑';
ck_name[9] :='辉煌炽火法剑';
ck_name[10] :='灭世天神掌';
ck_name[11] :='破馆珍剑';
ck_name[12] :='魅影之刃';
ck_name[30] :='赤血魔剑[神]';
ck_name[14] :='浮屠の弑仙斧';
ck_name[15] :='破馆珍剑-速';
ck_name[16] :='魅影之刃-速';
ck_name[17] :='社会主义菜刀-速';
ck_name[18] :='赤血魔剑-速';
ck_name[19] :='浮屠の弑仙斧-速';
ck_name[20] :='破馆珍剑-毒';
ck_name[21] :='魅影之刃-毒';
ck_name[22] :='赤血魔剑-毒';
ck_name[23] :='浮屠の弑仙斧-毒';
ck_name[24] :='社会主义菜刀-毒';
ck_name[25] :='社会主义菜刀-速';

ck_name[26] :='弑天裂地剑';
ck_name[27] :='玄月斩天剑';
ck_name[28] :='神器·末日使者';
ck_name[29] :='神器·神圣圣器';
ck_name[30] :='神器·神之最强';







ck_value[1] :=1000;
ck_value[2] :=1000;
ck_value[3] :=1000;
ck_value[4] :=1000;
ck_value[5] :=1000;
ck_value[6] :=1000;
ck_value[7] :=1000;
ck_value[8] :=1000;
ck_value[9] :=1000;
ck_value[10] :=1000;
ck_value[11] :=1000;
ck_value[12] :=1000;
ck_value[30] :=1000;
ck_value[14] :=1000;
ck_value[15] :=1000;
ck_value[16] :=1000;
ck_value[17] :=1000;
ck_value[18] :=1000;
ck_value[19] :=1000;
ck_value[20] :=1000;
ck_value[21] :=1000;
ck_value[22] :=1000;
ck_value[23] :=1000;
ck_value[24] :=1000;
ck_value[25] :=1000;
ck_value[26] :=1000;
ck_value[27] :=1000;
ck_value[28] :=1000;
ck_value[29] :=1000;
ck_value[30] :=1000;


end; 





procedure _Checkup_1;
begin
   This_NPC.Click_CommitItem(This_Player,1,'待祝福装备：'); 
end; 

procedure CommitItem(AType:word);
var
i,ck_num,ck_kind,ck_gold,ck_rand:integer;
ck_str,ck_red:string; 
begin
     ck_gold := 0;  //初始化 
     ck_str := '';  //初始化
     ck_red := '';  //初始化
     
     for ck_kind := 1 to 30 do
     begin
       if ck_name[ck_kind] = This_Item.ItemName then 
       begin                             
         ck_num := This_Item.AddPa4; 
         ck_gold := ck_value[ck_kind];
         if (ck_num > 0) and (ck_num < 30) then
         begin
           for i:= 1 to ck_num do
           begin
           ck_gold := ck_gold;
           end;
         end;  
       end;
     end;
     
     if ck_num = 6 then
     begin
     This_Player.NotifyClientCommitItem(0,'无法鉴定：你的'+This_Item.ItemName+'已激发出所有属性！');  
     end else
     if ck_gold > 0 then
     begin                                                                                             
       if This_Player.YBNum>= ck_gold then
       begin
         ck_rand := random(100);
	if ck_num <= 6  then
         begin 
         This_Item.AddPa4 := This_Item.AddPa4 + 1;   //幸运+1 
         ck_str :='3'; 
         end  else
	if (ck_rand > 10) and (ck_rand < 30) and (ck_num = 4 )then
         begin 
         This_Item.AddPa4 := This_Item.AddPa4 + 1;   //幸运+1 
         ck_str :='3'; 
         end  else
	if (ck_rand > 1) and (ck_rand < 10) then
         begin 
         This_Item.AddPa4 := This_Item.AddPa4 + 1;   //幸运+1 
         ck_str :='3'; 
         end  else
	if (ck_rand > 10) and (ck_rand < 60) and (ck_num > 3 )then
         begin 
         ck_str :='1'; 
         end  else
	if (ck_rand > 20) and (ck_num <= 3 ) then
         begin 
         This_Item.AddPa4 := This_Item.AddPa4 + 1;   //幸运+1 
         ck_str :='3'; 
         end  else
	if ck_rand > 40 then
         begin 
         ck_str :='1';
		   end;
       This_Player.PsYBConsum(This_NPC,'ForgeDiaYB',20151,ck_gold,1); 
         if ck_str = '3' then 
         begin
         This_Player.NotifyClientCommitItem(0,'你的武器获得了祝福！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
		  end  else
 if ck_str = '2' then 
         begin
         This_Player.NotifyClientCommitItem(0,'你的武器被诅咒了！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
		  end  else
 if ck_str = '1' then 
         begin
         This_Player.NotifyClientCommitItem(0,'无效！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);		 
         end;   
         if ck_red <> '' then This_NPC.NpcNotice('恭喜：'+This_Player.Name+'在幸运武器升级'+This_Item.ItemName+'时提升了'+ck_str+'！');
         This_Player.AddLogRec(6, This_Item.ItemName, 811152, ck_gold, ck_str);
       end else
       begin
       This_Player.NotifyClientCommitItem(0,'无法升级：你的元宝不足，需要'+inttostr(ck_gold)+'元宝。'); 
       end;
     end else 
     begin
        This_Player.NotifyClientCommitItem(0,'我只能为55级以上的武器加幸运！！');   
    end;
end;   





begin
  This_Npc.NpcDialog(This_Player,
  +'<我可以提升武器幸运(最高升6点) /color=70>| \'
  +'<但我听说也有民间高手能用普通祝福油喝到运7/color=242>|\'
  +'<如果你也能做到，此武器可是价值连城！/color=250>|\'
  +'<三大职业达到运九.有通天之威/color=243>|\'
  +'<每次扣取1000元宝！/color=244>|\'
  +'<每次都会成功哟~！/c=red>\'
  +'|{cmd}<开始升级武器幸运/@Checkup_1> \' 
  );

end.
   