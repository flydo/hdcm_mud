{********************************************************************
* 单元名称：英雄地长老  hero-hero001

* 摘    要：
* 备    注：用到任务号12的1，2，3号变量

*******************************************************************}


program mir2;

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _BuHero1;
begin
   This_Player.SetV(12,3,1);
   This_Npc.InputDialog(This_Player,'请输入英雄的名字：',0,101);
end;

procedure _BuHero2;
begin
   This_Player.SetV(12,3,2);
   This_Npc.InputDialog(This_Player,'请输入英雄的名字：',0,101);
end;

procedure _BuHero3;
begin
   This_Player.SetV(12,3,3);
   This_Npc.InputDialog(This_Player,'请输入英雄的名字：',0,101);
end;

procedure _BuHero4;
begin
   This_Player.SetV(12,3,4);
   This_Npc.InputDialog(This_Player,'请输入英雄的名字：',0,101);
end;

procedure _BuHero5;
begin
   This_Player.SetV(12,3,5);
   This_Npc.InputDialog(This_Player,'请输入英雄的名字：',0,101);
end;

procedure _BuHero6;
begin
   This_Player.SetV(12,3,6);
   This_Npc.InputDialog(This_Player,'请输入英雄的名字：',0,101);
end;

Procedure p101;
var
j : integer;
begin
   j := This_Player.GetV(12,3);
   if This_Npc.InputOK then
   begin
      if This_Player.CreateHero(This_Npc.InputStr,1,j) = 0 then
      begin
         if This_Player.GetV(111,38) = 1 then
         begin
            if This_Player.SetV(111,38,10) then
            begin
               This_Player.Give('经验',10000);
               This_Player.PlayerNotice('你已经完成了成长之路：英雄相伴任务。',2);
               //This_Player.DeleteTaskFromUIList(1038);
               This_Player.SetV(113,1,1);                                 //接英雄篇：英雄内功
               This_Player.SetV(113,34,1);                                //接英雄篇：护体神盾 
               This_Player.SetV(113,43,1);                                //接英雄篇：英雄合击 
            //   This_Player.AddTaskToUIList(1201);                    //英雄篇第一个任务
            //   This_Player.AddTaskToUIList(1234);                   //英雄34级任务 
               if (j = 2) or (j = 5) then
               begin
                  This_Player.SetV(113,40,1);                             //接英雄篇：法师四级盾 
               end;
            end;
         end;
      end;
   end;
end;


procedure _jineng;
begin
   if This_Player.GetV(12,2) = 1 then
   begin
      This_Npc.NpcDialog(This_Player,
      '您已经领取过书页了，不能再次领取。\ \'+
      '|{cmd}<离开/@doexit>');
   end else
   begin
      if This_Player.FreeBagNum >= 3 then
      begin
         if This_Player.HeroLevel >= 0 then
         begin             
              case This_Player.HeroJob of
               0 : This_Player.BindGive('白日门剑术',1);
               1 : This_Player.BindGive('白日门火球术',1);
               2 : This_Player.BindGive('白日门治愈术',1);
              end;
              
              This_Player.BindGive('书页',2); 
              This_Player.SetV(12,2,1);
              This_Npc.NpcDialog(This_Player,
             '跟随你的英雄都是白日门最优秀的精英，\'+
             '他们都可以掌握白日门特有的各种魔法技能。\'+
             '不过很多白日门的技能书都在一场灾难中被毁得残破不堪，\'+
             '现在只剩下一些零散的书页，而且其中的很多书页已经\'+
             '流散到玛法的各处很难凑齐。\'+
             '我这里还有两片残留的书页，你拿去找白日门的龙人，\'+
             '他也许能从残留的书页里找到一些技能秘籍。\ \'+
             '|{cmd}<好的,我这就去找他/@doexit>');
         end else
         This_Npc.NpcDialog(This_Player,
         '你必须召唤出你的英雄才能领取！点击左上方布局，自己配置按钮位置即可！');
      end else  
      begin
         This_Npc.NpcDialog(This_Player,
         '你的包裹空间不够，请整理后再来领取。');
      end;
   end;
end;



//脚本执行的入口
begin 
         This_Npc.NpcDialog(This_Player,
         '恭喜您可以带领您的英雄去闯荡玛法大陆了，\'+
         '您想带领哪位英雄呢？\ \'+
         '|{cmd}<英雄(男战士)/@BuHero1>      ^<英雄(男法师)/@BuHero2>      ^<英雄(男方士)/@BuHero3>\ \'+
         '|{cmd}<英雄(女战士)/@BuHero4>      ^<英雄(女法师)/@BuHero5>      ^<英雄(女方士)/@BuHero6>\\'+
		 '|{cmd}<点击左上角布局按钮可拉出召唤英雄等按钮/@jineng22>\'
		 );
  end.