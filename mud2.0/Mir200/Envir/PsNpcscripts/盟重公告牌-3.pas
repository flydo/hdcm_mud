{
*******************************************************************}

program Mir2;

{$I WarehouseAndMbind.pas} 
{$I ActiveValidateCom.pas}

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end; 

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;
var
goldPigId : integer;

procedure _move;
begin
   This_NPC.NpcDialog(This_Player,
   '你想移动到哪里去呢？\'+
   '|{cmd}<向白日门移动/@move1>                       \'+
   '|{cmd}<向毒蛇山谷移动/@move2>                     \'+
   '|{cmd}<想去苍月岛/@move4>\'+
  // '|{cmd}<想去魔龙城/@move6>\'+
   '|{cmd}<挑战暗之祖玛教主/@zuma>\'+
   '|{cmd}<返回/@main>');
end;

function getXYstr(Num:integer) : string; 
var temp_xy,temp_xy1,temp_xy2 : integer; 
begin 
    temp_xy1 := This_Player.GetActivePoint; 
    temp_xy2 := This_Player.GetTmpActivePoint;
    temp_xy :=temp_xy1+temp_xy2;
    case Num of
    0: result := inttostr(temp_xy);
    1: result := inttostr(temp_xy1);
    2: result := inttostr(temp_xy2);
    end;   
end;  

procedure _zuma;
begin
   if AutoOpen(2) then
   begin
   This_NPC.NpcDialog(This_Player,
   '我可以送你前往祖玛大厅，不过必须首先给我1万金币。\'+
   '目前玛法大陆魔物骚乱，魔王派出了手下的精英怪物占据了<祖玛神殿/c=red>\'+
   '<七层大厅/c=red>，此地图现在异常凶险，如果没有足够的传奇信用分我是不\'+
   '会放你进去送死的。\'+
   '你当前的传奇信用分为<' + getXYstr(0) + '/c=red>分，其中临时信用分为<' + getXYstr(2) + '/c=red>分，\'+
   '进入此地图需要达到<30/c=red>分。\ \'+     
   '|{cmd}<前往祖玛大厅/@dating>             ^<前往祖玛神殿七层1/@goToMonMap_1>\'
   +ActiveValidateStr
   );
   end else
   This_NPC.NpcDialog(This_Player,
   '我可以送你前往祖玛大厅，不过必须首先给我1万金币。\'+
   '目前玛法大陆魔物骚乱，魔王派出了手下的精英怪物占据了<祖玛神殿/c=red>\'+
   '<七层大厅/c=red>，此地图现在异常凶险，如果没有足够的传奇信用分我是不\'+
   '会放你进去送死的。\'+
   '你当前的传奇信用分为<' + getXYstr(0) + '/c=red>分，其中临时信用分为<' + getXYstr(2) + '/c=red>分，\'+
   '进入此地图需要达到<30/c=red>分。<（当前可直接进入，'+inttostr(AutoOpenDay(2))+'天后开启验证）/c=red>\ \'+     
   '|{cmd}<前往祖玛大厅/@dating>             ^<前往祖玛神殿七层1/@goToMonMap_1>\'
   +ActiveValidateStr
   );
end;

procedure _goToMonMap_1;  
begin  
    if This_Player.MapName = '3' then  
    This_Player.FlyTo('D5072' , 10 , 55);  
end;                      

procedure _dating;
begin
   if This_Player.GoldNum >= 10000 then
   begin
      if compareText(This_Player.MapName,'3') = 0 then
      begin
         if This_Player.GetActivePoint + This_Player.GetTmpActivePoint >= 30 then
         This_Player.DecGold(10000);
         
         This_Player.Flyto('D5071',9,11);
      end;
   end else
   begin 
      This_NPC.NpcDialog(This_Player,
      '如果你想去，就必须首先给我1万金币，赶快给我吧！\ \'+ 
      '|{cmd}<返回/@main>');
   end;
end;

procedure _move6;
begin
   if compareText(This_Player.MapName,'3') = 0 then
      This_Player.Flyto('6',121,154);
end;

procedure _move4;
begin
   if compareText(This_Player.MapName,'3') = 0 then
   begin
      This_Player.Flyto('5',140,330);
   end;
end;

procedure _move2;
begin
   This_NPC.NpcDialog(This_Player,
   '移动到毒蛇山谷需要金币。\'+ 
   '不能让你免费使用，你必须支付1000金币！\'+ 
   '你想花这笔钱吗？\ \'+  
   '|{cmd}<移动/@pay2>\'+ 
   '|{cmd}<退出/@talkwith>');
end;

procedure _talkwith;
begin
   This_NPC.NpcDialog(This_Player,
   '有人挖了一条通道，\'+
   '通过这个通道， 可以马上到达白日门。。 \'+
   '你想快速到达那里吗？ \ \'+
   '|{cmd}<向通道移动/@move1>\'+
   '|{cmd}<向毒蛇山谷移动/@move2>\'+
   '|{cmd}<返回/@main>');
end;

procedure _pay2;
begin
   if This_Player.GoldNum >= 1000 then
   begin
      if compareText(This_Player.MapName,'3') = 0 then
      begin
         This_Player.DecGold(1000);
         This_Player.Flyto('3',521,764);
      end;
   end else
   begin 
      This_NPC.NpcDialog(This_Player,
      '你在开玩笑吧？这点钱也没有。。。 \'+
      '还想移动？ 我不想再见到你。。。\ \'+ 
      '|{cmd}<返回/@main>');
   end;
end;

procedure _move1;
begin
   This_NPC.NpcDialog(This_Player,
   '移动到通道需要金币。\'+ 
   '不能让你免费使用，你必须支付1000金币！\'+ 
   '你想花这笔钱吗？\ \'+  
   '|{cmd}<移动/@pay1>\'+ 
   '|{cmd}<退出/@talkwith>');
end;

procedure _pay1;
begin
   if This_Player.GoldNum >= 1000 then
   begin
      if compareText(This_Player.MapName,'3') = 0 then
      begin
         This_Player.DecGold(1000);
         This_Player.Flyto('D701',108,155);
      end;
   end else
   begin 
      This_NPC.NpcDialog(This_Player,
      '你在开玩笑吧？这点钱也没有。。。 \'+
      '还想移动？ 我不想再见到你。。。\ \'+ 
      '|{cmd}<返回/@main>');
   end;
end;

procedure _swz;
begin
  This_Npc.NpcDialog(This_Player,
  '我可以帮你使用声望值来消减PK值，\每一点声望值可以帮你消减100点PK值。\ \'
  +'|{cmd}<消减100点PK值/@decpkdecsw>\'
  +'|{cmd}<返回/@main>'
  );
end;

procedure _decpkdecsw;
begin
   if This_Player.MyPkpoint >= 100 then
   begin
      if This_Player.MyShengwan >= 1 then
      begin
         This_Player.MyShengwan := This_Player.MyShengwan-1;
         This_Player.DecPkPoint(100);
      end
      else
      begin
         This_Npc.NpcDialog(This_Player,
         '对不起，你没有声望，不能消减PK值！'
         );
      end;
   end
   else
   begin
      This_Npc.NpcDialog(This_Player,
      '你现在不需要消减PK值！' 
      );
   end;
end;

procedure Execute;
var
BookCanyon_div,BookCanyon_mod,tempNow , tempTime :integer;
pigRdm ,pigX , pigY :integer;
pigStr : string;
begin
   //战争地图
   tempNow := GetDateNum(GetNow) *10000 +(GetHour * 100) + (GetMin);
   tempTime := tempNow mod 10000;
  if GetG(36,36) <> tempNow then
  begin 
     if AutoOpenDay(1) = 0 then      //衣服点开启红字提示 
     begin
        if (GetHour = 0) and (GetMin = 0) then
        This_NPC.NpcNotice('【恶魔降临】堕落坟场、死亡神殿、深渊魔域、钳虫巢穴、地狱烈焰、困惑殿堂的重装恶魔已经出现，勇士们可以前往探险！');
     end;
     
     if AutoOpenDay(2) = 0 then      //传奇信用分开启红字提示 
     begin
        if (GetHour = 0) and (GetMin = 0) then
        This_NPC.NpcNotice('【传奇信用分】传奇信用分系统现已开启，勇士们可以前往各传送使者和仓库管理员处验证传奇信用分，更好的体验游戏内容！');
     end;
     
   
     
     if AutoOpenDay(5) = 0 then      //沙巴克攻城开启红字提示 
     begin
        if (GetHour = 0) and (GetMin = 0) then
        This_NPC.NpcNotice('【决战沙城】沙巴克攻城申请现已开启，勇士们可以在比奇国王处申请攻城战役，群雄逐鹿，决战沙城！');
     end;
     
     if (GetHour = 22) then
     begin
        if GetMin = 15 then
        begin
          if This_NPC.GetCastleGuildName <> '' then
          begin
            if (This_NPC.CheckMapMonByName('H204','千年树妖16') = 0) then
            begin
            This_NPC.CreateMon('H204',50,50,50,'千年树妖16',1);
            This_NPC.NpcNotice('【沙巴克】盟重大地微微一颤，神秘的BOSS出现在沙巴克藏宝阁');
            end;
          end;
        end else
        if GetMin = 30 then
        begin
          if This_NPC.GetCastleGuildName <> '' then
          begin
            if (This_NPC.CheckMapMonByName('H204','黄泉教主16') = 0) then
            begin
            This_NPC.CreateMon('H204',50,50,50,'黄泉教主16',1);
            This_NPC.NpcNotice('【沙巴克】盟重大地微微一颤，神秘的BOSS出现在沙巴克藏宝阁');
            end;
          end;
        end else
        if GetMin = 45 then
        begin
          if This_NPC.GetCastleGuildName <> '' then
          begin
            if (This_NPC.CheckMapMonByName('H204','地藏魔王16') = 0) then
            begin
            This_NPC.CreateMon('H204',50,50,50,'地藏魔王16',1);
            This_NPC.NpcNotice('【沙巴克】盟重大地微微一颤，神秘的BOSS出现在沙巴克藏宝阁');
            end;
          end;
        end;  
     end; 
     
       
     SetG(36,36,tempNow);  
  end; 
end;

procedure _GoTocastlewar;
begin
   if AutoOpen(5) then
   begin
     if This_Player.MapName = '3' then This_Player.Flyto('3',700,400); 
   end;
end;

var
tem : integer;
say : string;
Begin
   tem := random(5)+1;
  { case tem of
     1 : say := '当封魔谷的禁咒被打破之后，封魔谷又成了人类的都市；\当通往苍月岛的路途被发现之后，玛法大陆更是热闹非凡；\而当勇者雕像数立在比奇城中的时候，一切又变得与众不同。\' ;
     2 : say := '魔龙城是人类最近发现的远古城市。\最近，魔龙军团更是将魔影巨人和魔龙树妖召来，\这两个凶狠的恶魔更是向玛法大陆发起了冲击。\';
     3 : say := '深藏在密室的勋章守护人也来到了土城仓库二楼！\';
     4 : say := '最近魔龙军团的进攻十分凶猛，盟重可谓岌岌可危！\在盟重这个勇士聚集的地方，传送戒指失去了它的作用，\不再可以依靠它传送到盟重各地。\';
     5 : say := '暗之魔龙教主使用邪恶力量封印了魔龙西关，\从此以后，勇士们再也不能进入此地，\原先镇守这里的魔龙力士等已经转而踞守魔龙血域这个恶魔老巢。\';
   end;  }
   say := '当封魔谷的禁咒被打破之后，封魔谷又成了人类的都市；\当通往苍月岛的路途被发现之后，玛法大陆更是热闹非凡；\而当勇者雕像数立在比奇城中的时候，一切又变得与众不同。\' ;

   if AutoOpen(5) then
   begin
     This_NPC.NpcDialog(This_Player,
     say+
     '\|{cmd}<使用移动/@move>                   ^<使用声望值/@swz>\ \'+
     '|{cmd}<传送至沙巴克攻城集合点/@GoTocastlewar>\'+
     '|{cmd}<退出/@exit>   '); 
   end else
   This_NPC.NpcDialog(This_Player,
   say+
   '\|{cmd}<使用移动/@move>                   ^<使用声望值/@swz>\ \'+
   '|{cmd}<退出/@exit>'); 
end.