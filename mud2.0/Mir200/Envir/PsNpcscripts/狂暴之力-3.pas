{**********************************

*********************************}  
program mir2; 
procedure givefenghao;
var fh0,fh1, fh2, fh3, fh4, fh5, exfh1 : integer;
begin
	fh0	 := This_Player.GetV(69,1);		//首位封号
	fh1  := This_Player.GetV(69,2);		//一体时装
	fh2  := This_Player.GetV(69,3);		//衣服
	fh3  := This_Player.GetV(69,4);		//武器
	fh4  := This_Player.GetV(69,5);		//翅膀
	fh5  := This_Player.GetV(69,6);		//斗笠
	exfh1  := This_Player.GetV(69,7);	//扩展封号1
	
	if fh0 < 0 then fh0 := 0;
	if fh1 < 0 then fh1 := 0;
	if fh2 < 0 then fh2 := 0;
	if fh3 < 0 then fh3 := 0;
	if fh4 < 0 then fh4 := 0;
	if fh5 < 0 then fh5 := 0;
	if exfh1 < 0 then exfh1 := 0;
	// 封号给予编号 = 素材ID号/2+1
	This_Player.QuestInfo(inttostr(fh0)+':'+inttostr(fh1)+':'+inttostr(fh2)+':'+inttostr(fh3)+':'+inttostr(fh4)+':'+inttostr(fh5)+':'+inttostr(exfh1));
end;

procedure _getfirstfh(id:integer);
begin
	This_Player.SetV(69,1,id);
	givefenghao;
end;

procedure _getfanison(id:integer);
begin
	This_Player.SetV(69,2,id);
	givefenghao;
end;

procedure _getdress(id:integer);
begin
	This_Player.SetV(69,3,id);
	givefenghao;
end;

procedure _getweapon(id:integer);
begin
	This_Player.SetV(69,4,id);
	givefenghao;
end;

procedure _getwing(id:integer);
begin
	This_Player.SetV(69,5,id);
	givefenghao;
end;

procedure _getextfh(id:integer);
begin
	This_Player.SetV(69,7,id);
	givefenghao;
end;

procedure _gethair(id:integer);
begin
	This_Player.SetV(69,6,id);
	givefenghao;
end;

procedure domain;
begin
  This_NPC.NpcDialog(This_Player,
	   +'====无限时装演示====|\'
	   +'{cmd}<狂暴之力/@getfirstfh~1>	^<登堂入室/@getfirstfh~4>		^<略有小成/@getfirstfh~5>|\'  
	   +'{cmd}<一体时装1/@getfanison~1>		^<一体时装2/@getfanison~2>			^<一体时装3/@getfanison~3>|\' 
	   +'{cmd}<一体时装4/@getfanison~4>		^<一体时装5/@getfanison~5>			^<一体时装6/@getfanison~6>|\' 
	   +'{cmd}<衣服外显1/@getdress~2>		^<衣服外显2/@getdress~4>			^<衣服外显3/@getdress~6>|\' 
	   +'{cmd}<衣服外显4/@getdress~8>		^<衣服外显5/@getdress~10>			^<衣服外显6/@getdress~12>|\' 
	   +'{cmd}<武器外显1/@getweapon~1>		^<武器外显2/@getweapon~2>			^<武器外显3/@getweapon~3>|\'  
	   +'{cmd}<翅膀外显1/@getwing~1>		^<翅膀外显2/@getwing~2>				^<翅膀外显3/@getwing~3>|\'
	   +'{cmd}<翅膀外显4/@getwing~4>|\'	   
	   +'{cmd}<头发外显1/@gethair~4>		^<头发外显2/@gethair~5>|\' 
	   +'{cmd}<二:初学乍练/@getextfh~2>		^<二:初窥门径/@getextfh~3>				|\' 
	   +'{cmd}<取消一体时装1/@getfanison~0>		^<取消衣服/@getdress~0>			^<取消武器/@getweapon~0>|\' 
	   +'{cmd}<取消翅膀/@getwing~0>		^<取消第一称号/@getfirstfh~0>			^<取消第二称号/@getextfh~0>|\'
		+'{cmd}<取消头发/@gethair~0>|\'	   
	); 
end;

begin 
domain;
end.

