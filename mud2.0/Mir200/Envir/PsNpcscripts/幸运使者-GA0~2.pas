{项链加幸运
作者：开心就好 and 孩子气
内容：项链升级幸运，每次鉴定请间隔5秒，待鉴定装备属性越高，鉴定消耗的元宝也越多,成功率越低
+1 99%成功 +2 50% +3 15%成功
}

program mir2;

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _Exit;
begin
  This_Npc.CloseDialog(This_Player);
end;
                                                                                                            
var
ck_name : array[1..3 ]of string;
ck_value : array[1..3] of integer; 

procedure OnInitialize; //这下面可以继续添加可以升级幸运的项链
begin
ck_name[1] :='白色虎齿项链';
ck_name[2] :='灯笼项链';
ck_name[3] :='记忆项链';


ck_value[1] :=400;  //这下面可以继续添加可以升级所需元宝或者修改
ck_value[2] :=400;
ck_value[3] :=800;

end; 

procedure _Checkup;
begin
  This_Npc.NpcDialog(This_Player,
  '|可以鉴定的装备：\'
  +'|<白色虎齿项链、灯笼项链,记忆项链/c=red>\'
  +'|鉴定成功有几率增加以下属性：<幸运/c=red>\'
  +'|每次鉴定消耗一定数量的元宝：\'
  +'<普通白色虎齿项链：400元宝/c=red>\'
  +'<普通灯笼项链：400元宝/c=red>\'
  +'<普通记忆项链：800元宝/c=red>\'
  +'|<待鉴定装备属性越高，鉴定消耗的元宝也越多,成功率越低。/c=red>\'
  +'|<注意:每次鉴定请间隔5秒，如果多次点击没反应，请关掉NPC重新放入装备升级/c=red>\'
  +'|{cmd}<开始鉴定/@Checkup_1>        ^<关闭/@doexit>' 
  );
  
end; 

procedure _Checkup_1;
begin
   This_NPC.Click_CommitItem(This_Player,1,'待升级装备：'); 
end; 

procedure CommitItem(AType:word);
var
i,ck_num,ck_kind,ck_gold,ck_rand:integer;
ck_str,ck_red:string;
begin
     ck_gold := 0;  //初始化 
     ck_str := '';  //初始化
     ck_red := '';  //初始化
     
     for ck_kind := 1 to 3 do  //如果上面添加到了10个  则这里的3改成10
     begin
       if ck_name[ck_kind] = This_Item.ItemName then 
       begin                             
         ck_num := This_Item.AddPa2; 
         ck_gold := ck_value[ck_kind];
          if (ck_num > 0) and (ck_num < 3) then //有添加的话这里相应改到10，下同
         begin
           for i:= 1 to ck_num  do
           begin
           ck_gold := ck_gold*2;
           end;
         end;  
       end;
     end;
     
     if ck_num >= 3 then
     begin
     This_Player.NotifyClientCommitItem(0,'无法鉴定：你的'+This_Item.ItemName+'已激发出所有属性！');  
     end else
     if ck_gold > 0 then
     begin ck_rand := random(100)                                                                                       
       if This_Player.YBNum>= ck_gold then
        case ck_num of 
        0 : 
        begin  
         if ck_rand < 1then
            begin
         This_Player.PsYBConsum(This_NPC,'xin',20152,ck_gold,1);  
         This_Player.NotifyClientCommitItem(0,'升级失败，你的项链属性未发生变化');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：'); 
           end else
           if ck_rand < 99 then
            begin 
         This_Item.AddPa2 := This_Item.AddPa2 + 1;   //幸运+1
           This_Player.PsYBConsum(This_NPC,'xin',20153,ck_gold,1);  
         ck_str :='幸运+1';
         ck_red :='红字公告';
         This_Player.NotifyClientCommitItem(0,'升级成功：你的'+This_Item.ItemName+'提升到了'+ck_str+'！');
         This_NPC.NpcNotice('恭喜：'+This_Player.Name+'在幸运使者处把'+This_Item.ItemName+'的幸运提升到了'+ck_str+'！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：');
            end;
        end;
       1 : begin  
         if ck_rand < 50 then
            begin
         This_Player.PsYBConsum(This_NPC,'xin',20152,ck_gold,1);  
         This_Player.NotifyClientCommitItem(0,'升级失败，你的项链属性未发生变化');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：'); 
           end else
         if ck_rand < 99 then
         begin 
         This_Item.AddPa2 := This_Item.AddPa2 + 1;   //幸运+2
           This_Player.PsYBConsum(This_NPC,'xin',20153,ck_gold,1);  
         ck_str :='幸运+2';
         ck_red :='红字公告';
         This_Player.NotifyClientCommitItem(0,'升级成功：你的'+This_Item.ItemName+'提升到了'+ck_str+'！');
         This_NPC.NpcNotice('恭喜：'+This_Player.Name+'在幸运使者处把'+This_Item.ItemName+'的幸运提升到了'+ck_str+'！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：');
         end;
         end;
         2:begin  ck_rand := random(100);
      
           if ck_rand < 85 then
            begin
         This_Player.PsYBConsum(This_NPC,'xin',20152,ck_gold,1);  
         This_Player.NotifyClientCommitItem(0,'升级失败，你的项链属性未发生变化');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：'); 
           end else
           if ck_rand < 99 then
            begin 
         This_Item.AddPa2 := This_Item.AddPa2 + 1;   //幸运+3
         This_Player.PsYBConsum(This_NPC,'xin',20153,ck_gold,1);  
         ck_str :='幸运+3';
         ck_red :='红字公告';
         This_Player.NotifyClientCommitItem(0,'升级成功：你的'+This_Item.ItemName+'提升到了'+ck_str+'！');
         This_NPC.NpcNotice('恭喜：'+This_Player.Name+'在幸运使者处把'+This_Item.ItemName+'的幸运提升到了'+ck_str+'！');
         This_NPC.NotifyClientUPdBagItem(This_Player,This_Item);
         This_NPC.Click_CommitItem(This_Player,1,'待升级装备：');
            end;
         end;
      end else
       begin
         This_Player.NotifyClientCommitItem(0,'无法鉴定：你的元宝不足，需要'+inttostr(ck_gold)+'元宝。！'); 
       end;
      end else 
     begin
        This_Player.NotifyClientCommitItem(0,'该物品不可升级，请投入可升级的装备！');   
     end;     
  end;


function xin(price, num: Integer):boolean; 
begin
   result := true; 
 
end;


begin
  This_Npc.NpcDialog(This_Player,
  '|我云游一生，见识过玛法大陆上的各种奇珍异宝，但是只对三种宝物\'
  +'|感兴趣，一种名为<白色虎齿项链/c=red>，另外两种名为<灯笼项链和记忆项链/c=red>。\'
  +'|我曾习得一种鉴宝秘术，可以激发出宝物隐藏的力量，不过也可能会\'
  +'|使其烟消云散。如果你手中有这三种种宝物，不妨交给我鉴定一番，当\'
  +'|然，我会收取你一定远程的手续费，如果失败了也千万不要怪我。\ \'
  +'|{cmd}<开始鉴定/@Checkup>\' 
  );
end.
   