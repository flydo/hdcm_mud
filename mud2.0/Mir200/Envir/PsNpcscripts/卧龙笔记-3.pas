{********************************************************************
* 单元名称：卧龙笔记 
* 单元作者：
* 摘    要：
* 备    注：

*******************************************************************}

program mir2;

procedure _gowolong;
begin
  if This_Player.Level >= 40 then
  begin
     if compareText(This_Player.MapName, '3') = 0 then
     begin
        This_Player.Flyto('hero1', 220, 212);
        This_Npc.CloseDialog(This_Player);
       
        if This_Player.GetV(111,39) = 1 then
        begin
           if This_Player.SetV(111,39,10) then
           begin
              This_Player.Give('经验',10000);
              This_Player.PlayerNotice('你已经完成了成长之路：卧龙山庄任务。',2);
              //This_Player.DeleteTaskFromUIList(1039);
           end;
        end; 
     end;
  end
  else
    This_Npc.NpcDialog(This_Player,
      '你的等级未到40级，不能进入!'
    );
end;

begin
 // This_Npc.showbook(This_Player, 1, 0, '@gowolong'); 
  This_Npc.NpcDialog(This_Player,
      '<完成任务/@gowolong>'
    );  
end.