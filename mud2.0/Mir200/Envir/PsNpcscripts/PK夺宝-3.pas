{
*******************************************************************}

program Mir2;

 

procedure _DoExit;
begin
  This_Npc.CloseDialog(This_Player);
end;

procedure _GOONE;
begin
  if This_Player.Level >= 40 then
  begin
    if (GetHour = 18) and (GetMin >= 30) and (GetMin <= 40) then
    begin
      if random(2) = 1 then
      begin
        This_Player.Flyto('G003~1',0,0);
        This_Player.CallOut(This_NPC,3600,'P101'); 
      end
      else
      begin
        This_Player.Flyto('G003~1',0,0);
        This_Player.CallOut(This_NPC,3600,'P101');
      end;
    end
    else
      exit; 
  end
  else
    exit;
end;

procedure P101;
begin
  if CompareText(This_Player.MapName,'G003~1') = 0 then
  begin
    This_Player.RandomFlyTo('3');
  end
  else if CompareText(This_Player.MapName,'F009~01') = 0 then
  begin
    This_Player.RandomFlyTo('3');
  end
  else if CompareText(This_Player.MapName,'F006~01') = 0 then
  begin
    This_Player.RandomFlyTo('3');
  end
  else if CompareText(This_Player.MapName,'F003~03') = 0 then
  begin
    This_Player.RandomFlyTo('3');
  end
  else if CompareText(This_Player.MapName,'F002~01') = 0 then
  begin
    This_Player.RandomFlyTo('3');
  end
  else if CompareText(This_Player.MapName,'D2079~06') = 0 then
  begin
    This_Player.RandomFlyTo('3');
  end
  else if CompareText(This_Player.MapName,'D2004~02') = 0 then
  begin
    This_Player.RandomFlyTo('3');
  end;
end;

var
  temp : integer;
begin
  if This_Player.Level >= 40 then
  begin
    if (GetHour = 18) and (GetMin > 40) then
    begin
      This_Npc.NpcDialog(This_Player,          
      '非常的抱歉，你已经错过了本次活动的进入时间，\请等待下次活动开启！\');
    end
    else if (GetHour = 18) and (GetMin >= 30) and (GetMin <= 40) then
    begin
      This_Npc.NpcDialog(This_Player,          
      '目前夺宝战已经正式开启，请你抓紧时间尽快清人，独占boss满满的丰收(101,161) ！\ \' 
      +'|{cmd}<进入夺宝/@GOONE>');
      if temp = GetDateNum(GetNow) then
      begin
        exit;
      end
      else
      begin
        temp := GetDateNum(GetNow);
        This_NPC.ClearMon('G003~1');
        This_NPC.ClearMon('F009~01');
        This_NPC.ClearMon('F006~01');
        This_NPC.ClearMon('F003~03');
        This_NPC.ClearMon('D2079~06');
        This_NPC.ClearMon('F002~01');
        This_NPC.CreateMon('G003~1',38,35,50,'重装使者',1);
		This_NPC.CreateMon('G003~1',38,35,50,'暗之双头血魔',1);
		This_NPC.CreateMon('G003~1',38,35,50,'暗之双头金刚',1);
		This_NPC.CreateMon('G003~1',38,35,50,'暗之黄泉教主',1);
		This_NPC.CreateMon('G003~1',38,35,50,'暗之骷髅精灵',1);
		This_NPC.CreateMon('G003~1',38,35,50,'暗之沃玛教主',1);
		This_NPC.CreateMon('G003~1',38,35,50,'暗之虹魔教主',1);
		This_NPC.CreateMon('G003~1',38,35,50,'暗之触龙神',1);
		

      end; 
    end
    else
    begin
      This_Npc.NpcDialog(This_Player,          
      '比赛正式开始时间是18点30分，请先耐心等待，谢谢！\');
    end;
  end
  else
  begin
    This_Npc.NpcDialog(This_Player,          
    '非常的遗憾，只有等级达到<40级/c=red>的玩家才能参加此活动！');
  end;
end.