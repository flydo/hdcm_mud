
PROGRAM Mir2;



{$I common.pas}



Procedure _doexit;

begin

   This_Npc.CloseDialog(This_Player);  

end;


procedure domain;

begin

 This_NPC.NpcDialog(This_Player,
   '|┌—☆装备★——☆元宝★——☆经验★——┐\'+
   '| <祖玛系列/c=254>     <0  元宝/c=249>　          <3万/c=250>   ^^^^^<回收/@1_all>\'+
   '| <赤月系列/c=254>     <3 元宝/c=249>　         <6万/c=250>   ^^^^^<回收/@11_all>\'+
   '| <40级系列/c=254>     <15 元宝/c=249>　         <15万/c=250>   ^^^^^<回收/@2_all>\'+
   '| <45级系列/c=254>     <25 元宝/c=249>　         <35万/c=250>   ^^^^^<回收/@22_all>\'+
   '| <50级系列/c=254>     <35元宝/c=249>　        <80万/c=250>   ^^^^^<回收/@3_all>\'+
   '| <55级系列/c=254>     <75元宝/c=249>            <150万/c=250>   ^^^^^<回收/@33_all>\'+
   '| <65级系列/c=254>     <150元宝/c=249>            <300万/c=250>   ^^^^^<回收/@4_all>\'+
   '| <70级系列/c=254>     <300元宝/c=249>            <400万 /c=250>  ^^^^^<回收/@44_all>\'+
   '| <80级系列/c=254>     <500元宝/c=249>            <700万 /c=250>  ^^^^^<回收/@5_all>\'+
   '| <90级系列/c=254>     <700元宝/c=249>            <1000万 /c=250>  ^^^^^<回收/@55_all>\'+
   '| <100级系列/c=254>     <900元宝/c=249>            <1500万 /c=250>  ^^^^^<回收/@6_all>\'+
   '|└—☆装备★——☆元宝★——☆经验★——┘\'+
   '|<温馨提示:重要的装备请放置仓库回收后果自负/c=70>\');

end;


function getZBnameById(ZBid : integer) : string;
var ZBlv , ZBlvId : integer;
begin
    ZBlv := ZBid div 100;
    ZBlvId := ZBid mod 100;
    result := '';
    case ZBlv of
        1 : 
        begin
            case ZBlvId of
                1 : result := '绿色项链';
                2 : result := '骑士手镯';
                3 : result := '力量戒指';
                4 : result := '恶魔铃铛';
                5 : result := '龙之手镯';
                6 : result := '紫碧螺';
                7 : result := '灵魂项链';
                8 : result := '三眼手镯';
                9 : result := '泰坦戒指';
                10 : result := '黑铁头盔';
                11 : result := '青铜腰带';
                12 : result := '紫绸靴';
				13 : result := '龙纹剑';
                14 : result := '骨玉权杖';
                15 : result := '裁决之杖';
            end;
        end;
        2 : 
        begin
            case ZBlvId of
                1 : result := '圣战头盔';
                2 : result := '圣战项链';
                3 : result := '圣战手镯';
                4 : result := '圣战戒指';
                5 : result := '法神头盔';
                6 : result := '法神项链';
                7 : result := '法神手镯';
                8 : result := '法神戒指';
                9 : result := '天尊头盔';
                10 : result := '天尊项链';
                11 : result := '天尊手镯';
                12 : result := '天尊戒指';
                13 : result := '钢铁腰带';
                14 : result := '避魂靴';
				15 : result := '逍遥扇';
                16 : result := '屠龙';
                17 : result := '嗜魂法杖';
				18 : result := '天魔神甲';
				19 : result := '天尊道袍';
				20 : result := '圣战宝甲';
				21 : result := '霓裳羽衣';
				22 : result := '天师长袍';
				23 : result := '法神披风';
            end;
        end;
		        3 : 
        begin
            case ZBlvId of
                1 : result := '勇士头盔';
                2 : result := '魔幻项链';
                3 : result := '魔幻手镯';
                4 : result := '魔幻戒指';
                5 : result := '骷髅骨';
                6 : result := '道域头盔';
                7 : result := '白银项链';
                8 : result := '白银手镯';
                9 : result := '白银戒指';
                10 : result := '吉娃娃';
                11 : result := '宙神头盔';
                12 : result := '浮游手镯';
                13 : result := '浮游戒指';
                14 : result := '浮游项链';
                15 : result := '情人花';
				16 : result := '圣龙魔袍(男)';
                17 : result := '圣龙魔袍(女)';
                18 : result := '白玉法杖';
                19 : result := '泰坦道衣(男)';
				20 : result := '泰坦道衣(女)';
				21 : result := '黑铁银蛇';
				22 : result := '钢盔战甲(男)';
				23 : result := '钢盔战甲(女)';
				24 : result := '绿玉裁决';
            end;
        end;
	   4 : 
        begin
            case ZBlvId of
                1 : result := '朱雀头盔';
                2 : result := '摄魂项链';
                3 : result := '摄魂手镯';
                4 : result := '摄魂戒指';
                5 : result := '魔法勋章';
                6 : result := '贝迪头盔';
                7 : result := '道尊项链';
                8 : result := '道尊手镯';
                9 : result := '道尊戒指';
                10 : result := '真爱钻石';
                11 : result := '黑暗头盔';
                12 : result := '铁血项链';
                13 : result := '铁血手镯';
                14 : result := '铁血戒指';
                15 : result := '灵魂钻石';
				16 : result := '朱雀神袍(男)';
                17 : result := '朱雀神袍(女)';
                18 : result := '蓝灵法杖';
                19 : result := '青龙道衣(男)';
				20 : result := '青龙道衣(女)';
				21 : result := '金域无极';
				22 : result := '白虎战甲(女)';
				23 : result := '白虎战甲(男)';
				24 : result := '绿玉屠龙';
		   end;
		   end;
		  5 : 
        begin
            case ZBlvId of
                1 : result := '风铃火山';
                2 : result := '魔天项链';
                3 : result := '魔天手镯';
                4 : result := '魔天戒指';
                5 : result := '卦';
                6 : result := '蓝灵头盔';
                7 : result := '蓝灵项链';
                8 : result := '蓝灵手环';
                9 : result := '蓝灵戒指';
                10 : result := '凤';
				11 : result := '将军头盔';
				12 : result := '将军项链';
				13 : result := '将军手镯';
				14 : result := '将军戒指';
				15 : result := '龙';
				16 : result := '猩猩魔袍(男)';
                17 : result := '猩猩魔袍(女)';
                18 : result := '摄魂之剑';
                19 : result := '麒麟道衣(男)';
				20 : result := '麒麟道衣(女)';
				21 : result := '仙人蒲扇';
				22 : result := '铁骑战甲(男)';
				23 : result := '铁骑战甲(女)';
				24 : result := '战神之刃';
		   end;
		   end;
		 6 : 
        begin
            case ZBlvId of
                1 : result := '龙皇霸气戒(战)';
                2 : result := '龙皇霸气戒(法)';
                3 : result := '龙皇霸气戒(道)';
                4 : result := '龙皇霸气镯(战)';
                5 : result := '龙皇霸气镯(法)';
                6 : result := '龙皇霸气镯(道)';
                7 : result := '龙皇霸气链(战)';
                8 : result := '龙皇霸气链(法)';
                9 : result := '龙皇霸气链(道)';
                10 : result := '龙皇霸气盔(战)';
				11 : result := '龙皇霸气盔(法)';
				12 : result := '龙皇霸气盔(道)';
				13 : result := '龙皇霸气心';
				14 : result := '龙皇霸龙甲(法)';
                15 : result := '龙皇霸凤铠(法)';
                16 : result := '龙皇霸气法剑';
				17 : result := '龙皇霸龙甲(道)';
                18 : result := '龙皇霸凤铠(道)';
                19 : result := '龙皇霸气道剑';
				20 : result := '龙皇霸龙甲(战)';
                21 : result := '龙皇霸凤铠(战)';
                22 : result := '龙皇霸气战剑';
		   end;
		   end;
		   7 : 
        begin
            case ZBlvId of
                1 : result := '不灭V神戒(战)';
                2 : result := '不灭V神镯(战)';
                3 : result := '不灭V神盔(战)';
                4 : result := '不灭V神链(战)';
                5 : result := '不灭V神灵';
				6 : result := '不灭V神戒(道)';
                7 : result := '不灭V神镯(道)';
                8 : result := '不灭V神链(道)';
                9 : result := '不灭V神盔(道)';
                10 : result := '不灭V神戒(法)';
				11 : result := '不灭V神镯(法)';
				12 : result := '不灭V神链(法)';
				13 : result := '不灭V神盔(法)';
				14 : result := '不灭V冰火战刀';
                15 : result := '不灭V紫神甲(战)';
                16 : result := '不灭V紫神铠(战)';
				17 : result := '不灭V冰火道刀';
                18 : result := '不灭V紫神甲(道)';
                19 : result := '不灭V紫神铠(道)';
				20 : result := '不灭V冰火法刀';
                21 : result := '不灭V紫神甲(法)';
                22 : result := '不灭V紫神铠(法)';
				
		   end;
		   end;
		   8 : 
        begin
            case ZBlvId of
                1 : result := '辉煌V神戒(战)';
                2 : result := '辉煌V神镯(战)';
                3 : result := '辉煌V神链(战)';
                4 : result := '辉煌V神冠(战)';
                5 : result := '辉煌神魄心';
				6 : result := '辉煌V神戒(道)';
                7 : result := '辉煌V神镯(道)';
                8 : result := '辉煌V神链(道)';
                9 : result := '辉煌V神冠(道)';
                10 : result := '辉煌V神戒(法)';
				11 : result := '辉煌V神镯(法)';
				12 : result := '辉煌V神链(法)';
				13 : result := '辉煌V神冠(法)';
				14 : result := '辉煌炽火战剑';
                15 : result := '辉煌寒冰甲(战)';
                16 : result := '辉煌寒冰铠(战)';
				17 : result := '辉煌炽火法剑';
                18 : result := '辉煌炽火道剑';
                19 : result := '辉煌寒冰甲(道)';
				20 : result := '辉煌寒冰甲(法)';
                21 : result := '辉煌寒冰铠(道)';
                22 : result := '辉煌寒冰铠(法)';
		   end;
		   end;
		   9 : 
        begin
            case ZBlvId of
				1 : result := '灭世神威戒';
                2 : result := '灭世神威镯';
                3 : result := '灭世神威链';
                4 : result := '灭世神威盔';
                5 : result := '灭世神威勋';
                6 : result := '灭世天神掌';
                7 : result := '灭世蝴蝶甲';
				8 : result := '灭世蝴蝶铠';

				
		   end;
		   end;
		   10 : 
        begin
            case ZBlvId of
				1 : result := '凤凰の飞戒';
                2 : result := '凤凰の飞镯';
                3 : result := '凤凰の飞链';
                4 : result := '凤凰の飞盔';
                5 : result := '凤凰の飞勋';
                6 : result := '破馆珍剑';
                7 : result := '凤凰の飞甲';
				8 : result := '凤凰の飞铠';
		   end;
		   end;
		   11 : 
        begin
            case ZBlvId of
                1 : result := '诅咒の戒';
                2 : result := '诅咒の镯';
                3 : result := '诅咒の链';
                4 : result := '诅咒の盔';
                5 : result := '诅咒の勋';
                6 : result := '魅影之刃';
                7 : result := '诅咒の甲'; 		
                8 : result := '诅咒の铠';
	   								
		   end;
		   end;
		   12 : 
        begin
            case ZBlvId of
                1 : result := '荣耀神剑';
                2 : result := '荣耀神甲(男)';
                3 : result := '荣耀神甲(女)';
                4 : result := '金牛战剑';
                5 : result := '金牛道扇';
                6 : result := '金牛魔杖';
                7 : result := '金牛战甲(男)';
                8 : result := '金牛战甲(女)';
                9 : result := '金牛魔衣(男)';
               10 : result := '金牛魔衣(女)';
               11 : result := '金牛道袍(男)';
               12 : result := '金牛道袍(女)';
				
		   end;
		   end;
    end;
end;



function getZexpNum(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 30000;
        2 : result := 60000;
        3 : result := 150000;
	    4 : result := 350000;
        5 : result := 800000;
        6 : result := 1500000;
		7 : result := 3000000;
        8 : result := 4000000;
	    9 : result := 7000000;
       10 : result := 10000000;
       11 : result := 15000000;
	   12 : result := 15000000;

    end;
end;

function getZYBNum(Zlv : integer) : integer;
begin
    case Zlv of
        1 : result := 0;
        2 : result := 3;
        3 : result := 15;
	    4 : result := 25;
        5 : result := 35;
        6 : result := 75;
		7 : result := 150;
        8 : result := 300;
	    9 : result := 500;
       10 : result := 700;
       11 : result := 900;
	   12 : result := 1000;
    end;
end;


procedure _1_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件祖玛系列装备，回收可获得:' + inttostr(getZexpNum(1) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(1) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@1_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _1_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(1));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(1));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;

procedure _11_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件赤月系列装备，回收可获得:' + inttostr(getZexpNum(2) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(2) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@11_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _11_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(2));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(2));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _2_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件40级系列装备,回收可获得:' + inttostr(getZexpNum(3) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(3) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@2_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _2_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(300 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(3));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(3));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _22_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件45级系列装备回收可获得:' + inttostr(getZexpNum(4) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(4) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@22_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _22_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(400 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(4));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(4));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _3_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件50级系列装备回收可获得:' + inttostr(getZexpNum(5) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(5) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@3_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _3_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(500 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(5));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(5));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;



procedure _33_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件55级系列装备回收可获得:' + inttostr(getZexpNum(6) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(6) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@33_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _33_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(600 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(6));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(6));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _4_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件65级系列装备回收可获得:' + inttostr(getZexpNum(7) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(7) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@4_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _4_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(700 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(7));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(7));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _44_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件70级系列装备回收可获得:' + inttostr(getZexpNum(8) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(8) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@44_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _44_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(800 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(8));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(8));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _5_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(900 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件80级系列,回收可获得:' + inttostr(getZexpNum(9) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(9) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@5_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _5_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(900 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(9));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(9));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _55_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(1000 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件90级系列装备，回收可获得:' + inttostr(getZexpNum(10) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(10) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@55_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _55_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(1000 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(10));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(10));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _6_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(1100 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件100级系列装备，回收可获得:' + inttostr(getZexpNum(11) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(11) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收/@6_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _6_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(1100 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(11));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(11));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;


procedure _66_all;
var ZMnum , i : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(1200 + i);
        if Iname <> '' then
        begin
            if This_Player.GetBagItemCount(Iname) > 0 then
            begin
                ZMnum := ZMnum + This_Player.GetBagItemCount(Iname);
            end;
        end; 
    end;
    
   This_NPC.NpcDialog(This_Player,
     '你的包裹中有' + inttostr(ZMnum) + '件金牛和荣耀剑甲，回收可获得:' + inttostr(getZexpNum(12) div 10000 * ZMnum) + '万经验，' + inttostr(getZYBNum(12) * ZMnum) + '个元宝\|'
    +'|{cmd}<确认回收所有金牛和荣耀剑甲/@66_True>'
    +'|{cmd}<返回/@main>');
end;

procedure  _66_True;
var ZMnum , i , j , itemNum : integer;
Iname : string;
begin
    ZMnum := 0;
    for i := 1 to 40 do
    begin
        Iname := getZBnameById(1200 + i);
        if Iname <> '' then
        begin
            itemNum := This_Player.GetBagItemCount(Iname);
            if itemNum > 0 then
            begin
                ZMnum := ZMnum + itemNum;
                This_Player.Take(Iname, itemNum);
                
                for j := 1 to itemNum do
				This_Player.ScriptRequestAddYBNum(getZYBNum(12));
				begin
				for j := 1 to itemNum do
                This_Player.Give('经验', getZexpNum(12));								
            end;
        end; 
    end;
	
   This_NPC.NpcDialog(This_Player,
    '恭喜你成功回收！' 
    +'|{cmd}<返回/@main>');
end;
end;



Begin

  domain;

end.