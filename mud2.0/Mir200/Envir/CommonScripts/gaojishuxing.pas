procedure SetAbil(AbilType, value:Integer);
//SetAbil函数内代码尽量不要修改,直接调用即可;
var H8, L8 : Integer;
begin
	if value < 65536 then
	begin
		H8 := value / 256;
		L8 := value mod 256;
		//下面这行是调试输出语句,正式使用时可屏蔽;
	//	This_Player.PlayerNotice('AbilType='+inttostr(AbilType)+';value='+inttostr(value)+';H8='+inttostr(H8)+';L8='+inttostr(L8),0);
		case AbilType of
			1://倍攻
			begin
			This_Player.AuthByHelped(6212 + 0,L8);
			end;
			2://备用1
			begin
			This_Player.AuthByHelped(6212 + 1,L8);	
			end;
			3://备用2
			begin
			This_Player.AuthByHelped(6212 + 2,L8);
			end;
			4://备用3
			begin
			This_Player.AuthByHelped(6212 + 3,L8);
			end;
			5://MAXHP
			begin
			This_Player.AuthByHelped(6212 + 4,L8);
			This_Player.AuthByHelped(6212 + 5,H8);
			end;
			6://MAXMP
			begin
			This_Player.AuthByHelped(6212 + 6,L8);
			This_Player.AuthByHelped(6212 + 7,H8);
			end;
			7://MAX攻击
			begin
			This_Player.AuthByHelped(6212 + 8,L8);
			This_Player.AuthByHelped(6212 + 20,H8);
			end;
			8://MAX魔法
			begin
			This_Player.AuthByHelped(6212 + 9,L8);
			This_Player.AuthByHelped(6212 + 21,H8);
			end;
			9://MAX道术
			begin
			This_Player.AuthByHelped(6212 + 10,L8);
			This_Player.AuthByHelped(6212 + 22,H8);
			end;
			10://MAX防御
			begin
			This_Player.AuthByHelped(6212 + 11,L8);
			This_Player.AuthByHelped(6212 + 23,H8);
			end;
			11://MAX魔御
			begin
			This_Player.AuthByHelped(6212 + 12,L8);
			This_Player.AuthByHelped(6212 + 13,H8);
			end;
			12://备用4
			begin
			This_Player.AuthByHelped(6212 + 14,L8);
			end;
			13://备用5
			begin
			This_Player.AuthByHelped(6212 + 15,L8);
			end;
			14://刺术上限
			begin
			This_Player.AuthByHelped(6212 + 16,L8);
			This_Player.AuthByHelped(6212 + 17,H8);
			end;
			15://刺术下限
			begin
			This_Player.AuthByHelped(6212 + 18,L8);
			This_Player.AuthByHelped(6212 + 19,H8);
			end;
			16://百分比免伤
			begin
			This_Player.AuthByHelped(1400 + 0,L8);
			end;			
			17://死亡防爆(小退属性消失,需要每次重新设置)
			begin
			This_Player.AuthByHelped(396 + 0,L8);
			This_Player.AuthByHelped(396 + 1,H8);
			end;
			18://杀人爆率(小退属性消失,需要每次重新设置)
			begin
			This_Player.AuthByHelped(1401 + 0,L8);
			end;
		end;	
    end; 
end;

procedure TZJH_NPC1;

var  shanghai ,baoji ,mianshang ,baolv ,sharen ,fangbao ,hp,dc,mc,sc : integer;
begin 	
 if This_Player.GetV(41,3)  < 0 then This_Player.SetV(41,3,0);
 if This_Player.GetV(41,4)  < 0 then This_Player.SetV(41,4,0);
 if This_Player.GetV(41,5)  < 0 then This_Player.SetV(41,5,0);
 if This_Player.GetV(41,6)  < 0 then This_Player.SetV(41,6,0);
 if This_Player.GetV(41,7)  < 0 then This_Player.SetV(41,7,0);
 if This_Player.GetV(41,8)  < 0 then This_Player.SetV(41,8,0); 
 if This_Player.GetV(41,9)  < 0 then This_Player.SetV(41,9,0);
 if This_Player.GetV(42,1)  < 0 then This_Player.SetV(42,1,0);
 if This_Player.GetV(42,2)  < 0 then This_Player.SetV(42,2,0);
 if This_Player.GetV(42,5)  < 0 then This_Player.SetV(42,5,0);
 if This_Player.GetV(42,6)  < 0 then This_Player.SetV(42,6,0);
 if This_Player.GetV(42,7)  < 0 then This_Player.SetV(42,7,0);
 if This_Player.GetV(42,8)  < 0 then This_Player.SetV(42,8,0);
 if This_Player.GetV(42,9)  < 0 then This_Player.SetV(42,9,0);
 if This_Player.GetV(170,1) < 0 then This_Player.SetV(170,1,0);	
 if This_Player.GetV(170,2) < 0 then This_Player.SetV(170,2,0);	
 if This_Player.GetV(170,3) < 0 then This_Player.SetV(170,3,0);	
 if This_Player.GetV(170,4) < 0 then This_Player.SetV(170,4,0);	
 if This_Player.GetV(170,5) < 0 then This_Player.SetV(170,5,0);
 if This_Player.GetV(170,6) < 0 then This_Player.SetV(170,6,0);	
 if This_Player.GetV(170,7) < 0 then This_Player.SetV(170,7,0);	
 if This_Player.GetV(170,8) < 0 then This_Player.SetV(170,8,0);
 if This_Player.GetV(170,9) < 0 then This_Player.SetV(170,9,0);
 if This_Player.GetV(170,10) < 0 then This_Player.SetV(170,10,0);
  if This_Player.GetV(170,11) < 0 then This_Player.SetV(170,11,0);
   if This_Player.GetV(170,12) < 0 then This_Player.SetV(170,12,0);
    if This_Player.GetV(170,13) < 0 then This_Player.SetV(170,13,0);
 if This_Player.GetV(166,181) < 0 then This_Player.SetV(166,181,0);	
 if This_Player.GetV(166,182) < 0 then This_Player.SetV(166,182,0);	
 if This_Player.GetV(166,183) < 0 then This_Player.SetV(166,183,0);	
 if This_Player.GetV(166,184) < 0 then This_Player.SetV(166,184,0);	
 if This_Player.GetV(166,185) < 0 then This_Player.SetV(166,185,0);
 if This_Player.GetV(166,186) < 0 then This_Player.SetV(166,186,0);	
 if This_Player.GetV(166,187) < 0 then This_Player.SetV(166,187,0);	
 if (This_Player.GetV(166,181)) and  (This_Player.GetV(166,182)) and  (This_Player.GetV(166,183)) and  (This_Player.GetV(166,184)) and  (This_Player.GetV(166,185)) and  (This_Player.GetV(166,186)) and  (This_Player.GetV(166,187)) = 1 then 
begin
//This_Player.SetV(170,1,This_Player.GetV(170,1)+20);
//This_Player.SetV(170,2,This_Player.GetV(170,2)+10);
//This_Player.SetV(170,6,This_Player.GetV(170,6)+50);
end;

	   	 SetAbil(1,0);
		 SetAbil(14,0);
	     SetAbil(16,0);
		 SetAbil(15,0);
		 SetAbil(7,0);
	     SetAbil(5,0);
		 SetAbil(10,0);	   
	shanghai := This_Player.GetV(170,2) + This_Player.GetV(170,3);
	baoji := This_Player.GetV(170,1);
	mianshang := This_Player.GetV(170,5);
	baolv := This_Player.GetV(170,10);
	sharen := This_Player.GetV(170,4);
	fangbao := This_Player.GetV(170,7);
	hp := This_Player.GetV(170,9);
	
	dc := This_Player.GetV(170,11);
	mc := This_Player.GetV(170,12);
	sc := This_Player.GetV(170,13);
		 
	   	 SetAbil(1, shanghai);
		 SetAbil(14, baoji);
	     SetAbil(16, mianshang);
		 SetAbil(15, baolv);
		 SetAbil(17, fangbao);
		 SetAbil(18, sharen);
		 SetAbil(5, hp);
		 SetAbil(7,dc);
		 SetAbil(8,mc);
		 SetAbil(9,sc);
if This_Player.GetV(41,66) > 0 then
begin
This_player.SetS(36,16,2); 
This_Player.LingXiValue := This_Player.GetS(36,16);
end else
begin
This_player.SetS(36,16,0); 
This_Player.LingXiValue := This_Player.GetS(36,16);
end;
end;

