//字符串分割函数
function GetParam(param,plus:string;index:integer):string; 
var 
	i , len , paramsCount , startIndex:integer;
begin
	result := '';
	if param = '' then
		exit;
	startIndex := 1;
	len := Length(param);
	paramsCount := 1;
	for i:=1 to len do
	begin
		if (param[i] = plus) or (i=len) then
		begin
			if index = paramsCount then
			begin
				if i = len then
				begin
					result := copy(param, startIndex, len);
				end else
				begin
					result := copy(param, startIndex, i-startIndex);
				end;
				exit; 
			end; 
			startIndex := i+1;
			paramsCount := paramsCount+1;
		end;
	end;
end;

//滚屏函数
procedure ServerSayPlus(content:string);
var wordlen, time, endpoint, txtlen:integer;
begin
	wordlen := 21; 								//单个汉字长度
	time := 5;									//默认滚屏消失时间，10s
	endpoint := -400;							//默认滚屏移动到消失的位置
	txtlen := Length(content);					//计算滚屏公告文字长度
	time := time + Round(txtlen * 0.4);			//计算所需时长，这个自己慢慢调节
	endpoint := endpoint - txtlen * wordlen;	//根据实际长度计算滚屏消失位置
	ServerSay('NC=Bg:alert:bg:center:0:nil:250|AutoHide:'+inttostr(time)+'|1:Text:1000:nil:18:'+content+'@243:left:'+inttostr(endpoint)+':nil:'+inttostr(time)+'', 5);
end;

//控制打开或关闭传统面板
procedure togglePanel(panel:string);
begin
	This_Player.PlayerNotice('TP=' + panel,5);
end;

//控制打开或关闭自定义面板
procedure toggleDTP(panel, jsonFile:string);
begin
	This_Player.PlayerNotice('DTP=' + panel + ':' + jsonFile,5);
end;

//小地图扩展，可用于显示自定义传送
procedure _bigmapExtend(id:string);
begin
	//如果是开区地图，则扩展不生效
	if This_Player.MapName = 'D5071~31' then
	begin
		exit;
	end;
	case id of 
		'1':
		begin
			This_Player.PlayerNotice('MP=1:RBtn:wp:labelnbiqi:410:770:@goworld~0|1:RBtn:wp:labelnmengzhong:600:560:@goworld~3',5);
			This_Player.PlayerNotice('MP=1:RBtn:wp:labelnbairi:310:480:@goworld~11',5);
			This_Player.PlayerNotice('MP=1:RBtn:wp:labelnmolong:820:740:@goworld~6|1:RBtn:wp:labelncangyue:320:300:@goworld~5',5);
			This_Player.PlayerNotice('MP=1:RBtn:wp:labelnfengmo:210:550:@goworld~4|1:RBtn:wp:labelnwoma:330:600:@goworld~1',5);
			This_Player.PlayerNotice('MP=1:RBtn:wp:labelndushe:580:700:@goworld~2',5);
		end;
		'0':
		begin 
			case This_Player.MapName of
				'3':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:530:50:@flyto~846~230|1:Text:530:50:14:抓毒宠区@251',5);
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:80:40:@flyto~145~93|1:Text:80:40:14:抓蛛王区@251',5);
					This_Player.PlayerNotice('MP=1:RBtn:wp:p:200:130:@flyto~337~371|1:Text:200:130:14:土城@250',5);
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:380:100:@flyto~676~335|1:Text:380:100:14:沙巴克@250',5);
					This_Player.PlayerNotice('MP=1:RBtn:wp:p:320:290:@flyto~518~768|1:Text:320:290:14:毒蛇山谷@250|1:RBtn:wp:p:170:290:@flyto~279~730|1:Text:170:290:14:毒蛇山谷@250',5);
				end;
				'2':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:p:570:30:@flyto~551~89|1:Text:570:30:14:土城@250|1:RBtn:wp:p:300:30:@flyto~290~70|1:Text:300:30:14:土城@250|1:RBtn:wp:p:420:380:@flyto~446~551|1:Text:420:380:14:比奇省@250', 5);
				end;
				'0':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:p:280:20:@flyto~321~48|1:Text:280:20:14:沃玛森林@250|1:RBtn:wp:p:590:30:@flyto~661~102|1:Text:590:30:14:毒蛇山谷@250', 5);
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:585:100:@flyto~658~219|1:Text:585:100:14:废矿@251|1:RBtn:wp:atk:120:30:@flyto~139~42|1:Text:120:30:14:兽人古墓@251|1:RBtn:wp:atk:45:70:@flyto~50~117|1:Text:45:70:14:骷髅洞@251', 5);
					This_Player.PlayerNotice('MP=1:RBtn:wp:p:545:325:@flyto~620~596|1:Text:545:325:14:新手村@250|1:RBtn:wp:p:242:325:@flyto~276~598|1:Text:242:325:14:边界村@250', 5);
				end;
				'1':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:p:575:350:@flyto~561~540|1:Text:575:350:14:比奇省@250|1:RBtn:wp:p:50:10:@flyto~55~40|1:Text:50:10:14:白日门@250|1:RBtn:wp:p:50:90:@flyto~59~165|1:Text:50:90:14:封魔谷@250', 5);
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:85:170:@flyto~93~285|1:Text:85:170:14:沃玛寺庙@251', 5);
				end;
				'11':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:430:170:@flyto~357~247|1:Text:430:170:14:丛林迷宫@251|1:RBtn:wp:p:50:370:@flyto~45~412|1:Text:50:370:14:沃玛森林@251', 5);
				end;
				'4':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:p:553:130:@flyto~431~192|1:Text:553:130:14:沃玛森林@250|1:RBtn:wp:atk:75:40:@flyto~63~73|1:Text:75:40:14:封魔矿区@251', 5);
				end;
				'5':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:420:50:@flyto~544~133|1:Text:420:50:14:骨魔洞@251|1:RBtn:wp:atk:510:220:@flyto~661~465|1:Text:510:220:14:牛魔寺庙@251|1:RBtn:wp:atk:400:290:@flyto~520~617|1:Text:400:290:14:尸魔洞@251', 5);
				end;
				'6':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:570:40:@flyto~458~79|1:Text:570:40:14:魔龙西郊@251|1:RBtn:wp:atk:575:285:@flyto~470~378|1:Text:575:285:14:魔龙东郊@251', 5);
				end;
				'12':
				begin
					This_Player.PlayerNotice('MP=1:RBtn:wp:atk:20:245:@flyto~21~301|1:Text:20:245:14:赤月南@251|1:RBtn:wp:atk:435:90:@flyto~326~126|1:Text:435:90:14:赤月东@251|1:RBtn:wp:atk:130:10:@flyto~97~21|1:Text:130:10:14:赤月北@251|1:RBtn:wp:atk:437:410:@flyto~322~469|1:Text:437:410:14:白日门@251', 5);
				end;
			end;
		end;
	end;
end;

//人物界面扩展初始化
procedure _equipExtend;
begin
	This_Player.PlayerNotice('EQP=1:Spr-sz:sz-preview:1:8:200:-380:0.08:0.2:已佩戴时装@243\佩戴可获得一体时装特效@251\收集时装也可增强自身属性@251:取下@hidesz',5);
end;

//隐藏时装
procedure _hidesz;
begin
	This_Player.PlayerNotice('EQP=0:Spr-sz',5);
end;



//更换地图脚本触发函数
procedure _onChangeMap;
begin
	//例如，跳出某个地图后，隐藏某些UDP元素。通常用于副本地图特殊展示
	if CompareText(This_Player.MapName,'d5071~17') <> 0 then
	begin
		This_Player.PlayerNotice('UGP=0:Img-m',5);
		This_Player.PlayerNotice('UGP=0:Text-1',5);
		This_Player.PlayerNotice('UGP=0:Text-2',5);
		This_Player.PlayerNotice('UGP=0:Text-3',5);
		This_Player.PlayerNotice('UGP=0:Text-4',5);
	end;
end;

//寻路地图中某个坐标
procedure walkAnywhare(mapid,x,y:string);
begin
	This_Player.PlayerNotice('GO:' + mapid + ':' + x + ':' + y, 5);
end;

//世界地图直飞
procedure _goworld(map:string);
begin
	case map of
		'0':This_Player.Flyto(map,331+random(3)-1,268+random(3)-1);
		'1':This_Player.Flyto(map,307+random(3)-1,62+random(3)-1);
		'2':This_Player.Flyto(map,500+random(3)-1,485+random(3)-1);
		'3':This_Player.Flyto(map,337+random(3)-1,371+random(3)-1);
		'4':This_Player.Flyto(map,242+random(3)-1,202+random(3)-1);
		'5':This_Player.Flyto(map,142+random(3)-1,331+random(3)-1);
		'6':This_Player.Flyto(map,128+random(3)-1,150+random(3)-1);
		'11':This_Player.Flyto(map,176+random(3)-1,326+random(3)-1);
	end;
	//隐藏大地图
	togglePanel('bigmap');
end;

//寻路
procedure _runtopos(mappos:string);
var mapid, x,y : string;
begin
	mapid := GetParam(mappos,'~',1);
	x := GetParam(mappos,'~',2);
	y := GetParam(mappos,'~',3);
	walkAnywhare(mapid, x, y);
end;

//直传
procedure _flyto(pos:string);
var x,y : integer;
begin
	if This_Player.GetBagItemCount('小飞鞋') >= 1 then
	begin
		This_Player.Take('小飞鞋',1);
		x := strtointdef(GetParam(pos,'~',1),2);
		y := strtointdef(GetParam(pos,'~',2),2);
		This_Player.Flyto(This_Player.MapName,x+random(3),y+random(3));
		This_Player.PlayerNotice('消耗1个小飞鞋', 1);
		togglePanel('bigmap');
	end else
	begin
		_runtopos(This_Player.MapName + '~' + pos);
		togglePanel('bigmap');
		This_Player.PlayerNotice('小飞鞋不足，寻路前往', 5);
	end;
end;

//背包扩展初始化
procedure _bagExtend;
begin
	//打开背包时的扩展，指令：BGP  见说明书
end;

//远程仓库扩展
procedure _cangku;
begin
	This_Player.NewBieGiftConsume;
end;

//timer扩展例子：自定义定时任务 upgrade
//在脚本任意位置调用指令：
//This_Player.PlayerNotice('TIMER=1:upgrade:1',5);
//指令中，第二个参数是 upgrade，对应此扩展函数为 _upgrade
procedure _upgrade;
begin
	if This_Player.Level < 55 then
	begin
		//判断是否为新手地图
		if CompareText(This_Player.MapName,'0122~2') = 0 then
		begin
			This_Player.Give('经验',38888);
		end; 
	end else   
	begin
		//停止时钟
		This_Player.PlayerNotice('TIMER=0:upgrade',5);
		This_Player.FlyTo('3', 337, 373);
		This_Player.PlayerNotice('恭喜你达到55级,可以独闯大陆了。',5);  
	end; 
end;

//吸血示例，通过此函数，可以在 攻击对方并造成伤害时触发
//此功能需要服务端下发状态才能生效
//下发方式：
//开启触发：This_Player.PlayerNotice('STATUS=Recover:1',5);
//关闭触发：This_Player.PlayerNotice('STATUS=Recover:0',5);
//你可以在用户穿戴某个装备是，下发以上状态，下发后则可实现攻击触发
procedure _canRecover(hp:string);
var i,ihp,rechp,currhp :integer;
	rcp:double;
	chibang , wuqi , xunzhang , douli , yifu , shizhuang , zuos , yous , zuoj , youj , xuezi , yaodai , fudu , xueshi , xianglian,dunpai : string;
begin
    //0衣服 1武器 2勋章 3项链 4头盔 5左手镯 6右手镯  7左戒指 8右戒指  9毒符 10腰带 11靴子 13斗笠  29血石 
	//14玉佩（29）  15 盾牌（34）
	chibang := This_Player.GetItemNameOnBody(4); 
	shizhuang := This_Player.GetItemNameOnBody(14);
	zuos := This_Player.GetItemNameOnBody(5);
	yous := This_Player.GetItemNameOnBody(6);
	zuoj := This_Player.GetItemNameOnBody(7);
	youj := This_Player.GetItemNameOnBody(8);
	xuezi := This_Player.GetItemNameOnBody(11);
	yaodai := This_Player.GetItemNameOnBody(10);
	xianglian := This_Player.GetItemNameOnBody(3);
	wuqi := This_Player.GetItemNameOnBody(1);
	yifu := This_Player.GetItemNameOnBody(0);
	fudu := This_Player.GetItemNameOnBody(9);
	dunpai := This_Player.GetItemNameOnBody(15);
	douli := This_Player.GetItemNameOnBody(13);
    xunzhang := This_Player.GetItemNameOnBody(2);
	xueshi := This_Player.GetItemNameOnBody(12);
	
    rcp := 0;
	
	if fudu = '精品嗜血[狂乱]' then rcp := rcp + 2;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '精品嗜血[狂暴]' then rcp := rcp + 4;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '精品嗜血[命器]' then rcp := rcp + 6;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '精品嗜血[主宰]' then rcp := rcp + 8;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '精品嗜血[神器]' then rcp := rcp + 10;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[青]' then rcp := rcp + 7;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[墨]' then rcp := rcp + 8;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[乌]' then rcp := rcp + 9;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[阴]' then rcp := rcp + 10;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[杀]' then rcp := rcp + 11;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[黯]' then rcp := rcp + 12;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[狂]' then rcp := rcp + 13;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[暴]' then rcp := rcp + 14;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[霸]' then rcp := rcp + 15;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[主]' then rcp := rcp + 17;      //如果佩戴了xx翅膀则增加10%吸血
	if fudu = '嗜血铭文[暗金]' then rcp := rcp + 20;      //如果佩戴了xx翅膀则增加10%吸血
	if wuqi = '赞助剧毒斩' then rcp := rcp + 2;      //如果佩戴了屠龙则增加10%吸血
	

	if hp <> nil then
	begin
		if rcp > 0 then
		begin
			rechp := integer(trunc(StrToFloat(hp) * (rcp / 100)));
			This_Player.DoScriptHpMpRecover(rechp, 0, 0, 0);
		end;
	end;
end;
 
 
 
 
 
 
 
 