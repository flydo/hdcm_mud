local config = require "config.application"
local mysql = require("resty.mysql")  
local json = require "cjson.safe"
local errcode = require "errorcode"
local m = {}

local timeout = config.timeout or 10000
local max_idle_time = config.max_idle_time or 10000
local pool_size = config.pool_size or 20
local props = {host = config.host, port = config.port,database = config.database,user = config.uid,password = config.password}  

local function close_db(db)  
    if not db then return  end  
	db:close()  
end  

local function get_db()
	local db,err = mysql:new()
	if not db then 
		ngx.log(ngx.ERR, "new  mysql failed:" .. err)
		return nil
	else
		db:set_timeout(timeout)	
		local res, er,errno,sqlState = db:connect(props)
		if not res then 
			ngx.log(ngx.ERR, "get connect failed" .. er)
			close_db(db)
			return nil
		end	
		return db
	end		
end

local function get_data(tableName ,where)
	if tableName == "" or where == "" then return nil, errcode.ERR_NONE_PARAM end
	local row=nil
	local errstr = errcode.ERR_INNER_FAILED
	local db =get_db()
	if db then		
		local strSql= string.format("select * from %s where %s",tableName,where)
		res, err,errno,sqlState= db:query(strSql)	
		if not res then
			ngx.log(ngx.ERR, "get_data failed:" .. strSql)
		else 
			row=res
			errstr=errcode.ERR_OK
		end		
		close_db(db)
	end	
	return row, errstr
end 
 
local function get_OneData(tableName ,where)
	local errstr=errcode.ERR_INNER_FAILED
	local row=nil
	local res ,err  =get_data(tableName ,where)
	if res then		
		local count=0
		for i,r in ipairs(res) do
			count=count+1		
		end		
		if  count > 0 then
			row=res[1]
			errstr=errcode.ERR_OK
		end
	end	
	return row,errstr
end 
 
local function update_db(tableName,sets,where)
	if tableName=="" or sets=="" or  where =="" then  return false,errcode.ERR_NONE_PARAM end
	local flag=false
	local errstr = errcode.ERR_INNER_FAILED
	local db =get_db()	
	if db then 			 
		local strSql= string.format("update %s set %s where %s",tableName,sets,where)
		local res, err,errno,sqlState= db:query(strSql)	
		if not res then 
			ngx.log(ngx.ERR, "update_db failed:" .. strSql)
		else
			flag=res.affected_rows
			errstr=errcode.ERR_OK
		end				
		close_db(db)
	end	
	return flag, errstr	
end
 
local function insert_db(tableName,keys,values)
	if tableName=="" or keys=="" or  values =="" then  return false,errcode.ERR_NONE_PARAM end
	local flag=false
	local errstr = errcode.ERR_INNER_FAILED

	local db=get_db()
	if db then 
		local strSql= string.format("insert into %s (%s) values (%s)",tableName,keys,values)
		res, err,errno,sqlState= db:query(strSql)	
		if not res then 
			ngx.log(ngx.ERR, "insert_db failed:" .. strSql)
		else
			flag=res.affected_rows
			errstr=errcode.ERR_OK
		end				
		close_db(db)	
	end	
	return flag, errstr	
end

local function delete_db(tbName,where)
	if tbName=="" or  where =="" then  return false,errcode.ERR_NONE_PARAM end
	local flag=false
	local errstr = errcode.ERR_INNER_FAILED
	
	local db = get_db()
	if db then 
		local strSql= string.format("delete from %s where %s",tbName,where)
		local res, err,errno,sqlState= db:query(strSql)	
		if not res then 
			ngx.log(ngx.ERR, "delete_db failed:" .. strSql)
		else
			flag=res.affected_rows
			errstr=errcode.ERR_OK
		end	
		close_db(db)				
	end
	return flag, errstr	
end

function m.get_acc_guest(machine_id)  				--通过机器编号获取游客账号 不存在则新建 返回该ptid码
	if machine_id == "" then return nil, errcode.ERR_NONE_PARAM end	
	local flag=false
	local errstr = errcode.ERR_INNER_FAILED
	
	local ptid =nil
	local row,err= get_OneData("guest","machine_id=".. ngx.quote_sql_str(machine_id))  -- 防止sql注入	
	if row then
			flag=true
			ptid= row["pt_id"]
			errstr=errcode.ERR_OK				
	else
		--暂时共20位
		ptid = "pt"..string.sub(ngx.md5(machine_id..":/"..ngx.time()), 8, -8)	
		
		-- 检查重复	guest
		local g_flag=false
		local n_flag=false
		local r,er= get_OneData("guest","where pt_id= '"..ptid.."'")			
		if r then g_flag =true end
		
		-- 检查重复	normal	
		r,er= get_OneData("normal","where pt_id= '"..ptid.."'")
		if r then n_flag =true end
		
		if not g_flag and not n_flag then
			r,er = insert_db("guest","pt_id,machine_id,login_time,create_time",string.format("'%s','%s','%s','%s'",ptid,machine_id,ngx.localtime(),ngx.localtime()))
			if r then 
				errstr = errcode.ERR_OK
			else
				ptid = nil
			end	
		else
			ptid=nil
		end
	end
	return ptid, errstr
end

function m.check_acc_normal(id, psw)				--校验正式账号:Login
	if id == "" or psw == "" then return nil, errcode.ERR_NONE_PARAM end
	local errstr = errcode.ERR_INNER_FAILED
	
	local ptid=nil
	local row,err = get_OneData("normal","uid="..ngx.quote_sql_str(id))
	if row then
		if row["password"] == psw then
			ptid= row["pt_id"]
			update_db("normal","login_time='"..ngx.localtime().."'","uid="..ngx.quote_sql_str(id))
		else
			errstr = errcode.ERR_PSW_INVALID
		end
	else
		errstr = errcode.ERR_ID_NOT_EXIST
	end
	return ptid, errstr
end

function m.reg_acc_normal(id, psw, safecode)		--注册正式账号
	if id == "" or psw == "" or safecode == "" then return false, errcode.ERR_NONE_PARAM end
	local flag=false
	local errstr = errcode.ERR_INNER_FAILED
	local db=get_db()
	if db then 				
		--新建数据
		local ptid = "pt"..string.sub(ngx.md5(id..":/"..ngx.time()), 8, -8)
		 -- 防止sql注入
		local strSql= " insert into  normal(pt_id,uid,password,safecode,login_time,create_time) " 
		strSql = strSql .." select '"..ptid.."','"..id.."','"..psw.."','"..safecode.."','"..ngx.localtime().."','"..ngx.localtime().."' from DUAL WHERE NOT EXISTS("
		strSql = strSql .." SELECT uid FROM normal WHERE uid = ".. ngx.quote_sql_str(id) ..")"

		local res, err,errno,sqlState= db:query(strSql)	
		if not res then 
			ngx.log(ngx.ERR, "reg account failed:" .. strSql)
		else
			flag=true
			errstr=errcode.ERR_OK
		end				
		close_db(db)		
	end
	return flag, errstr	
end

function m.modify_acc_psw(id, new_psw, safecode) 	--修改密码,安全码作 原始密码验证
	if id == "" or new_psw == "" or safecode == "" then return false, errcode.ERR_NONE_PARAM end
	local flag=false
	local errstr = errcode.ERR_INNER_FAILED

	local res, err= update_db("normal",string.format("password='%s',safecode='%s',login_time='%s'",new_psw,new_psw,ngx.localtime()),string.format("uid =%s and  password=%s", ngx.quote_sql_str(id),gx.quote_sql_str(safecode)))
	if res then 
		flag=true
		errstr=errcode.ERR_OK
	end
	return flag, errstr	
end

function m.save_ticket(ticket, ptid)       			--保存票据
	if ticket == "" or ptid == "" then return false, errcode.ERR_NONE_PARAM end
	local flag=false
	local errstr = errcode.ERR_INNER_FAILED
	
	local res, err = get_OneData("ticket","ticket="..ngx.quote_sql_str(ticket)) -- 防止sql注入 
	if res then 
		flag = true
		errstr=errcode.ERR_OK   --票据已存在
	else
		local r,er = delete_db("ticket","pt_id="..ngx.quote_sql_str(ptid))
		if r then
		   local ok,err1=insert_db("ticket","pt_id,ticket,create_time","'"..ptid.."','"..ticket.."','"..ngx.time().."'")
			if ok then 
				flag = true
				errstr = errcode.ERR_OK	
			end			
		end
	end	

	return flag, errstr	
end

function m.check_ticket(ticket, exptime)			--检查票据
	if ticket == "" or exptime < 0 then return false, errcode.ERR_NONE_PARAM end

	local errstr = errcode.ERR_INNER_FAILED
	local flag=false
	local row,err = get_OneData("ticket","ticket="..ngx.quote_sql_str(ticket))
	if not row then  
		errstr = errcode.ERR_CHECK_FAILD
	else if ngx.time() - exptime > tonumber(row["create_time"]) then -- 超时,删除票据
			errstr = errcode.ERR_TICKETID_EXPTIME
			delete_db("ticket", " ticket = ".. ngx.quote_sql_str(ticket))
		else
			flag = row["pt_id"]
			errstr = errcode.ERR_OK
		end		
	end		
	return flag, errstr
end

function m.bind_acc_normal(machine_id, new_normal_id, new_psw, safecode)		--绑定正式账号
	--查询游客表是否存在该机器ID
	--查询正式账号表是否存在该新ID
	--创建账号（PTID用游客表的）创建日期也使用游客表的
	--删除游客表信息

	if machine_id == "" or new_normal_id == "" or new_psw == "" or safecode == "" then return false, errcode.ERR_NONE_PARAM end
	local flag = false
	local errstr = errcode.ERR_INNER_FAILED

	local r,er=get_OneData("guest","machine_id="..ngx.quote_sql_str(machine_id))
	if r then		
		local rnew,err= get_OneData("normal","normal_id="..ngx.quote_sql_str(new_normal_id))		
		if not rnew then			
			--新建数据
			local ptid = r["pt_id"]
			local n,err= insert_db("normal","pt_id,uid,password,safecode,login_time,create_time",string.format("'%s','%s','%s','%s','%s','%s'",ptid,new_normal_id,new_psw,safecode,ngx.localtime(),ngx.localtime()))
			if n then 
				flag = true
				errstr = errcode.ERR_OK
				--删除游客表中数据
				n, err = delete_db("guest","id",r["id"])
				if not n then
					ngx.log(ngx.ERR, err)
				end
			else
				ngx.log(ngx.ERR, err) 
			end
		else
			errstr = errcode.ERR_ID_EXIST
		end
	else
		errstr = errcode.ERR_MACHINEID_INVALID
	end
	return flag, errstr	
end

function m.get_userphone(id)
	return ""
end

function m.get_usersecondpwd(userid)
	return ""
end

function m.get_lastloginserver(id)
	local row,err= get_OneData('login', "pt_id="..ngx.quote_sql_str(id))
	if row then
		return row["servername"]		
	end
	return nil
end

function m.set_lastloginserver(id,ticket,name)
	id = tostring(id)
	local row = get_OneData("login","pt_id="..ngx.quote_sql_str(id))
	if row then
		update_db("login",string.format("servername =convert(unhex(hex(convert('%s' using latin1))) using utf8)",name),"pt_id =" ..ngx.quote_sql_str(id))
	else
		insert_db("login","pt_id,servername","'"..id.."','"..name.."'")
	end	
end

return m