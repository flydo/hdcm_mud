//战神引擎 盘古插件
//目前和以后的插件开发 都会基于5月20日发布M2进行
//如果插件提示版本不正确,请确认使用了正确且未被篡改的M2


program mir2; 
var 
X: Integer;

procedure _exit;
begin
	This_Npc.CloseDialog(This_Player);
end; 



//以下命令任务号字段号都是写死的,插件通过监控写入的变量值调用相对应功能


procedure _test1;
begin
	This_Player.SetS(1,1,0);//转战士,转职前需要调用技能删除脚本删除原有技能
end;

procedure _test2;
begin
	This_Player.SetS(1,1,1);//转法师,转职前需要调用技能删除脚本删除原有技能
end;

procedure _test3;
begin
	This_Player.SetS(1,1,2);//转道士,转职前需要调用技能删除脚本删除原有技能
end;

procedure _test4;
begin
	This_Player.SetS(1,1,3);//变性
end;


//攻沙设置需配合NPC定时脚本使用,
procedure _test5;
begin

	SetG(1,1,1);//开始攻沙,这里改为服务器变量,方便NPC定时调用
	This_Player.PlayerNotice('开始攻沙,设置即时生效...',0);

end;

procedure _test6;
begin
	SetG(1,1,2);//结束攻沙,这里改为服务器变量,方便NPC定时调用
	This_Player.PlayerNotice('结束攻沙,设置即时生效...',0);
end;

procedure _test7;
begin
	This_Player.SetS(1,1,6);//给与沙巴克  ,已修复沙巴克不会自动占领bug
end;


//下面演示的是名字变色,第三个参数为颜色值0-255
//将玩家名字颜色存到其他任意变量中
//在登录脚本取出变量,并写到s,1,2变量中
procedure _test8;
var color : Integer;
begin
	color := random(256);
	This_Player.PlayerNotice(inttostr(color),0);
	This_Player.SetS(1,2,color);//设置名字颜色
end;
{
procedure _test9;
begin

	 This_NPC.NpcDialog(This_Player,
   +'<战神引擎插件演示/fcolor=253>||\'
   +'<远程仓库:>|\'
   +'<插件主要解决远离NPC无法存取的问题,开启远程仓库可使用callout循环检测>|\');
   This_Player.SetV(66,6,0);//现将变量初始化
   This_Player.CallOut(0,1,'cang'); //开启循环检测
end;

procedure cang;
begin
    if This_Player.GetV(66,6) = 1 then  //检测变量是否等于1,通过使用背包物品将设置该变量!
	begin
	    This_Player.SetV(66,6,0);
		This_Npc.Click_GetBack(This_Player);
	end;
	This_Player.CallOut(0,1,'cang');
end;
}

procedure _test10;
begin
	This_Player.SetS(1,1,7);//禁言命令   可在登录脚本 升级脚本中 判断等级后调用
end;

procedure _test11;
begin
	This_Player.SetS(1,1,8);//解除禁言   可在登录脚本 升级脚本中 判断等级后调用
end;


procedure _test12;
begin
	This_NPC.NpcDialog(This_Player,
   +'<战神引擎插件演示/fcolor=253>||\'
   +'<行会显示:>|\'
   +'<调用M2原生行会显示功能,最完美显示行会方案,在线玩家须小退才能显示>|\');
end;

procedure _test13;
begin
	This_Player.SetS(1,1,9);//将玩家IP 保存到配置文件 D:\mud2.0\Mir200\Share\config\IPTesting.ini   IPTestingMD5.ini
		This_NPC.NpcDialog(This_Player,
   +'<战神引擎插件演示/fcolor=253>||\'
   +'<玩家IP检测:>|\'
   +'<将玩家IP 保存到配置文件,方便写脚本调用二次验证>|\');
end;


procedure _test14;
begin
	This_Player.StrParam := '1|封号测试设置封号固定写法设置封号固定写法设置封号固定写法' + inttostr(random(10)); //  "1|"设置封号固定写法,"|"后面为封号文本,封号长度<=14(即最多七个汉字)
end;


procedure _test15;
begin
	This_Player.StrParam := '0|取消封号';//取消封号固定写法

end;

procedure _test16;
begin
	if This_Player.HeroLevel > -1 then
	begin
	This_Player.SetS(1,1,10);//英雄转战士,需要用脚本先判断英雄已召唤出,否则无效!
	//This_Player.StrParam := '2|破魂斩';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|噬魂沼泽';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|火龙气焰';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|雷霆一击';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|劈星斩';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|末日审判';// "2|"后面填写要删除的英雄技能
	end else
	begin
	This_Player.PlayerNotice('请先招出英雄!',0);	
	end;
end;

procedure _test17;
begin
	if This_Player.HeroLevel > -1 then
	begin
	This_Player.SetS(1,1,11);//英雄转法师,需要用脚本先判断英雄已召唤出,否则无效!
	//This_Player.StrParam := '2|破魂斩';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|噬魂沼泽';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|火龙气焰';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|雷霆一击';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|劈星斩';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|末日审判';// "2|"后面填写要删除的英雄技能
	end else
	begin
	This_Player.PlayerNotice('请先招出英雄!',0);	
	end;
end;

procedure _test18;
begin
	if This_Player.HeroLevel > -1 then
	begin
	This_Player.SetS(1,1,12);//英雄转道士,需要用脚本先判断英雄已召唤出,否则无效!
	//This_Player.StrParam := '2|破魂斩';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|噬魂沼泽';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|火龙气焰';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|雷霆一击';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|劈星斩';// "2|"后面填写要删除的英雄技能
	//This_Player.StrParam := '2|末日审判';// "2|"后面填写要删除的英雄技能
	end else
	begin
	This_Player.PlayerNotice('请先招出英雄!',0);	
	end;
end;

procedure _test19;
begin
	if This_Player.HeroLevel > -1 then
	begin
	This_Player.SetS(1,1,13);//英雄变性,需要用脚本先判断英雄已召唤出,否则无效!
	end else
	begin
	This_Player.PlayerNotice('请先招出英雄!',0);	
	end;
end;

procedure _test20;
begin
	This_Player.SetS(1,1,14);//杀死宝宝,可在登录脚本调用!
end;


procedure _test21;
begin
	This_Player.StrParam := '2|破魂斩';// "2|"后面填写要删除的英雄技能
	This_Player.StrParam := '2|噬魂沼泽';// "2|"后面填写要删除的英雄技能
	This_Player.StrParam := '2|火龙气焰';// "2|"后面填写要删除的英雄技能
	This_Player.StrParam := '2|雷霆一击';// "2|"后面填写要删除的英雄技能
	This_Player.StrParam := '2|劈星斩';// "2|"后面填写要删除的英雄技能
	This_Player.StrParam := '2|末日审判';// "2|"后面填写要删除的英雄技能
end;

procedure _test22;
begin
	This_Player.ChgSkillLv('基本剑术',10,0);
	This_Player.ChgSkillLv('攻杀剑术',3,0);
	This_Player.ChgSkillLv('刺杀剑术',3,0);
	This_Player.ChgSkillLv('半月弯刀',3,0);
	This_Player.ChgSkillLv('施毒术',3,0);
	This_Player.ChgSkillLv('烈火剑法',3,0);
	This_Player.ChgSkillLv('狮子吼',3,0);
	This_Player.ChgSkillLv('逐日剑法',3,0);
end;

procedure _test23;
var
  X ,Y ,a ,b , c , d, rand : Integer;
begin
	X := 322;
	Y := 322 ;
	rand := 18;
	a := X + random(rand);
	b := Y + random(rand);
	c := X + random(rand);
	d := Y + random(rand);	
	This_player.CreateCampAnimal('强盗假人',0, a,b, 1, 1, c,d);
	a := X + random(rand);
	b := Y + random(rand);
	c := X + random(rand);
	d := Y + random(rand);
	This_player.CreateCampAnimal('山贼假人',0, a,b, 1, 1, c,d);
	a := X + random(rand);
	b := Y + random(rand);
	c := X + random(rand);
	d := Y + random(rand);
	This_player.CreateCampAnimal('喽喽假人',0, a,b, 1, 1, c,d);
end;


procedure _test24;
begin
	This_Player.SetS(1,1,15);//假人白名,改名 此命令调用时,调用角色必须和假人在同一地图
	
	This_Player.Flyto(This_Player.MapName,This_Player.My_X,This_Player.My_Y);//刷新地图才能看到效果
end;

procedure _test25;
begin
	This_NPC.ClearMonEx('3',True);//清除指定地图所有怪物,包括假人 '3'为盟重地图编号,可替换
	This_NPC.ClearMonEx('D2079',True);//清除指定地图所有怪物,包括假人 '3'为盟重地图编号,可替换
end;

procedure _test26;
begin
	This_Player.SetS(1,1,16);//踢玩家下线
end;

procedure _test27;
begin
	This_Player.SetS(1,1,17);//开启假人白名,有效期20秒,超时需要重新开启
end;

procedure _test28;
begin
	//一个角色最多带20只宝宝,超过无效
	This_player.MakeSlaveEx('卧龙守将12',1,1);
	This_player.MakeSlaveEx('卧龙守将15',1,1);
	This_player.MakeSlaveEx('卧龙守将5',1,1);
	This_player.MakeSlaveEx('卧龙庄主',1,1);
end;

procedure _test29;
begin
	This_Player.SetS(1,1,18);//去除宝宝名称
	This_Player.Flyto(This_Player.MapName,This_Player.My_X,This_Player.My_Y);//刷新地图才能看到效果
end;


procedure _test30;
begin
	This_Player.SetV(1,2,100);  //设置角色杀人爆率,小退属性消失需在登录脚本调用重置    最高255,超出会造成严重后果!最高255,超出会造成严重后果!最高255,超出会造成严重后果!
end;

procedure _test31;
begin
	This_Player.SetV(1,3,100);  //设置角色防爆属性,小退属性消失需在登录脚本调用重置 
end;

procedure _test32;
begin
{
	This_Player.CallOut(1,3,'testCallOut');
}
end;

{
procedure testCallOut;
begin
	This_Player.GetSignInDayActPrizer(random(256));//设置玩家名字颜色函数取值范围0-255
	This_Player.CallOut(1,3,'testCallOut');
end;
}

procedure _test33;
begin
	This_Player.LingXiValue := 1;
	This_Player.PlayerNotice('破复活状态已生效',0);
	//This_Player.PlayerNotice('灵犀值:'+inttostr(This_Player.LingXiValue),0);
end;

procedure _test34;
begin
	This_Player.LingXiValue := 0;
	This_Player.PlayerNotice('破复活状态已取消',0);
	//This_Player.PlayerNotice('灵犀值:'+inttostr(This_Player.LingXiValue),0);
end;

procedure _test35;
begin
	 This_Npc.InputDialog(This_Player,'设置自动重生次数：', 0 , 100);
end;

Procedure P100;
var i : Integer;
begin
    This_Player.PlayerNotice('当前自动重生次数:'+This_Npc.InputStr,0);
	i:= StrToIntDef(This_Npc.InputStr,0);
	This_Player.SetV(66,20,i);//保存设置重生次数
	This_Player.SetV(66,21,i);//刷新剩余复活次数
end;



procedure _test36;
var  color, back : integer;
begin
//自定义文字颜色,文字颜色:color,底色:back,取值范围都是0-255;
	color := random(256);
	back  := random(256);
	ServerSay('ServerSay函数,颜色自定义!',back * 256 + color);
end;

procedure _test37;
begin
//调用在指定时间后会触发RunQuest.pas中相应脚本,请参照触发脚本例子!
//延时函数,参数一时间毫秒,参数二为延时ID
	This_Player.DoRelive(3000,1);//此处取随机值 random(5) 方便测试,正式使用时直接填入指定ID值
end;

procedure _test38;
begin
	This_Player.NewBieGiftConsume;//调用此行代码即可打开仓库
end;

procedure _test39;
begin 
	This_Player.GetSignInDayActPrizer(random(256));//设置玩家名字颜色函数取值范围0-255
end;


procedure _test40;
var FPlayer:TPlayer;
	UserName : String;
begin
{
//这里读取数据库比较消耗资源仅为了方便测试,正式使用请使用脚本保存在线玩家,然后读取!
	This_DB.ExecuteQuery('select ChrName from mir3.user_index where IsSelect = "1" ;');
	UserName := This_DB.PsFieldByName('ChrName');
	if This_Npc.GetEngageChance(UserName) then
	begin 
		FPlayer:= This_Npc.GetAIdleDynRoomIndex(UserName);
		FPlayer.PlayerDialog('【幽冥圣地活动开始了】:请各位勇士踊跃参与|<我要参加/@youming>');
	end;
	While (This_DB.PsEof() = False) do
	begin
		This_DB.PsNext();
		UserName := This_DB.PsFieldByName('ChrName');
		if This_Npc.GetEngageChance(UserName) then
		begin 
			FPlayer:= This_Npc.GetAIdleDynRoomIndex(UserName);
			FPlayer.PlayerDialog('【幽冥圣地活动开始了】:请各位勇士踊跃参与|<我要参加/@youming>');
		end;
	end;
}
end;

procedure _test41;
begin
	//设置称号
	This_Player.QuestInfo('1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890');
	//获取刺术下限
	This_Player.PlayerNotice(inttostr(This_Player.GetMyTaskState(0)),0);
	//获取刺术上限
	This_Player.PlayerNotice(inttostr(This_Player.GetMyTaskState(1)),0);
	//杀死宝宝
	This_Player.PlayerNotice(inttostr(This_Player.GetMyTaskState(2)),0);
end;

procedure _test42;

begin

end;

procedure domain;
var temp_str1,temp_str2,temp_str3 : string;
begin

temp_str1 := inttostr(random(256));
temp_str2 := inttostr(random(256));
temp_str3 := inttostr(random(256));

	if This_Player.Level > 0 then
	begin
  This_NPC.NpcDialog(This_Player,
	   +'{cmd}<转战士/@test1>     ^<转法师/@test2>      ^<转道士/@test3>|\'
	   +'{cmd}<变性/@test4>     ^<开始攻沙/@test5>      ^<结束攻沙/@test6>|\'
	   +'{cmd}<给与沙巴克/@test7>     ^<名字变色/@test8>      ^<开启远程仓库/@test9>|\'
	   +'{cmd}<玩家禁言/@test10>     ^<解除禁言/@test11>     ^<行会显示/@test12>|\'
	   +'{cmd}<玩家IP检测/@test13>   ^<给与封号/@test14>   ^<取消封号/@test15>|\'
	   +'{cmd}<英雄转战士/@test16>     ^<英雄转法师/@test17>      ^<英雄转道士/@test18>|\'
	   +'{cmd}<英雄变性/@test19> 	^<杀死宝宝/@test20> ^<删除英雄技能/@test21>|\'
	   +'{cmd}<技能提升/@test22> 	^<刷假人/@test23> ^<假人白色改名/@test24>|\'
	   +'{cmd}<清除假人/@test25> 	^<踢玩家下线/@test26> ^<开启假人白名/@test27>|\'
	   +'{cmd}<刷打怪假人/@test28> 	^<去除宝宝名称/@test29> ^<提升杀人爆率/@test30>|\'
	   +'{cmd}<提升死亡防爆/@test31> 	^<callout/@test32> ^<设置破复活/@test33>|\'
	   +'{cmd}<取消破复活/@test34> 	^<设置重生次数/@test35> ^<系统公告测试/@test36>|\'  
	   +'{cmd}<延时脚本测试/@test37> 	^<随身仓库/@test38> ^<设置名字颜色/@test39>|\' 
	   +'{cmd}<全服弹窗测试/@test40> 	^<test41/@test41> ^<test42/@test42>|\' 
		); 
	end else
	begin
	  This_NPC.NpcDialog(This_Player,
	   +'<需要5级GM权限!/fcolor=253>|\' 
		);	
	end;
end;

begin  
domain;
end.

