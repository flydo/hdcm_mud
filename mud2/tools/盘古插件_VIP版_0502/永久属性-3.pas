{******永久属性和永久属性高级脚本不可混用,要配套相应插件功能******}

program mir2; 
procedure _exit;
begin
	This_Npc.CloseDialog(This_Player);
end; 


procedure _SetAbil(AbilType:Integer);
var value, return : Integer;
begin
{
type	属性				允许最大值,超出将溢出	备注
0		MAXHP				word(0-65535)			无
1		MAXMP				word(0-65535)			无
2		MAX攻击				byte(0-127)				无
3		MAX魔法				byte(0-127)				无
4		MAX道术				byte(0-127)				无
5		MAX防御				byte(0-127)				无
6		MAX魔御				byte(0-127)				无
7		MAX刺术				dword(0-4294967295)		无
8		百分比免伤			byte(0-255)				value>=100即百分之百免伤,value=1即百分之一免伤
9		倍攻				byte(0-255)				每点属性加0.01倍攻击
13		备用				byte(0-255)				备用属性,暂时无用
14		备用				byte(0-255)				备用属性,暂时无用
15		备用				byte(0-255)				备用属性,暂时无用
10		备用				dword(0-4294967295)		备用属性,暂时无用
11		死亡防爆(临时属性)	dword(0-4294967295)		临时属性,小退会置零且需要开启插件爆率设置页功能
12		杀人爆率(临时属性)	byte(0-255)				临时属性,小退会置零且需要开启插件爆率设置页功能
}
	value := random(256);//取随机数值方便测试观察
	//value := 19;
	return := This_Player.AuthByHelped(AbilType,value);//返回值:成功return=1,失败return=0
	This_Player.PlayerNotice('type='+inttostr(AbilType)+';value='+inttostr(value)+';return='+ inttostr(return),0);//调试输出
end;



procedure domain;
begin
  This_NPC.NpcDialog(This_Player,
	   +'{cmd}<MAXHP/@SetAbil~0>     ^<MAXMP/@SetAbil~1>      ^<MAX攻击/@SetAbil~2>|\'
	   +'{cmd}<MAX魔法/@SetAbil~3>     ^<MAX道术/@SetAbil~4>      ^<MAX防御/@SetAbil~5>|\'
	   +'{cmd}<MAX魔御/@SetAbil~6>     ^<MAX刺术/@SetAbil~7>      ^<百分比免伤/@SetAbil~8>|\'
	   +'{cmd}<倍攻/@SetAbil~9>     ^<备用2/@SetAbil~10>     ^<死亡防爆/@SetAbil~11>|\'
	   +'{cmd}<杀人爆率/@SetAbil~12>  |\'
		); 
end;

begin  
domain;
end.

