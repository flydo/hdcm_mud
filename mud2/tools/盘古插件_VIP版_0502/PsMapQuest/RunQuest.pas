program Mir2;

{$I ActiveValidateCom.pas}

{$I TaoZhuang.pas}

//@OnDoRelive  延时触发脚本,多次调用不冲突!
procedure OnDoRelive(ID:Integer);
begin
	This_Player.PlayerNotice(inttostr(ID),0);
	if ID = 1 then  //根据传递ID的不同,区分不同延时功能
	begin
	This_Player.Give('经验', 99);
	This_Player.GetSignInDayActPrizer(random(256));
	This_Player.DoRelive(3000,1);//延时函数,参数一时间毫秒,参数二为延时ID
	end;
end;

//@SetNoKillMapLv 调用GM命令  @SetNoKillMapLv ID 触发此脚本
procedure SetNoKillMapLv(ID:Integer);
begin
	This_Player.PlayerNotice(inttostr(ID),0);
	if ID = 1 then  //根据传递ID的不同,实现不同功能
	begin
	This_Player.NewBieGiftConsume;//一行代码实现随身仓库
	end;
end;


//心灵启示触发脚本
procedure Revelation(X,Y:Integer);
begin
	This_Player.Flyto(This_Player.MapName,X,Y);
end;

//挖矿触发
procedure OnDig();
var i : Integer;
begin
	i := random(5);
	if This_Player.MapName = '3' then
	begin
		if i = 0 then
		begin
			This_Player.Give('复活戒指',1);
		end
		else if i = 1 then
		begin
			This_Player.Give('麻痹戒指',1);
		end;
	end;
end;

//召唤神兽
procedure SummonShinsu();
begin
	if This_Player.Level >= 45 then
	begin
		if This_Player.GetSlaveCount('') < 2 then
		This_Player.MakeSlaveEx('月灵', 1 ,7);
	end;
end;

//召唤骷髅
procedure SummonSkele();
begin
	if This_Player.Level >= 45 then
	begin
		if This_Player.GetSlaveCount('') < 2 then
		This_Player.MakeSlaveEx('白虎', 1 ,7);
	end;
end;

//魔法攻击触发
procedure MagicAttack(VictimName:string;IsPlayer:Boolean;SkillID:Integer);
//默认传递 攻击玩家对象(This_Player),参数一传被攻击名字(VictimName),参数二传递是否为玩家(IsPlayer),参数三使用技能ID
//通过This_Player.FindPlayerByName('VictimName');获取被攻击玩家对象.
var Victim:TPlayer;//声明被攻击玩家对象
begin
	if IsPlayer then
	begin
			This_Player.PlayerNotice('魔法攻击触发----玩家目标: ' + VictimName + ' 技能ID: ' + inttostr(SkillID),0);
			Victim := This_Player.FindPlayerByName(VictimName);
		
			//利用被攻击玩家对象,配合技能ID写一个简易擒龙手例子,传送坐标简单编写,可自行编写更完美坐标算法
			if SkillID = 35 then 
			begin
				Victim.Flyto(This_Player.MapName,This_Player.My_X - 1 + random(3),This_Player.My_Y - 1 + random(3));	
			end else
				//利用被攻击玩家对象,配合技能ID写一个简易十步一杀例子,可自行编写更完美坐标算法
			if SkillID = 48 then 
			begin
					This_Player.Flyto(Victim.MapName,Victim.My_X - 1 + random(3),Victim.My_Y - 1 + random(3));
					Victim.AddPlayerAbil(13, 1, 1);//触发一秒定身
			end;

	end
	else
	begin
		This_Player.PlayerNotice('魔法攻击触发----怪物目标: ' + VictimName + ' 技能ID: ' + inttostr(SkillID),0);
		if '练功师'=VictimName then
			begin
				This_Player.Give('经验', 9999999);
			end;
	end;	
end;

//物理攻击触发
procedure Attack(VictimName:string;IsPlayer:Boolean);
//默认传递 攻击玩家对象(This_Player),参数一传被攻击名字(VictimName),参数二传递是否为玩家(IsPlayer)
//通过This_Player.FindPlayerByName('VictimName');获取被攻击玩家对象.
var Victim:TPlayer;//声明被攻击玩家对象
begin
	if IsPlayer then
	begin
		This_Player.PlayerNotice('物理攻击触发----玩家目标: '+VictimName,0);
		Victim := This_Player.FindPlayerByName(VictimName);
	
		//利用被攻击玩家对象,写一个简单随机位移例子
		Victim.Flyto(This_Player.MapName,This_Player.My_X-1+random(3),This_Player.My_Y-1+random(3));
	end
	else
	begin
		This_Player.PlayerNotice('物理攻击触发----怪物目标: '+VictimName,0);
		if '练功师'=VictimName then
		begin
		This_Player.Give('经验', 9999999);
		end;
	end;	
end;


//点击回城按钮触发
procedure OnBackButton();
begin
//点击回城按钮后触发脚本,此文件调用函数好像和物品类似,要把函数放到LogonQuest.pas才能调用,具体自行测试!
	This_Player.PlayerDialog(
		+'|{cmd}<回城/@back> ^<BOSS直达/@zjcs>'
		+'|{cmd}<领取金币/@lqjb> ^<新手攻略/@xsgl>'  
		+'|{cmd}<随身仓库/@ssck>  ^<会员服务/@jrpz>' 
	   );
end;


//穿戴触发
procedure ChangeEquip();
//var i : Integer;
begin	
//	for i := 0 to 13 do
//begin
//	This_Player.PlayerNotice(This_Player.GetItemNameOnBody(i),0);
//end;
//	This_Player.AddPlayerAbil(4, 2000,72000);	
//	This_Player.PlayerNotice('赤月套装属性激活,攻魔道增加100点、防御增加50、血量增加2000！',2);
	TZdemo();
end;

//死亡触发  
procedure OnDie();
var i : Integer;
begin
	i := This_Player.GetV(66,21);//获取剩余复活次数
	if (i > 0) and (This_Player.YBNum >= 1000) then 
	begin
		This_Player.ScriptRequestSubYBNum(1000);
		This_Player.SetV(66,21,i -1);//刷新剩余复活次数
		This_Player.PlayerNotice('成功重生!!!!!',0);	
		This_Player.AutoReLive(This_Player.MapName,This_Player.My_X,This_Player.My_Y,true,true);
	end else
	begin
		This_Player.PlayerNotice('元宝不足或本次重生次数已用完!',0);
	end;
end;


//击杀触发  
procedure OnKill(KillerName ,MapDesc:string);
//默认传递 被击杀玩家对象(This_Player),参数一传递杀人玩家名字(KillerName),参数二传递当前地图名称(MapDesc)
//通过This_Player.FindPlayerByName('KillerName');获取杀人玩家对象.
var Killer:TPlayer;
	KillNumber , KilledNumber : Integer;
begin
	//获取杀人玩家对象
	Killer := This_Player.FindPlayerByName(KillerName);
	
	//击杀提示
	ServerSay('击杀快报: 玩家 [' + KillerName + '] 挥刀在 '+MapDesc+'('+inttostr(This_Player.My_X)+','+inttostr(This_Player.My_Y)+') 将 ['+This_Player.Name
+'] 斩于刀下!!!',3);

	//玩家变量设置,例如:斩杀牺牲数量,狂暴状态等!
	//设置被击杀玩家牺牲次数 任务号5 字段号2  不可改变
	KilledNumber := This_Player.GetV(5,2);	
	if KilledNumber < 1 then 
	begin
	KilledNumber := 1;
	end else
	begin
	KilledNumber := KilledNumber + 1;
	end;
	This_Player.SetV(5,2,KilledNumber);
	//设置杀人玩家斩杀次数 任务号5 字段号1  不可改变
	KillNumber := Killer.GetV(5,1);
	if KillNumber < 1 then 
	begin
	KillNumber := 1;
	end else
	begin
	KillNumber := KillNumber + 1;
	end;
	Killer.SetV(5,1,KillNumber);
	
	Killer.PlayerNotice('击杀效果!!!!!!',5);
	//狂暴玩法请参照例子自行添加
end;






procedure Test1(Test:string);
begin
	This_Player.PlayerNotice('TEST:'+Test,0);
end;

procedure Test2(Test:Integer);
begin
	This_Player.PlayerNotice('TEST:'+inttostr(Test),0);
end;

procedure Test3(T1:Integer;T2:string);
begin
	This_Player.PlayerNotice('TEST:'+inttostr(T1)+'|'+T2,0);
end;

procedure Test4(B:Boolean);
begin
	if B then
	begin
		This_Player.PlayerNotice('TEST: 真',0);
	end else
	begin
		This_Player.PlayerNotice('TEST: 假',0);
	end;
end;

procedure Test5(T:TPlayer);
begin
	This_Player.PlayerNotice('TEST: 假',0);
	//This_Player.PlayerNotice(T.Name,0);
end;


function UseYB(nType, nNum: Integer): Boolean;
begin
  Result := True;
end;

function UseLF(nType, nNum: Integer): Boolean;
begin
  Result := True;
end;  

procedure SetAwardCodeCDTime();
var
 d2 , d3 : integer;
s1 , td : double;
temp_2 : integer;
begin
      s1 := GetNow;
      d2 := This_Player.GetS(23,1);
      //将getnow的浮点数转换为整型进行保存； 

      td := ConvertDBToDateTime(d2);
      
      d3 := minusDataTime(s1,td);
      
      temp_2 := This_Player.GetS(23,2);
      
      
      if temp_2 <0 then temp_2 := 0;
      
      if (d3 <= 300)  then
      begin
         This_Player.SetS(23,2,temp_2 + 1);
         This_Player.PlayerNotice('已连续输入错误卡密' + inttostr(temp_2 + 1) + '/3次!',0);
      end 
      else  
      begin                     
          This_Player.SetS(23,1, ConvertDateTimeToDB(s1));
          This_Player.SetS(23,2,1);
      end;
end; 
//卡密

function getActiveItem(atvId : integer) : string;  //激活码AwardCodeType字段必须与该方法中的id匹配 
begin
    case atvId of
        1 : result:= '天卡';
        2 : result:= '周卡';
        3 : result:= '月卡';
        else
        result := ''; 
    end;
end;
procedure AwardCodeExecCallBack(ExecRes: Integer; const CodeStr: string; AwardCodeType, ActiveParam: Integer);
var 
lc : integer; 
begin
  //AWARDCODE_EXEC_ERROR        = 0;
  //AWARDCODE_EXEC_QUERY        = 1;
  //AWARDCODE_EXEC_UPDATE       = 2;
  //AWARDCODE_EXEC_ADD          = 3;
  //AWARDCODE_EXEC_DEL          = 4;  
  //
  //This_Player.PlayerNotice('ExecRes：' + inttostr(ExecRes) + '！',0);  
  //This_Player.PlayerNotice('AwardCodeType：' + inttostr(AwardCodeType) + '！',0);  
  //This_Player.PlayerNotice('ActiveParam：' + inttostr(ActiveParam) + '！',0);  
  case ExecRes of
       0 : 
       begin
           SetAwardCodeCDTime();
           This_Player.PlayerNotice('还没充钱你领个鸡巴！!',0);
           
       end;
       1 : 
       begin 
           if ActiveParam = 1 then
           This_Player.SetAwardCodeActiveParam(CodeStr, -1)
           else if ActiveParam = 2 then 
           begin
              if getActiveItem(AwardCodeType) = '' then 
              This_Player.PlayerNotice('该激活码未配置物品！',0)
              else if This_Player.FreeBagNum >= 1 then
              This_Player.SetAwardCodeActiveParam(CodeStr, -2)
              else
              This_Player.PlayerNotice('包裹空间不足！',0);
           
           end else
           This_Player.PlayerNotice('你已经领取过了，不要重复点击！',0);
       end;
       2 : 
       begin
          lc:= This_Player.GetV(92,1);
          This_Player.SetS(23,1,0);
          This_Player.SetS(23,2,0); 
           if ActiveParam = -1 then 
           begin
              This_Player.ScriptRequestAddYBNum(AwardCodeType);
              This_Player.PlayerNotice('尊贵玩家您的元宝领取成功了！' ,0);
              This_Player.SetV(92,1,lc + AwardCodeType);  
              ServerSay('玩家<' + This_Player.Name + '>充值了' + inttostr(AwardCodeType) + '元宝，感谢他对本服作出的贡献！',0);   
               
           end else 
           if ActiveParam = -2 then
           begin 
            This_Player.Give(getActiveItem(AwardCodeType),1);
            ServerSay('玩家<' + This_Player.Name + '>兑换了' + getActiveItem(AwardCodeType) + '，感谢他对本服作出的贡献！',0);
           end;
       end;
       5 : This_Player.PlayerNotice('卡密操作失败!',0);
  end;   
end;


//主号升级
procedure PlayerExpLevelUp();
var
  iLev , GLv40 , GLv45, LastTime, ShowTime : integer;
begin
	//This_Player.PlayerNotice('PlayerExpLevelUp',0);
	iLev := This_Player.Level;
   
   case iLev of
        17:
        begin
		       if This_Player.MapName='0139~9' then exit;//前面为你的点卡地图
		        LastTime := This_Player.GetS(10,1);
		        ShowTime := minusDataTime(ConvertDBToDateTime(LastTime),GetNow);	          
	         if ShowTime < 0 then
	         begin
			        This_Player.PlayerNotice('请购买点卡！',2);
			        This_Player.RandomFlyTo('0107~1');//前面GA0~01为你的点卡地图和坐标X-Y
	         end;
        end;
       40:
       begin
           if GetG(2,1) <> 888 then
           begin
               SetG(2,1,888);
               SetG(2,2,0);          
           end;
           GLv40 := GetG(2,2);
           if This_Player.GMLevel <= 0 then //gm升级不计数 
           SetG(2,2,GLv40 + 1);
       end;    
       45:
       begin
           if GetG(2,3) <> 888 then
           begin
               SetG(2,3,888);
               SetG(2,4,0);
               
           end;
           GLv45 := GetG(2,4);
           if This_Player.GMLevel <= 0 then //gm升级不计数 
           SetG(2,4,GLv45 + 1);
       end; 
   end;
end;                             


//英雄升级
procedure HeroExpLevelUp();
begin

end;


function FreshmanTaskCommand(const FreshmanTaskNo: Integer): Integer;   //客户端调用 
begin
 //-1 未知错误 -2无此任务 -3不能再次使用
    Result := -1;
    case FreshmanTaskNo of 
        1: 
        begin  
           if This_Player.GetV(89,1) <> 111 then  
           begin
             This_Player.Flyto('0',333,274);
             This_Player.SetV(89,1,111);
             Result := 0;
           end
           else
           begin
             //This_Player.PlayerDialog('你已经获取过奖励了。');
             Result := -3;
           end;      
        end;       
        else
        Result := -2;
    end;
end;

//活跃度系统 
procedure PlayerActivePoint(paytape,payNO,PayNUM:integer;PayName:string);
var
addPoint:integer;
begin
   if paytape = 0 then        //使用道具、武器合成 
   begin
     if PayName = '祝福油' then addPoint := 3 else
     if PayName = '洗红名' then addPoint := 2 else
     if PayName = '井中月' then addPoint := 5 else
     if PayName = '无极棍' then addPoint := 5 else
     if PayName = '血饮' then addPoint := 5 else
     if PayName = '命运之刃' then addPoint := 5 else
     if PayName = '裁决之杖' then addPoint := 30 else
     if PayName = '骨玉权杖' then addPoint := 30 else
     if PayName = '龙纹剑' then addPoint := 30 else
     if PayName = '屠龙' then addPoint := 100 else
     if PayName = '逍遥扇' then addPoint := 100 else
     if PayName = '嗜魂法杖' then addPoint := 100 else
     if PayName = '开天' then addPoint := 500 else
     if PayName = '镇天' then addPoint := 500 else
     if PayName = '玄天' then addPoint := 500 else
     if PayName = '雷霆战甲(男)' then addPoint := 100 else
     if PayName = '雷霆战甲(女)' then addPoint := 100 else
     if PayName = '烈焰魔衣(男)' then addPoint := 100 else
     if PayName = '烈焰魔衣(女)' then addPoint := 100 else
     if PayName = '光芒道袍(男)' then addPoint := 100 else
     if PayName = '光芒道袍(女)' then addPoint := 100 ;
       
   end else
   if paytape = 1 then
   begin
     //以下为商城元宝购买
     if PayName = '烟花' then addPoint := 50 else
     if PayName = '如雾似梦包' then addPoint := 1 else
     if PayName = '长空火舞包' then addPoint := 1 else
     if PayName = '绮梦幻想包' then addPoint := 2 else
     if PayName = '飞火流星包' then addPoint := 2 else
     if PayName = '心心相印包' then addPoint := 2 else
     if PayName = '一心一意包' then addPoint := 2 else
     if PayName = '传情烟花' then addPoint := 88 else
     if PayName = '个性发型' then addPoint := 30 else
     if PayName = '反璞归真' then addPoint := 2 else
     if PayName = '比奇传送石' then addPoint := 1 else
     if PayName = '盟重传送石' then addPoint := 1 else
     if PayName = '随机传送石' then addPoint := 1 else
     if PayName = '修复神水' then addPoint := 1 else
     if PayName = '沃玛号角' then addPoint := 30 else
     if PayName = '千里传音' then addPoint := 2 else
     if PayName = '千里传音(大)' then addPoint := 3 else
     if PayName = '传音筒' then addPoint := 3 else
     if PayName = '超级传音筒' then addPoint := 20 else
     if PayName = '魔龙之血' then addPoint := 20 else
     if PayName = '虔诚挂坠' then addPoint := 1;
     
   end else
   if paytape = 2 then
   begin
     //以下为商城金锭购买
     if PayName = '烟花' then addPoint := 50 else
     if PayName = '如雾似梦包' then addPoint := 1 else
     if PayName = '长空火舞包' then addPoint := 1 else
     if PayName = '绮梦幻想包' then addPoint := 2 else
     if PayName = '飞火流星包' then addPoint := 2 else
     if PayName = '心心相印包' then addPoint := 2 else
     if PayName = '一心一意包' then addPoint := 2 else
     if PayName = '传情烟花' then addPoint := 88 else
     if PayName = '个性发型' then addPoint := 30 else
     if PayName = '反璞归真' then addPoint := 2 else
     if PayName = '比奇传送石' then addPoint := 1 else
     if PayName = '盟重传送石' then addPoint := 1 else
     if PayName = '随机传送石' then addPoint := 1 else
     if PayName = '修复神水' then addPoint := 1 else
     if PayName = '沃玛号角' then addPoint := 30 else
     if PayName = '千里传音' then addPoint := 2 else
     if PayName = '千里传音(大)' then addPoint := 3 else
     if PayName = '传音筒' then addPoint := 3 else
     if PayName = '超级传音筒' then addPoint := 20 else
     if PayName = '魔龙之血' then addPoint := 20 else
     if PayName = '虔诚挂坠' then addPoint := 1;
     
   end; 
   
   This_Player.IncActivePoint(addPoint);    
    
end;

procedure PlayerActiveWithMap(MapName:string);
begin
  This_Player.PlayerDialog(
  '对不起，你的传奇信用分不足以进入<'+MapName+'/c=red>，\'
  +'请增强自己的传奇信用分以后再来挑战。\ \'
  +'|{cmd}<了解增强传奇信用分的方式/@AboutActive>'
  );
end;

procedure PlayerActiveValidate;
begin
  if AutoOpen(2) then
  begin 
  This_Player.PlayerDialog(
  '尊敬的玛法勇士，您当前为未验证用户。您可以通过体验游戏内容，\'
  +'提高传奇信用分至30分，在<各仓库管理员和传送员处成为验证用户/c=red>。\'
  +'您当前的的传奇信用分为<'+inttostr(This_Player.GetActivePoint)+'/c=red>分，其中临时分为<'+inttostr(This_Player.GetTmpActivePoint)+'分/c=red>。\'
  +'未验证的角色可以体验以下游戏内容：\'
  +'|1.可携带200万金币和使用20格仓库。\'
  +'|2.可以获得所有物品，但无法转移、丢弃物品给其他玩家。\ \'
  +'|{cmd}<了解增强传奇信用分的方式/@AboutActive>\'
  +'|{cmd}<验证传奇信用分，成为验证用户/@ValidateActive>'
  );
  end else
  This_Player.PlayerDialog(
  '尊敬的玛法勇士，您当前为未验证用户。您可以通过体验游戏内容，\'
  +'提高传奇信用分至30分，在<各仓库管理员和传送员处成为验证用户/c=red>。\'
  +'您当前的的传奇信用分为<'+inttostr(This_Player.GetActivePoint)+'/c=red>分，其中临时分为<'+inttostr(This_Player.GetTmpActivePoint)+'分/c=red>。\'
  +'未验证的角色可以体验以下游戏内容：\'
  +'1.可携带200万金币和使用20格仓库。\'
  +'2.可以获得所有物品，但无法转移、丢弃物品给其他玩家。\'
  +'<将于'+inttostr(AutoOpenDay(2))+'天后开启信用分验证，在此之前您可以正常体验游戏，/c=red>\'
  +'<当然您也可以提前验证传奇信用分，更好的体验游戏内容。/c=red>\' 
  +'<了解增强传奇信用分的方式/@AboutActive>     <验证传奇信用分，成为验证用户/@ValidateActive>'
  );
end;

procedure OpenTimeWithMap(MapName:string);
begin
  This_Player.PlayerDialog(
  '对不起，<'+MapName+'/c=red>尚未开启，暂时还不能前往。\'
  );
end;


begin

end.