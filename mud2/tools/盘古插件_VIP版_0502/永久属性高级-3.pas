{
******永久属性和永久属性高级脚本不可混用,要配套相应插件功能******
type	属性				允许最大值,超出将溢出	备注
1		倍攻				byte(0-255)				每点属性加0.01倍攻击
2		备用1				byte(0-255)				备用属性,暂时无用
3		备用2				byte(0-255)				备用属性,暂时无用
4		备用3				byte(0-255)				备用属性,暂时无用
5		MAXHP				word(0-65535)			无
6		MAXMP				word(0-65535)			无
7		MAX攻击				word(0-65535)			无
8		MAX魔法				word(0-65535)			无
9		MAX道术				word(0-65535)			无
10		MAX防御				word(0-65535)			无
11		MAX魔御				word(0-65535)			无
12		备用4				byte(0-255)				备用属性,暂时无用
13		备用5				byte(0-255)				备用属性,暂时无用
14		刺术上限			word(0-65535)			增加暴击机率
15		刺术下限			word(0-65535)			增加怪物爆率
16		百分比免伤			byte(0-255)				value>=100即百分之百免伤,value=1即百分之一免伤
17		死亡防爆(临时属性)	word(0-65535)			临时属性,小退会置零且需要开启插件爆率设置页功能
18		杀人爆率(临时属性)	byte(0-255)				临时属性,小退会置零且需要开启插件爆率设置页功能
}

program mir2; 
procedure _exit;
begin
	This_Npc.CloseDialog(This_Player);
end; 


procedure SetAbil(AbilType, value:Integer);
//SetAbil函数内代码尽量不要修改,直接调用即可;
var H8, L8 : Integer;
begin
	if value < 65536 then
	begin
		H8 := value / 256;
		L8 := value mod 256;
		//下面这行是调试输出语句,正式使用时可屏蔽;
		This_Player.PlayerNotice('AbilType='+inttostr(AbilType)+';value='+inttostr(value)+';H8='+inttostr(H8)+';L8='+inttostr(L8),0);
		case AbilType of
			1://倍攻
			begin
			This_Player.AuthByHelped(6212 + 0,L8);
			end;
			2://备用1
			begin
			This_Player.AuthByHelped(6212 + 1,L8);	
			end;
			3://备用2
			begin
			This_Player.AuthByHelped(6212 + 2,L8);
			end;
			4://备用3
			begin
			This_Player.AuthByHelped(6212 + 3,L8);
			end;
			5://MAXHP
			begin
			This_Player.AuthByHelped(6212 + 4,L8);
			This_Player.AuthByHelped(6212 + 5,H8);
			end;
			6://MAXMP
			begin
			This_Player.AuthByHelped(6212 + 6,L8);
			This_Player.AuthByHelped(6212 + 7,H8);
			end;
			7://MAX攻击
			begin
			This_Player.AuthByHelped(6212 + 8,L8);
			This_Player.AuthByHelped(6212 + 20,H8);
			end;
			8://MAX魔法
			begin
			This_Player.AuthByHelped(6212 + 9,L8);
			This_Player.AuthByHelped(6212 + 21,H8);
			end;
			9://MAX道术
			begin
			This_Player.AuthByHelped(6212 + 10,L8);
			This_Player.AuthByHelped(6212 + 22,H8);
			end;
			10://MAX防御
			begin
			This_Player.AuthByHelped(6212 + 11,L8);
			This_Player.AuthByHelped(6212 + 23,H8);
			end;
			11://MAX魔御
			begin
			This_Player.AuthByHelped(6212 + 12,L8);
			This_Player.AuthByHelped(6212 + 13,H8);
			end;
			12://备用4
			begin
			This_Player.AuthByHelped(6212 + 14,L8);
			end;
			13://备用5
			begin
			This_Player.AuthByHelped(6212 + 15,L8);
			end;
			14://刺术上限
			begin
			This_Player.AuthByHelped(6212 + 16,L8);
			This_Player.AuthByHelped(6212 + 17,H8);
			end;
			15://刺术下限
			begin
			This_Player.AuthByHelped(6212 + 18,L8);
			This_Player.AuthByHelped(6212 + 19,H8);
			end;
			16://百分比免伤
			begin
			This_Player.AuthByHelped(1400 + 0,L8);
			end;			
			17://死亡防爆(小退属性消失,需要每次重新设置)
			begin
			This_Player.AuthByHelped(396 + 0,L8);
			This_Player.AuthByHelped(396 + 1,H8);
			end;
			18://杀人爆率(小退属性消失,需要每次重新设置)
			begin
			This_Player.AuthByHelped(1401 + 0,L8);
			end;
		end;	
    end;   
end;


procedure _test(AbilType : Integer);
var value : Integer;
begin
	value := random(65536);//取随机数值方便测试观察,实际使用时设定value为固定值即可
	//下面开始正式调用,日常使用仅需引用 SetAbil 函数并调用此行代码;
	SetAbil(AbilType, value);
end;

procedure domain;
begin
  This_NPC.NpcDialog(This_Player,
	   +'{cmd}<倍攻/@test~1>     ^<备用1/@test~2>      ^<备用2/@test~3>|\'
	   +'{cmd}<备用3/@test~4>     ^<MAXHP/@test~5>      ^<MAXMP/@test~6>|\'
	   +'{cmd}<MAX攻击/@test~7>     ^<MAX魔法/@test~8>      ^<MAX道术/@test~9>|\'
	   +'{cmd}<MAX防御/@test~10>     ^<MAX魔御/@test~11>      ^<备用4/@test~12>|\'
	   +'{cmd}<备用5/@test~13>     ^<刺术上限/@test~14>      ^<刺术下限/@test~15>|\'
	   +'{cmd}<百分比免伤/@test~16>     ^<死亡防爆/@test~17>     ^<杀人爆率/@test~18>|\'
		); 
end;

begin  
domain;
end.

